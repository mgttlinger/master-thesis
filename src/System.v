From SQEMA Require Export Utils SQEMAUtils ListUtils CT Equation BoxFormula BiModal Polarity.

Require Import Lists.List.
Require Import Bool.
Section System.
  Definition pro := nat.

  Context {opu opb nom : Set}.
  Notation "'bmf'" := (@bmf opu opb nom).
  Notation "'eqn'" := (@eqn opu opb nom).

  (** SQEMA system consisting of multiple equations *)
  Record system := { eqns : list eqn;
                   }.
  Definition to_system : list eqn -> system := Build_system.
  Hint Unfold to_system.

  Definition is_beta `{Polarity eqn} (p : pro) (e : eqn) := match polarity_of e p with
                                                            | p_neg => true
                                                            | p_none => true
                                                            | _ => false
                                                            end.
  Definition Beta (p : pro) (e : eqn) := SNegIn p (eqn_toformula e).
  
  Definition is_alpha' `{IsAtom bmf} p e : option bmf :=
    if andb (bmf_pure_in p e.(a)) (is_atom'' p e.(b)) then Some e.(a) else
      if andb (bmf_pure_in p e.(b)) (is_atom'' p e.(a)) then Some e.(b) else None.
  
  Definition is_alpha `{IsAtom bmf} p e := match is_alpha' p e with Some _ => true | None => false end.

  Inductive Alpha (p : pro) : eqn -> Prop :=
  | a_left : forall f, PureIn p f -> Alpha p {|a := b_prop p; b := f|}
  | a_right : forall f, PureIn p f -> Alpha p {|b := b_prop p; a := f|}
  .

  Lemma alpha_no_beta : forall e p, Alpha p e -> ~ Beta p e. intros e p. introversion; introversion; try inversion H5; try inversion H6; congruence. Qed.
  Lemma beta_no_alpha : forall e p, Beta p e -> ~ Alpha p e. intros e p. introversion. intro Hf. induction Hf; cbn [Equation.a Equation.b] in *; try inversion H2; try inversion H3; congruence. Qed.

  Lemma Beta_reflect : forall e p, reflect (Beta p e) (is_beta p e).
    intros e p.
    unfold Beta, is_beta, polarity_of, e_polarity, eqn_toformula. apply iff_reflect. rewrite (reflect_iff (SNegIn p _)); auto using SNegIn_reflect.
    cbn [mult polarity_monoid bmf_polarity_of].
    tauto.
  Qed.

  Lemma Alpha_reflect : forall e p, reflect (Alpha p e) (is_alpha p e).
    intros e p.
    apply iff_reflect. split; intro H.
    - inversion H;
        unfold is_alpha, is_alpha', a, b, is_atom'' at 2, is_atom', IsAtom_bmf, bmf_is_atom';
        rewrite (reflect_iff _ _ (PureIn_reflect _ _)) in H0;
        rewrite !H0, ?PeanoNat.Nat.eqb_refl;
        replace (bmf_pure_in p (b_prop p : bmf)) with (false) by (cbv -[Nat.eqb]; rewrite PeanoNat.Nat.eqb_refl; auto);
        cbn [andb]; try reflexivity.
      cbv -[Nat.eqb]; rewrite PeanoNat.Nat.eqb_refl; auto.
    - destruct e as [av bv].
      cbv [is_alpha] in H.
      match_hyp; try congruence. clear H.
      cbv [is_alpha' a Equation.b] in H0.
      match_hyp.
      + rewrite andb_true_iff in H; destruct H as [Hp H].
        rewrite <- (reflect_iff _ _ (PureIn_reflect _ _)) in Hp.
        clear H0.
        cbv [is_atom''] in H.
        match_hyp; try congruence.
        rewrite PeanoNat.Nat.eqb_eq in H.
        cbv [is_atom' IsAtom_bmf bmf_is_atom'] in H0.
        match_hyp; try congruence.
        inversion H0; subst.
        constructor; auto.
      + match_hyp; try congruence.
        clear H0 H.
        rewrite andb_true_iff in H1; destruct H1 as [Hp H].
        rewrite <- (reflect_iff _ _ (PureIn_reflect _ _)) in Hp.
        cbv [is_atom''] in H.
        match_hyp; try congruence.
        rewrite PeanoNat.Nat.eqb_eq in H.
        cbv [is_atom' IsAtom_bmf bmf_is_atom'] in H0.
        match_hyp; try congruence.
        inversion H0; subst.
        constructor; auto.
  Qed.
  
  
  Definition split_system `{Polarity eqn} `{IsAtom bmf} `{Polarity bmf} p (sy : system) : (list eqn) * ((list eqn) * (list eqn)) :=
    let es := sy.(eqns) in
    let alphas := List.filter (is_alpha p) es in
    let betas := List.filter (is_beta p) es in
    let rest := List.filter (fun e => andb (negb (is_alpha p e)) (negb (is_beta p e))) es in
    (alphas, (betas, rest)).

  Definition join_system (s1 s2 : system) := to_system (s1.(eqns) ++ s2.(eqns)).
  Definition system_app (s : system) (l : list eqn) : system := {| eqns := l ++ s.(eqns) |}.
  
  Definition sys_map f s := {| eqns := map (eqn_map f) s.(eqns) |}.
  Definition sys_toformula s := b_bigconj (map eqn_toformula s.(eqns)).

  Global Instance s_nominal `{N : Nominals nom eqn} : Nominals system :=
    {| nominals sy := flat_map nominals sy.(eqns) |}.

  Global Instance s_polarity `{Polarity eqn}: Polarity system :=
    {| polarity_of := fun sy p => ListUtils.fold_map (fun f => polarity_of f p) sy.(eqns);
       pure_in := fun p sy => fold_right (fun e acc => pure_in p e && acc) true sy.(eqns)
    |}.
  
  Global Instance s_polarity_switch `{Polarity_Switch eqn eqn} : Polarity_Switch system _ :=
    {| polarity_switch sy p := to_system (map (fun f => polarity_switch f p) sy.(eqns) : list eqn) |}.
  
  Global Instance s_atoms `{Atoms eqn} : Atoms system := {| atoms sy := flat_map atoms sy.(eqns) |}.
  
  Global Instance s_elimtriv `{ElimTriv eqn} : ElimTriv system :=
    {| elim_triv p sy := to_system (map (elim_triv p) sy.(eqns))
    |}.
End System.