From SQEMA Require Import DPair.

From Coq.Lists Require Import List.

Import ListNotations.

(** Heterogeneus list of elements of type [t] for a varying argument [p] *)
Inductive hlist {p : Set} (t : p -> Set) : list p -> Set :=
| hnil : hlist t []
| hcons {pv} {tlvs} : t pv -> hlist t tlvs -> hlist t (pv :: tlvs).

Notation "h 'h::' t" := (hcons _ h t) (right associativity, at level 20).
Notation "'h[]'" := (hnil _).

Section hlist.
  Context {p : Set} {t : p -> Set}.
  Fixpoint to_list {l} (h : hlist t l) {struct h} : list (p ** t) :=
    match h with
    | @hnil _ _ => []
    | @hcons _ _ pv tlvs head tail => (pv ** head) :: to_list tail
    end.

  Fixpoint hlist_fold {l} {u} (f : (p ** t) -> u -> u) (s : u) (h : hlist t l) {struct h} : u :=
    match h with
    | hnil _ => s
    | @hcons _ _ pv _ x x0 => f (pv ** x) (hlist_fold f s x0)
    end.

  Definition hlist_fold' {l} {u} (f : forall x, t x -> u -> u) := @hlist_fold l u (dpair_fold f).

  Inductive hIn : forall l, (p ** t) -> hlist t l -> Prop :=
  | headIn {pv} {tv} {tpvs} {tl} : hIn (pv :: tpvs) (pv ** tv) (tv h:: tl)
  | tailIn {pv} {tv} {ph} {h} {tpvs} {tl} : hIn tpvs (pv ** tv) tl -> hIn (ph :: tpvs) (pv ** tv) (h h:: tl).
  
End hlist.