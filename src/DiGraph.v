From SQEMA Require Import Utils ML Reify BoxFormula Atoms Polarity ListUtils.

From Coq.Lists Require Import List ListDec.
Require Export Classes.EquivDec.
Require Import Bool.Bool.

Import ListNotations.
Import ListUtils.
Section DiGraph.
  Definition t := nat.
  Record DiGraph := { nodes : list t;
                      edges : list (t * t);
                    }.

  Program Instance DiGraph_EqDEc : EqDec DiGraph eq.
  Next Obligation. destruct x, y. destruct (list_eq_dec equiv_dec nodes0 nodes1); destruct (list_eq_dec equiv_dec edges1 edges0); subst; [left; trivial | right | right | right]; intro H'; inversion H'; congruence. Defined.
  
  Definition leaf d : option { e : t | In e (nodes d) /\ forallb (fun ed => e <>b (fst ed)) (edges d) = true } :=
    dep_find (fun e => forallb (fun ed => e <>b (fst ed)) (edges d)) (nodes d).


  Definition empty d : bool := d ==b {| nodes := []; edges := []|}.

  Definition digraph_size : DiGraph -> nat := fun d => match d with
                                                 | Build_DiGraph nodes edges => length nodes
                                                 end.

  Program Definition drop_leaf dg : option { d : DiGraph | digraph_size d < digraph_size dg } := match leaf dg with
                                                                                                 | Some n => match dg with
                                                                                                            | Build_DiGraph nds edgs => Some {| nodes := remove equiv_dec (proj1_sig n) nds;
                                                                                                                                               edges := filter (fun ed => let n := (proj1_sig n) in (n <>b (fst ed)) && (n <>b (snd ed))) edgs
                                                                                                                                            |}
                                                                                                            end
                                                                                                 | None => None
                                                                                                 end.
  Next Obligation. simpl in *. unfold digraph_size in *. apply remove_shorter. trivial. Defined.
                     
                     
  Program Fixpoint acyclic d {measure (digraph_size d)} : bool := match drop_leaf d with
                                                               | Some x => acyclic (proj1_sig x)
                                                               | None => empty d
                                                               end.
  Lemma acyclic_unfold : forall d, acyclic d = match drop_leaf d with
                                          | Some x => acyclic (proj1_sig x)
                                          | None => empty d
                                          end.
  Admitted.
  Opaque acyclic.
  
  Definition dep_digraph {M} {nom : Set} `{RegularFormula (nom := nom) M} (m : M) : DiGraph := 
    match fold_right (fun '(n, cs) '(ns, es) => (n :: ns, (fmap (fun o => (o, n)) cs) ++ es)) ([],[]) (occurrence_pairs m) with
    | (ns, es') => let es := filter (fun '(a, b) => andb (elem a ns) (elem b ns)) es' in
                  {| nodes := ns;
                     edges := es
                  |}
    end.
  
End DiGraph.