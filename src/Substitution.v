From SQEMA Require Import Algebra Reify ML Polyadic BiModal VectorUtils CT Polarity Atoms.

Require Export Classes.EquivDec.
Require Import Lists.List.
Require Import Nat.
Require Vectors.Vector.

Section Substitution.
  Context {op opb nom : Set} {ops : nat -> Set}.

  Definition Substitution_BPL {M} `{@BPL nat nom M} : @BPL nat nom (nat -> M -> M) :=
    {| verum := fun _ _ => verum;
       falsum := fun _ _ => falsum;
       prop q := fun p g => if p =? q then g else prop q;
       nomi n := fun _ _ => nomi n;
       conj a b := fun p g => conj (a p g) (b p g);
       disj a b := fun p g => disj (a p g) (b p g);
    |}.

  Definition Substitution_PL {M} `{@PL nat nom M} : @PL nat nom (nat -> M -> M) :=
    {| B := Substitution_BPL;
       neg f := fun p g => neg (f p g);
    |}.

  Definition Substitution_UML {M} `{@UML op M} : @UML op (nat -> M -> M) :=
    {| diau m f := fun p g => diau m (f p g);
       boxu m f := fun p g => boxu m (f p g);
    |}.
  
  Definition Substitution_BML {M} `{@BML op opb M} : @BML op opb (nat -> M -> M) :=
    {| BU := Substitution_UML;
       diab m f g := fun p h => diab m (f p h) (g p h);
       boxb m f g := fun p h => boxb m (f p h) (g p h);
    |}.
  
  Definition Substitution_PML {M} `{@PML ops M} : @PML ops (nat -> M -> M) :=
    {| pdia n m f := fun p g => pdia n m (Vector.map (fun e => e p g) f);
       pbox n m f := fun p g => pbox n m (Vector.map (fun e => e p g) f);
    |}.


  Class Substitution M N := { substitute : M -> nat -> N -> N }.
  Class NegSubstitution M N O := { neg_subst : N -> nat -> M -> O }.
  Class ElimTriv M `{Polarity M} `{Atoms M} := { elim_triv : nat -> M -> M
                                               }.
  
  Global Instance Substitution_uml {M N} `{UML op N} `{PL nat nom N} `{Reify _ M _} : Substitution M N :=
    {| substitute := reify (Substitution_UML, Substitution_PL) |}.
  Global Instance Substitution_bml {M N} `{BML op opb N} `{PL nat nom N} `{Reify _ M _} : Substitution M N :=
    {| substitute := reify (Substitution_BML, Substitution_PL) |}.
  Global Instance Substitution_pml {M N} `{PML ops N} `{PL nat nom N} `{Reify _ M _} : Substitution M N :=
    {| substitute := reify (Substitution_PML, Substitution_PL) |}.

  Fixpoint boxarg_s p g (f : boxarg) :=
    match f with
    | bc_disj x x0 => (boxarg_s p g x) \\// (boxarg_s p g x0)
    | bc_prop x => if x =? p then g else prop x
    | bc_negprop x => if x =? p then (neg g) else neg (prop x)
    | bc_nom x => nomi x
    | bc_negnom x => neg (nomi x)
    | bc_verum => verum
    | bc_falsum => falsum
    | bc_dia x x0 => pdia _ x (fmap (F := fun t => Vector.t t _) (diaarg_s p g) x0)
    | bc_box x x0 => pbox _ x (fmap (F := fun t => Vector.t t _) (boxarg_s p g) x0)
    end
  with diaarg_s p g (f : @diaarg nat nom ops) :=
         match f with
         | dc_conj x x0 => (diaarg_s p g x) //\\ (diaarg_s p g x0)
         | dc_prop x => if x =? p then g else prop x
         | dc_negprop x => if x =? p then (neg g) else neg (prop x)
         | dc_nom x => nomi x
         | dc_negnom x => neg (nomi x)
         | dc_verum => verum
         | dc_falsum => falsum
         | dc_dia x x0 => pdia _ x (fmap (F := fun t => Vector.t t _) (diaarg_s p g) x0)
         | dc_box x x0 => pbox _ x (fmap (F := fun t => Vector.t t _) (boxarg_s p g) x0)
         end.
  
  Global Instance boxarg_substitution : Substitution boxarg smf_mdnf :=
    {| substitute f p g := boxarg_s p g f |}.
  Global Instance diaarg_substitution : Substitution diaarg smf_mdnf :=
    {| substitute f p g := diaarg_s p g f |}.


  Fixpoint boxarg_et p (f : boxarg) :=
    match f with
    | bc_disj x x0 => bc_disj (boxarg_et p x) (boxarg_et p x0)
    | bc_prop x => if x =? p then bc_verum else bc_prop x
    | bc_negprop x => if x =? p then bc_falsum else bc_negprop x
    | bc_nom x => bc_nom x
    | bc_negnom x => bc_negnom x
    | bc_verum => bc_verum
    | bc_falsum => bc_falsum
    | bc_dia x x0 => bc_dia x (fmap (F := fun t => Vector.t t _) (diaarg_et p) x0)
    | bc_box x x0 => bc_box x (fmap (F := fun t => Vector.t t _) (boxarg_et p) x0)
    end
  with diaarg_et p (f : @diaarg nat nom ops) :=
         match f with
         | dc_conj x x0 => dc_conj (diaarg_et p x) (diaarg_et p x0)
         | dc_prop x => if x =? p then dc_verum else dc_prop x
         | dc_negprop x => if x =? p then dc_falsum else dc_negprop x
         | dc_nom x => dc_nom x
         | dc_negnom x => dc_negnom x
         | dc_verum => dc_verum
         | dc_falsum => dc_falsum
         | dc_dia x x0 => dc_dia x (fmap (F := fun t => Vector.t t _) (diaarg_et p) x0)
         | dc_box x x0 => dc_box x (fmap (F := fun t => Vector.t t _) (boxarg_et p) x0)
         end.

  Global Program Instance boxarg_elimtriv : ElimTriv boxarg :=  {| elim_triv := boxarg_et |}.
  Global Program Instance diaarg_elimtriv : ElimTriv diaarg :=  {| elim_triv := diaarg_et |}.
  Global Instance bmf_elim_triv : ElimTriv (@bmf op opb nom) :=
    {| elim_triv p :=
         fix et f :=
           match f with
           | b_conj x x0 => b_conj (et x) (et x0)
           | b_disj x x0 => b_disj (et x) (et x0)
           | b_prop x => if x =? p then b_verum else b_prop x (* contrary to intuition replacing both with verum is the right thing to do *)
           | b_negprop x => if x =? p then b_verum else b_negprop x
           | b_nom x => b_nom x
           | b_negnom x => b_negnom x
           | b_verum => b_verum
           | b_falsum => b_falsum
           | b_diau x x0 => b_diau x (et x0)
           | b_boxu x x0 => b_boxu x (et x0)
           | b_diab x x0 x1 => b_diab x (et x0) (et x1)
           | b_boxb x x0 x1 => b_boxb x (et x0) (et x1)
           end
    |}.  

  Global Instance bmf_NegSubst {n} : NegSubstitution (@bmf op opb n) (@bmf op opb n) (@bmf op opb n) :=
    {| neg_subst f p := fix s g := match g with
                                   | b_verum => b_verum
                                   | b_falsum => b_falsum
                                   | b_prop x => b_prop x
                                   | b_nom x => b_nom x
                                   | b_negprop x => if x =? p then f else b_negprop x
                                   | b_negnom x => b_negnom x
                                   | b_conj x x0 => b_conj (s x) (s x0)
                                   | b_disj x x0 => b_disj (s x) (s x0)
                                   | b_diau x x0 => b_diau x (s x0)
                                   | b_boxu x x0 => b_boxu x (s x0)
                                   | b_diab x x0 x1 => b_diab x (s x0) (s x1)
                                   | b_boxb x x0 x1 => b_boxb x (s x0) (s x1)
                                   end
    |}.
End Substitution.

Notation "B [ A \ p ]" := (substitute B p A) (at level 10).
Notation "B [ A \ ~ p ]" := (neg_subst A p B) (at level 10).
(* Notation "B - [ A \ p ]" := (neg_substitute B p A) (at level 11). *)
