Require Import Vectors.Vector.

Require Import SQEMA.Algebra.

Import VectorNotations.
Section CT.
  Context {F : Type -> Type}.

  Record Invariant__Dict := { imap__ {a b} : (a -> b) -> (b -> a) -> F a -> F b;
                            }.
  (** Invariant functor *)
  Definition Invariant := forall r, (Invariant__Dict -> r) -> r.
  Existing Class Invariant.

  Definition imap `{I : Invariant} {a b} : (a -> b) -> (b -> a) -> F a -> F b :=
    I _ (imap__) a b.
  
  Record Contra__Dict := { contramap__ {a b} : (a -> b) -> F b -> F a;
                         }.
  (** Contravariant functor *)
  Definition Contra := forall r, (Contra__Dict -> r) -> r.
  Existing Class Contra.

  Definition contramap `{C : Contra} {a b} : (a -> b) -> F b -> F a :=
    C _ (contramap__) a b.
  
  Record Functor__Dict := {
                           fmap__ {a b} : (a -> b) -> F a -> F b;
                           (* fmap_id__ {t} : forall ft, fmap__ (fun a : t => a) ft = ft; *)
                           (* fmap_fusion__ {a b c} : forall (f : a -> b) (g : b -> c) fa, fmap__ g (fmap__ f fa) = fmap__ (fun e => g (f e)) fa *)
                         }. (*Laws cant be CPS encoded*)
  (** Covariant functor *)
  Definition Functor := forall r, (Functor__Dict -> r) -> r.
  Existing Class Functor.

  Definition fmap `{f : Functor} {a b} : (a -> b) -> F a -> F b :=
    f _ (fmap__) a b.

  Record Pure__Dict := { pure__ {t} : t -> F t }.
  (** Pointed functor *)
  Definition Pure := forall r , (Pure__Dict -> r) -> r.
  Existing Class Pure.

  Definition pure `{p : Pure} {t} : t -> F t :=
    p _ (pure__) t.

  Record CoPure__Dict := { copure__ {t} : F t -> t }.
  (** Copointed functor *)
  Definition CoPure := forall r , (CoPure__Dict -> r) -> r.
  Existing Class CoPure.

  Definition copure `{p : CoPure} {t} : F t -> t :=
    p _ (copure__) t.

  
  Record Applicative__Dict := { _f__ :> Functor;
                                _p__ :> Pure;
                                zip__ {a b} : F a -> F b -> F (a * b)%type;
                              }.
  (** Applicative functor *)
  Definition Applicative :=
    forall r, (Applicative__Dict -> r) -> r.
  Existing Class Applicative.

  Definition zip `{A : Applicative} {a b} : F a -> F b -> F (a * b)%type :=
    A _ (zip__) a b.

  Definition _f `{A : Applicative} : Functor :=
    A _ (_f__).

  Definition _p `{A : Applicative} : Pure :=
    A _ (_p__).

  Definition ap `{A : Applicative} {a b} : F (a -> b) -> F a -> F b :=
    fun ff fa => let _ := _f in fmap (fun t => match t with (f, e) => f e end) (zip ff fa).
    
  Definition zipWith `{A : Applicative} {a b c} : (a -> b -> c) -> F a -> F b -> F c :=
    fun f fa fb => let _ := _f in ap (fmap f fa) fb.

  Record Pointed__Dict := { point__ {t} : F t }.
  Definition Pointed := forall r, (Pointed__Dict -> r) -> r.
  Existing Class Pointed.
  
  Definition point `{P : Pointed} {t} : F t :=
    P _ (point__) t.

  Record Monad__Dict := { m_f :> Functor;
                          join__ {t} : F (F t) -> F t
                        }.
  Definition Monad := forall r, (Monad__Dict -> r) -> r.
  Existing Class Monad.
  Record CoMonad__Dict := { c_f :> Functor;
                            cojoin__ {t} : F t -> F (F t)
                          }.
  Definition CoMonad := forall r, (CoMonad__Dict -> r) -> r.
  Existing Class CoMonad.

  
  Definition join `{M : Monad} {t} : F (F t) -> F t :=
    M _ (join__) _.

  Definition bind `{M : Monad} {t u} : (t -> F u) -> F t -> F u :=
    let _ := M _ (m_f) in fun f ft => join (fmap f ft).

  Definition mon_fun `{M : Monad} : Functor :=
    M _ (m_f).

  Definition cojoin `{M : CoMonad} {t} : F t -> F (F t) :=
    M _ (cojoin__) _.

  Definition cobind `{M : CoMonad} {t u} : (F t -> u) -> F t -> F u :=
    let _ := M _ (c_f) in fun f ft => fmap f (cojoin ft).

  Definition comon_fun `{M : CoMonad} : Functor :=
    M _ (c_f).

  
  Record Filter__Dict := { filter__ {t} (p : t -> bool) : F t -> F {e : t | p e = true} }.
  Definition Filter := forall r, (Filter__Dict -> r) -> r.
  Existing Class Filter.

  Definition filter `{Fi : Filter} {t} (p : t -> bool) : F t -> F {e : t | p e = true} :=
    Fi _ (filter__) _ p.
End CT.

Infix "<$>" := (fmap) (at level 15).
Infix "<:>" := (ap) (at level 15).

Section ExtCT.
  Context {F : Type -> Type} {G : Type -> Type}.
  Record Traversable__Dict := { t_f__ :> @Functor F; 
                                sequence__ `{@Applicative G} {t} : F (G t) -> G (F t);
                              }.
  Definition Traversable := forall r, (Traversable__Dict -> r) -> r.
  Existing Class Traversable.

  Definition sequence `{T : Traversable} `{@Applicative G} {t} : F (G t) -> G (F t) :=
    T _ (sequence__) _ _.

  Definition traverse `{T : Traversable} `{@Applicative G} {t u} : (t -> G u) -> F t -> G (F u) :=
    let f := T _ (t_f__) in fun f ft => sequence (fmap f ft). 
End ExtCT.