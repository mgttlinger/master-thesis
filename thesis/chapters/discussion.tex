\chap{Error in the Algorithm Description}
\label{sec:error}

As previously hinted when describing the algorithm SQEMA and justifying our alternative implementation, the description given in \textcite{conradie2006algorithmic, conradie2006algorithmicII, conradie2007algorithmic} leaves a few questions unanswered.
For example, how to solve for an atom or when and to what extent the propositional reasoning should be done.

Most notably however is a missed step in the correctness proof of the algorithm which --- at least in the polymodal case --- lead to an error in the algorithm slipping through peer review unnoticed.
Both the papers \cite{conradie2006algorithmic, conradie2006algorithmicII} and Conradies PhD thesis \cite{conradie2007algorithmic} describe the algorithm such that the systems are joined together into \(pure(\phi)\) before the standard translation and the existential quantification over the free variable \(x\) is done.

Given that the systems will be joined with conjunction (disjunction and then negation) and the rules only ensure that for each system there exists a world where the resulting formula holds, joining together with only one existentially quantified world requires all those worlds to coincide.
This step is not justified by the authors although the formulas can always be transformed in a way such that those worlds coincide because the worlds are all connected (arbitrary many steps on arbitrary relations) to the world where the input formula held.

The problematic case causing this error is the box rule which works by effectively shifting the world where the resulting formula holds to one of the successors along the relation of the modality used.
If we look at when the box rule gets applied, we see that this is the case when there are positive occurrences of a variable \(p\) under a box and \(p\) is non-trivial in the equation.
Thus, tracing that box through the execution of the algorithm back to the input formula, this situation occurs when there are negative occurrences of \(p\) under a diamond where \(p\) is not trivial in that conjunct of the input formula after normalization (we call this a \uline{non-trivial diamond}).
We also note that the rule will move the world where the resulting formula part will hold to one of the worlds where the formula under the diamond would have held.

Hence, if two or more of the conjuncts contain non-trivial diamonds and the formulas under the diamonds necessarily hold in different successors, the resulting formula parts will necessarily have different worlds where they hold.
This would result in a possibly unsatisfiable formula when simply joined together.
An easy fix for this issue is to translate into \ac{FOL} and then existentially quantify those worlds before joining the systems together.
When staying in modal logic, that step requires the use of the global diamond modality to handle this quantification over worlds.
In any case the resulting formula would still be a local first-order correspondent and only slightly more complicated than the one the original algorithm would have produced.

Take the frame \(\mathcal{F}\) consisting of worlds \(\{w, x , y\}\) and relations \(\{\alpha, \beta, \gamma\}\).  
\begin{center}
  \begin{tikzpicture}
    \node[draw, circle] (w) {$w$};
    \node[draw, circle, above right = .5em and 2em of w] (x) {$x$};
    \node[draw, circle, below right = .5em and 2em of w] (y) {$y$};
    \draw[thick, ->] (w) -- node[above] {$\alpha$} (x);
    \draw[thick, ->, loop above] (x) to node[above] {$\gamma$} (x);
    \draw[thick, ->] (w) -- node[below] {$\alpha$} (y);
    \draw[thick, ->, loop below] (y) to node[below] {$\beta$} (y);
  \end{tikzpicture}  
\end{center}
There \(x\) is \(\gamma\)-reflexive and \(y\) is \(\beta\)-reflexive.
Therefore, \(w\) has an \(\alpha\)-successor where \(p \impl \Dia{\gamma}{p}\) holds as well as an \(\alpha\)-successor where  \(p \impl \Dia{\beta}{p}\) holds under all valuations.
Hence, \(\Dia{\alpha}{(p \impl \Dia{\gamma}{p})} \land \Dia{\alpha}{(p \impl \Dia{\beta}{p})}\) holds at \(w\) under all valuations.

The algorithm SQEMA as described in \textcite{conradie2006algorithmicII, conradie2007algorithmic} and implemented in the reference implementation\footnote{\url{https://store.fmi.uni-sofia.bg/fmi/logic/sqema/sqema_gwt_20170412_1/Polyadic/SQEMA.html}} produces the two systems (after negation) \(\Dia{\gamma}{\IDia{-1}{\alpha}{\nom i}} \land \IDia{-1}{\alpha}{\nom i}\) which holds at \(x\) and \(\Dia{\beta}{\IDia{-1}{\alpha}{\nom i}} \land \IDia{-1}{\alpha}{\nom i}\) which holds at \(y\) under all \([\nom i := w]\)-valuations.
Since both of those only hold at \(x\) and \(y\) respectively, their conjunction will hold nowhere in this frame.

Hence, the conjunction which the algorithm produces is not a local frame-correspondent to the input formula and the algorithm therefore produced an incorrect result.
Note that the online implementation still produces a correct \ac{FOL} formula but does not justify how to get to that formula from the systems at the end of its trace.

If there also exists a counterexample for the monomodal case from the first SQEMA paper \cite{conradie2006algorithmic} is not clear, although we see no reason why such a formula should not exist.