From SQEMA Require Export Utils CT.

Section OptionUtils.
  Global Program Instance option_Functor : @Functor option := fun r f => f {| fmap__ := option_map;
                                                                        |}.
  
  Global Instance option_Pointed : @Pointed option := fun r f => f {| point__ := @None |}.
  Global Instance option_Pure : @Pure option := fun r f => f {| pure__ := @Some |}.
  Global Program Instance option_Applicative : @Applicative option := fun r f => f {| zip__ A B fa fb :=
                                                                                     match (fa, fb) return option (A * B) with
                                                                                     | (None, _) => None
                                                                                     | (_, None) => None
                                                                                     | (Some a, Some b) => Some (a, b)
                                                                                     end
                                                                                |}.
  Global Instance option_Monad : @Monad option := fun r f => f {| join__ A ffa := match ffa with
                                                                             | Some x => x
                                                                             | None => None
                                                                             end
                                                            |}.

  Definition option_ {A B} := option_rect (A := A) (fun _ => B).
End OptionUtils.