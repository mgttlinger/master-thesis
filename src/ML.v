From SQEMA Require Export Inverse CT.
Require Import VectorUtils.
Require Import Vectors.Vector.
Require Lists.List.
Section ML.
  Context {atom nom : Set}.

  (** Basic propositional syntax *)
  Class BPL m := { verum : m;
                   falsum : m;
                   prop : atom -> m;
                   nomi : nom -> m;
                   conj : m -> m -> m;
                   disj : m -> m -> m;
                 }.

  (** Arbitrary length conjunction *)
  Definition bigconj {m} `{BPL m} : list m -> m := fun fs => List.fold_right conj verum fs.
  (** Arbitrary length disjunction *)
  Definition bigdisj {m} `{BPL m} : list m -> m := fun fs => List.fold_right disj falsum fs.

  (** Invariant functor instace for deriving BPL instances *)
  Global Instance bpl_invar : Invariant (F := BPL) :=
    fun r f => f {| imap__ A B := fun ab ba (fa : BPL A) => 
                                 {| verum := ab verum;
                                    falsum := ab falsum;
                                    prop p := ab (prop p);
                                    nomi n := ab (nomi n);
                                    conj a b := ab (conj (ba a) (ba b));
                                    disj a b := ab (disj (ba a) (ba b))
                                 |}
              |}.
  
  (** Basic syntax with negation *)
  Class PL m := { B :> BPL m;
                  neg : m -> m;
                }.

  (** Implication defined from negation *)
  Definition impl {m} `{PL m} : m -> m -> m := fun a b => disj (neg a) b.
  (** Equivalence defined from conjunction and implication *)
  Definition eqv {m} `{PL m} : m -> m -> m := fun a b => conj (impl a b) (impl b a).

  (** Invariant functor instance for deriving PL instances *)
  Global Instance pl_invar : Invariant (F := PL) :=
    fun r f => f {| imap__ A _ := fun ab ba (fa : PL A) => 
                                 let b : BPL A := B in
                                 {| B := imap ab ba b; 
                                    neg f := ab (neg (ba f))
                                 |}
              |}.

  (** Unary modalities *)
  Context {opu : Set}.
  
  (** Syntax with unary modalities *)
  Class UML m := { diau : opu -> m -> m;
                   boxu : opu -> m -> m;
                 }.

  (** Invariant functor instance for derivation of UML instances *)
  Global Instance uml_invar : Invariant (F := UML) :=
    fun r f => f {| imap__ A B := fun ab ba (fa : UML A) => 
                                 {| diau o f := ab (diau o (ba f));
                                    boxu o f := ab (boxu o (ba f)); 
                                 |}
              |}.

  (** Binary modalities *)
  Context {opb : Set}.

  (** Syntax with binary modalities *)
  Class BML m := { BU :> UML m;
                   diab : opb -> m -> m -> m;
                   boxb : opb -> m -> m -> m;
                 }.

  (** Invariant functor instance for deriving BML instances *)
  Global Instance bml_invar : Invariant (F := BML) :=
    fun r f => f {| imap__ A _ := fun ab ba (fa : BML A)  => 
                                 let B : UML A := BU in
                                 {| diab o f g := ab (diab o (ba f) (ba g));
                                    boxb o f g := ab (boxb o (ba f) (ba g));
                                    BU := imap ab ba B
                                 |}
              |}.

  (** Polyadic modalities *)  
  Context {ops : nat -> Set}.

  (** Syntax with polyadic modalities *)
  Class PML m := { pdia n : ops n -> Vector.t m n -> m;
                   pbox n : ops n -> Vector.t m n -> m;
                 }.

  (** Inverse polyadic diamond *)
  Definition pdiai {m} `{PML m} n `{PInverse ops} : ops n -> list (Fin.t n) -> Vector.t m n -> m := fun o i => pdia _ (List.fold_right pinv o i).
  (** Inverse polyadic box *)
  Definition pboxi {m} `{PML m} n `{PInverse ops} : ops n -> list (Fin.t n) -> Vector.t m n -> m := fun o i => pbox _ (List.fold_right pinv o i).

  (** Invariant functor instance to derive PML instances *)
  Global Instance pml_invar : Invariant (F := PML) :=
    fun r f => f {| imap__ A _ := fun ab ba (fa : PML A) => 
                                 {| pdia n o fs := ab (pdia n o (fmap ba fs));
                                    pbox n o fs := ab (pbox n o (fmap ba fs));
                                 |}
              |}.
End ML.

(** Inverse unary diamond *)
Definition diaui {m} {o : Set} `{UML (opu := FreeUInverse o) m} : FreeUInverse o -> m -> m := fun oe => diau (inv oe).
(** Inverse unary box *)
Definition boxui {m} {o : Set} `{UML (opu := FreeUInverse o) m} : FreeUInverse o -> m -> m := fun oe => boxu (inv oe).

(** Inverse binary diamond *)
Definition diabi {m} {ou o : Set} `{BML (opu := ou) (opb := FreeBInverse o) m} : FreeBInverse o -> bool -> m -> m -> m := fun oe i => if i then diab (inv_fst (oe)) else diab (inv_snd oe).
(** Inverse binary box *)
Definition boxbi {m} {ou o : Set} `{BML (opu := ou) (opb := FreeBInverse o) m} : FreeBInverse o -> bool -> m -> m -> m := fun oe i => if i then boxb (inv_fst oe) else boxb (inv_snd oe).


Notation "\'bot'" := falsum.
Notation "\'top'" := verum.
(* Notation "[# A #] f" := (box A f) (at level 12). *)
(* Notation "<# A #> f" := (dia A f) (at level 12). *)
Notation "[| A |]" := (pbox _ A) (at level 10).
Notation "<| A |>" := (pdia _ A) (at level 10).
Notation "[+ A i +]" := (pboxi _ A i) (at level 9, i at level 20).
Notation "<+ A i +>" := (pdiai _ A i) (at level 9, i at level 20).
Infix "//\\" := conj (at level 15).
Infix "\\//" := disj (at level 16).
Notation "! f" := (neg f) (at level 17).
Infix "=>>" := impl (at level 14).
Infix "<=>" := eqv (at level 13).