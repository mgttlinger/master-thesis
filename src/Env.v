From SQEMA Require Export Utils CT.

Section Env.
 (** Environment comonad  *)
  Definition Env t u := (t * u)%type.
  Definition env {t u} : t -> u -> Env t u := pair.
  Definition runEnv {t u} : Env t u -> (t * u) := id.
  Definition ask {t u} : Env t u -> t := fst.
  Definition local {t u v} f : Env t u -> Env v u := fun etu => match etu with (s, c) => (f s, c) end.
  
  Global Instance env_Functor {t} : @Functor (Env t) := fun r f => f {| fmap__ A B ab fa := match fa with (s, c) => (s, ab c) end |}. 
  Global Instance env_CoMonad {t} : @CoMonad (Env t) := fun r f => f {| cojoin__ A (fa : (t * A)) := (ask fa, fa) |}.

  Definition ask_map {t u v} (f : t -> u -> v) : Env t u -> Env t v := fun etu => fmap (f (ask etu)) etu. 
  Definition ask_bind {t u v} (f : t -> u -> Env t v) : Env t u -> Env t v := fun etu => snd (fmap (f (ask etu)) etu).
  Definition ask_bind' {t u v} (f : t -> u -> (t * v)) : Env t u -> Env t v := ask_bind f.

  Definition EnvT (f : Type -> Type) t u := Env t (f u).
  Global Instance envt_Functor {f} {t} `{F : Functor f} : @Functor (EnvT f t) :=
    fun r f => f {| fmap__ A B ab fa := fmap (f := env_Functor) (fmap (f := F) ab) fa |}. 
  Global Instance envt_Monad {f} {t} `{M : Monad f} `{C : CoPure f} : @Monad (EnvT f t) :=
    fun r f => let _ := mon_fun in f {| join__ A ffa := let n := snd ffa in
                                                     (copure (fmap fst n), bind snd n)
                                  |}.
  Global Instance envt_CoMonad {t} {f} `{P: Pure f} `{F: Functor f}: @CoMonad (EnvT f t) :=
    fun r f => f {| cojoin__ A fa := (ask fa, pure (p := P) fa) |}.
End Env.