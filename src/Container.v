Class Container (F : Set -> Set) := { inP : forall {t : Set}, t -> F t -> Prop }.
Class DecContainer (F : Set -> Set) := { inB : forall {t : Set}, t -> F t -> bool}.
Instance DecContainerContainer {F} `{DecContainer F} : Container F := {| inP t te ts := is_true (inB te ts) |}.