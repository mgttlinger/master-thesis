From SQEMA Require Export Utils CT.
Require Import SQEMA.Algebra.
Require Import Lists.List.
Import ListNotations.
Require Export Classes.EquivDec.
Require Export Sorting.Permutation.
Require Import Program.Equality.

Module ListUtils.
  Global Instance list_Functor : @Functor list := fun r f => f {| fmap__ := map;
                                                            |}.

  Global Instance list_Pointed : @Pointed list := fun r f => f {| point__ := @nil |}.
  Global Instance list_Pure : @Pure list := fun r f => f {| pure__ _ a := [a] |}.
  Global Instance list_Applicative : @Applicative list := fun r f => f {| zip__ := @list_prod |}.
  Global Instance list_Monad : @Monad list := fun r f => f {| join__ := fun _ => flat_map id |}.

  Global Program Instance list_Monoid {t} : Monoid (list t) := {| mult := app (A := t);
                                                                  one := [];
                                                                  one_neutral_l := app_nil_l (A := t);
                                                                  one_neutral_r := app_nil_r (A := t)
                                                               |}.
  Next Obligation. red. symmetry. apply app_assoc. Defined.

  Fixpoint lSequence {G} `{A: Applicative G} T (l : list (G T)) : G (list T) := let _ := @_p G A in
                                                                                  let _ := @_f G A in
                                                                                  match l with
                                                                                  | [] => pure ([])
                                                                                  | gt :: gts => fmap cons gt <:> (lSequence _ gts)
                                                                                  end.
  Global Instance list_Traversable {G} : @Traversable list G := fun r f => f {| sequence__ _ := lSequence;
                                                                          |}.


  Tactic Notation "ap_unfold" := cbv [_f _f__ _p__ fmap ListUtils.list_Functor fmap__ ap zip zip__ ListUtils.list_Applicative].
  Definition is_empty {A} (l : list A) := match l with
                                          | nil => true
                                          | cons x x0 => false
                                          end.

  Definition elem {A} `{EqDec A eq} : A -> list A -> bool := fun e es => match in_dec equiv_dec e es with
                                                                 | left _ => true
                                                                 | right _ => false
                                                                 end.
  
  Fixpoint splits {t} l : list (list t * list t) :=
    match l with
    | [] => [([],[])]
    | t :: ts => ([], t :: ts) :: (fmap (fun p => match p with (b, c) => (t :: b, c) end) (splits ts))
    end.
  
  Fixpoint permutations {t} (l : list t) : list (list t) :=
    match l with
    | [] => [[]]
    | [x] => [[x]]
    | t :: ts => fmap (fun p => match p with (b, c) => b ++ (t :: c) end) (bind splits (permutations ts))
    end.

  Definition fold_map {A B} (f : A -> B) `{M : Monoid B} : list A -> B :=
    fold_right (fun a accu => mult (f a) accu) one.
  
  Definition collect {A B} (f : A -> option B) : list A -> list B :=
    fold_right (fun a accu => match f a with Some b => b :: accu | None => accu end) nil.

  Fixpoint maxBy {t} (f : t -> nat) l : nat := match l with
                                          | [] => 0
                                          | x :: xs => PeanoNat.Nat.max (f x) (maxBy f xs)
                                          end.
  
  
  Program Definition dep_filter {U} f (l : list U) : { l' : list U | forall e, In e l' -> f e = true } := filter f l.
  Next Obligation. apply filter_In in H. tauto. Defined.

  Lemma filter_app {T} : forall (p : T -> bool) l1 l2, filter p (l1 ++ l2) = filter p l1 ++ filter p l2.
    induction l1; eauto with datatypes.
    cbn [app filter].
    deceq; eauto.
    cbn [app].
    intro; rewrite IHl1.
    reflexivity.
  Qed.
  
  Program Definition dep_filter' {T} (p : T -> bool) : list T -> list {e : T | p e = true} := 
    fix f l := match l with
               | [] => []
               | x :: l0 => match p x with true => x :: f l0 | false => f l0 end
               end.
  
  Program Fixpoint dep_find {U} f (l : list U) : option { u : U | In u l /\ f u = true } :=
  match l with
  | nil => None
  | cons x x0 => match f x with true => Some x | false => option_map (fun e => match e with exist _ el p => exist _ el _ end) (dep_find f x0) end  
  end.

  Lemma remove_le_length {T} `{EqDec T eq} l a : length (remove equiv_dec a l) <= length l.
    induction l; simpl; intuition. deceq; intuition. destruct IHl; simpl; intuition.
  Qed.
  
  Lemma remove_shorter {T} `{EqDec T eq} (a : T) l : In a l -> length (remove equiv_dec a l) < length l.
    induction l; simpl; try tauto.
    intros [He | Hi].
    - rewrite He. deceq; try congruence.
      destruct (remove_le_length l a); auto. apply Lt.le_lt_n_Sm. auto.
    - mp. destruct (a == a0); auto.
      cbn [length]. rewrite <- PeanoNat.Nat.succ_lt_mono; auto.
  Qed.

  Lemma remove_preserves_not_In {A} `{EqDec A eq} : forall (a : A) l, ~ In a l -> forall b, ~ In a (remove equiv_dec b l).
    induction l; simpl. tauto. intros. deceq; auto. intro. inversion H1; auto. apply IHl with (b := b); auto.
  Qed.
  
  Lemma remove_preserves_NoDup {A} `{EqDec A eq} : forall l (a : A), NoDup l -> NoDup (remove equiv_dec a l).
    induction l; simpl. tauto. intros. inversion H0. subst. deceq; auto. constructor; auto. apply remove_preserves_not_In; auto.
  Qed.
  
  
  Program Fixpoint dep_map {U V} {P : U -> Prop} (f : forall u, P u -> V) l : (forall e, In e l -> P e) -> list V := fun p =>
    match l with
    | [] => []
    | h :: t => f h (p h _) :: dep_map f t _
    end.
  Next Obligation. constructor. trivial. Defined.
  Next Obligation. apply p. constructor 2. trivial. Defined.

  Program Fixpoint dep_flat_map {U V} {P : U -> Prop} (f : forall u, P u -> list V) l : (forall e, In e l -> P e) -> list V := fun p =>
    match l with
    | [] => []
    | h :: t => f h (p h _) ++ dep_flat_map f t _
    end.
  Next Obligation. constructor. trivial. Defined.
  Next Obligation. apply p. constructor 2. trivial. Defined.
  
  Fixpoint remove_first {T} `{EqDec T eq} t l {struct l} : list T := match l with
                                                                     | nil => nil
                                                                     | cons x x0 => if x ==b t then x0 else x :: remove_first t x0
                                                                     end.

  Lemma neg_In_iff : forall t a b (e : t), ~List.In e (a ++ b) <-> ~List.In e a /\ ~List.In e b. intros t a b e. rewrite List.in_app_iff. tauto. Qed.

  Lemma Forall_app {T} : forall l1 l2 (P : T -> Prop), (Forall P l1 /\ Forall P l2) <-> Forall P (l1 ++ l2).
  Proof with auto with datatypes.
    intros l1 l2 P. induction l1, l2; cbn [app] in *; rewrite_all app_nil_r...
    1-3: solve [intuition].
    split; intro H.
    - destruct H. inversion H. constructor; auto. apply IHl1; auto.
    - inversion H. apply IHl1 in H3. destruct H3...
  Qed.

  Lemma Forall_cons {T} : forall h l (P : T -> Prop), (P h /\ Forall P l) <-> Forall P (h :: l).
  Proof with auto with datatypes.
    intros h l P. split; intro H; inversion H... 
  Qed.

  Lemma Forall_unrelated_disj {T} : forall P (Q : T -> Prop) l, Forall (fun t => P \/ (Q t)) l <-> P \/ (Forall Q l).
    intros P Q l.
    induction l.
    - intuition auto with datatypes.
    - rewrite <- !ListUtils.Forall_cons, IHl. tauto.
  Qed.

  Lemma filter_empty {T} : forall (p : T -> bool) l, filter p l = [] <-> Forall (fun e => p e = false) l.
    induction l; simpl; split; auto with datatypes.
    - destruct (p a) eqn:Hpa; try congruence.
      rewrite IHl.
      auto.
    - intro H.
      inversion H.
      rewrite H2.
      rewrite IHl.
      auto.
  Qed.

  Lemma filter_Forall {T} : forall (p : T -> bool) l, Forall (fun el => p el = true) (filter p l).
    induction l; cbn [filter]; auto.
    destruct (p a) eqn:Hp; auto with datatypes.
  Qed.
  
  Lemma Exists_app {T} : forall l1 l2 (P : T -> Prop), (Exists P l1 \/ Exists P l2) <-> Exists P (l1 ++ l2).
  Proof with auto with datatypes.
    intros l1 l2 P. induction l1, l2; cbn [app] in *; rewrite_all app_nil_r...
    1-3: intuition.
    inversion H0.
    split; intro H.
    - destruct H; inversion H...
      constructor 2.
      apply IHl1...
      constructor 2.
      apply IHl1...
      constructor 2.
      apply IHl1...
    - inversion H.
      left...
      apply IHl1 in H1.
      destruct H1.
      left...
      right...
  Qed.

  Lemma Permutation_Forall {T} {P : T -> Prop}: forall (l l' : list T), Permutation l l' -> Forall P l <-> Forall P l'.
    intros l l' Hp.
    induction Hp; try tauto.
    - split; intro H; inversion H; constructor; trivial; apply IHHp; trivial.
    - split; intro H; inversion H; inversion H3; constructor; trivial; constructor; trivial.
  Qed.

  Lemma Permutation_Exists {T} {P : T -> Prop}: forall (l l' : list T), Permutation l l' -> Exists P l <-> Exists P l'.
    intros l l' Hp.
    induction Hp; try tauto.
    - split; intro H; inversion H; constructor; trivial; apply IHHp; trivial.
    - split; intro H; inversion H.
      1,3: constructor 2; constructor 1; trivial.
      all: inversion H1.
      1,3: constructor 1; trivial.
      all: constructor 2; constructor 2; trivial.
  Qed.

  Lemma map_ap {T U V W} : forall (f : T -> W) (g : U -> V -> T) (l : list U) (l' : list V), map f ((map g l) <:> l') = (map (fun x y => f (g x y)) l) <:> l'.
    intros f g.
    induction l.
    - cbn. tauto.
    - rewrite !map_cons.
      revert IHl.
      ap_unfold.
      intros IHl l'.
      pose proof (IHl l') as IH.
      clear IHl.
      rewrite map_map in *.
      cbn [list_prod].
      rewrite !map_app.
      rewrite IH.
      f_equal.
      clear IH.
      induction l'; auto.
      rewrite !map_cons.
      f_equal; auto.
  Qed.

  Lemma map_ap_f {T U W} : forall (f : T -> U) (g : U -> U -> W) (l l' : list T), ((map (fun a b => g (f a) (f b)) l) <:> l') = (map g (map f l)) <:> (map f l').
    intros f g.
    induction l.
    - cbn. tauto.
    - rewrite !map_cons.
      revert IHl.
      ap_unfold.
      intros IHl l'.
      pose proof (IHl l') as IH.
      clear IHl.
      rewrite map_map in *.
      cbn [list_prod].
      rewrite !map_app.
      rewrite IH.
      f_equal.
      clear IH.
      induction l'; auto.
      rewrite !map_cons.
      f_equal; auto.
  Qed.

  Lemma Forall_map {T U} : forall P Q (l : list T) (f : T -> U), (forall t, P t <-> Q (f t)) -> Forall P l <-> Forall Q (map f l).
    induction l; cbn.
    - intros. split; intro; constructor.
    - intros. split; intro H'; inversion H'.
      + rewrite H in H2. rewrite IHl in H3; try apply H. constructor; trivial.
      + rewrite <- H in H2. rewrite <- IHl in H3; try apply H. constructor; trivial.
  Qed.

  Lemma Exists_map {T U} : forall P Q (l : list T) (f : T -> U), (forall t, P t <-> Q (f t)) -> Exists P l <-> Exists Q (map f l).
    induction l; cbn.
    - intros. split; introversion.
    - intros. split; intro H'; inversion H'.
      1,3: constructor; apply H; auto.
      constructor 2; auto; apply IHl; auto.
      constructor 2; auto; apply <- IHl; auto; exact H1.
  Qed.

  Lemma Forall_pullout {T U} : forall (P : U -> Prop) (Q : U -> T -> Prop) (l : list T), Forall (fun g => forall v, P v -> Q v g) l <-> forall v, P v -> Forall (Q v) l.
    intros P Q l.
    induction l; auto.
    - split; auto.
    - split; intro H.
      inversion H.
      intros.
      constructor; auto.
      apply IHl; auto.
      constructor; [idtac | apply IHl]; intros v Pv; pose proof (H v Pv) as H'; inversion H'; auto.
  Qed.

  Lemma Exists_pullout {T U} : forall (P : U -> Prop) (Q : U -> T -> Prop) (l : list T), Exists (fun g => exists v, P v /\ Q v g) l <-> exists v, P v /\ Exists (Q v) l.
    intros P Q l.
    induction l.
    - split; intro H; inversion H.
      destruct H0 as [_ H']; inversion H'.
    - split; intro H; inversion H.
      + destruct H1 as [v [Pv Qva]].
        exists v. auto with datatypes.
      + rewrite IHl in H1.
        destruct H1 as [v [Pv Evl]].
        exists v. auto with datatypes.
      + destruct H0 as [Px Eqal].
        inversion Eqal.
        constructor; exists x; auto.
        constructor 2. apply IHl. exists x; auto.
  Qed.

  Lemma app_nil_iff {T} : forall l1 l2 : list T, l1 ++ l2 = [] <-> l1 = [] /\ l2 = [].
    induction l1; cbn [app]; try tauto.
    intro l2.
    split.
    introversion.
    intros [F _]; inversion F.
  Qed.
  
  Lemma Forall_iff : forall (A : Type) (P Q : A -> Prop), (forall a : A, P a <-> Q a) -> forall l : list A, Forall P l <-> Forall Q l.
    intros A P Q H l.
    split; apply Forall_impl; apply H.
  Qed.
  Hint Resolve Forall_app Exists_app Permutation_Forall Permutation_Exists neg_In_iff : datatypes.
End ListUtils.