\documentclass[a4paper,11pt,numbers=noenddot,pagesize=false,twoside,BCOR=2cm,DIV=13] {scrbook}

%\usepackage[top=2cm,lmargin=1in,rmargin=1in,bottom=3cm,hmarginratio=1:1]{geometry}
\usepackage{fontspec}
\usepackage{polyglossia}        %babel successor
\setdefaultlanguage[variant=uk]{english}
\setotherlanguage{german}
\usepackage{amssymb}
\usepackage{acronym}
\usepackage{amsthm}
\usepackage{thmtools}
\usepackage{fancyvrb}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{stmaryrd}
\usepackage{ifthen}
\usepackage{xspace}
\usepackage{hyperref}
\usepackage[nameinlink]{cleveref} %better references
\usepackage{makeidx}
\usepackage{graphicx}
\usepackage{prftree}
\usepackage{xspace}
\usepackage[obeyFinal]{todonotes}
\usepackage{tabularx}           %more flexible tables
\usepackage{tikz}
\usepackage[normalem]{ulem}                %underlining
\usepackage{caption}                       %somehow improve captions
\usepackage[cache=false, newfloat]{minted} %source listings with pygments
\usepackage[title, titletoc]{appendix}
% \usepackage{spelling}                      %spellchecking
\usepackage[english=british]{csquotes}           %biblatex recommended to load this
\usepackage[style=alphabetic]{biblatex}    %advanced citations
\bibliography{bib}
\usetikzlibrary{graphs, graphdrawing, arrows, calc}
\usegdlibrary{layered}

\definecolor{bg}{rgb}{0.95,0.95,0.95}
\setminted[coq]{bgcolor=bg, style=emacs, breaklines}
\newenvironment{clisting}{\captionsetup{type=listing}}{}
\newcommand{\cinput}[2]{\inputminted[#1]{coq}{#2} \vspace{-2em}}

\title{Formalising Algorithmic Correspondence for Modal Languages}
\author{Merlin Göttlinger}

\newcommand\chap[1]{%
  \chapter{#1}}%
  % \chaptermark{#1}%
  % \addcontentsline{toc}{chapter}{#1}}


\renewcommand{\phi}{\varphi}
\newcommand{\impl}{\to}
\newcommand{\eqv}{\leftrightarrow}
\newcommand{\MBox}[2]{\Box_{#1}#2}
\newcommand{\IBox}[3]{\Box^{#1}_{#2}#3}
\newcommand{\Dia}[2]{\Diamond_{#1}#2}
\newcommand{\IDia}[3]{\Diamond^{#1}_{#2}#3}
\newcommand{\nom}[1]{\mathbf{#1}}
\newcommand{\ar}[0]{Ackermann rule\xspace}

\declaretheorem[name=Definition,style=definition,numberwithin=chapter]{definition}
\declaretheorem[name=Example,style=definition,sibling=definition]{example}
\declaretheorem[style=definition,numbered=no]{exercise}
\declaretheorem[name=Remark,style=definition,sibling=definition]{remark}
\declaretheorem[name=Assumption,style=definition,sibling=definition]{assumption}
\declaretheorem[name=Observation,style=definition,sibling=definition]{observation}
\declaretheorem[name=Theorem,sibling=definition]{theorem}
\declaretheorem[sibling=definition]{corollary}
\declaretheorem[name=Fact,sibling=definition]{fact}
\declaretheorem[sibling=definition]{lemma}
\declaretheorem[sibling=lemma]{proposition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                   Spacing settings                                      %
%                                                                         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt}
\setlength{\marginparsep}{0cm}
\setlength{\marginparwidth}{2.5cm}
\makeatletter
% kill those ugly red rectangles around links and set metadata
\hypersetup{
  colorlinks,%
  citecolor=black,%
  filecolor=black,%
  linkcolor=black,%
  urlcolor=black,%
  pdftitle={\@title},%
  pdfauthor={\@author},%
  pdfcreator={LuaLaTeX}
}
\makeatother

\acrodef{SOL}{Second-Order Logic}
\acrodef{MSOL}{Monadic Second-Order Logic}
\acrodef{FOL}{First-Order Logic}
\acrodef{DML}{distributive modal logic}
\acrodef{DNF}{disjunction normal form}
\acrodef{NNF}{negation normal form}

\acrodef{AST}{abstract syntax tree}
\acrodef{CPS}{continuation passing style}

\acrodef{dg}[digraph]{directed graph}
\acrodefplural{dg}[digraphs]{directed graphs}

\begin{document}
\pagestyle{plain}

\frontmatter
\input{src/titlepage}%
\chapter*{Disclaimer}
\begin{german}
Ich versichere, dass ich die Arbeit ohne fremde Hilfe und ohne Benutzung anderer als der angegebenen Quellen angefertigt habe und dass die Arbeit in gleicher oder ähnlicher Form noch keiner anderen Prüfungsbehörde vorgelegen hat und von dieser als Teil einer Prüfungsleistung angenommen wurde.
Alle Ausführungen, die wörtlich oder sinngemäß übernommen wurden, sind als solche gekennzeichnet.

\vspace{5em}
Nürnberg, \today{} \rule{7cm}{1pt}\\
\phantom{Nürnberg, \today{}} Merlin Göttlinger
\end{german}
\chapter*{Abstract}
\paragraph{English}
\citeauthor{conradie2006algorithmicII} proposed an extension of their algorithm SQEMA for translating a subset of hybrid polyadic modal logic into first-order conditions on frames.
Implementing a simplified version of this extension in the proof assistant Coq, we uncover inaccuracies in the algorithm description, an error in the PhD thesis of \citeauthor{conradie2007algorithmic}, and propose ways to mitigate those discoveries.
Further, we implement and prove that our proposed algorithm description leads to a correct algorithm.

\begin{german}
  \paragraph{Deutsch}
  \citeauthor{conradie2006algorithmicII} haben eine Erweiterung ihres Algorithmus SQEMA entwickelt um eine Teilmenge der hybriden polyadischen Modallogik in Aussagen über Rahmen in Prädikatenlogik erster Stufe zu übersetzen.
  In dieser Arbeit implementieren wir eine vereinfachte Version dieser Erweiterung im Beweisassistenten Coq.
  Anhand dieser Implementierung klären wir Unklarheiten in Definitionen und Beweisen des ursprünglichen Algorithmus und zeigen einen Fehler in der Doktorarbeit von \citeauthor{conradie2007algorithmic}.
  Des Weiteren implementieren und beweisen wir eine korrigierte Version des Algorithmus.
\end{german}

\tableofcontents
\mainmatter
\include{chapters/introduction}
\include{chapters/fundamentals}
\include{chapters/implementation}
\include{chapters/discussion}
\include{chapters/conclusion}
\printbibliography
%\listoffigures
\listoftheorems[ignoreall, show={definition,lemma,theorem,corollary}]
\begin{appendices}
\include{chapters/appendix}
\end{appendices}
\end{document}