From SQEMA Require Export Utils Algebra CT ML SQEMAUtils Nominals Polarity Substitution BiModal.

Require Import Lists.List.
Require Import Bool.
Import ListNotations.
Section Equation.
  Definition pro := nat.
  Context {opu opb nom : Set}.
  Notation "'bmf'" := (@bmf opu opb nom).
  
  (** A SQEMA equation which always has the form a \/ b *)
  Record eqn := { a : bmf;
                  b : bmf
                }.

  Definition eqn_map (f : bmf -> bmf) (e : eqn) := {| a := f e.(a);
                                                     b := f e.(b)
                                                  |}. 
      
  Definition eqn_toformula e : bmf := b_disj e.(a) e.(b).
  
  Global Instance e_nominals `{Nominals nom bmf} : @Nominals nom eqn :=
    {| nominals e := nominals e.(a) <*> nominals e.(b) |}.

  Global Instance e_polarity : Polarity eqn :=
    {| polarity_of e p := bmf_polarity_of p e.(a) <*> bmf_polarity_of p e.(b);
       pure_in := fun p e => andb (bmf_pure_in p e.(a)) (bmf_pure_in p e.(b))
    |}.

  Definition ETrivialIn p e := TrivialIn p (b_disj e.(a) e.(b)).
  Lemma ETrivialIn_trivial_in_reflect : forall p e, reflect (ETrivialIn p e) (trivial_in e p).
    unfold trivial_in, polarity_of, e_polarity, ETrivialIn.
    intros p [av bv].
    cbn [a b].
    destruct bmf_polarity_of eqn:D; cbn [mult polarity_monoid polarity_add].
    4: cbn [negb peqb]; constructor;
      introversion; inversion H0; [ rewrite (reflect_iff _ _ (PureIn_reflect' _ _)) in H4 | rewrite (reflect_iff _ _ (SPosIn_reflect _ _)) in H4 | rewrite (reflect_iff _ _ (SNegIn_reflect _ _)) in H4 ]; rewrite D in H4; cbn in H4; congruence.
    all: destruct (bmf_polarity_of p bv) eqn:D'; cbn [peqb negb]; constructor.
    1,3,9: constructor 2; constructor; rewrite (reflect_iff _ _ (SPosIn_reflect _ _)); rewrite ?D, ?D' ; auto.
    4,5,7,8: constructor 3; constructor; rewrite (reflect_iff _ _ (SNegIn_reflect _ _)); rewrite ?D, ?D' ; auto.
    all: introversion; inversion H0; [ rewrite (reflect_iff _ _ (PureIn_reflect' _ _)) in H4; rewrite (reflect_iff _ _ (PureIn_reflect' _ _)) in H5 | rewrite (reflect_iff _ _ (SPosIn_reflect _ _)) in H4; rewrite (reflect_iff _ _ (SPosIn_reflect _ _)) in H5 | rewrite (reflect_iff _ _ (SNegIn_reflect _ _)) in H4; rewrite (reflect_iff _ _ (SNegIn_reflect _ _)) in H5 ]; rewrite D in H4; rewrite D' in H5; cbn in *; congruence.
  Qed.
  
  Global Instance e_polarity_switch `{Polarity_Switch bmf bmf}: Polarity_Switch eqn eqn :=
    {| polarity_switch e p :=
         {| a := (polarity_switch e.(a) p);
            b := (polarity_switch e.(b) p)
         |}
    |}.

  Global Instance e_atoms : Atoms eqn :=
    {| atoms e := bmf_atoms e.(a) ++ bmf_atoms e.(b) |}.

  Global Program Instance e_eqdec `{EqDec bmf eq} : EqDec eqn eq.
  Next Obligation. destruct x, y. dec a0 a1; try (solve [uneq]). dec b0 b1; try (solve [uneq]). left; auto. Defined. 

  Definition e_subst `{@Substitution bmf bmf} : pro -> bmf -> eqn -> eqn := fun p f e => {| a := (e.(a) [f \ p]);
                                                                                      b := (e.(a) [f \ p])
                                                                                   |}.

  Global Instance e_elim_triv `{ElimTriv bmf} : ElimTriv eqn :=
    {| elim_triv p e := {| a := elim_triv p e.(a);
                           b := elim_triv p e.(b)
                        |}
    |}.

  Global Instance e_neg_subst `{NegSubstitution bmf bmf bmf} : NegSubstitution eqn bmf eqn :=
    {| neg_subst f p g := {| a := neg_subst f p g.(a);
                             b := neg_subst f p g.(b)
                          |}
    |}.

  
  Global Instance e_complexity `{Complexity bmf} : Complexity eqn := {| complexity p e := complexity p e.(a) + complexity p e.(b) |}.
  
End Equation.