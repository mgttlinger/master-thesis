From SQEMA Require Import Utils Reify Algebra ML CT Atoms Polyadic BiModal VectorUtils ListUtils.

Require Import Classes.EquivDec.
Require Import Lists.List.

Require Import Bool.

Inductive polarity : Set := p_pos | p_neg | p_none | p_mixed.
Definition peqb a b := match (a, b) with
                       | (p_pos, p_pos) => true
                       | (p_neg, p_neg) => true
                       | (p_none, p_none) => true
                       | (p_mixed, p_mixed) => true
                       | ow => false
                       end.
Infix "p=?" := peqb (at level 40).

Definition polarity_add p q :=
  match (p, q) with
  | (p_mixed, _) => p_mixed
  | (_, p_mixed) => p_mixed
  | (p_none, o) => o
  | (o, p_none) => o
  | (p_pos, p_pos) => p_pos
  | (p_neg, p_neg) => p_neg
  | (p_pos, p_neg) => p_mixed
  | (p_neg, p_pos) => p_mixed
  end.

Definition polarity_inv p :=
  match p with
  | p_pos => p_neg
  | p_neg => p_pos
  | p_none => p_none
  | p_mixed => p_mixed
  end.
Hint Unfold peqb polarity_add polarity_inv.

Lemma polarity_add_neutral_l : forall p, polarity_add p_none p = p.
  destruct p; intuition.
Qed.

Lemma polarity_add_neutral_r : forall p, polarity_add p p_none = p.
  destruct p; intuition.
Qed.

Global Program Instance polarity_monoid : Monoid polarity := {| mult := polarity_add;
                                                                one := p_none
                                                             |}.
Next Obligation. intro. induction a; auto; induction b; auto; induction c; auto. Defined.
Solve Obligations with (induction t; auto). 

Lemma polarity_add_none_iff : forall p1 p2, polarity_add p1 p2 = p_none <-> p1 = p_none /\ p2 = p_none.
  destruct p1, p2; split; intro H; inversion H; try congruence; auto.
Qed.

Section Polarity.
  Context {op opb nom : Set} {ops : nat -> Set}.
  Require Import Nat.
  Definition Polarity_Switch_BPL {M} `{@BPL nat nom M} `{@PL nat nom M} : @BPL nat nom (nat -> (bool * M)) :=
    {| verum := fun _ => (false, verum);
       falsum := fun _ => (false, falsum);
       nomi n := fun _ => (false, nomi n);
       prop q := fun p => if p =? q then (true, neg (prop p)) else (false, prop q);
       conj a b := fun p => (false, conj (snd (a p)) (snd (b p)));
       disj a b := fun p => (false, disj (snd (a p)) (snd (b p)));
    |}.

  Definition Polarity_Switch_PL {M} `{@PL nat nom M} : @PL nat nom (nat -> (bool * M)) :=
    {| B := Polarity_Switch_BPL;
       neg f := fun p => match f p with (true, _) => (false, prop p) | (false, v) => (false, neg v) end;
    |}.

  Definition Polarity_Switch_UML {M} `{@UML op M} : @UML op (nat -> (bool * M)):=
    {| diau mo f := fun p => (false, diau mo (snd (f p)));
       boxu mo f := fun p => (false, boxu mo (snd (f p)));
    |}.
  
  Definition Polarity_Switch_BML {M} `{@BML op opb M} : @BML op opb (nat -> (bool * M)):=
    {| BU := Polarity_Switch_UML;
       diab mo f g := fun p => (false, diab mo (snd (f p)) (snd (g p)));
       boxb mo f g := fun p => (false, boxb mo (snd (f p)) (snd (g p)));
    |}.
  
  Definition Polarity_Switch_PML {M} `{@PML ops M} : @PML ops (nat -> (bool * M)):=
    {| pdia n mo f := fun p => (false, pdia n mo (Vector.map (fun e => snd (e p)) f));
       pbox n mo f := fun p => (false, pbox n mo (Vector.map (fun e => snd (e p)) f));
    |}.


  Definition Polarity_BPL : @BPL nat nom (nat -> polarity) :=
    {| verum := fun _ => p_none;
       falsum := fun _ => p_none;
       prop q := fun p => if p =? q then p_pos else p_none;
       nomi _ := fun _ => p_none;
       conj a b := fun p => mult (a p) (b p);
       disj a b := fun p => mult (a p) (b p);
    |}.

  Definition Polarity_PL : @PL nat nom (nat -> polarity) :=
    {| B := Polarity_BPL;
       neg f := fun p => polarity_inv (f p);
    |}.

  Definition Polarity_UML : @UML op (nat -> polarity) :=
    {| diau mo f := fun p => f p;
       boxu mo f := fun p => f p;
    |}.

  Definition Polarity_BML : @BML op opb (nat -> polarity) :=
    {| BU := Polarity_UML;
       diab mo f g := fun p => mult (f p) (g p);
       boxb mo f g := fun p => mult (f p) (g p);
    |}.
  
  Definition Polarity_PML : @PML ops (nat -> polarity) :=
    {| pdia n mo f := fun p => Vector.fold_right (fun e acc => (e p) <*> acc) f <1>;
       pbox n mo f := fun p => Vector.fold_right (fun e acc => (e p) <*> acc) f <1>;
    |}.
  

  Class Polarity M := { polarity_of : M -> nat -> polarity;
                        pure_in : nat -> M -> bool;
                        strict_pos_in : nat -> M -> bool := fun p f => polarity_of f p p=? p_pos;
                        strict_neg_in : nat -> M -> bool := fun p f => polarity_of f p p=? p_neg;
                        trivial_in : M -> nat -> bool := fun f p => negb (polarity_of f p p=? p_mixed);
                      }.
  Hint Unfold trivial_in pure_in polarity_of strict_pos_in strict_neg_in.
  Class Polarity_Switch M N := { polarity_switch : M -> nat -> N }.

  Definition bmf_polarity_of a : (@bmf op opb nom) -> polarity :=
    fix p f :=
      match f with
      | b_verum => p_none
      | b_falsum => p_none
      | b_prop x => if x =? a then p_pos else p_none
      | b_nom x => p_none
      | b_negprop x => if x =? a then p_neg else p_none
      | b_negnom x => p_none
      | b_conj x x0 => polarity_add (p x) (p x0)
      | b_disj x x0 => polarity_add (p x) (p x0)
      | b_diau x x0 => p x0
      | b_boxu x x0 => p x0
      | b_diab x x0 x1 => polarity_add (p x0) (p x1)
      | b_boxb x x0 x1 => polarity_add (p x0) (p x1)
      end.
  
  Global Instance bmf_Polarity : Polarity (@bmf op opb nom) :=
    {| polarity_of f a := bmf_polarity_of a f;
       pure_in := bmf_pure_in 
    |}.

  (** witnesses that a formula is positive in an atom p *)
  Inductive SPosIn p : (@bmf op opb nom) -> Prop :=
  | sp_verum : SPosIn p b_verum
  | sp_falsum : SPosIn p b_falsum
  | sp_prop : forall x, SPosIn p (b_prop x)
  | sp_nom : forall n, SPosIn p (b_nom n)
  | sp_negprop : forall x, ~ x = p -> SPosIn p (b_negprop x)
  | sp_negnom : forall n, SPosIn p (b_negnom n)
  | sp_conj : forall a b, SPosIn p a -> SPosIn p b -> SPosIn p (b_conj a b)
  | sp_disj : forall a b, SPosIn p a -> SPosIn p b -> SPosIn p (b_disj a b)
  | sp_diau {o} : forall a, SPosIn p a -> SPosIn p (b_diau o a)
  | sp_boxu {o} : forall a, SPosIn p a -> SPosIn p (b_boxu o a)
  | sp_diab {o} : forall a b, SPosIn p a -> SPosIn p b -> SPosIn p (b_diab o a b)
  | sp_boxb {o} : forall a b, SPosIn p a -> SPosIn p b -> SPosIn p (b_boxb o a b)
  .

  (** witnesses that a formula is negative in an atom p *)
  Inductive SNegIn p : (@bmf op opb nom) -> Prop :=
  | sn_verum : SNegIn p b_verum
  | sn_falsum : SNegIn p b_falsum
  | sn_prop : forall x, ~ x = p -> SNegIn p (b_prop x)
  | sn_nom : forall n, SNegIn p (b_nom n)
  | sn_negprop : forall x, SNegIn p (b_negprop x)
  | sn_negnom : forall n, SNegIn p (b_negnom n)
  | sn_conj : forall a b, SNegIn p a -> SNegIn p b -> SNegIn p (b_conj a b)
  | sn_disj : forall a b, SNegIn p a -> SNegIn p b -> SNegIn p (b_disj a b)
  | sn_diau {o} : forall a, SNegIn p a -> SNegIn p (b_diau o a)
  | sn_boxu {o} : forall a, SNegIn p a -> SNegIn p (b_boxu o a)
  | sn_diab {o} : forall a b, SNegIn p a -> SNegIn p b -> SNegIn p (b_diab o a b)
  | sn_boxb {o} : forall a b, SNegIn p a -> SNegIn p b -> SNegIn p (b_boxb o a b)
  .

  Hint Constructors PureIn SPosIn SNegIn. 
  Lemma pure_neg_pos_in : forall p f, PureIn p f <-> SPosIn p f /\ SNegIn p f.
  Proof with solve [auto].
    intros p f.
    induction f; split; introversion; auto.
    inversion H1...
    inversion H0...
    1,3,5,7,9,11: split; constructor; tauto.
    all: inversion H0; inversion H1; constructor; tauto.
  Qed.
    
  Inductive TrivialIn p : (@bmf op opb nom) -> Prop :=
  | t_pure : forall f, PureIn p f -> TrivialIn p f
  | t_pos : forall f, SPosIn p f -> TrivialIn p f
  | t_neg : forall f, SNegIn p f -> TrivialIn p f
  .

  Hint Constructors TrivialIn.

  Lemma f_match_polarity {T U}: forall p a b c d (f : T -> U), f (match p with
                                                            | p_pos => a
                                                            | p_neg => b
                                                            | p_none => c
                                                            | p_mixed => d
                                                            end) = match p with
                                                                   | p_pos => f a
                                                                   | p_neg => f b
                                                                   | p_none => f c
                                                                   | p_mixed => f d
                                                                   end.
    intros p a b c d f. destruct p; auto.
  Qed.  

  Lemma SNegIn_reflect : forall p f, reflect (SNegIn p f) (match bmf_polarity_of p f with p_neg | p_none => true | _ => false end).
    induction f; cbn [bmf_polarity_of polarity_add]; try destruct eqb eqn:He; rewrite ?PeanoNat.Nat.eqb_eq, ?PeanoNat.Nat.eqb_neq in *; subst.
    1-8: constructor; auto; introversion; congruence.
    all: destruct bmf_polarity_of; try destruct bmf_polarity_of.
    all: try inversion IHf1; try inversion IHf2; try inversion IHf; constructor; auto.
    all: introversion; congruence.
  Qed.
  
  Lemma SPosIn_reflect : forall p f, reflect (SPosIn p f) (match bmf_polarity_of p f with p_pos | p_none => true | _ => false end).
    induction f; cbn [bmf_polarity_of polarity_add]; try destruct eqb eqn:He; rewrite ?PeanoNat.Nat.eqb_eq, ?PeanoNat.Nat.eqb_neq in *; subst.
    1-8: constructor; auto; introversion; congruence.
    all: destruct bmf_polarity_of; try destruct bmf_polarity_of.
    all: try inversion IHf1; try inversion IHf2; try inversion IHf; constructor; auto.
    all: introversion; congruence.
  Qed.

  Lemma PureIn_reflect' : forall p f, reflect (PureIn p f) (bmf_polarity_of p f p=? p_none).
    induction f; cbn [bmf_polarity_of]; unfold "p=?".
    1,2,4,6: constructor; solve [auto].
    1,2: destruct (a =? p) eqn:D; constructor; rewrite ?PeanoNat.Nat.eqb_eq, ?PeanoNat.Nat.eqb_neq in *; auto; introversion; congruence.
    all: destruct bmf_polarity_of eqn:D; cbn [peqb] in *.
    9-16: constructor; inversion IHf; (introversion; congruence) || (constructor; auto).
    all: inversion IHf1; destruct (bmf_polarity_of p f2) eqn:D'; cbn [polarity_add]; constructor; inversion IHf2.
    all: introversion || constructor; auto.
  Qed.
  
  Lemma TrivialIn_reflect : forall p f, reflect (TrivialIn p f) (trivial_in f p).
    intros p f.
    destruct trivial_in eqn:D; constructor; cbv [trivial_in negb] in D; match_hyp' H; try congruence; clear D; cbv [polarity_of bmf_Polarity] in H; unfold "p=?" in H.
    - match_hyp' H'; try congruence; clear H; [constructor 2; apply (reflect_iff _ _ (SPosIn_reflect _ _)) | constructor 3; apply (reflect_iff _ _ (SNegIn_reflect _ _))  | constructor 1; apply (reflect_iff _ _ (PureIn_reflect' _ _)) ]; rewrite H'; auto.
    - match_hyp' H'; try congruence; clear H.
      introversion.
      + apply (reflect_iff _ _ (PureIn_reflect' _ _)) in H0. unfold "p=?" in H0; match_hyp; congruence. 
      + apply (reflect_iff _ _ (SPosIn_reflect _ _)) in H0. match_hyp; congruence. 
      + apply (reflect_iff _ _ (SNegIn_reflect _ _)) in H0. match_hyp; congruence. 
  Qed.
  
  Lemma TrivialIn_conj : forall p f1 f2, TrivialIn p (b_conj f1 f2) -> TrivialIn p f1 /\ TrivialIn p f2.
    intros p f1 f2 H.
    inversion H; inversion H0; auto.
  Qed.
  
  Lemma TrivialIn_disj : forall p f1 f2, TrivialIn p (b_disj f1 f2) -> TrivialIn p f1 /\ TrivialIn p f2.
    intros p f1 f2 H.
    inversion H; inversion H0; auto.
  Qed.

  Lemma TrivialIn_diab {o} : forall p f1 f2, TrivialIn p (b_diab o f1 f2) -> TrivialIn p f1 /\ TrivialIn p f2.
    intros p f1 f2 H.
    inversion H; inversion H0; auto.
  Qed.

  Lemma TrivialIn_boxb {o} : forall p f1 f2, TrivialIn p (b_boxb o f1 f2) -> TrivialIn p f1 /\ TrivialIn p f2.
    intros p f1 f2 H.
    inversion H; inversion H0; auto.
  Qed.

                           
  Global Instance Polarity_Switch_uml {M N} `{@UML op N} `{@PL nat nom N} `{Reify _ M _} : Polarity_Switch M N :=
    {| polarity_switch m p := @snd bool _ ((reify (Polarity_Switch_UML, Polarity_Switch_PL)) m p) |}.
  Global Instance Polarity_Switch_bml {M N} `{@BML op opb N} `{@PL nat nom N} `{Reify _ M _} : Polarity_Switch M N :=
    {| polarity_switch m p := @snd bool _ ((reify (Polarity_Switch_BML, Polarity_Switch_PL)) m p) |}.
  Global Instance Polarity_Switch_pml {M N} `{@PML ops N} `{@PL nat nom N} `{Reify _ M _} : Polarity_Switch M N :=
    {| polarity_switch m p := @snd bool _ ((reify (Polarity_Switch_PML, Polarity_Switch_PL)) m p) |}.

  Global Instance Polarity_uml {M} `{Reify _ M _} : Polarity M := {| polarity_of := reify (Polarity_UML, Polarity_PL);
                                                                     pure_in := fun p f => reify (Polarity_UML, Polarity_PL) f p p=? p_none;
                                                                  |}.
  Global Instance Polarity_bml {M} `{Reify _ M _} : Polarity M := {| polarity_of := reify (Polarity_BML, Polarity_PL);
                                                                     pure_in := fun p f => reify (Polarity_BML, Polarity_PL) f p p=? p_none;
                                                                  |}.
  Global Instance Polarity_pml {M} `{Reify _ M _} : Polarity M := {| polarity_of := reify (Polarity_PML, Polarity_PL);
                                                                     pure_in := fun p f => reify (Polarity_PML, Polarity_PL) f p p=? p_none;
                                                                  |}.

  Definition variable_positive {M} `{Polarity M} `{Atoms M} (f : M) := forallb (fun e => polarity_of f e p=? p_pos) (atoms f).
  Definition variable_negative {M} `{Polarity M} `{Atoms M} (f : M) := forallb (fun e => polarity_of f e p=? p_neg) (atoms f).

  Fixpoint bmf_variable_positive (f : @bmf op opb nom) := match f with
                                                          | b_verum => true
                                                          | b_falsum => true
                                                          | b_prop x => true
                                                          | b_nom x => true
                                                          | b_negprop x => false
                                                          | b_negnom x => true
                                                          | b_conj x x0 => bmf_variable_positive x && bmf_variable_positive x0
                                                          | b_disj x x0 => bmf_variable_positive x && bmf_variable_positive x0
                                                          | b_diau x x0 => bmf_variable_positive x0
                                                          | b_boxu x x0 => bmf_variable_positive x0
                                                          | b_diab x x0 x1 => bmf_variable_positive x0 && bmf_variable_positive x1
                                                          | b_boxb x x0 x1 => bmf_variable_positive x0 && bmf_variable_positive x1
                                                          end.
  
  Fixpoint bmf_variable_negative (f : @bmf op opb nom) := match f with
                                                          | b_verum => true
                                                          | b_falsum => true
                                                          | b_prop x => false
                                                          | b_nom x => true
                                                          | b_negprop x => true
                                                          | b_negnom x => true
                                                          | b_conj x x0 => bmf_variable_negative x && bmf_variable_negative x0
                                                          | b_disj x x0 => bmf_variable_negative x && bmf_variable_negative x0
                                                          | b_diau x x0 => bmf_variable_negative x0
                                                          | b_boxu x x0 => bmf_variable_negative x0
                                                          | b_diab x x0 x1 => bmf_variable_negative x0 && bmf_variable_negative x1
                                                          | b_boxb x x0 x1 => bmf_variable_negative x0 && bmf_variable_negative x1
                                                          end.

  
  Inductive VariableNegative : @bmf op opb nom -> Prop :=
  | VN_verum : VariableNegative b_verum
  | VN_falsum : VariableNegative b_falsum
  | VN_nom {x} : VariableNegative (b_nom x)
  | VN_negprop {x} : VariableNegative (b_negprop x)
  | VN_negnom {x} : VariableNegative (b_negnom x)
  | VN_conj a b : VariableNegative a -> VariableNegative b -> VariableNegative (b_conj a b)
  | VN_disj a b : VariableNegative a -> VariableNegative b -> VariableNegative (b_disj a b)
  | VN_diau {o} a : VariableNegative a -> VariableNegative (b_diau o a)
  | VN_boxu {o} a : VariableNegative a -> VariableNegative (b_boxu o a)
  | VN_diab {o} a b : VariableNegative a -> VariableNegative b -> VariableNegative (b_diab o a b)
  | VN_boxb {o} a b : VariableNegative a -> VariableNegative b -> VariableNegative (b_boxb o a b)
  .
  Hint Constructors VariableNegative.

  Lemma VariableNegative_reflect : forall f, reflect (VariableNegative f) (bmf_variable_negative f).
  Proof with rewrite ?andb_true_l, ?andb_true_r, ?andb_false_l, ?andb_false_r; eauto with bool.
    induction f...
    1: cbv; constructor; solve [introversion].
    all: cbn [bmf_variable_negative].
    all: destruct bmf_variable_negative...
    all: try destruct (bmf_variable_negative f2).
    all: constructor; try inversion IHf; try inversion IHf1; try inversion IHf2...
    all: introversion... 
  Qed.
      
  Inductive VariablePositive : @bmf op opb nom -> Prop :=
  | VP_verum : VariablePositive b_verum
  | VP_falsum : VariablePositive b_falsum
  | VP_nom {x} : VariablePositive (b_nom x)
  | VP_prop {x} : VariablePositive (b_prop x)
  | VP_negnom {x} : VariablePositive (b_negnom x)
  | VP_conj a b : VariablePositive a -> VariablePositive b -> VariablePositive (b_conj a b)
  | VP_disj a b : VariablePositive a -> VariablePositive b -> VariablePositive (b_disj a b)
  | VP_diau {o} a : VariablePositive a -> VariablePositive (b_diau o a)
  | VP_boxu {o} a : VariablePositive a -> VariablePositive (b_boxu o a)
  | VP_diab {o} a b : VariablePositive a -> VariablePositive b -> VariablePositive (b_diab o a b)
  | VP_boxb {o} a b : VariablePositive a -> VariablePositive b -> VariablePositive (b_boxb o a b)
  .
  Hint Constructors VariablePositive.

  Lemma VariablePositive_reflect : forall f, reflect (VariablePositive f) (bmf_variable_positive f).
  Proof with rewrite ?andb_true_l, ?andb_true_r, ?andb_false_l, ?andb_false_r; eauto with bool.
    induction f...
    1: cbv; constructor; solve [introversion].
    all: cbn [bmf_variable_positive].
    all: destruct bmf_variable_positive...
    all: try destruct (bmf_variable_positive f2).
    all: constructor; try inversion IHf; try inversion IHf1; try inversion IHf2...
    all: introversion... 
  Qed.
  
  (** Polarity switch for diamond and box arguments *)
  Fixpoint diaarg_pols {n} (p : nat) (f : @diaarg nat n ops) :=
    match f return @diaarg nat n ops with
    | dc_conj x x0 => dc_conj (diaarg_pols p x) (diaarg_pols p x0)
    | dc_prop x => if p =? x then dc_negprop x else dc_prop x
    | dc_negprop x => if p =? x then dc_prop x else dc_negprop x
    | dc_nom _ as i => i
    | dc_negnom _ as i => i
    | dc_verum as i => i
    | dc_falsum as i => i
    | dc_dia x x0 => dc_dia x (fmap (diaarg_pols p) x0)
    | dc_box x x0 => dc_box x (fmap (boxarg_pols p) x0)
    end
  with boxarg_pols {n} p (f : @boxarg nat n ops) :=
         match f return @boxarg nat n ops with
         | bc_disj x x0 => bc_disj (boxarg_pols p x) (boxarg_pols p x0)
         | bc_prop x => if p =? x then bc_negprop x else bc_prop x
         | bc_negprop x => if p =? x then bc_prop x else bc_negprop x
         | bc_nom _ as i => i
         | bc_negnom _ as i => i
         | bc_verum as i => i
         | bc_falsum as i => i
         | bc_dia x x0 => bc_dia x (fmap (diaarg_pols p) x0)
         | bc_box x x0 => bc_box x (fmap (boxarg_pols p) x0)
         end.
  Global Instance diaarg_polarity_switch {n} : Polarity_Switch (diaarg (nom := n)) (diaarg (nom := n)) := {| polarity_switch d p := diaarg_pols p d |}.
  Global Instance boxarg_polarity_switch {n} : Polarity_Switch (boxarg (nom := n)) (boxarg (nom := n)) := {| polarity_switch d p := boxarg_pols p d |}.
End Polarity.