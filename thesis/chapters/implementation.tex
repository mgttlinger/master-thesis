\chap{Implementation}
\label{chap:implementation}

In our Coq formalization we implemented both the original algorithm described in \textcite{conradie2006algorithmicII} and our corrected description of it albeit with the reduced syntax described in \Cref{subsub:red_syn}.
The goal of the implementation was to formalize as much as possible of the hybrid polyadic extension of SQEMA proposed in \textcite{conradie2006algorithmicII}, while still being easy to work with.
This resulted in subsetting the input language of the algorithm to avoid a few issues that would have introduced a lot of complexity into the formalisation.
Let us first discuss a few general design decisions.

\section{Typeclasses}
\label{sec:tc}

Coq has language level support for typeclasses since at least version 8.3 \cite{sozeau2008first}.
Given those were successfully used in other formalizations \cite{james2009reflection, spitters2011type} and given the familiarity with typeclasses as an abstraction from other languages like Haskell, Idris, and Scala, we decided to make use of that feature in our formalization where it fits.

As we initially tried to formalize fully polyadic SQEMA with arbitrary arity modal terms, our formalization made use of both \mintinline{coq}{List} and \mintinline{coq}{Vector.t} from \mintinline{coq}{Vectors.Vector}.
Thus, it made sense to abstract the functorial aspects of both into a typeclass \mintinline{coq}{Functor} like e.g. Haskell does.
Along with that, we also implemented other parts of that common typeclass hierarchy like \mintinline{coq}{Applicative}, \mintinline{coq}{Monad}, \mintinline{coq}{Traversable} \cite{moggi1989abstract, wadler1995monads, mcbride2008applicative}, and other useful category theoretical abstractions we are so used to work with in functional programming.

Given that Coq does not enforce typeclass coherence like e.g. Haskell does, we decided to implement those classes not using the functions they are commonly implemented with, but rather with only the functionality they provide on top of the classes they extend.
For example the \mintinline{coq}{Applicative} definition would normaly also provide a \mintinline{coq}{Functor} instance, given that every applicative functor also is a functor.
Normally those are implemented by the operations \mintinline{coq}{pure {a} : a -> f a} and \mintinline{coq}{ap {a b} : f (a -> b) -> f a -> f b} from which \mintinline{coq}{fmap {a b} : (a -> b) -> f a -> f b} is easily derivable by composition.
We rather used an operation \mintinline{coq}{zip {a b} : f a -> f b -> f (a * b)} and the operation \mintinline{coq}{pure {a} : a -> f a} which it inherits from a typeclass \mintinline{coq}{Point} to implement it.

We quickly realized that using typeclasses as an abstraction in Coq comes at a price because having typeclasses defining functions with signatures being too broadly applicable causes a few rather annoying issues:
\begin{enumerate}
\item They easily cause loops in the typeclass resolution given that their signature might produce a fitting instance for them.
\item They cause terms which do not typecheck to produce errors mentioning those typeclasses without having anything to do with the issue in the code.
\item The ordering of imports suddenly becomes the crucial difference between code typechecking successfully and looping infinitely during compilation.
\end{enumerate}
Using typeclasses in general interferes with the use of tactics like \mintinline{coq}{auto} which do not seem to handle functions defined by typeclasses very well.

A further issue is that using higher order functions defined in typeclasses breaks termination checking even in structurally recursive \mintinline{coq}{Fixpoint}s due to their definitions not getting unfolded.
This issue was recognized in \textcite{spector2017total} and a workaround using \ac{CPS} \cite{pierce2005advanced, harper2016practical} transformed typeclass definitions developed (see Appendix \labelcref{app:cps} for an explanation on what this looks like and why it works). 

While this works to make those functions usable, it also comes at an additional cost, both in code quality and in usability of the formalization.
As far as code quality is concerned, a multiple of the original lines of code are needed to define those typeclasses now, with a lot of boilerplate code needing to be written.
Also, instantiating those typeclasses is no longer as straight forward as before and the intuition of a typeclass being a record defining certain functions is no longer valid.
As far as proving is concerned, we now hide every actual call to one of those typeclass functions behind multiple indirections that need to be unfolded, introducing noise in our proofs.

One would generally want to aid reasoning about those implementations --- as well as ensuring their correctness --- by encoding the laws their instances have to abide within the typeclass as well.
This works perfectly fine with typeclasses as is and can also be required in those records introduced by the \ac{CPS} transformed variant, however, we found no way defining new \ac{CPS} transformed accessors to use those laws in proofs.

All of this lead us to removing much of the typeclass based abstractions from our formalization later on in favour of specialised function implementations with similar names for the datatypes we needed.
This also made improvements in the proofs, not only due to fewer indirections, but also due to knowing exactly what code is used which otherwise might not be clear due to the lack of coherence guarantees.
At places this prevented proving if the proof not only depends on properties enforced in the typeclass itself.

\section{Decidable Equality}
\label{sec:deceq}
Given that e.g. the substitution implementation necessary for the \ar needs to check equality, we needed to decide on a way to treat decidable equality in our formalization.
Coq has multiple mechanisms to work with decidable equality:
\begin{enumerate}
\item Passing around a decision function.
\item Extending one of the modules from \mintinline{coq}{Coq.Structures.DecidableType}.
\item Instantiating the \mintinline{coq}{EqDec} typeclass from \mintinline{coq}{Coq.Classes.EquivDec}.
\end{enumerate}
Different parts of the standard library seem to use different ones of these possibilities, but all the parts relevant to our formalization seemed to just pass around the decision function.

We nonetheless chose to use the \mintinline{coq}{EqDec} typeclass for our own code as it is equally flexible as passing around the function, but without the need to actually pass around the function.
This reduces noise in the code while still allowing to use functions expecting the decision function as argument by filling that with \mintinline{coq}{equiv_dec}.

Now as one might expect, Coq reduces terms like \mintinline{coq}{5 ==b 5} --- or if using the native implementation of decidable equality on natural numbers \mintinline{coq}{5 =? 5} --- to \mintinline{coq}{true} successfully with the standard reduction tactics.
If we have a term like \mintinline{coq}{a ==b a} however, this is not so easy any more.
When \mintinline{coq}{a} is defined to be a certain value e.g. \mintinline{coq}{Definition a := 5}, this gets reduced like normal, but if instead it is \textit{some} unknown natural number, Coq no longer figures out that this is \mintinline{coq}{true} regardless of the actual value of \mintinline{coq}{a}.

Given that \mintinline{coq}{EqDec} requires the underlying relation to be an equivalence relation it is easily proven that \mintinline{coq}{forall a, a ==b a = true}, however we could not get Coq to use that fact in standard reduction tactics.
This is a major inconvenience when proving theorems involving arbitrary formulas, as we do not know the exact values contained.
As a consequence this requires carefully unfolding terms to prevent e.g. an \mintinline{coq}{if a ==b a then X else Y} statement from unnecessarily unfolding both branches where the decision predicate is clear.

\section{Syntax}
\label{sec:impl_syntax}

In our implementation we deviate from the modal syntax used in \textcite{conradie2006algorithmicII} because their setup with having the construct of modal terms would lead to complications in Coq.
As mentioned at the end of \Cref{subsub:red_syn} we only implemented unary and binary modalities.
This change removes a lot of complexity stemming from the need for nested inductive types and complicated dependent types, while having unary and binary modalities already covers many use cases.

Recall our modal syntax over a set \(\mathcal{A}\) of atoms, a set \(\mathcal{N}\) of nominals, a set \(\mathcal{O}_u\) of unary modalities, and a set \(\mathcal{O}_b\) of binary modalities.
\[\phi,\psi ::= \top \mid \bot \mid p \mid \neg p \mid \nom k \mid \neg\nom k \mid \phi \land \psi \mid \phi \lor \psi \mid \Dia{\alpha}{\phi} \mid \MBox{\alpha}{\phi} \mid \Dia{\beta}{(\phi, \psi)} \mid \MBox{\beta}{(\phi, \psi)}\]
where \(p \in \mathcal{A}\), \(\nom k \in \mathcal{N}\), \(\alpha \in \mathcal{O}_u\), \(\beta \in \mathcal{O}_b\).

We fixed the set of atoms to be the natural numbers in our implementation because there is no gain from only having finitely many atom symbols available.
The set of nominals can be an arbitrary \mintinline{coq}{Set}, given that there are cases where no, or only finitely many nominals should be in the language.
The type of modal formulas \mintinline{coq}{bmf} is parametric over all of those sets.
We also decided to implement the syntax directly in \ac{NNF}, given that during the entire run of the SQEMA algorithm the formulas stay in this form.

To enable inversion of modalities we extended those sets freely with all inverses using the types \mintinline{coq}{FreeUInverse} and \mintinline{coq}{FreeBInverse}.

% In Coq this looks like this
% \begin{clisting}
%   \cinput{firstline=8, lastline=24}{../src/BiModal.v}
%   \caption{The type of modal formulas}
% \end{clisting}

\subsection{More than Induction}
\label{subsec:induction}
If we wanted to implement the modal language setup used in \textcite{conradie2006algorithmicII} exactly, we notice that the types of modal formulas, modal terms and their arity function are defined mutually.
We want to write something like the following snippet in Coq.
\begin{minted}[gobble=2]{coq}
  Inductive MT : Set
  with Fixpoint arity (t : MT) : nat
  with Inductive MF : Set.
\end{minted}
But Coq does not allow defining \mintinline{coq}{Inductive} types mutually with functions on those types.
This is called induction-recursion \cite{dybjer2000general, dybjer2001indexed} and while there is ongoing effort to make Coq capable of handling those kinds of type definitions, at the time of writing this was not available in a release of Coq\footnote{\url{https://github.com/mattam82/coq/tree/IR}}.

Given that both \mintinline{coq}{MT} and \mintinline{coq}{nat} are small types, we are dealing with small induction-recursion here which is expressible in Coq \cite{hancock2013small} by indexing the modal terms with their arity.
\begin{minted}[gobble=2]{coq}
  Inductive MT : nat -> Set
  with Inductive MF : Set.
\end{minted}

Recall that there was also the case of literal free formulas being modal terms of arity 0 in the definition of modal terms.
To implement that we would need a predicate telling us that a formula doesn't contain literals.
Given that we need that in the definition of \mintinline{coq}{MT}, we have to define this mutually as well.
\begin{minted}[gobble=2]{coq}
  Inductive MT : nat -> Set
  with Inductive MF : Set
  with Inductive LiteralFree : MF -> Prop.
\end{minted}
This requires a language feature known as induction-induction \cite{forsberg2010inductive} which is equally unsupported in Coq.
It is also a feature being worked on together with induction-recursion support.

As we need a special case of induction-induction where the predicate we are trying to define is decidable based on the contained subformulas, we can use a similar transfomation as with the small induction-recursion and index our type over this predicate.
\begin{minted}[gobble=2]{coq}
  Inductive MT : nat -> Set
  with Inductive MF : bool -> Set.
\end{minted}

While this makes everything expressible, it introduces huge amounts of complexity, both in implementing functions on these types, as well as proving theorems about those.
For example every function that does not care about the purity of its arguments now has to take and produce sigma types hiding those boolean type indices.
Also, the generated induction principle is useless if it was not already so due to the nested inductive vector for applying arguments to arbitrary arity modalities.
We can generate a new working induction principle automatically using the \mintinline{coq}{Scheme Induction} vernacular command but only if we do not have nested inductive types.
Thus, we would have to mutually define our own vector implementation as well, but the generated schemes will be very complicated to use due to multiple mutually defined dependent types \cite{chlipala2011certified}.


\subsection{Mutual Induction}
\label{subsec:mutual}
Having mutually defined types in the language also has the consequence that proving facts about those types often also requires mutually defined lemmas or mutually defined fixpoints.
In and of itself this does not sound like a major inconvenience, but given the way mutually defined proofs in Coq work, this severely limits the automation capabilities.
When doing mutual proofs in Coq there are hypotheses in context exactly matching the goal of both proofs which \mintinline{coq}{auto} and similar tactics will use to solve the goal immediately.
Only when reaching the final check caused by \mintinline{coq}{Qed}, Coq checks if the proof is circular and produces an error if those hypotheses were used on too large goals.

\subsection{Usability Improvements}
\label{subsec:usa}

\paragraph{Syntactic sugar}
To provide convenience to the user we implemented convenience functions like \mintinline{coq}{b_neg} so that formulas do not have to be stated in \ac{NNF} directly.

% \begin{clisting}
%   \cinput{firstline=135, lastline=148, gobble=2}{../src/BiModal.v}
%   \caption{Negation on modal formulas}
% \end{clisting}

For having an extensible way to write formulas of different logics in a unified way we implemented a typeclass based syntax for writing formulas.

\begin{clisting}
  \cinput{firstline=8, lastline=15, gobble=2}{../src/ML.v}
  \cinput{firstline=34, lastline=37, gobble=2}{../src/ML.v}
  \cinput{firstline=56, lastline=59, gobble=2}{../src/ML.v}
  \cinput{firstline=72, lastline=76, gobble=2}{../src/ML.v}
  \caption{Abstract syntax}
\end{clisting}

% Here it is visible how we got around needing inverse modalities when we did not choose to use this modal term algebra.
% We used typeclasses \mintinline{coq}{Class UInverse} and \mintinline{coq}{Class BInverse} to require the types of unary and binry modal terms to be closed under inverses.
% This allows us to only provide inverse modalities when the modal terms are capable of doing that and those typeclasses are instantiated.

This abstract syntax also allows us to define abstract algorithm implementations which then work for all implementing types, similar to what is described in \textcite{carette2009finally, oliveira2012extensibility}.
Such abstract implementations are however, limited to catamorphisms which, while being expressive enough given that we have products over all types \cite{hutton1999tutorial}, makes implementations, needing access to multiple levels of the \ac{AST}, overly complicated to implement and prove.
We did only make use of this in some places and also rarely used this abstract syntax in the formalization itself due to the issues we discussed in \Cref{sec:tc}.
It however works very well for having a nice syntax to write formulas with.

\paragraph{Pretty printing}
To make the output of Coq more readable, both for console outputs as well as interactive proving, we added printing notations.
\begin{clisting}
  \cinput{firstline=422, lastline=428}{../src/BiModal.v}
  \caption{Pretty printing notations}
\end{clisting}
For doing trivial simplifications on the results or inputs of the algorithm we implemented a simplification procedure \mintinline{coq}{bmf_simplify}.
It does simplification using the propositional equivalences
\[\phi \land \bot \equiv \bot \qquad \phi \lor \top \equiv \top \qquad p \lor \neg p \equiv \top \qquad p \land \neg p \equiv \bot\]
and their commutative variants to make the result of SQEMA easier to grasp or preprocess the input of the algorithm beforehand.

% \begin{clisting}
%   \cinput{firstline=9, lastline=40, gobble=2}{../src/Simplify.v}
%   \mint{coq}{      (** left out for brevity *)}\vspace{-2em}
%   \cinput{firstline=62, lastline=66, gobble=2}{../src/Simplify.v}
%   \caption{Simple propositional simplification}
% \end{clisting}

\section{Semantics}
\label{sec:code_sem}
For formalizing semantics we first need the notion of a frame to interpret our formulas on.
It needs to have an interpretation for our modal terms which we extend with the inverses.
\begin{clisting}
  \cinput{firstline=40, lastline=50, gobble=2}{../src/Model.v}
  \caption{Type of Kripke frames with interpretation for modal terms}
\end{clisting}
Next we need valuation and nominal valuation which we bundle together in one record for convenience.
\begin{clisting}
  \cinput{firstline=58, lastline=61, gobble=2}{../src/Model.v}
  \caption{Type of valuations}
\end{clisting}

We put a \mintinline{coq}{fr : bframe} and \mintinline{coq}{val : valuation} together to define pointed model satisfaction of modal formulas.
\begin{clisting}
  \cinput{firstline=51, lastline=65, gobble=2}{../src/Satisfaction.v}
  \caption{Satisfaction of modal formulas}
\end{clisting}

\section{SQEMA}
\label{sec:code_sqema}

Our SQEMA implementation follows the three phase description of the algorithm closely.
\begin{clisting}
  \cinput{firstline=175, lastline=175, gobble=2}{../src/SQEMA.v}
  \caption{The algorithm SQEMA without standard translation}
\end{clisting}

Here \mintinline{coq}{failure} is a simple monad capturing the effect of failure (actually it is a bimonad in the sense of \textcite{mesablishvili2011bimonads}) resulting in a formula of type \mintinline{coq}{bmf_plus} in both cases (which is just \mintinline{coq}{bmf op opb (nom + nat)}) because the formula type gets extended with an unlimited supply of fresh nominals during the execution of the algorithm.
It is also visible here that we did not implement translating to \ac{FOL} in the end as it is already clear that a pure modal formula will result in a \ac{FOL} formula when translated.

To avoid the issues coming with adding the global diamond modality to our modal syntax, we used quantification over worlds in the meta language in our proven equivalence.
We also implemented our corrected version of SQEMA which is identical apart from not joining the systems together in the end.
This missing joining together was also captured using \mintinline{coq}{Forall} of the meta language to capture the semantics of arbitrary length conjunction in the equivalence.
The lack of joining now requires traversing the resulting list to run the overall purity check, which is implemented using the \mintinline{coq}{traverse} operation of the traversable functor \mintinline{coq}{list} and the applicative functor \mintinline{coq}{failure} \cite{mcbride2008applicative}.
\begin{clisting}
  \cinput{firstline=177, lastline=177, gobble=2}{../src/SQEMA.v}
  \caption{Corrected version of SQEMA}
\end{clisting}

\subsection{Preprocessing}
\label{subsec:impl_pre}

The algorithm starts with the preprocessing phase which extends the set of nominals to \mintinline{coq}{nom + nat}, negates, and does the normalization described in \Cref{subsec:sqema}, producing the list of disjuncts.
\begin{clisting}
  \cinput{firstline=168, lastline=169, gobble=2}{../src/SQEMA.v}
  \caption{The preprocessing phase}
\end{clisting}

\subsection{Elimination}
\label{subsec:impl_eli}

Then the initial systems are formed by prefixing with \(\neg\nom i\), returning a single equation for which we defined the type \mintinline{coq}{eqn} with disjunctive semantics.
\begin{clisting}
  \cinput{firstline=11, lastline=14, gobble=2}{../src/Equation.v}
  \caption{The type of equations}
\end{clisting}

Afterwards, every trivially occurring atom is replaced by \(\top\) or \(\bot\) as described in \Cref{subsec:sqema}.
We then proceed to eliminate all remaining atoms using \mintinline{coq}{elim_all} inside of \mintinline{coq}{eliminate_all} which takes care of returning the \textit{best} result from all elimination orders.
The \textit{best} result here is the system which has the least atoms remaining after completing the elimination (if it has failed). 
\begin{clisting}
  \cinput{firstline=50, lastline=53, gobble=2}{../src/SQEMA.v}
  \caption{Trying all elimination orders}
\end{clisting}

The function \mintinline{coq}{eliminate_inorder} is defined in the typeclass \mintinline{coq}{Class Eliminate} and folds \mintinline{coq}{eliminate : system -> atom -> failure system} over the initial system in an effort to eliminate all atom occurrences.
\begin{clisting}
  \cinput{firstline=157, lastline=163, gobble=2}{../src/SQEMA.v}
  \caption{Elimination of an atom}
\end{clisting}
Here the function \mintinline{coq}{split_system} splits the system into those equations that already have one of the two forms the \ar requires (here called \(\alpha\) and \(\beta\) form as the first SQEMA paper named their formulas this way \cite{conradie2006algorithmic}) and the ones that still need to be transformed.

Then we use \mintinline{coq}{recurse'} on those, which is just a glorified fold, handling the monad stack to accumulate the results and passing on the context for use with \mintinline{coq}{rule_transform}.
\begin{clisting}
  \cinput{firstline=83, lastline=88, gobble=2}{../src/SQEMA.v}
  \caption{Folding with state, failure and list}
\end{clisting}

\subsubsection{Applying the rules}
\label{subsub:impl_rules}

The core of the algorithm implementation is the function \mintinline{coq}{rule_transform} which recursively applies the SQEMA rules to solve for a given atom.
\begin{clisting}
  \cinput{firstline=104, lastline=141, gobble=2}{../src/SQEMA.v}
  \caption{Solving for a variable}
\end{clisting}
We used \mintinline{coq}{Program Fixpoint} here because the function is not structurally recursive and \mintinline{coq}{Program} provides the nice syntax for using a \mintinline{coq}{measure} function to ensure termination.
Apart from that, \mintinline{coq}{Program} provides other syntactic sugar to make the pattern match as clean as it looks.
Had we used the old approach with well-founded recursion and \mintinline{coq}{refine} here, we would have had to apply the convoy pattern (see Appenix \labelcref{app:convoy}) everywhere in the match by hand, given that termination depends on the case that matched.

The parameter \mintinline{coq}{free_nominals : Stream nat} carries our context with an unlimited supply of nominals which are fresh in the system containing our equation.
It is necessary to modify and pass this context in subsequent recursive calls advancing the stream with the function \mintinline{coq}{advance : nat -> Stream nat -> Stream nat} to ensure overall freshness of the contained nominals.
This introduces complexity by requiring the use of a state monad to keep track of which nominals are still available, which might change in recursive calls and therefore forces us to sequence those calls.
While not posing so much of an issue here in our reduced syntax where we have at most two recursive calls, when implementing arbitrary arity modalities this requires complicated higher order functions.
We chose to use the \mintinline{coq}{Env} monad and the \mintinline{coq}{EnvT} monad transformer for syntactic convenience when working with that state.

As a termination measure, we use a function \mintinline{coq}{complexity : atom -> eqn -> nat} which is nearly structural complexity of the passed formula but with the difference that formulas pure in the atom \(p\) to be solved for, have complexity \(0\) so that pulling out occurrences of \(p\) from larger formulas drastically reduces complexity.
This measure together with the fact that we only reduce formulas which are not pure in \(p\) leads to guaranteed termination which is even automatically provable with a modified obligation tactic using a few custom tactics together with \mintinline{coq}{congruence} and the \mintinline{coq}{omega} solver for the \mintinline{coq}{nat} reasoning.

After solving the system, the algorithm proceeds to do the elimination using the function \mintinline{coq}{ackermann_rule} which does the substitution for the negated atom.

\subsection{Higher Order Functions}
\label{subsec:hof}
Higher order functions like \mintinline{coq}{map}, \mintinline{coq}{flat_map} or \mintinline{coq}{traverse} are rather common in functional programming.
When using those in recursive function definitions, it is necessary to either unfold those calls when termination is checked or explicitly pass the termination proof to subsequent calls.
When the termination of \mintinline{coq}{Fixpoint} and \mintinline{coq}{Program Fixpoint} is checked, higher order functions are unfolded, but as soon as a \mintinline{coq}{measure} function is used this is no longer the case.

In that case we need to explicitly tailor the higher order function for the specific use-case to ensure the passed function is only called on arguments with decreasing measure value.
When implementing fully polyadic SQEMA, the \mintinline{coq}{rule_transform} function needs to recurse on an arbitrary number of equations after applying the diamond rule.
Then those recursive calls cannot simply be inlined in let bindings to pass their resulting contexts into the next call, but rather have to be writen using a custom higher order function similar to \mintinline{coq}{recurse'} that takes care of those subsequent calls and manages the context.

This then further increases the complexity of that function and also makes it harder to strip of the parts of the body which are only necessary for termination checking.
It is beneficial to strip those parts of using an \textit{unfold lemma} which establishes the equality between a call to that function and its body without proofs in there.
Doing this greatly reduces term sizes, which especially when dealing with terms produces by \mintinline{coq}{Program}, hugely improves usability and performance when proving.

\subsection{Finite Memory}
\label{subsec:memory}
We have already talked about the convoy pattern and changes \mintinline{coq}{Program} does causing term blow up.
If that is combined with pattern matches on inductive types with many cases like our \mintinline{coq}{bmf} type, the terms get really large.
If we look at the definition of \mintinline{coq}{rule_transform}, it matches on an equation consisting of two formulas, each having twelve constructors.
Even though most of those cases get handled by the wildcard cases, the resulting term will contain all of them, so we have a 144 case dependent pattern match to deal with.
We also match on two boolean predicates, so we are up to 576 cases already.

This might still seem workable, but unfolding this term to a point where properties can be proved about it blows up the RAM usage of the \mintinline{coq}{coqtop} process beyond what our machine with its 8GiB of physical memory plus 16GiB of swap has to offer and the Linux kernel kills the process.
Even for terms not as extreme as \mintinline{coq}{rule_transform}, unfolding has to happen in a carefully controlled fashion to avoid unfolding multiple pattern matches on \mintinline{coq}{bmf}.
Hence, the simplification tactics like \mintinline{coq}{simpl} or \mintinline{coq}{cbv} cannot simply be used any more.
Especially in conjunction with Coq not figuring out that decidable equality is reflexive as we discuss in \Cref{sec:deceq}, those tactics quickly blow up terms to where they become a problem.
Even if terms do not get large enough to kill the process, they at least slow down interactive proving and compilation times to the point where stepping one tactic in the proof in either direction results in multiple minutes of waiting for the process  on my machine.


\subsection{Postprocessing}
\label{subsec:impl_post}

Now the only missing piece of the algorithm implementation is the postprocessing phase, implemented in the function \mintinline{coq}{postprocess} or \mintinline{coq}{postprocess'}.
\begin{clisting}
  \cinput{firstline=170, lastline=172, gobble=2}{../src/SQEMA.v}
  \caption{The postprocessing phase}
\end{clisting}
Here, \mintinline{coq}{postprocess} is for the original algorithm description and \mintinline{coq}{postprocess'} our modified one.
The function \mintinline{coq}{b_bigconj : list bmf -> bmf} is just an arbitrary length conjunction or \(\top\) in the case the passed list is empty.

\section{Verification}
\label{sec:veri}

Verification suffered from the complexity of \mintinline{coq}{rule_transform} because we needed a lemma which unfolds that function, stripping all the modifications \mintinline{coq}{Program} does and the termination proofs to reduce the term size keeping memory usage low.

The memory constraints discussed in \Cref{subsec:memory} prevented us from proving this unfold lemma of the \mintinline{coq}{rule_transform} function and we had to assume it.
We are however as certain as we could be, that it is correct, given that the right-hand side of the equality is just a copy of the body in the function implementation due to its purpose being to strip of the changes done by \mintinline{coq}{Program}.
Also keep in mind that not using \mintinline{coq}{Program} here would have caused the same issues as the problematic modifications are necessary for the termination proofs in well-founded recursion.

We verified correctness of our alternative implementation \mintinline{coq}{SQEMA'}.
\begin{clisting}
  \cinput{firstline=2309, lastline=2320, gobble=2}{../src/SQEMACorrect.v}
  \caption{Correctness of \mintinline{coq}{SQEMA'}}
\end{clisting}
Here the \mintinline{coq}{Forall} captures joining the systems together in the end and the \mintinline{coq}{exists v} is capturing the semantics of the global diamond modality which we used in the algorithm description.
The \mintinline{coq}{forall V, V.(valN) nom_i = w -> (* ... *)} part is capturing the notion of \([\nom i := w]\)-validity, given that we chose to use \mintinline{coq}{nom_i : nom + nat} as the special \(\nom i\).

We initially tried to verify the original implementation \mintinline{coq}{SQEMA} but ran into the issue, that joining the systems together into one formula requires all the worlds where their resulting formulas holds to coincide.
This is not always the case as we will discuss further in \Cref{sec:error}.

\subsection{Reflection}
\label{subsec:reflection}

The implementation uses \mintinline{coq}{filter} and predicate functions like \mintinline{coq}{bmf_pure_in} or\linebreak \mintinline{coq}{trivial_in} in many places.
To reason about those is rather tedious even with a custom tactic \mintinline{coq}{match_hyp} we wrote to automate destructing matches in hypotheses and many of the resulting subproofs being dispatchable by following with \mintinline{coq}{try congruence}.

To improve the automation capabilities of our proofs and make reasoning about those predicate functions easier in general, we used a technique called \textit{reflection} to reflect them into actual predicates of type \mintinline{coq}{bmf -> Prop}.
Reflection in a nutshell works by having a predicate function \mintinline{coq}{f : X -> bool} which decides a corresponding \mintinline{coq}{P : X -> Prop} and proving \mintinline{coq}{forall x, reflect (P x) (f x)} from the Coq standard library.
Then there are the two lemmas \mintinline{coq}{reflect_iff} and \mintinline{coq}{reflect_not_iff} to move back and forth between the boolean valued statement and the predicate or negated predicate.

This allows to use the predicate function for automatically computing proofs for the inductive predicate and in the other direction allows us to reason about the predicate function more conveniently e.g. with \mintinline{coq}{inversion}.

We also found the tactics \mintinline{coq}{notHyp} and \mintinline{coq}{extend}, defined by \citeauthor{chlipala2017formal} for his latest book \cite{chlipala2017formal}, to be really useful in automating those proofs for reasoning e.g. about purity for which we developed the tactic \mintinline{coq}{auto_purity} automating contradicting false hypotheses about purity or proving that things are pure by using reflection.
% \begin{clisting}
%   \cinput{firstline=2519, lastline=2552, gobble=2}{../src/SQEMAFacts.v}
%   \caption{Automated reasoning about purity}
% \end{clisting}

\subsection{Program Fixpoint for Well-Founded Induction}
\label{subsec:wfi}
When reasoning about functions defined by well-founded recursion, \textit{normal} induction using the generated induction schemes does not work any more.
The way to deal with this is to either write custom induction schemes that provide a hypothesis for every term smaller with respect to the measure function used or use \mintinline{coq}{well_founded_ind} from \mintinline{coq}{Coq.Init.Wf} if \mintinline{coq}{Fix} was used directly in the implementation.

Another great alternative recommended by Joachim Breitner on the coq-club mailing list\footnote{\url{https://sympa.inria.fr/sympa/arc/coq-club/2018-03/msg00016.html}} is to abuse \mintinline{coq}{Program Fixpoint} with the same \mintinline{coq}{measure} function used in the function to be reasoned about, to handle the well foundedness part and leave the proof as obligation.
This way the well foundedness part of the proof can be dealt with by \mintinline{coq}{Program}.
The context then will contain a hypothesis for all terms with smaller measure as expected with well founded induction.