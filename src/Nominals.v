From SQEMA Require Import Algebra VectorUtils ML Reify Polyadic BiModal.

Require Import Lists.List.
Require Vectors.Vector.

Import ListNotations.

Section Nominals.
  Context {op opb atom nom : Set} {ops : nat -> Set} `{PInverse ops}.

  Definition Nominals_BPL : @BPL atom nom (list nom) :=
    {| verum := [];
       falsum := [];
       prop _ := [];
       nomi n := [n];
       conj a b := a ++ b;
       disj a b := a ++ b;
    |}.

  Definition Nominals_PL : @PL atom nom (list nom) := {| B := Nominals_BPL;
                                                         neg a := a
                                                      |}.

  Definition Nominals_UML : @UML op (list nom) := {| diau _ a := a;
                                                     boxu _ a := a
                                                  |}.

  Definition Nominals_BML : @BML op opb (list nom) := {| BU := Nominals_UML;
                                                         diab _ a b := a ++ b;
                                                         boxb _ a b := a ++ b
                                                      |}.
  
  Definition Nominals_PML : @PML ops (list nom) := {| pdia _ _ args := Vector.fold_right (fun a b => a ++ b) args [];
                                                      pbox _ _ args := Vector.fold_right (fun a b => a ++ b) args [];
                                                   |}.

  Class Nominals M := { nominals : M -> list nom }.
  Global Instance Nominals_uml {M} `{Reify _ M _} : Nominals M := {| nominals := reify (Nominals_UML, Nominals_PL) |}.
  Global Instance Nominals_bml {M} `{Reify _ M _} : Nominals M := {| nominals := reify (Nominals_BML, Nominals_PL) |}.
  Global Instance Nominals_pml {M} `{Reify _ M _} : Nominals M := {| nominals := reify (Nominals_PML, Nominals_PL) |}.
  Global Instance Nominals_bmf : Nominals (@bmf op opb nom) := {| nominals := bmf_nominals |}.
  (* (** Nominals for diamond and box arguments *) *)
  (* Fixpoint diaarg_n {p} (f : @diaarg p nom ops) := match f with *)
  (*                                                  | dc_conj x x0 => diaarg_n x <+> diaarg_n x0 *)
  (*                                                  | dc_prop x => [] *)
  (*                                                  | dc_negprop x => [] *)
  (*                                                  | dc_nom x => [x] *)
  (*                                                  | dc_negnom x => [x] *)
  (*                                                  | dc_verum => [] *)
  (*                                                  | dc_falsum => [] *)
  (*                                                  | dc_dia x x0 => VectorUtils.fold_map diaarg_n x0 *)
  (*                                                  | dc_box x x0 => VectorUtils.fold_map boxarg_n x0 *)
  (*                                                  end *)
  (* with boxarg_n {p} f := match f with *)
  (*                        | bc_disj x x0 => boxarg_n x <+> boxarg_n x0 *)
  (*                        | bc_prop x => [] *)
  (*                        | bc_negprop x => [] *)
  (*                        | bc_nom x => [x] *)
  (*                        | bc_negnom x => [x] *)
  (*                        | bc_verum => [] *)
  (*                        | bc_falsum => [] *)
  (*                        | bc_dia x x0 => VectorUtils.fold_map diaarg_n x0 *)
  (*                        | bc_box x x0 => VectorUtils.fold_map boxarg_n x0 *)
  (*                        end. *)

  (* Global Instance diaarg_nominals {p} : Nominals (diaarg (pro := p)) := {| nominals := diaarg_n |}. *)
  (* Global Instance boxarg_nominals {p} : Nominals (boxarg (pro := p)) := {| nominals := boxarg_n |}. *)
  
  (* Global Instance d_clause_nominals {p} : Nominals (d_clause (pro := p)) := *)
  (*   {| nominals := *)
  (*        fix g d := match d with *)
  (*                   | d_conj x x0 => g x <+> g x0 *)
  (*                   | d_prop x => [] *)
  (*                   | d_negprop x => [] *)
  (*                   | d_nom x => [x] *)
  (*                   | d_negnom x => [x] *)
  (*                   | d_verum => [] *)
  (*                   | d_falsum => [] *)
  (*                   | d_dia x x0 => VectorUtils.fold_map nominals x0 *)
  (*                   | d_box x x0 => VectorUtils.fold_map nominals x0 *)
  (*                   end *)
  (*   |}.   *)
End Nominals.
  