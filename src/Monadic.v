From SQEMA Require Import Utils ListUtils Algebra ML Reify Atoms Substitution Satisfaction.
Require Import Lists.List.
Require Import RelationClasses.
Require Export Classes.EquivDec.

Import ListNotations.

Section monadic.
  Context {op : Set} `{EqDec op eq}. (** Set of unary modalities *)
  Context {atom nom : Set} `{EqDec atom eq} `{EqDec nom eq}. (** Atoms and nominals *)

  (** ADT of modal formulas *)
  Inductive ml : Set :=
  | ml_verum : ml
  | ml_prop : atom -> ml
  | ml_nom : nom -> ml
  | ml_neg : ml -> ml
  | ml_conj : ml -> ml -> ml
  | ml_dia : op -> ml -> ml.

  Program Instance ml_EqDec : EqDec ml eq.
  Next Obligation. assert ({x = y} + {x <> y}) as Heq by (decide equality; deceq). destruct Heq; [left | right]; auto. Defined.
  
                     
  (** Reflection from abstract modal terms into our ADT *)
  Global Instance ml_BPL : BPL ml := {| verum := ml_verum;
                                        prop := ml_prop;
                                        nomi := ml_nom;
                                        conj := ml_conj;
                                        falsum := ml_neg ml_verum;
                                        disj a b := ml_neg (ml_conj (ml_neg a) (ml_neg b));
                                     |}.
  Global Instance ml_PL : PL ml := {| neg := ml_neg |}.

  Global Instance ml_ML : UML ml := {| diau := ml_dia;
                                       boxu mo f := ml_neg (ml_dia mo (ml_neg f));
                                    |}.

  (** Conversion from formula ADT to acstract modal terms *)
  Global Instance ml_Reify {O} : Reify (@UML op O * @PL atom nom O) ml O :=
    {| reify inst := match inst with (Ha, Hb) => fix re f :=
                                       match f with
                                       | ml_verum => verum
                                       | ml_prop p => prop p
                                       | ml_nom n => nomi n
                                       | ml_neg f => neg (re f)
                                       | ml_conj a b => conj (re a) (re b)
                                       | ml_dia mo f => diau mo (re f)
                                       end
                     end
    |}.

  (** Intermediate formula type for use in SQEMA (with the inverse diamond) *)
  Inductive ml' : Set :=
  | ml'_verum : ml'
  | ml'_prop : atom -> ml'
  | ml'_nom : nom -> ml'
  | ml'_neg : ml' -> ml'
  | ml'_conj : ml' -> ml' -> ml'
  | ml'_dia : op -> ml' -> ml'
  | ml'_diai : op -> ml' -> ml'.

  Global Program Instance ml'_EqDec : EqDec ml' eq.
  Next Obligation. assert ({x = y}+{x <> y}) as Heq by (decide equality; deceq). destruct Heq; [left | right]; auto. Defined.

  (** Reflection from abstract modal terms to extended formulas *)
  Global Instance ml'_BPL : BPL ml' := {| verum := ml'_verum;
                                          prop := ml'_prop;
                                          nomi := ml'_nom;
                                          conj := ml'_conj;
                                          falsum := ml'_neg ml'_verum;
                                          disj a b := ml'_neg (ml'_conj (ml'_neg a) (ml'_neg b));
                                       |}.
  Global Instance ml'_PL : PL ml' := {| neg := ml'_neg |}.
  
  Global Instance ml'_UML : UML ml' := {| diau := ml'_dia;
                                          boxu mo f := neg (ml'_dia mo (ml'_neg f));
                                       |}.
                                       
  (** Reflection from inverse diamonds to extended formulas *)
  Global Instance ml'_UML' : UML' ml' := {| diaui := ml'_diai;
                                            boxui mo f := neg (ml'_diai mo (neg f))
                                         |}.

  (** Conversion from extended formulas to abstract modal terms with inverse diamonds *)
  Global Instance ml'_Reify {O} : Reify (@UML' op O * @PL atom nom O) ml' O :=
    {| reify inst := match inst with (Hi1, Hi3) => fix re f := match f with
                                                              | ml'_verum => verum
                                                              | ml'_prop x => prop x
                                                              | ml'_nom x => nomi x
                                                              | ml'_neg x => neg (re x)
                                                              | ml'_conj x x0 => conj (re x) (re x0)
                                                              | ml'_dia x x0 => diau x (re x0)
                                                              | ml'_diai x x0 => diaui x (re x0)
                                                              end
                     end
    |}.

  (** Embedding of formulas into extended formulas *)
  Definition ml_ml' (f : ml) : ml' := reify (ml'_UML, ml'_PL) f.

  Coercion ml_ml' : ml >-> ml'.
  Coercion ml_prop : atom >-> ml.
  Coercion ml_nom : nom >-> ml.

  Hint Constructors ml ml'.

  (* Lemma satisfaction_matches : forall (f : ml) rel valP valN w, satisfaction rel valP valN f w <-> satisfaction' rel valP valN (f : ml') w. *)
  (* Proof with intuition. *)
  (*   induction f; simpl... *)
  (*   - destruct (not_iff_compat (IHf w))... *)
  (*   - destruct (not_iff_compat (IHf w))... *)
  (*   - destruct (IHf1 w)... *)
  (*   - destruct (IHf2 w)... *)
  (*   - destruct (IHf1 w)... *)
  (*   - destruct (IHf2 w)... *)
  (*   - destruct H3 as [e He]. exists e. destruct (IHf e)... *)
  (*   - destruct H3 as [e He]. exists e. destruct (IHf e)... *)
  (* Qed. *)

  Lemma substitute_removes (p : atom) (A : ml') (Hpa : ~ In p (atoms A)) : forall B, ~ In p (atoms (B [A \ p])).
  Proof with intuition.
    induction B...
    - simpl in *. destruct (p == a)... inversion H2...
    - simpl in H2. rewrite in_app_iff in H2. destruct H2...
  Qed.
  

End monadic.


 
