From SQEMA Require Export Utils CT Env ML OptionUtils SumUtils Atoms.

Require Import Lists.List.

Section SQEMA_Classes.
  Definition Stream t := nat -> t.
  Definition advance {A} n (f : Stream A) : Stream A := fun n' => f (n + n').
  
  Class ToFormula t u {a n} := { to_formula : t -> u;
                                 tf_pl :> @PL a n u;
                               }.
  (** sum for signaling failure to eliminate (as right) but continuing anyway to eliminate as much as possible *)
  Inductive failure t :=
  | fl : t -> failure t
  | sc : t -> failure t.
  Notation "fail:" := (fl _).
  Notation "succ:" := (sc _).

  Global Instance failure_Functor : @Functor failure := fun r f => f {| fmap__ A B f fa := match fa with
                                                                                        | succ: x => succ: (f x)
                                                                                        | fail: x => fail: (f x)
                                                                                        end
                                                                  |}.
  Global Instance failure_CoPure : @CoPure failure := fun r f => f {| copure__ A fa := match fa with
                                                                                    | fail: x => x
                                                                                    | succ: x => x
                                                                                    end
                                                                |}.
  Global Instance failure_Monad : @Monad failure := fun r f => f {| join__ A ffa := match ffa with
                                                                                 | succ: x => x
                                                                                 | fail: x => fail: (copure x)
                                                                                 end
                                                              |}. 
  Global Instance failure_Pure : @Pure failure := fun r f => f {| pure__ A := succ: |}.
  Global Instance failure_Applicative : @Applicative failure := fun r f => f {| zip__ A B fa (fb : failure B) := match fa, fb with
                                                                                                | fail: x, o => fail: (x, copure o : B)
                                                                                                | succ: x, succ: y => succ: (x, y)
                                                                                                | o, fail: y => fail: (copure o, y)
                                                                                                end
                                                                          |}.
  
  Program Definition inl' {A} {P : A -> Prop} : {a : A | P a} -> {fa : failure A | P (copure fa)} := fun a' => succ: (proj1_sig a').
  
  Definition succ_map {A} fa (f : A -> A) := match fa with
                                            | fail: x => fail: x
                                            | succ: x => succ: (f x)
                                            end.  
  
  Definition fcbind {A B C} (f : A -> Env C (failure B)) fa : Env C (failure B) := match fa with
                                                                                  | succ: x => f x 
                                                                                  | fail: x =>
                                                                                    fmap (fun e => fail: (copure e)) (f x)
                                                                                  end.
  
  Program Definition fcbind' {A B C} {P : A -> Prop} (f : forall (a:A), P a -> Env C (failure B)) (fa : {f : failure A | P (copure f)}) : Env C (failure B) :=
    match proj1_sig fa with
    | succ: x => f x (proj2_sig fa)
    | fail: x => fmap (fun fb => fail: (copure fb)) (f x (proj2_sig fa))                      
    end.
  
  Definition fcapp {A C} cfla fta : Env C (failure (list A)) :=
    fmap (fun fla => bind (fun x => fmap (fun y => x ++ y) fta) fla) cfla.

  Lemma match_match_failure {T U} : forall (a : failure T) (f g : T -> failure U), (match match a with succ: x => succ: (f x) | fail: x => fail: (g x) end with fail: x => fail: (copure x) | succ: x => x end) = match a with succ: x => f x | fail: x => fail: (copure (g x)) end. intros. destruct a; auto. Qed.

  Lemma f_match_failure {T U V} : forall (a : failure T) (f : U -> V) (g h : T -> U), f (match a with fail: x => g x | succ: x => h x end) = match a with fail: x => f (g x) | succ: x => f (h x) end. intros; destruct a; auto. Qed.
  
  Class Eliminate s :=
    { eliminate : s -> nat -> failure s;
      eliminate_inorder : s -> list nat -> failure s := fun sy => fold_right (fun p os => bind (F := failure) (fun sv => (eliminate sv p)) os) (succ: sy);
    }.
  
  Class Complexity t := { complexity : nat -> t -> nat }.
End SQEMA_Classes.
Notation "fail:" := (fl _).
Notation "succ:" := (sc _).
