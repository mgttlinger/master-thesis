Require Import SQEMA.CT.

Class Reify I T O := { reify : I -> T -> O }.
Global Program Instance reify_functor {I T} : Functor (F := Reify I T) := fun r f => f {| fmap__ A _ ab fa := 
                                                                                         let _ : Reify I T _ := fa in 
                                                                                         {| reify i t := ab (reify i t) |};
                                                                                    |}.

Global Program Instance reify_contra {I O} : Contra (F := fun t => Reify I t O) :=
  fun r f => f {| contramap__ A B ba fa := 
                 let _ : Reify I B O := fa in 
                 {| reify i t := (reify i (ba t)) |};
            |}.

Class DReify C T O := { dreify : C -> forall t : T, O t}.