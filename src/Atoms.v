From SQEMA Require Import Reify Algebra Polyadic VectorUtils ListUtils ML BiModal.

Require Import Lists.List.
Require Vectors.Vector.

Import ListNotations.

Section Atoms.
  Context {op opb nom : Set} {ops : nat -> Set} `{PInverse ops}.
  Notation "'atom'" := (nat).
  
  Definition Atoms_BPL : @BPL atom nom (list atom) :=
    {| verum := [];
       falsum := [];
       prop p := [p];
       nomi _ := [];
       conj a b := a ++ b;
       disj a b := a ++ b;
    |}.

  Definition Atoms_PL : @PL atom nom (list atom) := {| B := Atoms_BPL;
                                                       neg a := a
                                                    |}.

  Definition Atoms_UML : @UML op (list atom) := {| diau _ a := a;
                                                   boxu _ a := a
                                                |}.

  Definition Atoms_BML : @BML op opb (list atom) := {| BU := Atoms_UML;
                                                       diab _ a b := a ++ b;
                                                       boxb _ a b := a ++ b
                                                    |}.
  
  Definition Atoms_PML : @PML ops (list atom) := {| pdia _ _ args := Vector.fold_right (fun a b => a ++ b) args [];
                                                    pbox _ _ args := Vector.fold_right (fun a b => a ++ b) args [];
                                                 |}.

  Class Atoms M := { atoms : M -> list atom;
                     pure := fun m => Nat.eqb (length (atoms m)) 0;
                     is_pure : M -> Prop := fun m => pure m = true;
                     occurrences `{EqDec atom eq} : atom -> M -> nat := fun p m => count_occ equiv_dec (atoms m) p 
                   }.
  Global Instance Atoms_bmf : Atoms (@bmf nom op opb) :=
    {| atoms := fix atm g := match g with
                             | b_conj a b => atm a ++ atm b
                             | b_disj a b => atm a ++ atm b
                             | b_prop x => [x]
                             | b_negprop x => [x]
                             | b_nom x => []
                             | b_negnom x => []
                             | b_verum => []
                             | b_falsum => []
                             | b_diau x a => atm a
                             | b_boxu x a => atm a
                             | b_diab x a b => atm a ++ atm b
                             | b_boxb x a b => atm a ++ atm b
                             end
    |}.
  
  Global Instance Atoms_uml {M} `{Reify _ M _} : Atoms M := {| atoms := reify (Atoms_UML, Atoms_PL) |}.
  Global Instance Atoms_bml {M} `{Reify _ M _} : Atoms M := {| atoms := reify (Atoms_BML, Atoms_PL) |}.
  Global Instance Atoms_pml {M} `{Reify _ M _} : Atoms M := {| atoms := reify (Atoms_PML, Atoms_PL) |}.

  (** Atoms for diamond and box arguments *)
  Fixpoint diaarg_a {n} (f : @diaarg atom n ops) := match f with
                                                   | dc_conj x x0 => diaarg_a x <*> diaarg_a x0
                                                   | dc_prop x => [x]
                                                   | dc_negprop x => [x]
                                                   | dc_nom x => []
                                                   | dc_negnom x => []
                                                   | dc_verum => []
                                                   | dc_falsum => []
                                                   | dc_dia x x0 => VectorUtils.fold_map diaarg_a x0
                                                   | dc_box x x0 => VectorUtils.fold_map boxarg_a x0
                                                   end
  with boxarg_a {n} f := match f with
                         | bc_disj x x0 => boxarg_a x <*> boxarg_a x0
                         | bc_prop x => [x]
                         | bc_negprop x => [x]
                         | bc_nom x => []
                         | bc_negnom x => []
                         | bc_verum => []
                         | bc_falsum => []
                         | bc_dia x x0 => VectorUtils.fold_map diaarg_a x0
                         | bc_box x x0 => VectorUtils.fold_map boxarg_a x0
                         end.

  Global Instance diaarg_atoms {n} : Atoms (diaarg (nom := n)) := {| atoms := diaarg_a |}.
  Global Instance boxarg_atoms {n} : Atoms (boxarg (nom := n)) := {| atoms := boxarg_a |}.
End Atoms.
  