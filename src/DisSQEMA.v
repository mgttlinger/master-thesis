From SQEMA Require Import Utils SQEMA BiModal Model CT ML Inverse Satisfaction.
Require Import Classical.

Section DisSQEMA.
  Inductive Three : Set := A | B | C.

  Definition frame : @bframe Three Three Empty_set :=
    {| relu' o w v := match o return Prop with
                     | A => w = A /\ (v = B \/ v = C)
                     | B => w = B /\ v = B
                     | C => w = C /\ v = C
                     end;
       relb' _ _ _ _ := False
    |}.

  Definition f : @bmf (FreeUInverse Three) (FreeBInverse Empty_set) Empty_set := b_conj (b_diau (u_direct A) (b_prop 0 =>> (b_diau (u_direct B) (b_prop 0)))) (b_diau (u_direct A) (b_prop 0 =>> (b_diau (u_direct C) (b_prop 0)))).

  Theorem SQEMA_incorrect : ~ ((forall V, bmf_satisfaction frame V A f) <-> forall V, V.(valN) (inr 0) = A -> exists (v : Three), bmf_satisfaction frame V v (copure (SQEMA f))).
    unfold f.
    cbv [impl neg disj ML.B bmf_PL bmf_BPL].
    cbv -[bmf_satisfaction frame iff].
    intros [H1 _].
    assert (forall V : valuation,
       bmf_satisfaction (nom := Empty_set) frame V A
                        (b_conj (b_diau (u_direct A) (b_disj (b_negprop 0) (b_diau (u_direct B) (b_prop 0))))
                                (b_diau (u_direct A) (b_disj (b_negprop 0) (b_diau (u_direct C) (b_prop 0)))))).
    intros [vP vN].
    cbv. split.
    exists B.
    split; auto.
    classical_right.
    exists B.
    split; auto.
    apply NNPP; auto.
    exists C.
    split; auto.
    classical_right.
    exists C.
    split; auto.
    apply NNPP; auto.
    pose proof (H1 H {| valP _ _ := True; valN _ := A |} ltac:(auto)) as H'.
    clear H1 H.
    cbv in H'.
    destruct H' as [v [Ha [Hb _]]].
    intuition.
    destruct H0.
    destruct H1.
    destruct H2.
    destruct H3.
    destruct H.
    destruct H0.
    destruct H1.
    destruct H2.
    subst.
    destruct H2.
    destruct H1.
    destruct H0.
    destruct H.
    subst.
    inversion H1.
  Qed.
End DisSQEMA.