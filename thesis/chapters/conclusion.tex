\chap{Conclusion}

Now we give a quick summary of what was achieved in this work and what is still left for further research.

\section{Summary}
\label{sec:sum}

We implemented the algorithm SQEMA for hybrid polyadic modal formulas \cite{conradie2006algorithmicII} but only up to arity two using Coq (see \Cref{sec:code_sqema}).
Then we stated correctness with respect to local frame correspondence and completeness with respect to polyadic inductive formulas as well as simple Sahlqvist formulas.
The completeness claims were not proved in Coq in this work.

Proving correctness uncovered an issue in the algorithm and the proofs for it in the literature \cite{conradie2006algorithmicII, conradie2007algorithmic} where under certain circumstances the algorithm produces incorrect results.
This was proved in \Cref{sec:error} as well as in our Coq formalization.
We implemented and described a corrected version of SQEMA that only differs slightly from the original description and proceeded to prove that this now is correct on all inputs.
It was also uncovered that Corollary 2.4.7 in Willem Conradies PhD thesis \cite{conradie2007algorithmic} is wrong and disproved in \Cref{subsec:sqema} as well as in our Coq formalization.

Another result of this work is that Coq was possibly not the ideal language for this formalization due to all the issues with inductive type capabilities and memory issues. 

\section{Future Work}
\label{sec:future}

There are a few directions to build upon these results in future research.

\paragraph{Full Algorithm}
To our knowledge the fully hybrid polyadic SQEMA algorithm described in \textcite{conradie2006algorithmicII} was implemented beginning with the diploma of \textcite{georgiev2006implementation} which only implemented the basic variant of the algorithm described in \textcite{conradie2006algorithmic}.
It was later extended to the variant called SQEMA+U \cite{georgiev2015sqema} which has the universal modalities in the syntax as well.
At some point the implementation then got extended to the polyadic variant which is currently running on their website\footnote{\url{https://store.fmi.uni-sofia.bg/fmi/logic/sqema/sqema_gwt_20170412_1/Polyadic/SQEMA.html}}.
Sadly the source code for the work by Georgiev is no longer publicly available and the internet archive\footnote{\url{https://archive.org/}} does not have a cache of those files either.
Parts of the source are available in Georgievs thesis which is sadly not written in English.
The currently running implementation on the site is available only as minified JavaScript which is loaded when visiting the site.

Hence, it would be beneficial to have a full implementation and/or formalization publicly available for further research to extend upon.
Ideally one would use a dependently typed language for this which provides a convenient way to work with nested inductive types as those will be necessary to implement arbitrary arity modalities in a type safe manner.

\paragraph{Prove Completeness}
Due to time constraints we did not complete proving completeness w.r.t. the class of inductive formulas or their subclass the Sahlqvist formulas.
For proving completeness w.r.t. the class of inductive formulas the main open issue is writing a predicate witnessing that a given system is an inductive system to reflect the current decision procedure.
That predicate is likely going to be complicated given that the acyclicity of the dependency \ac{dg} is not a trivial property to state inductively.
This could possibly be investigated in a future student project.

\paragraph{Formalize Canonicity}
In this work we focused solely on correctness and completeness of SQEMA, but the algorithm --- like the Sahlqvist-van Benthem algorithm --- has the additional property of succeeding only on canonical formulas and can therfore be used as an automated way to prove canonicity of formulas.
This could be a relevant addition to our formalization but would require getting one of the available topology libraries to work with a more recent version of Coq.

\paragraph{Extending Capabilities}
Our solving procedure covers a bit more than inductive formulas but could be extended to be capable of handling a lot more by adding propositional reasoning capabilities.
For example the detection of situations like \(\phi \lor \neg\phi\) or \(\phi \land \neg\phi\) could improve elimination of variables.
We suspect that the setting of \textcite{schmidt2012ackermann} might be better suited for this, given that there detection of those situations is much more straight forward due to formulas being in modal normal form instead of \ac{NNF} throughout the run of the algorithm.

\paragraph{Proving \mintinline{coq}{rule_transform_unfold}}
As discussed in \Cref{sec:veri} we were not able to prove the \mintinline{coq}{rule_transform_unfold} lemma due to memory constraints.
Completing this proof on a machine with sufficient memory would fill the missing hole in the proved theorems of this thesis.
