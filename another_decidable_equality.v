
(* ##################### *)

(** Rather than defining boolean equality manually, let us try something new. *)

Scheme Equality for tm.

(** From Chapter 13 of the Reference Manual: *)

(** The [Scheme] command is a high-level tool for generating automatically (possibly mutual) induction principles for given types and sorts ...

[Scheme Equality for] _ident1_  tries to generate a Boolean equality and a proof of the decidability of the usual equality. If identi involves some other inductive types, their equality has to be defined first. *)

(** In order to understand this last sentence, you can try: *)

(* => Print tm_beq. *)


(** *** Some new tactics *)

(** In order to fully utilize the output of [Scheme Equality], we define a bunch of new tactics. It should be clear what they do. *)

(* HIDE: I wish I could find some more natural and generic solutions, which work good with automation... *)

 Ltac hideq H :=
      match goal with
        E : id_beq ?x ?i = _ |- _ => have H := (internal_id_dec_bl x i E); subst; clear E
      end.

Ltac htmeq H :=
      match goal with
        E : tm_beq ?x ?i = _ |- _ => have H := (internal_tm_dec_bl x i E); subst; clear E
      end.

Ltac eideq i :=
  (*let H := fresh "H" in
  have H := *)
  rewrite -> (internal_id_dec_lb  i i) in *; trivial.

Ltac etmeq t :=
  let H := fresh "H" in
  have H := (internal_tm_dec_lb t t);
            rewrite -> H in *; trivial.

Ltac tbeqself := match goal with
                   H : forall t' : tm, reflect _ (tm_beq ?t t') |- _ =>
                   specialize (H t);  rewrite internal_tm_dec_lb in H; trivial
                 end.

Ltac tnbeq := match goal with
  H : forall t' : tm, reflect (?h t') (tm_beq ?t t'),
  H1 : ?h ?t'  |- _ => specialize (H t'); inversion H
end.

Ltac deseq H := match goal with |- _ (tm_beq ?t ?t') => destruct (tm_beq t t') eqn:H end.

Ltac Ttref := match goal with
  H : forall t' : tm, reflect (?h t') (tm_beq ?t t'),
  H1 : ?h ?t'  |- _ => specialize (H t'); apply (introT H) in H1; clear H
end.

Ltac neqid := let Hf := fresh "Hf" in
              let h1 := fresh "h1" in
              match goal with  H: ?x <> ?i |- _ => assert (Hf: id_beq x i = false) by (rewrite <- Bool.not_true_iff_false; move => ?; by hideq h1); rewrite -> Hf in *; clear H; clear Hf
              end.


