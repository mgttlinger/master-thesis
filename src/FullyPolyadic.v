From SQEMA Require Import VectorUtils ListUtils DPair Algebra.
Require Export Classes.EquivDec.
Require Import Lists.List.
From Coq.Vectors Require Fin Vector.
Require Import Bool.
Require Import Relations.
Require Import Logic.JMeq.

Import ListNotations.


Module full_polyadic.
  Section fp.
  Variable tau : nat -> Set.       (* base modal operators parametrised over their arity *)
  Variable prop : Set.          (* propositional atoms  *)
  Variable nom : Set.           (* nominals *)
  Context `{EqDec prop eq} `{EqDec nom eq} `{forall n, EqDec (tau n) eq}. (* atoms and nominals should have decidable equality *)

  (* full_polyadic would need either induction-induction or induction-recursion which coq doesn't have ... *)
  Inductive mt : nat -> Set :=
  | op_direct {n} : tau n -> mt n
  | op_ap {n} {k} : mt (S n) -> mt k -> mt (n + k)
  | op_mf : mf true -> mt 0
  | op_rev {n} : mt n -> Fin.t n -> mt n
  with mf : bool -> Set :=
  | m_prop : prop -> mf false
  | m_nom : nom -> mf true
  | m_neg {b} : mf b -> mf b 
  | m_disj {b1 b2} : mf b1 -> mf b2 -> mf (b1 && b2)
  | m_op {n} : forall (l : list bool), mt n -> length l = n -> hlist l -> mf (forallb id l)
  with hlist : list bool -> Set :=
  | hnil : hlist []
  | hcons {b} {l} : mf b -> hlist l -> hlist (b :: l).

  Hint Constructors mt mf hlist.

  Scheme mf_ind' := Induction for mf Sort Prop
    with mt_ind' := Induction for mt Sort Prop
    with hlist_ind' := Induction for hlist Sort Prop.

  Definition formula : Set := (bool ** mf).

  Definition mf_formula {b} (f : mf b) : (bool ** mf) := (b ** f).
  Definition mt_sig {n} (o : mt n) : (nat ** mt) := (n ** o).

  Definition f_neg (f : formula) : formula := match f with
                                              | (b ** f) => mf_formula (m_neg f)
                                              end.
  Definition f_disj (a b : formula) : formula := match (a, b) with
                                                 | ((b1 ** f1), (b2 ** f2)) => mf_formula (m_disj f1 f2)
                                                 end.

  Fixpoint non_reversive' {b} (f : mf b) : bool := 
    match f with
    | m_prop _ => true
    | m_nom _ => true
    | @m_neg b f => non_reversive' f
    | @m_disj b1 b2 f1 f2 => non_reversive' f1 && non_reversive' f2
    | @m_op n l op _ args => non_reversive'' op && hlist_rec (fun _ _ => bool) true (fun _ _ mfb _ accu => non_reversive' mfb && accu) l args
    end
  with non_reversive'' {n} (o : mt n) {struct o} : bool :=
    match o with
    | op_direct _ => true
    | @op_ap n k a b => non_reversive'' a && non_reversive'' b
    | op_mf x => non_reversive' x
    | op_rev _ _ => false 
    end.
  
  Definition non_reversive := dpair_fold (@non_reversive').

  Fail Inductive Non_Reversive : formula -> Prop :=
  | nr_prop : forall p, Non_Reversive (mf_formula (m_prop p))
  | nr_nom : forall n, Non_Reversive (mf_formula (m_nom n))
  | nr_neg : forall f, Non_Reversive f -> Non_Reversive (f_neg f)
  | nr_disj : forall a b, Non_Reversive a -> Non_Reversive b -> Non_Reversive (f_disj a b)
  | nr_op : forall l op p args, Non_Reversive' op -> hlist_rect (fun _ _ => Prop) True (fun b _ mfb _ accu => Non_Reversive (mf_formula mfb) /\ accu) l args -> Non_Reversive (mf_formula (m_op l (snd _ _ op) p args))
  with Non_Reversive' : (nat ** mt) -> Prop :=
  | nr_direct : forall n (t : tau n), Non_Reversive' (mt_sig (op_direct t))
  | nr_ap : forall n (a : mt (S n)) b, Non_Reversive' (mt_sig a) -> Non_Reversive' b -> Non_Reversive' (mt_sig (op_ap a (snd _ _ b)))
  | nr_mf : forall f, Non_Reversive (mf_formula f) -> Non_Reversive' (mt_sig (op_mf f)).
  (* strict positiveness issue... *)
  
  Fixpoint only_reversive' {b} (f : mf b) {struct f} : bool :=
    match f with
    | m_prop _ => true
    | m_nom _ => true
    | @m_neg b f => only_reversive' f
    | @m_disj b1 b2 f1 f2 => only_reversive' f1 && only_reversive' f2
    | @m_op n l op _ args => only_reversive'' op && hlist_rec (fun _ _ => bool) true (fun _ _ mfb _ accu => only_reversive' mfb && accu) l args
    end
  with only_reversive'' {n} (o : mt n) : bool :=
    match o with
    | op_direct _ => true
    | @op_ap n k a b => only_reversive'' a && only_reversive'' b
    | op_mf x => only_reversive' x
    | @op_rev n x _ => non_reversive'' x
    end.

  Definition only_reversive := dpair_fold (@only_reversive').

  

  (* Fixpoint non_reversive_reversive {b} (f : mf b) {struct f} : non_reversive' f = true -> only_reversive' f = true := *)
  (*   match f as f' return non_reversive' f' -> only_reversive' f' with *)
  (*   | m_prop _ as f => fun _ => eq_refl (only_reversive' f) *)
  (*   | m_nom _ as f => fun _ => eq_refl (only_reversive' f) *)
  (*   | m_neg g as f => fun x =>  *)
  (*   | m_disj x1 x2 => _ *)
  (*   | m_op l x p args => _ *)
  (*   end. *)

  (** Completely reversive formulas *)
  Definition crmf : Set := formula.
  (** Reversive formulas (no nesting of inverses) *)
  Definition rmf : Set := {f : formula | only_reversive f = true}.
  (** Basic modal formulas (no inverse modalities) *)
  Definition bmf : Set := {f : formula | non_reversive f = true}.


  End fp.
                             
End full_polyadic.
