From SQEMA Require Export Utils CT.

Section SumUtils.
  (** Left biased sum instances *)
  Definition s t u := (u + t)%type.
  Hint Unfold s.
  
  Global Program Instance sum_Functor {t} : @Functor (s t) :=
    fun r f => f {| fmap__ _ _ := fun f fa => match fa with
                                        | inl x => inl (f x)
                                        | inr x => inr x
                                        end
              |}.
  
  Global Instance sum_Pure {t} : @Pure (s t) := fun r f => f {| pure__ _ := inl (B := t) |}.
  Global Program Instance sum_Applicative {t} : @Applicative (s t) := fun r f => f {| zip__ A B fa fb :=
                                                                                     match (fa, fb) with
                                                                                     | (inr x, _) => inr x
                                                                                     | (_, inr x) => inr x
                                                                                     | (inl a, inl b) => inl (a, b)
                                                                                     end
                                                                                |}.
  Global Instance sum_Monad {t} : @Monad (s t) := fun r f => f {| join__ A ffa := match ffa with
                                                                               | inl x => x
                                                                               | inr x => inr x
                                                                               end
                                                            |}.

  Definition sum_ {A B C} := @sum_rect A B (fun _ => C).
End SumUtils.