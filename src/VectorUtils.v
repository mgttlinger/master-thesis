From SQEMA Require Export Utils CT.
Require Export Vectors.Vector.
Require Import Eqdep.
Require Import SQEMA.Algebra.

Import VectorNotations. 
Module VectorUtils.
  Global Instance vector_Functor {n} : @Functor (fun t => Vector.t t n) :=
    fun r f => f {| fmap__ := fun a b (f : a -> b) => map f;
              |}.

  Global Instance vector_Pointed : @Pointed (fun t => Vector.t t 0) := fun r f => f {| point__ := nil |}.
  Global Instance vector_Pure : @Pure (fun t => Vector.t t 1) := fun r f => f {| pure__ _ x := [x] |}.

  Definition vSequence {G} {n} `{Applicative G} : forall T, Vector.t (G T) n -> G (Vector.t T n).
    intros. induction X; pose proof _f as f; pose proof _p as p. apply pure; trivial. apply (@point (fun f => Vector.t f 0) _ _). eapply ap. apply (fmap (fun a b => a :: b) h). exact IHX. 
  Defined. 
  (** How to construct the proof term by hand??? *)
  
  Global Instance vector_Traversable {n} {G} : @Traversable (fun t => Vector.t t n) G := fun r f => f {| sequence__ _ := vSequence;
                                                                                                |}.

  Create HintDb vectors.
  Hint Constructors Vector.t In Forall : vectors.

  Definition fold_map {n} {t u} (f : t -> u) `{M : Monoid u} : Vector.t t n -> u :=
    fun v => fold_right (fun a accu => mult (f a) accu) v one.
  
  Definition zipWith {n} {t u r} (f : t -> u -> r) := @rect2 _ _ (fun n _ _ => Vector.t r n) (nil _) (fun _ _ _ zv1v2 a b => cons _ (f a b) _ zv1v2) n.

  Definition fill {a} (f : nat -> a) n : Vector.t a n := nat_rect (Vector.t a) [] (fun l v => shiftin (f l) v) n. 

  Definition length {a} {n} : Vector.t a n -> nat := fun _ => n.
  Definition existsb {n} {t} (f : t -> bool) := t_rec t (fun _ _ => bool) false (fun h _ _ pt => andb (f h) pt) n. 
  
  Ltac predicate_cons := match goal with
                         | [H :_ _ (cons _ _ _ _) |- _] => inversion H; clear_existT; clear H
                         end.

  
  Lemma forall_function {n} {t} {v : Vector.t t n} : forall (A : t -> Prop), (forall f, In f v -> A f) <-> Forall A v.
    intro A. induction v; split; intros; auto with vectors.
    - inversion H0.
    - constructor. apply H. constructor. apply IHv. intros. apply H. constructor. trivial.
    - repeat predicate_cons. trivial. apply IHv; trivial. 
  Qed.

  Lemma in_map_preservation : forall n t u (f : t -> u) (v : Vector.t t n) e, In e v -> In (f e) (map f v).
  Proof with intuition (auto with vectors).
    intros n t u f v e H1. induction v. inversion H1. simpl. VectorUtils.predicate_cons...
  Qed.
  
  Lemma in_exists {n} {t u} {v : Vector.t t n} : forall e (f : t -> u), In e (Vector.map f v) <-> exists e', In e' v /\ f e' = e. 
    intros. induction v; split; intros.
    - inversion H.
    - destruct H as [e' [He1 He2]]. inversion He1.
    - inversion H; clear_existT.
      + exists h. intuition.
      + destruct ((proj1 IHv) H2). exists x. intuition.  
    - destruct H as [e' [He1 He2]]. simpl. inversion He1; clear_existT; intuition. constructor 2. apply in_map_preservation. intuition.
  Qed.
  
  Lemma forall_tail {A} {P} {n}: forall h t, Forall P (cons A h n t) <-> P h /\ Forall P t. 
    intros h t. split.
    - intros. predicate_cons. auto. 
    - intros [Hh Ht]. constructor; trivial.
  Qed.

  Hint Resolve forall_tail forall_function in_map_preservation in_exists : vectors.

  
  Ltac tail := match goal with
               | [H : Forall ?P (cons _ _ _ ?t) |- Forall ?P ?t] => eapply forall_tail; apply H 
               end.

      
  Lemma fold_preservation {t} {n} : forall u f (v : Vector.t t n) (P : u -> Prop) (Q : t -> Prop) s, (forall a b, In a v -> Q a -> P b -> P (f a b)) -> (Forall Q v -> P s -> P (fold_right f v s)).
  Proof with (auto with vectors).
    intros u f v P Q s Hf. induction v; simpl; intros H2 H3... apply Hf... predicate_cons... apply IHv... predicate_cons...
  Qed.
    
  Lemma forall_map_preservation {t} {n} : forall u (f : t -> u) (v : Vector.t t n) (P : u -> Prop) (Q : t -> Prop), (forall a, In a v -> Q a -> P (f a)) -> Forall Q v -> Forall P (map f v).
  Proof with (auto with vectors).
    intros u f v P Q. induction v; simpl; intros H1 H2... predicate_cons... 
  Qed.

  Lemma forall_map_fusion {t} {n} : forall u f (v : Vector.t t n) (P : u -> Prop) (Q : t -> Prop), Forall Q v -> (forall a, In a v -> Q a -> P (f a)) -> Forall P (map f v).
  Proof with (auto with vectors).
    intros u f v P Q. induction v; simpl... intros. predicate_cons... 
  Qed.

  Lemma fold_map_fusion {s} {t} {u} {n} : forall (f : s -> t) g (v : Vector.t s n) (s : u), fold_right g (map f v) s = fold_right (fun e => g (f e)) v s.
  Proof with (auto with vectors).
    intros f g v. induction v... simpl. intro. f_equal...
  Qed.  
    
  Hint Resolve forall_map_preservation forall_map_fusion : vectors.
  Fail Hint Resolve fold_preservation : vectors. (* Why does this fail??? *)
  
  Ltac map_fusion_with H := eapply forall_map_fusion; try apply H.

  Lemma map_fixpoint {n} {t} : forall g (v : Vector.t t n), (forall f, In f v -> f = g f) <-> v = map g v.
  Proof with (auto with vectors).
    intros g v. induction v; simpl; split; intros; try predicate_cons...
    - inversion H0.
    - rewrite <-H... f_equal. rewrite <-IHv...
    - predicate_cons. rewrite <-H1...
    - predicate_cons. apply IHv...
  Qed.

  Lemma fold_fixpoint {n} {t} : forall u (g : t -> u -> u) (v : Vector.t t n), (forall f, In f v -> forall a, a = g f a) -> forall s, s = fold_right g v s.
  Proof with (auto with vectors).
    intros u g v. induction v; simpl... intros.  pose proof (H h). rewrite <-H0... 
  Qed.

  Hint Resolve map_fixpoint fold_fixpoint : vectors.

  Lemma forall_ap {n} {t} : forall (A B : t -> Prop) (v : Vector.t t n), Forall (fun f => A f -> B f) v -> Forall (fun f => A f) v -> Forall B v.
  Proof with (auto with vectors).
    intros A B v. induction v; simpl; intros H1 H2; constructor; predicate_cons; inversion H1... apply IHv; predicate_cons... 
  Qed.

  Hint Resolve forall_ap : vectors.


  Lemma map_fusion {n} {s t u} : forall (f : t -> u) (g : s -> t) (v : Vector.t s n), map f (map g v) = map (fun e => f (g e)) v.
  Proof with auto with vectors.
    intros f g v. induction v... simpl. rewrite IHv...
  Qed.

  Hint Resolve map_fusion in_map_preservation : vectors.
  Ltac vector := repeat (auto with vectors; try tail; try predicate_cons; match goal with
                               | [ |- ~ (Forall _ _)] => intro
                               | [ |- ~ (Exists _ _)] => intro
                               | [ |- Forall _ (nil _)] => constructor
                               | [ |- Forall _ (cons _ _ _ _)] => constructor
                               | [ |- In _ (cons _ _ _ _)] => constructor
                               | [H : _ (nil _) |- _] => inversion H; clear H
                               | [H : Forall (fun x => ?P (?f x)) ?v |- Forall (fun x => ?P x) (map ?f ?v)] => map_fusion_with H (* doesnt seem to match ... *)
                               | [ _ : forall a, In a ?v -> ?Q a -> ?P (?f a), _ : Forall ?Q ?v |- Forall ?P (map ?f ?v)] => apply map_fixpoint
                               end).

End VectorUtils.
