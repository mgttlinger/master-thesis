From SQEMA Require Import CT BiModal Nominals.
From SQEMA Require Export System.
Require Import Lists.List.
Require Import Nat.
Require Import Program.Equality.
Require Import Omega.

Require Import Bool.Bool.
Import ListNotations.

Section SQEMA.
  Context {nom opu opb : Set}.
  
  Notation "'bmf'" := (@bmf (FreeUInverse opu) (FreeBInverse opb) nom).
  Notation "'bmf_plus'" := (@BiModal.bmf (FreeUInverse opu) (FreeBInverse opb) (nom + nat)).
  Notation "'system'" := (@system (FreeUInverse opu) (FreeBInverse opb) (nom + nat)).
  Notation "'eqn'" := (@eqn (FreeUInverse opu) (FreeBInverse opb) (nom + nat)).
  (* if those aliases are definitions instead Coq gets super confused causing things like rewrites no longer working as expected due to implicit arguments not being beta reduced but only sometimes... *)
  
  (** A fresh nominal in the given system (never 0/i though) *)
  Definition next_nom `{@Nominals (nom + nat) system} (s : system) : nat :=
    S (ListUtils.maxBy (fun e => match e with
                              | inl x => 0
                              | inr x => x
                              end) (nominals s)).
  Hint Unfold next_nom.
  
  (** build the initial equation by prefixing with the special i nominal *)
  Definition prefix_i : bmf_plus -> eqn := fun f => {| a := b_negnom (inr 0);
                                                   b := f
                                                |}. 
  Hint Unfold prefix_i.
  
  (** Eliminate trivial (only neg or only pos) occurences of variables *)
  Definition eliminate_trivial `{ElimTriv eqn} : eqn -> eqn :=
    fun e => fold_right elim_triv e (filter (trivial_in e) (atoms e)).
  Hint Unfold eliminate_trivial.
  
  (** When there are alternatives choose the one with the most variables eliminated *)
  Definition better_system `{Atoms system} (a b : system) : system :=
    let aa := atoms a in
    let ab := atoms b in
    let uaa := length (nodup equiv_dec aa) in
    let uab := length (nodup equiv_dec ab) in
    if uaa <? uab then a
    else if uab <? uaa then b
         else if length aa <? length ab then a else b.

  (** Try every order of elimination *)
  Definition elim_all `{E : Eliminate system} : eqn -> list system :=
    fun e => let s := to_system ([e]) in
          map (fun ats => copure (eliminate_inorder s ats))
              (ListUtils.permutations (atoms e)).
  Hint Unfold better_system elim_all.
  
  (** Eliminate every variable occurrence or as much as possible *)
  Definition eliminate_all `{Eliminate system} (e : eqn) : system :=
    let s := to_system ([e]) in
    if Atoms.pure e then s else fold_right better_system (s) (elim_all e).
  Hint Unfold eliminate_all.

  Global Instance bmf_Complexity : Complexity bmf_plus := {| complexity p f :=
                                                       if bmf_pure_in p f then 0 else (fix c f :=
                                                                                         match f with
                                                                                         | b_verum => 0
                                                                                         | b_falsum => 0
                                                                                         | b_prop q => 1
                                                                                         | b_nom x => 0
                                                                                         | b_negprop q => 1
                                                                                         | b_negnom x => 0
                                                                                         | b_conj a b => S (c a + c b)
                                                                                         | b_disj a b => S (c a + c b)
                                                                                         | b_diau o a => S (c a)
                                                                                         | b_boxu o a => S (c a)
                                                                                         | b_diab o a b => S (c a + c b)
                                                                                         | b_boxb o a b => S (c a + c b)
                                                                                         end) f
                                                  |}.
  Definition easier {t} `{C: Complexity t} p a b : Prop := complexity p a < complexity p b.
  Infix "<<" := (easier) (at level 60).

  (** glorified fold under the monad stack *)
  Fixpoint recurse' {S} s (f : S -> eqn -> Env S (failure (list eqn))) (l l': list eqn) :=
    match l with
    | nil => l'
    | e :: es => let (s', rl) := f s e in
                copure rl ++ recurse' s' f es l'
    end.
  

  Ltac record_rewrite := repeat match goal with
                                | [H : b_conj _ _ = a _ _ |- _] => try (rewrite_all <- H; clear H)
                                | [H : b_conj _ _ = b _ _ |- _] => try (rewrite_all <- H; clear H)
                                end.

  Ltac pure_rewrite := match goal with
                       | [H : true = bmf_pure_in ?p ?a |- context[bmf_pure_in ?p ?a]] => rewrite <- H
                       | [H : false = bmf_pure_in ?p ?a |- context[bmf_pure_in ?p ?a]] => rewrite <- H
                       | |- context[if ?p then _ else _] => destruct p
                       end.

  Local Obligation Tactic := unfold complement, equiv; Tactics.program_simpl; simpl; record_rewrite; repeat (pure_rewrite; cbn); record_rewrite; try omega; try solve [repeat split; congruence].
  (** solves an equation ed for the atom p in a context with free nominals free_nominals *)
  Program Fixpoint rule_transform (p : pro) (free_nominals : Stream nat) (ed : eqn) {measure (complexity p ed)} : EnvT failure (Stream nat) (list eqn) :=
    if orb (is_alpha p ed) (is_beta p ed) then (free_nominals, succ: ([ed])) else
      let av := ed.(a) in
      let bv := ed.(b) in
      match (av, bv, (bmf_pure_in p av, bmf_pure_in p bv)) with
      | (b_conj a1 a2, o, (false, _)) | (o, b_conj a1 a2, (_, false)) => (* /\ rule *)
        let (n1, r1) := rule_transform p free_nominals {|a := a1; b:= o|} in
        let res := rule_transform p n1 {|a := a2; b := o|} in
        fcapp res r1
      | (b_negnom x, b_diab o a1 a2, (_, false)) | (b_diab o a1 a2, b_negnom x, (false, _)) => (* binary dia rule *)
        let (n1, n2) := (inr (free_nominals 0) : nom + nat, inr (free_nominals 1)) in
        (* type must be annotated toprevent universe inconsistency *)
        let (fn1, r1) := rule_transform p (advance 2 free_nominals) {|a:=b_negnom n1;b:=a1|} in
        let res := rule_transform p fn1 {|a:=b_negnom n2;b:=a2|} in
        fmap (fun rs => {|a := b_negnom x; b := diab o (b_nom n1) (b_nom n2)|} :: rs) (fcapp res r1)
      | (b_diau o a1, b_negnom x, (false, _)) | (b_negnom x, b_diau o a1, (_, false)) => (* unary dia rule *)
        let n1 := inr (free_nominals 0) : nom + nat in
        fmap (fun r => {|a := b_negnom x; b := b_diau o (b_nom n1)|} :: r) (rule_transform p (advance 1 free_nominals) {|a:=b_negnom n1;b:=a1|})
      | (b_disj a1 a2, f, (false, true)) | (f, b_disj a1 a2, (true, false)) => (* disj rule *)
        let a1pp := bmf_pure_in p a1 in
        let a2pp := bmf_pure_in p a2 in
        match (a1pp, a2pp) with
        | (true, false) => rule_transform p free_nominals {|a:=a2;b:= b_disj a1 f |}
        | (false, true) => rule_transform p free_nominals {|a:=a1;b:= b_disj f a2 |}
        | _ => (free_nominals, fail: ([ed]))
        end
      | (f, b_boxu o a1, (true, false)) | (b_boxu o a1, f, (false, true)) => (* unary box rule *)
        rule_transform p free_nominals {|a:=a1;b:=boxui o f|}
      | (b_boxb o a1 a2, f, (_, true)) | (f, b_boxb o a1 a2, (true, _)) => (* binary box rule *)
        let a1pp := bmf_pure_in p a1 in
        let a2pp := bmf_pure_in p a2 in
        match (a1pp, a2pp) with
        | (true, false) => rule_transform p free_nominals {|a:=a2;b:=boxbi o false a1 f|}
        | (false, true) => rule_transform p free_nominals {|a:=a1;b:=boxbi o true f a2|}
        | _ => (free_nominals, fail: ([ed]))
        end
      | (_, _, (_, _)) => (free_nominals, fail: ([ed]))
      end.

  (* Global Opaque rule_transform. *)
  
  Definition split_alpha_beta `{Polarity eqn} `{Polarity bmf_plus} `{IsAtom bmf_plus} (p : pro) (sy : system) : option ((list bmf_plus) * (list eqn)) :=
    let (als, br) := split_system p sy in
    let (betas, rest) := br in
    if andb (ListUtils.is_empty rest) (negb (ListUtils.is_empty betas)) then Some (ListUtils.collect (is_alpha' p) als, betas) else None.

  Definition ackermann_rule `{Polarity system} `{NegSubstitution eqn bmf_plus eqn} (p : pro) (sy : system) : failure system :=
    match split_alpha_beta p sy with
    | Some (alphas, betas) => succ: (to_system ((map (neg_subst (b_bigconj alphas) p)) betas))
    | None => fail: sy
    end.
 

  Global Instance bmf_plus_eliminate : Eliminate system :=
    {| eliminate ss p := 
         let (alphas, br) := split_system p ss in
         let (betas, rest) := br in
         let init_free := (fun off => next_nom ss + off) in
         ackermann_rule p (to_system (recurse' init_free (fun fn e => rule_transform p fn e) rest (alphas ++ betas)))
    |}.


  Definition check_purity f : failure bmf_plus := if bmf_pure f then succ: f else fail: f.

  (** Transform to modal disjunctive normal form and split into clauses *)
  Definition preprocess : bmf -> list (bmf_plus) := fun f => map b_neg (split_conj (bmf_nmap inl f)).
  (** Transform back into a single formula and negate *)
  Definition postprocess : list system -> bmf_plus := fun ss => b_bigconj (map (fun s => b_neg (sys_toformula s)) ss).
  Definition postprocess' : list system -> list bmf_plus := map (fun s => b_neg (sys_toformula s)).
  Hint Unfold preprocess postprocess postprocess'.
  (** The SQEMA algorithm as described in the literature *)
  Definition SQEMA : bmf -> failure bmf_plus := fun f => check_purity (postprocess (map (fun e => eliminate_all (eliminate_trivial (prefix_i e))) (preprocess f))).
  (** The corrected SQEMA implementation which doesn't join together in the end *)
  Definition SQEMA' : bmf -> failure (list bmf_plus) := fun f => traverse check_purity (postprocess' (map (fun e => eliminate_all (eliminate_trivial (prefix_i e))) (preprocess f))). 
  Hint Unfold SQEMA SQEMA'.
  
  Definition p := 5.
  Definition s := 6.
  Notation "'i'" := (b_nom (inr 0)) (only printing).
  Parameter o : FreeUInverse opu.
  Parameter b : FreeBInverse opb.

  Definition cr : bmf := ((diau o (boxu o (prop p))) =>> (boxu o (diau o (prop p)))).
  Compute SQEMA cr.
  Compute SQEMA' cr.
  Example cr_regular : is_regular_formula cr = true. simpl; reflexivity. Qed.
  
  Definition uni : bmf := ((boxu o ((boxu o (prop p)) <=> (prop s))) =>> (prop p)).
  Compute SQEMA uni. (* works with sufficient prop reasoning *)
  Compute SQEMA' uni.
  Example uni_irregular : is_regular_formula uni = false. simpl; reflexivity. Qed.

  Compute SQEMA (boxu o (boxu o (prop p)) =>> prop p).
  Compute SQEMA' (boxu o (boxu o (prop p)) =>> prop p).
  Compute SQEMA (boxb b (neg (boxb b falsum (prop p))) (diab b (prop p) verum)).
  Compute SQEMA' (boxb b (neg (boxb b falsum (prop p))) (diab b (prop p) verum)).
  Example should_fail : exists f, SQEMA (neg (boxb b (prop p) (prop p)) \\// diab b (prop p) (prop p)) = fail: f. vm_compute; eexists; reflexivity. Qed.
  Example should_fail' : exists f, SQEMA' (neg (boxb b (prop p) (prop p)) \\// diab b (prop p) (prop p)) = fail: f. vm_compute; eexists; reflexivity. Qed.
  Compute SQEMA (prop p //\\ (boxu o (diau o (prop p) =>> boxu o (prop s)) =>> diau o (boxu o (boxu o (prop s))))).
  Compute SQEMA' (prop p //\\ (boxu o (diau o (prop p) =>> boxu o (prop s)) =>> diau o (boxu o (boxu o (prop s))))).

End SQEMA.

(** Test if it is easy to proove that if there are only unary and binary operators it really can only be unary or binary *)
Section A.
  Variables A B : Set.
  Variables (a : A) (b : B).
  Definition ops n : Set := match n with
                            | O => Empty_set
                            | 1 => A
                            | 2 => B 
                            | S (S (S x)) => Empty_set
                            end.

  Program Definition t {n} : ops n -> nat := fun o => n.

  Lemma only1_2 {n} : forall (o : ops n), t o = 1 \/ t o = 2.
  Proof.
    destruct n eqn:Hn1; simpl.
    - induction o0.
    - destruct n0 eqn:Hn2; simpl.
      + unfold t. tauto.
      + destruct n1 eqn:Hn3; simpl.
        * unfold t. tauto.
        * induction o0.
  Qed.          
      
End A.