Require Export Utils Vectors.Fin Classes.EquivDec.

Class PInverse m := { pinv {n} : Fin.t n -> m n -> m n }.

(** Freely extends m with unary inverse symbols *)
Inductive FreeUInverse (m : Set) : Set :=
| u_direct : m -> FreeUInverse m
| u_inverse : m -> FreeUInverse m
.

Arguments u_inverse {m} _.
Arguments u_direct {m} _.

Program Instance fui_eqdec {m : Set} {E : EqDec m eq} : EqDec (FreeUInverse m) eq.
Next Obligation. destruct x,y; try uneq; (destruct (m0 == m1); [rewrite e; left; reflexivity | uneq]). Defined.

(** unary inverse *)
Definition inv {m} : FreeUInverse m -> FreeUInverse m :=
  fun o => match o with
        | u_direct o => u_inverse o
        | u_inverse o => u_direct o
        end.

(** Freely extends m with binary inverses *)
Inductive FreeBInverse (m : Set) : Set :=
| b_direct : m -> FreeBInverse m
| b_inv_fst : FreeBInverse m -> FreeBInverse m
| b_inv_snd : FreeBInverse m -> FreeBInverse m
.

Arguments b_direct {m} _.
Arguments b_inv_fst {m} _.
Arguments b_inv_snd {m} _.

(** invert on first argument *)
Definition inv_fst {m} : FreeBInverse m -> FreeBInverse m :=
  fun o => match o with
        | b_inv_fst o => o
        | e => b_inv_fst e 
        end.

(** inverse on second argument *)
Definition inv_snd {m} : FreeBInverse m -> FreeBInverse m :=
  fun o => match o with
        | b_inv_snd o => o
        | e => b_inv_snd e 
        end.
