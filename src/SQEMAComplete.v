From SQEMA Require Import Utils SQEMA BiModal CT BoxFormula DiGraph SQEMAUtils Equation System ListUtils Satisfaction Polarity Atoms SQEMAFacts.

From Coq.Sets Require Import Ensembles Partial_Order.

Require Import Lists.List.
Require Import Bool.Bool.
Require Import Nat.
Require Import Program.Equality.
Require Import Omega.
Require Import Sorting.Permutation.
Require Import FunInd.
Require Import Classical.

Import ListNotations.
Import ListUtils.

Section SQEMAComplete.

  Notation opu := (SQEMAFacts.opu).
  Notation opb := (SQEMAFacts.opb).
  Notation nom := (SQEMAFacts.nom).

  Context `{Neq: EqDec (nom + nat) eq}.
  
  Notation "'bmf_' n" := (@bmf (FreeUInverse opu) (FreeBInverse opb) n) (at level 20). 
  Notation "'bmf'" := (bmf_ nom).
  Notation "'bmf_plus'" := (bmf_ (nom + nat)).
  Notation "'eqn'" := (@eqn (FreeUInverse opu) (FreeBInverse opb) (nom + nat)).
  Notation "'system'" := (@system (FreeUInverse opu) (FreeBInverse opb) (nom + nat)).
  
  Tactic Notation "fmap_unfold" := cbv [fmap ListUtils.list_Functor fmap__ Equation.b Equation.a].
  Tactic Notation "ap_unfold" := cbv [_f _f__ _p__ fmap ListUtils.list_Functor fmap__ Equation.b Equation.a ap zip zip__ ListUtils.list_Applicative].

  Ltac auto_neg_shift := cbn [b_neg] in *; match goal with
                                           | H : b_neg ?f = ?g |- _ => apply -> (neg_shift f g) in H; cbn [b_neg] in H
                                           | H : ?f = b_neg ?g |- _ => apply <- (neg_shift f g) in H; cbn [b_neg] in H
                                           end.

  Ltac arg_triv := cbn [b_neg]; match goal with
                                | |- exists v, SQEMA ?f = succ: v => notHyp (VariableNegative f); assert (VariableNegative f) by (subst; repeat constructor; eauto); eauto
                                | |- exists v, SQEMA ?f = succ: v => notHyp (VariablePositive f); assert (VariablePositive f) by (subst; repeat constructor; eauto); eauto
                                end.

  
  Ltac helper_tac := match goal with
                     | |- context[p_none <*> _] => rewrite polarity_add_neutral_l
                     | |- context[_ <*> p_none] => rewrite polarity_add_neutral_l
                     | |- context[_ && false] => rewrite andb_false_r
                     | |- context[false && _] => rewrite andb_false_l
                     | |- context[_ && true] => rewrite andb_true_r
                     | |- context[true && _] => rewrite andb_true_l
                     | |- context[_ || false] => rewrite orb_false_r
                     | |- context[false || _] => rewrite orb_false_l
                     | |- context[_ || true] => rewrite orb_true_r
                     | |- context[true || _] => rewrite orb_true_l
                     | |- context[b_neg (b_bigconj ?a)] => rewrite neg_bigconj
                     | |- context[b_neg (b_bigdisj ?a)] => rewrite neg_bigdisj
                     | |- context[atoms (b_neg ?a)] => replace (atoms (b_neg a)) with (atoms a) by eauto with datatypes
                     | |- context[map _ (_ ++ _)] => rewrite map_app
                     | |- context[map _ (_ :: _)] => rewrite map_cons
                     | |- context[map _ (map _ _)] => rewrite map_map
                     | |- context[_ ++ []] => rewrite app_nil_r
                     | |- context[[] ++ _] => rewrite app_nil_l
                     | |- context[bmf2bdiaarg (b_boxu ?o ?f)] => replace (bmf2bdiaarg (b_boxu o f)) with ([fold_right da_conj da_verum (map (da_boxu o) (bmf2bboxarg f))]) by eauto with datatypes
                     | |- context[bmf2bdiaarg (b_conj ?a ?b)] => replace (bmf2bdiaarg (b_conj a b)) with ((map da_conj (bmf2bdiaarg a)) <:> bmf2bdiaarg b) by eauto with datatypes
                     | |- context[bmf2bdiaarg (b_prop ?p)] => cbn [bmf2bdiaarg]
                     | [H: ?f = false |- context[?f]] => rewrite H
                     | [H: ?f = true |- context[?f]] => rewrite H
                     | [H: false = ?f |- context[?f]] => rewrite <- H
                     | [H: true = ?f |- context[?f]] => rewrite <- H
                     | [H: (?a =? ?b) = _ |- context[?b =? ?a]] => rewrite PeanoNat.Nat.eqb_sym; rewrite H
                     end.

 

  
  Lemma pos_polarity_not_pure : forall (f g : bmf_plus) p, polarity_of {| a := f; b := g |} p = p_pos -> bmf_pure_in p f = false \/ bmf_pure_in p g = false. Admitted.
  Lemma neg_polarity_not_pure : forall (f g : bmf_plus) p, polarity_of {| a := f; b := g |} p = p_neg -> bmf_pure_in p f = false \/ bmf_pure_in p g = false. Admitted.
  Lemma mixed_polarity_not_pure : forall (f g : bmf_plus) p, polarity_of {| a := f; b := g |} p = p_mixed -> bmf_pure_in p f = false \/ bmf_pure_in p g = false. Admitted.
  Lemma none_polarity_pure_iff : forall (f g : bmf_plus) p, polarity_of {| a := f; b := g |} p = p_none <-> bmf_pure_in p f = true /\ bmf_pure_in p g = true. Admitted.
  
  Lemma prop_pure_iff : forall p q, bmf_pure_in q (b_prop p : bmf_plus) = negb (q =? p). unfold bmf_pure_in; auto. Qed.
  Lemma negprop_pure_iff : forall p q, bmf_pure_in q (b_negprop p : bmf_plus) = negb (q =? p). unfold bmf_pure_in; auto. Qed.
  
  Ltac auto_purity := match goal with
                      | [H: bmf_pure_in ?q (b_prop ?p) = _ |- context[?q =? ?p]] => rewrite prop_pure_iff in H
                      | [H: bmf_pure_in ?q (b_negprop ?p) = _ |- context[?q =? ?p]] => rewrite negprop_pure_iff in H
                      | [H: bmf_pure_in ?q (b_prop ?p) = _ |- context[?p =? ?q]] => rewrite prop_pure_iff in H
                      | [H: bmf_pure_in ?q (b_negprop ?p) = _ |- context[?p =? ?q]] => rewrite negprop_pure_iff in H
                      | [H: polarity_of {| a := ?f; b := ?g |} ?p = p_pos |- _] => notHyp (bmf_pure_in p f = false); notHyp (bmf_pure_in p g = false); destruct (pos_polarity_not_pure f g p H); try congruence
                      | [H: polarity_of {| a := ?f; b := ?g |} ?p = p_neg |- _] => notHyp (bmf_pure_in p f = false); notHyp (bmf_pure_in p g = false); destruct (neg_polarity_not_pure f g p H); try congruence
                      | [H: polarity_of {| a := ?f; b := ?g |} ?p = p_mixed |- _] => notHyp (bmf_pure_in p f = false); notHyp (bmf_pure_in p g = false); destruct (mixed_polarity_not_pure f g p H); try congruence
                      | [H: polarity_of {| a := ?f; b := ?g |} ?p = p_none |- _] => notHyp (bmf_pure_in p f = true); notHyp (bmf_pure_in p g = true); apply -> none_polarity_pure_iff in H; destruct H as [H1 H2]
                      end.

  Tactic Notation "bmf_tac" := cbn [b_neg bdiaarg_nmap atoms bdiaarg2bmf a b bmf_pure_in bmf_atoms bmf_is_atom' peqb negb is_alpha]; try helper_tac; try if_tac; rewrite ?dne; try auto_purity; eauto with datatypes.
  
  Definition InductiveFormula (f : bmf) := IsRegularFormula f /\ acyclic (dep_digraph f) = true.

  Record InductiveSystem := { hbs : list {e : eqn | IsHeadedBox e.(b) /\ exists n, e.(a) = b_negnom n};
                              hlbs : list {e : eqn | VariableNegative e.(b) /\ exists n, e.(a) = b_negnom n}
                            }. 


  Ltac dont_fail := let H := fresh in
                    match goal with
                    | |- exists a b, (if ?p then (_, fail: _) else _) = (a, succ: b)  => assert (p = false) as H; try rewrite H; eauto with datatypes
                    | |- exists a b, (if ?p then _ else (_, fail: _)) = (a, succ: b) => assert (p = true) as H; try rewrite H; eauto with datatypes
                    end.

  Lemma e_polarity_unfold : forall (f g : bmf_plus) p, polarity_of {| a := f; b := g |} p = polarity_add (polarity_of f p) (polarity_of g p). auto. Qed.
  
  Lemma varneg_polarity : forall (f : bmf_plus) p, VariableNegative f -> bmf_polarity_of p f = p_neg \/ bmf_polarity_of p f = p_none.
    intros f p H.
    induction H; eauto; cbn [bmf_polarity_of].
    1: cbv -[eqb]; destruct eqb; solve [eauto].
    all: destruct IHVariableNegative1 as [H' | H']; rewrite H'.
    all: destruct IHVariableNegative2 as [H'' | H'']; rewrite H''.
    all: eauto.
  Qed.

  Lemma nom_polarity : forall n p, polarity_of (b_nom n : bmf_plus) p = p_none. auto. Qed.
  Lemma negnom_polarity : forall n p, polarity_of (b_negnom n : bmf_plus) p = p_none. auto. Qed.

  Ltac auto_polarity := rewrite ?nom_polarity, ?negnom_polarity, ?polarity_add_neutral_l, ?polarity_add_neutral_r.
  
  Lemma box_equation_solvable : forall (f : bmf_plus), IsBoxFormula f -> forall p s n, exists s' v, rule_transform p s (Build_eqn (b_negnom n) f) = (s', succ: v).
  Proof with repeat bmf_tac.
    intros f H p s n.
    dependent induction f;
      inversion H;
      inversion H0...    
    all: rewrite rule_transform_unfold...
    all: try dont_fail.
    all: cbv [is_alpha is_alpha' is_atom'' is_atom' IsAtom_bmf]...
    1: try destruct (p =? a) eqn:Hp...
    all: cbv [is_beta pure_in]...
    all: rewrite e_polarity_unfold...
    all: auto_polarity...
    (* all: rewrite_all PeanoNat.Nat.eqb_sym... destruct (p ?= x) eqn:Hp... *)
    (* repeat bmf_tac. *)
    (*   simpl. *)
    (* destruct (Nat.eqb p x) eqn:Hp; simpl... *)
    (* 1: cbv [strict_neg_in polarity_of e_polarity mult polarity_monoid polarity_add bmf_Polarity]; *)
    (*   repeat bmf_tac; *)
    (*   try rewrite PeanoNat.Nat.eqb_sym; *)
    (*   rewrite_all Hp; *)
    (*   repeat bmf_tac; *)
    (*   simpl... *)
  Admitted.
  
  Definition headed_count isy : nat := length (hbs isy).
  
  

  Lemma SQEMA_pure : forall (f : bmf), bmf_atoms f = [] -> exists v, SQEMA f = succ: v.
  Admitted.
  Lemma SQEMA_trivial : forall (f : bmf), Forall (fun p => trivial_in f p = true) (bmf_atoms f) -> exists v, SQEMA f = succ: v.
  Admitted.

  Lemma SQEMA_positive : forall (f : bmf), VariablePositive f -> exists v, SQEMA f = succ: v. Admitted.
  Lemma SQEMA_negative : forall (f : bmf), VariableNegative f -> exists v, SQEMA f = succ: v. Admitted.
  Lemma SQEMA_neg_prop : forall p, exists v, @SQEMA nom opu opb (b_negprop p) = succ: v. Admitted.
  Lemma SQEMA_prop : forall p, exists v, @SQEMA nom opu opb (b_prop p) = succ: v. Admitted.
  Lemma SQEMA_nom : forall p, exists v, @SQEMA nom opu opb (b_nom p) = succ: v. vm_compute; eauto. Qed.
  Lemma SQEMA_negnom : forall p, exists v, @SQEMA nom opu opb (b_negnom p) = succ: v. vm_compute; eauto. Qed.
  Lemma SQEMA_verum : exists v, @SQEMA nom opu opb (b_verum) = succ: v. vm_compute; eauto. Qed.
  Lemma SQEMA_falsum : exists v, @SQEMA nom opu opb (b_falsum) = succ: v. vm_compute; eauto. Qed.
  
 
  Lemma variable_negative_neg_variable_positive : forall (f : bmf_plus), variable_negative (b_neg f) = variable_positive f. Admitted.
  Lemma variable_positive_neg_variable_negative : forall (f : bmf_plus), variable_positive (b_neg f) = variable_negative f. Admitted.
  Lemma variable_negative_box_normalize : forall (f : bmf_plus), forallb variable_negative (bmf_box_normalize f) = variable_negative f. Admitted.

  Hint Resolve SQEMA_positive SQEMA_negative SQEMA_neg_prop SQEMA_prop SQEMA_falsum SQEMA_verum SQEMA_negnom SQEMA_nom.

 
  Hint Constructors IsRegularFormula.
  Lemma varpos_acyclic {n} : forall (f : bmf_ n), VariablePositive f -> IsRegularFormula f -> acyclic (dep_digraph f) = true.
  Proof with repeat bmf_tac.
    intros f Hp Hr.
    assert (variable_positive f = true) by admit.
    inversion Hp.
    1,2,3,5: vm_compute; reflexivity.
    1: cbv [dep_digraph occurrence_pairs neg_arguments bmf_regular_formula bmf_box_normalize map b_neg ListUtils.collect];
      unfold fold_right at 2;
      cbv [is_headed_box'' bmf_box_formula bmf_box_normalize partition].
      (* rewrite <- variable_negative_neg_variable_positive in H; *)
    (*   subst; *)
    (*   simpl in *; *)
    (*   rewrite H; *)
    (*   vm_compute; *)
    (*   reflexivity.     *)
    (* all: inversion Hp; try congruence. *)
    (* cbv [dep_digraph occurrence_pairs ListUtils.collect neg_arguments bmf_regular_formula bmf_box_normalize map]; fmap_unfold... *)
    (* simpl. *)
  Admitted.
  Lemma inductive_distributes_conj : forall f g, InductiveFormula (b_conj f g) -> InductiveFormula f /\ InductiveFormula g.
    intros f g [Hr Hac].
    inversion Hr.
    inversion H; subst.
    split; constructor; auto using varpos_acyclic.    
  Qed.

  Lemma bigconj_splits : forall f (fs : list bmf), preprocess (b_bigconj (f :: fs)) = preprocess f ++ preprocess (b_bigconj fs).
  Admitted.
  

  Lemma pure_bigconj {n} : forall (fs : list (bmf_ n)), bmf_pure (b_bigconj fs) = forallb bmf_pure fs.
    induction fs; bmf_tac.
    rewrite bigconj_cons.
    unfold forallb in *.
    replace (bmf_pure (b_conj a (b_bigconj fs))) with (andb (bmf_pure a) (bmf_pure (b_bigconj fs))) by auto.
    destruct (bmf_pure a); auto.
  Qed.
  
  Lemma postprocess_pure_app : forall (a b : list system), bmf_pure (postprocess (a ++ b)) = andb (bmf_pure (postprocess a)) (bmf_pure (postprocess b)).
    intros a b.
    unfold postprocess.
    rewrite_all (pure_bigconj (n := nom + nat)).
    rewrite map_app.
    rewrite forallb_app.
    reflexivity.
  Qed.
  
  Lemma check_purity_post_app : forall (a b : list system), (exists v, check_purity (postprocess (a ++ b)) = succ: v) <-> (exists u, check_purity (postprocess a) = succ: u) /\ (exists v, check_purity (postprocess b) = succ: v).
    intros a b.
    unfold check_purity.
    rewrite postprocess_pure_app.
    destruct (bmf_pure (postprocess a)), (bmf_pure (postprocess b)); simpl; intuition; eauto; try inversion H; try inversion H0;try inversion H1; try inversion H2; try inversion H.
  Qed.

  Lemma co_HeadedBox {n} : forall (f : bmf_ n), IsHeadedBox (b_neg f) -> (exists p, f = b_negprop p) \/ (exists o a, IsHeadedBox (b_neg a) /\ f = b_diau o a) \/ (exists a b, IsHeadedBox (b_neg a) /\ VariablePositive b /\ f = b_conj a b) \/ (exists a b, IsHeadedBox (b_neg b) /\ VariablePositive a /\ f = b_conj a b) \/ (exists o a b, IsHeadedBox (b_neg a) /\ VariablePositive b /\ f = b_diab o a b) \/ (exists o a b, IsHeadedBox (b_neg b) /\ VariablePositive a /\ f = b_diab o a b). Admitted.
  
  Theorem SQEMA_complete_hybrid_poly : forall f, InductiveFormula f -> exists v, SQEMA f = succ: v.
  Proof with (rewrite ?dne; repeat bmf_tac; rewrite ?dne; eauto with datatypes).
    intros f Hi.
    induction f;
      try arg_triv.
  Admitted.

   Theorem SQEMA_complete_conj_hybrid_poly : forall fs, Forall InductiveFormula fs -> exists v, SQEMA (b_bigconj fs) = succ: v.
    unfold SQEMA.
    intros fs H.
    induction fs.
    - vm_compute; eauto.
    - inversion H; subst; mp.
      rewrite bigconj_splits.
      rewrite map_app.
      apply <- check_purity_post_app.
      split; eauto.
      apply SQEMA_complete_hybrid_poly; trivial.
   Qed.

   (*** Completeness wrt Sahlqvist *)

   Inductive BoxedAtom {n} : bmf_ n -> Prop :=
   | ba_prop : forall p : pro, BoxedAtom (b_prop p)
   | ba_box : forall o f, BoxedAtom f -> BoxedAtom (b_boxu o f).

   (** Simple Sahlqvist Antecedent *)
   Inductive SSA {n} : bmf_ n -> Prop :=
   | ssa_verum : SSA b_verum
   | ssa_falsum : SSA b_falsum
   | ssa_ba : forall f, BoxedAtom f -> SSA f
   | ssa_conj : forall f g, SSA f -> SSA g -> SSA (b_conj f g)
   | ssa_diau : forall o f, SSA f -> SSA (b_diau o f)
   | ssa_diab : forall o f g, SSA f -> SSA g -> SSA (b_diab o f g).

 
   Lemma pos_neg_neg {n} (f : bmf_ n) : Positive f -> Negative (b_neg f). Admitted.
   Lemma neg_neg_pos {n} (f : bmf_ n) : Negative f -> Positive (b_neg f). Admitted.
   Lemma trivial_neg_trivial {n} (f : bmf_ n) : Trivial f -> Trivial (b_neg f). Admitted.

   Lemma pure_pos {n} (f : bmf_ n) : IsPure f -> Positive f.
     unfold IsPure, Positive.
     induction f; cbn; introversion; intro p; constructor; auto; rewrite app_nil_iff in H; destruct H; auto.
   Qed.
   Lemma pure_neg {n} (f : bmf_ n) : IsPure f -> Negative f.
     unfold IsPure, Negative.
     induction f; cbn; introversion; intro p; constructor; auto; rewrite app_nil_iff in H; destruct H; auto.
   Qed.

   Lemma BoxedAtom_pos {n} (f : bmf_ n) : BoxedAtom f -> Positive f.
     intro H.
     induction H; intro p'; try constructor; auto. 
   Qed.
   
   Lemma SSA_pos {n} (f : bmf_ n) : SSA f -> Positive f.
     intro H.
     induction H; intro p; try constructor; auto; apply BoxedAtom_pos; auto.
   Qed.
   
   (** Simple Sahlqvist Formula *)
   Inductive SSF : bmf -> Prop :=
   | ssf_impl : forall f g, SSA f -> Positive g -> SSF (f =>> g).

   Hint Constructors SSF SSA BoxedAtom.

   Lemma pos_widening_pos (f : bmf) : Positive f -> Positive (bmf_nmap inl f : bmf_plus). Admitted.
   Lemma neg_widening_neg (f : bmf) : Negative f -> Negative (bmf_nmap inl f : bmf_plus). Admitted.
   Lemma triv_widening_triv (f : bmf) : Trivial f -> Trivial (bmf_nmap inl f : bmf_plus). Admitted.
   Lemma neg_bmf2disj_neg (f : bmf_plus) : Negative f -> Forall Negative (bmf2disj f). Admitted.
   Lemma triv_bmf2conj_triv (f : bmf_plus) : Trivial f -> Forall Trivial (bmf2conj f). Admitted.
   Lemma pure_neg_pure {n} (f : bmf_ n) : IsPure f <-> IsPure (b_neg f).
     induction f; cbn; auto; unfold IsPure in *; cbn; rewrite ?app_nil_iff, ?IHf, ?IHf1, ?IHf2; tauto.
   Qed.
   
   
   Lemma boxed_widening_boxed (f : bmf) : BoxedAtom f -> BoxedAtom (bmf_nmap inl f : bmf_plus).
     intros H.
     induction H; cbn; auto.
   Qed.
   
     
   Lemma neg_widen_neg (f : bmf) : b_neg (bmf_nmap inl (b_neg f) : bmf_plus) = bmf_nmap inl f.
     induction f; auto; cbn; rewrite ?IHf, ?IHf1, ?IHf2; auto.
   Qed.
   
     
   Lemma neg_purity : forall f : bmf_plus, bmf_pure (b_neg f) = bmf_pure f.
     induction f; cbn; auto; rewrite IHf1, IHf2; auto.
   Qed.

   Lemma sys_toformula_purity : forall s : system, bmf_pure (sys_toformula s) = forallb bmf_pure (map eqn_toformula (eqns s)).
     unfold sys_toformula.
     intros [s].
     cbn [eqns].
     induction (map eqn_toformula s); auto.
     cbn.
     rewrite IHl.
     auto.
   Qed.

     
   Lemma traverse_check_purity_succ_iff : forall (l : list bmf_plus), traverse check_purity l = succ: l <-> Forall IsPure l. Admitted.

   Lemma Trivial_dec : forall (f : bmf), {Trivial f} + {~ Trivial f}. Admitted.

   Lemma trivial_in_i : forall (f : bmf_plus) p, trivial_in {|a:=b_negnom (inr 0); b:= f|} p = trivial_in f p.
     intros f p.
     unfold trivial_in.
     cbn [polarity_of e_polarity a mult polarity_monoid].
     assert (PureIn p (b_negnom (inr 0) : bmf_plus)) as H by constructor.
     apply (reflect_iff _ _ (PureIn_reflect' _ _)) in H.
     unfold "p=?" in H.
     match_hyp; try congruence.
     rewrite polarity_add_neutral_l.
     auto.
   Qed.

   Lemma boxed_atom_undisj : forall f, BoxedAtom f -> bmf2disj (bmf_nmap inl f : bmf_plus) = [b_conj (bmf_nmap inl f) b_verum] \/ bmf2disj (bmf_nmap inl f : bmf_plus) = [bmf_nmap inl f]
   with boxed_atom_unconj : forall f, BoxedAtom f -> bmf2conj (bmf_nmap inl f : bmf_plus) = [bmf_nmap inl f].
     all: intros.
     all: induction H; [cbn; auto | cbn [b_neg bmf_nmap bmf2disj bmf2conj]].
     all: rewrite boxed_atom_unconj; auto.
   Qed.

   Lemma SSA_disj_SSA : forall f, SSA f -> Forall SSA (bmf2disj (bmf_nmap inl f : bmf_plus))
   with SSA_conj_SSA : forall f, SSA f -> Forall SSA (bmf2conj (bmf_nmap inl f : bmf_plus)).
     all: intros f H.
     all: induction H.
     1,2,7,8: cbn; constructor; auto. 
     1: destruct (boxed_atom_undisj _ H) as [H' | H']; rewrite H'; constructor; auto; [constructor 4 | idtac]; auto; apply boxed_widening_boxed in H; auto.
     4: rewrite (boxed_atom_unconj _ H); constructor; auto; constructor; apply boxed_widening_boxed; auto.
     all: cbn; rewrite <- ?Forall_app; auto.
   Admitted.

   Lemma eliminate_only_b : forall f : bmf_plus, eliminate_trivial {| a := b_negnom (inr 0); b := f |} = {| a:= b_negnom (inr 0); b := fold_right elim_triv f (bmf_atoms f)|}.
   Admitted.
   
   Lemma bigconj_pure : forall fs : list bmf_plus, IsPure (b_bigconj fs) <-> Forall IsPure fs.
     induction fs; cbn; auto.
     all: split; intro H; auto.
     constructor.
     cbv [IsPure] in H.
     cbn in H. rewrite app_nil_iff in H.
     destruct H.
     constructor; auto.
     apply IHfs; auto.
     inversion H.
     subst.
     apply IHfs in H3.
     unfold IsPure.
     cbn.
     rewrite app_nil_iff.
     auto.
   Qed.
   
   Theorem SQEMA'_SSF_complete : forall f, SSF f -> SQEMA' f = succ: (postprocess' (map (fun e : bmf_plus => eliminate_all (eliminate_trivial (prefix_i e))) (preprocess f))).
     intros f H.
     inversion H.
     subst.
     clear H.
     unfold SQEMA'.
     rewrite traverse_check_purity_succ_iff.
     unfold postprocess'.
     rewrite <- map_map.
     rewrite <- Forall_map with (P := fun f' : bmf_plus => IsPure (b_neg f')); try tauto.
     apply Forall_impl with (P := IsPure).
     apply pure_neg_pure.
     rewrite <- Forall_map with (P := fun s : system => IsPure (sys_toformula s)); try tauto.
     unfold sys_toformula.
     rewrite <- map_map.
     rewrite <- Forall_map with (P := fun e : eqn => IsPure (b_bigconj (map eqn_toformula (eqns (eliminate_all e))))); try tauto.
     rewrite Forall_forall in *.
     intros x Ix.
     rewrite bigconj_pure.
     (* pose proof (only_nontriv_after_elim_triv (f0 =>> g)) as H. *)
     (* rewrite Forall_forall in H. *)
     (* pose proof (H x Ix) as H'. *)
     (* clear H. *)
     (* rewrite in_map_iff in Ix. *)
     (* destruct Ix as [f' [Ef' If']]. *)
     (* induction Sx; cbn -[elim_triv bmf_pure]. *)
     (** produces useless IHs because being trivial in something can't generally be joined*)
     (* all: try apply IHSSA; auto. *)
     (* 4: intro F; apply N; constructor; auto. *)
     (* 4: intro p; pose proof (Po p) as H'; inversion H'; auto. *)
     (* Focus 4. *)
     (* all: induction x'; cbn -[elim_triv] in *. *)
     (* all: let H' := fresh "H'" in *)
     (*      match goal with *)
     (*      | H : context Negative [b_prop ?p] |- _ => pose proof (H p) as H'; inversion H'; try congruence *)
     (*      | H : context Positive [b_negprop ?p] |- _ => pose proof (H p) as H'; inversion H'; try congruence *)
     (*      | |- ?ow => idtac *)
     (*      end. *)
     (* all: rewrite ?app_nil_r, ?app_nil_r in *. *)
   Admitted.
End SQEMAComplete.