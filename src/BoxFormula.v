From SQEMA Require Import ML Reify CT Polarity Atoms ListUtils OptionUtils BiModal Polarity.

Require Import Vectors.Vector.
Require Import Bool.Bool.
Require Import Lists.List.
Require Export Classes.EquivDec.
Require Import Nat.

Import List.ListNotations.
Import ListUtils.
Section BoxFormula.
  Context {nom opu opb : Set} {ops : nat -> Set}.
  Definition atom := nat.
  
  Definition IsBox_BPL : @BPL atom nom bool :=
    {| verum := true; (* because of special symbils *)
       falsum := true; (* because of special symbols *)
       prop p := false;
       nomi n := false;
       conj a b := false; (* diamond_iota_2 *)
       disj a b := true; (* box_iota_2 *)
    |}.

  Definition IsBox_PL : @PL atom nom bool :=
    {| B := IsBox_BPL;
       neg _ := false;
    |}.

  Definition IsBox_UML : @UML opu bool :=
    {| diau _ _ := false;
       boxu _ _ := true
    |}.

  Definition IsBox_BML : @BML opu opb bool :=
    {| BU := IsBox_UML;
       diab _ _ _ := false;
       boxb _ _ _ := true
    |}.

  Definition IsBox_PML : @PML ops bool :=
    {| pdia _ _ _ := false;
       pbox _ _ _ := true
    |}.


  Class IsBox M := { is_box : M -> bool }.
  Global Instance IsBox_pml {M} `{Reify _ M _} : IsBox M := {| is_box := reify (IsBox_PML,IsBox_PL) |}.
  Global Instance IsBox_bml {M} `{Reify _ M _} : IsBox M := {| is_box := reify (IsBox_BML, IsBox_PL) |}.
  
  Definition IsAtom_BPL : @BPL atom nom (option atom) :=
    {| verum := None; 
       falsum := None;
       prop p := Some p;
       nomi n := None;
       conj a b := None; 
       disj a b := None;
    |}.

  Definition IsAtom_PL : @PL atom nom (option atom) :=
    {| B := IsAtom_BPL;
       neg _ := None;
    |}.

  Definition IsAtom_UML : @UML opu (option atom) :=
    {| diau _ _ := None;
       boxu _ _ := None
    |}.

  Definition IsAtom_BML : @BML opu opb (option atom) :=
    {| BU := IsAtom_UML;
       diab _ _ _ := None;
       boxb _ _ _ := None
    |}.

  Definition IsAtom_PML : @PML ops (option atom) :=
    {| pdia _ _ _ := None;
       pbox _ _ _ := None
    |}.


  Class IsAtom M := { is_atom' : M -> option atom;
                      is_atom : M -> bool := fun f => match is_atom' f with Some _ => true | None => false end;
                      is_atom'' : atom -> M -> bool := fun p f => match is_atom' f with Some x => p =? x | None => false end;
                      get_atom : forall m, is_atom m = true -> atom :=
                        fun f p => match is_atom' f as ia return is_atom' f = ia -> atom with
                                | Some a => fun _ => a
                                | None => fun p' => ltac:(unfold is_atom in *; match_hyp; congruence)
                                end eq_refl
                    }.

  Global Instance IsAtom_bmf : IsAtom (@bmf opu opb nom) :=
    {| is_atom' := bmf_is_atom'
    |}.
  Global Instance IsAtom_pml {M} `{Reify _ M _} : IsAtom M :=
    {| is_atom' m := reify (IsAtom_PML,IsAtom_PL) m |}.
  Global Instance IsAtom_bml {M} `{Reify _ M _} : IsAtom M :=
    {| is_atom' m := reify (IsAtom_BML, IsAtom_PL) m |}.
  
  Definition DSF_BPL {M} `{@BPL atom nom M} : @BPL atom nom ((list M) * M) :=
    {| verum := ([], verum); 
       falsum := ([], falsum);
       prop p := ([], prop p);
       nomi n := ([], nomi n);
       conj a b := ([snd a; snd b], conj (snd a) (snd b)); (* could they be true due to iota2? *)
       disj a b := ([snd a; snd b], disj (snd a) (snd b))
    |}.

  Definition DSF_PL {M} `{@PL atom nom M} : @PL atom nom ((list M) * M) :=
    {| B := DSF_BPL;
       neg f := ([snd f], neg (snd f));
    |}.

  Definition DSF_UML {M} `{@UML opu M} : @UML opu ((list M) * M) :=
    {| diau o a := ([snd a], diau o (snd a));
       boxu o a := ([snd a], boxu o (snd a))
    |}.

  Definition DSF_BML {M} `{@BML opu opb M} : @BML opu opb ((list M) * M) :=
    {| BU := DSF_UML;
       diab o a b := ([snd a; snd b], diab o (snd a) (snd b));
       boxb o a b := ([snd a; snd b], boxb o (snd a) (snd b))
    |}.
  
  Definition DSF_PML {M} `{@PML ops M} : @PML ops ((list M) * M) :=
    {| pdia n mo args := let args' := Vector.map snd args in (Vector.to_list args', pdia n mo args');
       pbox n mo args := let args' := Vector.map snd args in (Vector.to_list args', pbox n mo args') 
    |}.

  (* Direct Subformulas *)
  Class DSF M N := { dir_subformulas : M -> list N }.
  Global Instance DSF_bml {M N} `{@PL atom nom N} `{@BML opu opb N} `{Reify _ M _} : DSF M N :=
    {| dir_subformulas := fun m => @fst _ N (reify (DSF_BML, DSF_PL) m)
    |}. 

  Global Instance DSF_pml {M N} `{@PL atom nom N} `{@PML ops N} `{Reify _ M _} : DSF M N :=
    {| dir_subformulas := fun m => @fst _ N (reify (DSF_PML, DSF_PL) m)
    |}. 
  
  Class BoxFormula M := { is_headed_box'' : M -> option (atom * list atom);
                          is_headless_box : M -> bool;
                          is_headed_box' : M -> option atom := fun m => fmap fst (is_headed_box'' m);
                          is_headed_box : M -> bool := fun m => option_ (fun _ => true) false (is_headed_box' m);
                          is_box_formula : M -> bool := fun m => orb (is_headless_box m) (is_headed_box m)
                        }.
  Class RegularFormula M `{@PL atom nom M} `{BoxFormula M} := { neg_arguments : M -> list M;
                                                                is_regular_formula : M -> bool := fun m => forallb is_box_formula (neg_arguments m);
                                                                occurrence_pairs : M -> list (atom * list atom) := fun m => ListUtils.collect is_headed_box'' (neg_arguments m) 
                                                              }.

  (** Accumulate all leading boxes returning all resulting box arguments *)
  Fixpoint bmf_box_normalize f : list (@bmf opu opb nom) :=
    match f with
    | b_verum => [b_verum]
    | b_falsum => [b_falsum]
    | b_prop x => [b_prop x]
    | b_nom x => [b_nom x]
    | b_negprop x => [b_negprop x]
    | b_negnom x => [b_negnom x]
    | b_conj a b => [b_conj a b]
    | b_disj a b => (bmf_box_normalize a) ++ (bmf_box_normalize b) (* following boxes can be accumulated into this box *)
    | b_diau x x0 => [b_diau x x0]
    | b_boxu x x0 => bmf_box_normalize x0 (* following boxes can be accumulated into this box *)
    | b_diab x x0 x1 => [b_diab x x0 x1]
    | b_boxb x x0 x1 => (bmf_box_normalize x0) ++ (bmf_box_normalize x1)
    end.
  
  
  Global Instance bmf_box_formula : BoxFormula (@bmf opu opb nom) :=
    {| is_headless_box f := variable_negative f;
       is_headed_box'' f := match partition variable_negative (bmf_box_normalize f) with
                            | (n, [b_prop x]) => Some (x, bind atoms n)
                            | _ => None
                            end
    |}.

  Definition IsHeadlessBox : @bmf opu opb nom -> Prop := VariableNegative.
  Inductive IsHeadedBox : @bmf opu opb nom -> Prop :=
  | HB_prop x : IsHeadedBox (b_prop x)
  | HB_boxu o a : IsHeadedBox a -> IsHeadedBox (b_boxu o a)
  | HB_disj_l a b : IsHeadedBox a -> VariableNegative b -> IsHeadedBox (b_disj a b)
  | HB_disj_r a b : IsHeadedBox b -> VariableNegative a -> IsHeadedBox (b_disj a b)
  | HB_boxb_l o a b : IsHeadedBox a -> VariableNegative b -> IsHeadedBox (b_boxb o a b)
  | HB_boxb_r o a b : IsHeadedBox b -> VariableNegative a -> IsHeadedBox (b_boxb o a b)
  .
  
  Definition IsBoxFormula f := IsHeadedBox f \/ IsHeadlessBox f.
  Hint Unfold IsHeadlessBox IsBoxFormula.
  Hint Constructors IsHeadedBox.
  
  Global Program Instance bmf_regular_formula : RegularFormula (@bmf opu opb nom) :=
    {| neg_arguments f := map b_neg (bmf_box_normalize f)
    |}.

  Inductive IsRegularFormula : @bmf opu opb nom -> Prop :=
  | RF_negprop x : IsRegularFormula (b_negprop x)
  | RF_pos f : VariablePositive f -> IsRegularFormula f
  | RF_disj_l a b : IsHeadedBox (b_neg a) -> VariablePositive b -> IsRegularFormula (b_disj a b)
  | RF_disj_r a b : IsHeadedBox (b_neg b) -> VariablePositive a -> IsRegularFormula (b_disj a b)
  | RF_boxu o a : IsHeadedBox (b_neg a) -> IsRegularFormula (b_boxu o a)
  | RF_boxb_l o a b : IsHeadedBox (b_neg a) -> VariablePositive b -> IsRegularFormula (b_boxb o a b)
  | RF_boxb_r o a b : IsHeadedBox (b_neg b) -> VariablePositive a -> IsRegularFormula (b_boxb o a b)
  .
End BoxFormula.