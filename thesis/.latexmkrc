$pdf_mode = 1;
$pdflatex = 'lualatex -shell-escape --synctex=1 %O -interaction=nonstopmode %S; languagetool-commandline --api -c utf-8 -l en-GB -m de-DE %B.spell.txt > %B.spell.xml';
