Require Import Relations.
Require Import Program.Tactics.
Require Vectors.Vector.
Require Import Lists.List.

Definition assoc {T} (r : T -> T -> T) := forall a b c, r (r a b) c = r a (r b c).
Definition comm {T} (r : T -> T -> T) := forall a b, r a b = r b a.
Definition dist {T} (r s : T -> T -> T) := forall a b c, r a (s b c) = s (r a b) (r a c).
Definition idem {T} (r : T -> T -> T) := forall a, r a a = a.

Hint Unfold assoc comm dist idem : sets.

Section algebra.
  Class Monoid T := { mult : T -> T -> T;
                      one : T;
                      mult_assoc : assoc mult;
                      one_neutral_r : forall t, mult t one = t;
                      one_neutral_l : forall t, mult one t = t
                    }.
  Class Group T `{Monoid T} := { inv : T -> T;
                                 mult_comm : comm mult;
                                 inv_ok : forall t, mult t (inv t) = one
                               }.
  
  Class Lattice B := { and : B -> B -> B;
                       or : B -> B -> B;
                       verum : B;
                       falsum : B;
                       and_assoc : assoc and;
                       and_comm : comm and;
                       and_id_l : forall a, a = and a verum;
                       and_idem : idem and;
                       and_anni_l : forall a, and a falsum = falsum;
                       or_assoc : assoc or;
                       or_comm : comm or;
                       or_id_l : forall a, a = or a falsum;
                       or_anni_l : forall a, or a verum = verum;
                       and_or_absorb : forall a b, and a (or a b) = a;
                       or_and_absorb : forall a b, or a (and a b) = a
                     }. (* TML: some of these are derivable in presence of others. *)


  Corollary and_id_r `{Lattice} : forall a, a = and verum a. intro. rewrite and_comm. exact (and_id_l _). Qed.
  Corollary and_anni_r `{Lattice} : forall a, and falsum a = falsum. intro. rewrite and_comm. exact (and_anni_l _). Qed.
  Corollary or_id_r `{Lattice} : forall a, a = or falsum a. intro. rewrite or_comm. exact (or_id_l _). Qed.
  Corollary or_anni_r `{Lattice} : forall a, or verum a = verum. intro. rewrite or_comm. exact (or_anni_l _). Qed.
  Corollary or_idem  `{Lattice} : idem or. intro. pose proof (or_and_absorb a a) as H1. rewrite and_idem in H1. assumption. Qed.

                                           
  Ltac lattice := repeat match goal with
                  | [ |- context[and _ verum]] => rewrite <-and_id_l
                  | [ |- context[and verum _]] => rewrite <-and_id_r
                  | [ |- context[or _ falsum]] => rewrite <-or_id_l
                  | [ |- context[or falsum _]] => rewrite <-or_id_r
                  | [ |- context[or ?a ?a]] => rewrite or_idem
                  | [ |- context[and ?a ?a]] => rewrite and_idem
                  | [ |- context[and ?a (or ?a _)]] => rewrite and_or_absorb
                  | [ |- context[or ?a (and ?a _)]] => rewrite or_and_absorb
                  (* | [ H : Lattice _ |- _ ] => try rewrite and_anni_r; try rewrite and_anni_l; try rewrite or_idem; try rewrite and_idem  *)
                  end.
  
  Hint Resolve and_assoc and_comm and_id_l and_id_r and_idem and_anni_l and_anni_r or_assoc or_comm or_id_l or_id_r or_idem or_anni_l or_anni_r and_or_absorb or_and_absorb : sets.
  
  Class Poset S := { lte : relation S;
                     lte_order : order S lte
                   }.

  Hint Resolve lte_order : sets.
  Program Instance lattice_poset {A} `{Lattice A} : Poset A := {| lte := fun a b => a = and a b |}.
  Obligation 1. constructor.
  - intro. lattice. reflexivity.
  - intros a b c H0 H1. rewrite H0, and_assoc, <- H1. reflexivity.
  - intros x y H0 H1. rewrite H0, and_comm, <- H1. reflexivity.
  Defined.
  (* I would expect auto with sets to solve all the obligations but it fails completely at every step of all of the required proofs... Why? *) (* TML: Some Hint Unfold missing, perhaps? I don't know.*)

  (** Heyting algebra *)
  Class HeyA S `{Lattice S} := { impl : S -> S -> S;
                                 impl_ok : forall a c b, impl a c = b <-> lte (and a b) c;
                                 impl_refl : forall a, impl a a = verum;
                                 impl_and_a : forall a c, and a (impl a c) = and a c;
                                 impl_and_c : forall a c, and c (impl a c) = c;
                                 impl_dist : forall a c1 c2, impl a (and c1 c2) = and (impl a c1) (impl a c2)
                               }.

  Definition neg `{HeyA} a := impl a falsum.
  
  Hint Resolve impl_ok impl_refl impl_and_a impl_and_c impl_dist : sets.
  Hint Unfold neg : sets.

  Definition em `{HeyA} := forall a, or a (neg a) = verum.

  (** Boolean algebra *)
  Class BA S `{HeyA S} := { ba_em : em }.

  Hint Resolve ba_em : sets.
  
  Definition operator_t {S} n := Vector.t S n -> S.
  Definition additive {S} `{HeyA S} {n} (m : operator_t n) := forall i args y, let x := Vector.nth args i in or (m args) (m (Vector.replace args i y)) = m (Vector.replace args i (or x y)).
  Definition multiplicative {S} `{HeyA S} {n} (m : operator_t n) := forall i args y, let x := Vector.nth args i in and (m args) (m (Vector.replace args i y)) = m (Vector.replace args i (and x y)).
  Definition additive_normal {S} `{HeyA S} {n} (m : operator_t n) := forall i args, m (Vector.replace args i falsum) = falsum.
  Definition multiplicative_normal {S} `{HeyA S} {n} (m : operator_t n) := forall i args, m (Vector.replace args i verum) = verum.

  (** Heyting algebra with operators *)
  Class HAO S `{HeyA S} (ops : nat -> Set) (o : forall n, ops n -> operator_t n) := { ops_additive {n} : forall op, additive (o n op) }.
  Class mHAO S `{HeyA S} (ops : nat -> Set) (o : forall n, ops n -> operator_t n) := { ops_multiplicative {n} : forall op, multiplicative (o n op) }.
  
  Definition Normal S {ops o} `{HAO S ops o} := forall n op, additive_normal (o n op).
  Definition mNormal S {ops o} `{mHAO S ops o} := forall n op, multiplicative_normal (o n op).

  Hint Unfold operator_t additive multiplicative additive_normal multiplicative_normal Normal mNormal : sets.
  Hint Resolve ops_additive ops_multiplicative : sets.

  (** Boolean algebra with operators *)
  Class BAO S ops o `{HAO S ops o} := { bao_em : em }.
  (* Blackburn, de Rijke and Venema call this a BAO iff it is Normal according to the definition of Goldblatt (every operator normal) *)
  
  Hint Resolve bao_em : sets.

  (** BAO is also a BA; Note that this is necessary to ensure coherence of the contained heyting algebra *)
  Program Instance bao_ba {S} {ops} `{BAO S ops} : BA S := {| ba_em := bao_em |}.
End algebra.

Infix "<*>" := (mult) (at level 80).
Notation "<1>" := (one).