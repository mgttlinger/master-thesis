Require Export Classes.EquivDec.
Require Export Bool.

Ltac notHyp P := (* From FRAP code https://github.com/achlipala/frap/blob/master/IntroToProofScripting.v *)
  match goal with
  | [ _ : P |- _ ] => fail 1
  (* A hypothesis already asserts this fact. *)
  | _ =>
    match P with
    | ?P1 /\ ?P2 =>
      (* Check each conjunct of [P] separately, since they might be known by
       * different means. *)
      first [ notHyp P1 | notHyp P2 | fail 2 ]
    | _ => idtac
    (* If we manage to get this far, then we found no redundancy, so
     * declare success. *)
    end
  end.

Ltac extend pf := (* From FRAP code https://github.com/achlipala/frap/blob/master/IntroToProofScripting.v *)
  let t := type of pf in
  notHyp t; pose proof pf.

Lemma neq_sym {T} : forall (a b : T), ~ a = b <-> ~ b = a.
  intros a b. split; intros H Hf; apply H; symmetry; exact Hf.
Qed.

Tactic Notation "introversion" := let H := fresh in intro H; inversion H.

Ltac mp := repeat match goal with
           | [H : _ /\ _ |- _] => destruct H
           | [H1 : ?A -> _, H2 : ?A |- _] => extend (H1 H2)
           end.

Ltac apply_deq := match goal with
                  | [H : _ === _ |- _] => inversion H; subst
                  end.

Lemma forall_dist {T} : forall (g h : T -> Prop), ((forall (f : T), (g f) /\ (h f)) <-> (forall (f : T), (g f)) /\ (forall (f : T), (h f))).
  intros g h.
  split; intro H.
  - split; intros; mp; tauto.
  - destruct H; intros; mp; tauto.
Qed.

Lemma forall_dist' {T} : forall (i g h : T -> Prop), ((forall (f : T), (i f) -> (g f) /\ (h f)) <-> (forall (f : T), (i f) -> (g f)) /\ (forall (f : T), (i f) -> (h f))).
  intros g h.
  split; intro H.
  - split; intros; mp; tauto.
  - destruct H; intros; mp; tauto.
Qed.

Lemma exists_dist {T} : forall (Q R : T -> Prop), (exists t, (Q t \/ R t)) <-> ((exists t, Q t) \/ (exists t, R t)).
  intros.
  split.
  - intros [t [Qt | Rt]]; [left | right]; exists t; intuition.
  - intros [[t Qt] | [t Rt]]; exists t; intuition.
Qed.

Lemma exists_dist' {T} : forall (P Q R : T -> Prop), (exists t, P t /\ (Q t \/ R t)) <-> ((exists t, P t /\ Q t) \/ (exists t, P t /\ R t)).
  intros.
  split.
  - intros [t [Pt [Qt | Rt]]]; [left | right]; exists t; intuition.
  - intros [[t [Pt Qt]] | [t [Pt Rt]]]; exists t; intuition.
Qed.

Lemma impl_dist : forall (i g h : Prop), ((i -> g /\ h) <-> (i -> g) /\ (i -> h)).
  intros i g h.
  split; intro H.
  - split; intros; mp; tauto.
  - destruct H; intros; mp; tauto.
Qed.

Lemma triv_impl {T} : forall (f g : T -> Prop), (forall (h : T), g h) -> (forall (h : T), (f h) -> (g h)). auto. Qed.

Tactic Notation "dec" ident(a) ident(b) := destruct (a == b); try apply_deq.

Lemma equivb_refl {A} `{EqDec A eq} : forall a, a ==b a = true.
  intro. unfold equiv_decb, equiv_dec. destruct (H a a); auto. congruence.
Qed.

Lemma equivb_refl' {A} `{EqDec A eq} : forall a b, a = b <-> a ==b b = true.
  intros. unfold equiv_decb, equiv_dec. destruct (H a b); auto.
  - inversion e. split; congruence.
  - split; intros; congruence.
Qed.

Lemma equivb_neg_refl {A} `{EqDec A eq} : forall a b, ~ a = b <-> a ==b b = false.
  intros. unfold equiv_decb, equiv_dec. destruct (H a b); auto.
  - inversion e. split; congruence.
  - split; intros; congruence.
Qed.

Ltac deceq := repeat let H' := fresh "D" in match goal with
                     | [ |- context[?p ==b ?p]] => rewrite (equivb_refl p)
                     | [ |- context[if ?p then _ else _]] => destruct p eqn:H'
                     | |- match ?p with _ => _ end => destruct p eqn:H'
                     | [H : _ === _  |- _] => rewrite H in *; clear H
                     | [|- {?x = ?y} + {?x <> ?y}] => destruct (x == y); [left; auto | right; introversion; congruence]
                     end.

Lemma f_if {T U} : forall (f : T -> U) (p : bool) a b, (f (if p then a else b)) = if p then f a else f b.
  destruct p; auto.
Qed.

Lemma f_match_option {T U V} : forall (f : T -> U) (e : option V) a b, f (match e with Some x => a x | None => b end) = match e with Some x => f (a x) | None => f b end. destruct e; auto. Qed.

Lemma match_if {T U} : forall (p : bool) (a : T) (b : T -> U) (c : U), match (if p then Some a else None) with
                                                              | Some x => (b x)
                                                              | None => c
                                                              end = if p then (b a) else c.
  intros p a b c. destruct p; reflexivity.
Qed.

Lemma if_flatten {t} : forall (a : bool) (b c o : t), (if a then b else if a then o else c) = if a then b else c. destruct a; auto. Qed.
Lemma if_useless {t} : forall (a : bool) (b : t), (if a then b else b) = b. destruct a; auto. Qed.
Lemma if_else_conj {t} : forall (a b : bool) (c d e f : t), (if andb a b then c else if a then if b then d else e else f) = if andb a b then c else if a then e else f. destruct a, b; auto. Qed.

Hint Immediate f_if match_if if_flatten if_useless if_else_conj.

Tactic Notation "if_tac" := rewrite ?f_if, ?match_if, ?if_flatten, ?if_useless, ?if_else_conj.

Ltac match_hyp' H :=
  match goal with
  | [_ : match ?p with _ => _ end = _ |- _] => destruct p eqn:H
  | [_ : _ = match ?p with _ => _ end |- _] => destruct p eqn:H
  | [_ : _ = if ?p then _ else _ |- _] => destruct p eqn:H
  | [_ : if ?p then _ else _ = _  |- _] => destruct p eqn:H
  end.

Tactic Notation "match_hyp" := let H := fresh in match_hyp' H.

Ltac clear_existT := match goal with
                     | [He : existT ?P ?x _ = existT ?P ?x _ |- _] => apply Eqdep.EqdepTheory.inj_pair2 in He; subst
                     end.

Tactic Notation "uneq" := right; introversion; repeat clear_existT; congruence.

Lemma reflect_not_iff : forall P f, reflect P f -> f = false <-> ~ P.
  intros P f R.
  split; intro H.
  - intro Hp.
    apply -> reflect_iff in Hp; eauto.
    congruence.
  - apply not_true_is_false.
    intro Hf.
    apply H.
    apply <- reflect_iff; eauto.
Qed.

Definition codiagonal {A} (e : A + A) : A := match e with
                                             | inl x => x
                                             | inr x => x
                                             end.

Definition diagonal {A} (a : A) : A * A := (a, a).