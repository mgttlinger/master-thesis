\chapter{The Convoy Pattern}
\label{app:convoy}

The convoy pattern is used when the body of a match case needs access to the contextual information which case was taken.
In a regular pattern match on an expression \mintinline{coq}{expr : bool} like this
\begin{minted}[gobble=2]{coq}
  match expr with
  | true => f
  | false => g
  end.
\end{minted}
the expressions \mintinline{coq}{f : T} and \mintinline{coq}{g : T} do not have access to the information that\linebreak \mintinline{coq}{expr = true} or \mintinline{coq}{expr = false} respectively.

The convoy pattern uses dependent pattern matching and dependent functions to modify this pattern match by rewriting it to
\begin{minted}[gobble=2]{coq}
  (match expr as v return expr = v -> T with
  | true => fun p => f
  | false => fun p => g
  end eq_refl).
\end{minted}
which now provides the required information in \mintinline{coq}{p}.
Thus, it changes the type of the match expression into a function which gets applied to the equality proof to give back a value of the original type \mintinline{coq}{T} of the old expression.

This is done automatically when using \mintinline{coq}{Program} but makes reasoning about those pattern matches much more complicated even if \mintinline{coq}{f} and \mintinline{coq}{g} do not actually use the proof values.
Therefore this pattern should be avoided when not actually needed to prevent complications in proving and unnecessary term blow-up.

When using \mintinline{coq}{Program} this rewriting can be prohibited by adding (in our example) \mintinline{coq}{return T} to the match statement.
Then \mintinline{coq}{Program} will not be able to add a different \mintinline{coq}{return} annotation to change the type of the expression.

\chapter{\acs{CPS} Encoded Typeclasses}
\label{app:cps}

Normally a typeclass witnessing that a datatype behaves functorially would be implemented along the lines of the following code.
\begin{minted}[gobble=2]{coq}
  Class Functor (f : Type -> Type) := { fmap {a b} : (a -> b) -> f a -> f b }.
\end{minted}
Then every call to \mintinline{coq}{fmap} will cause pattern matching on a record as this is how Coq handles typeclasses.

Alternatively we can use \ac{CPS} to have those calls cause function calls instead.
\begin{minted}[gobble=2]{coq}
  Context {f : Type -> Type}.
  
  Record Functor__Dict := {
    fmap__ {a b} : (a -> b) -> f a -> f b;
  }.
  Definition Functor := forall r, (Functor__Dict -> r) -> r.
  Existing Class Functor.
  
  Definition fmap `{F : Functor} {a b} : (a -> b) -> f a -> f b :=
    F _ (fmap__) a b.
\end{minted}
The way this works is that we define a record (here \mintinline{coq}{Functor__Dict}) which has the signature we wanted for our typeclass.
Then we define a \ac{CPS} function which uses this record to compute the desired result and declare that as our typeclass.
Afterwards we define a \ac{CPS} transformed wrapper for each function of the signature by using the record accessors and the \ac{CPS} function we just defined.

It is crucial here that, when implementing that typeclass instance, the record (in our example \mintinline{coq}{Functor__Dict}) is not instantiated by itself but rather defined inline in the implementation.
An instantiation for the \mintinline{coq}{List} functor for example would look like this.
\begin{minted}[gobble=2]{coq}
  Global Instance list_Functor : @Functor list :=
    fun r f => f {| fmap__ := map;
                |}.
\end{minted}
  
This trick was discovered by \textcite{spector2017total} to get around unfolding issues due to pattern matches on records not getting unfolded automatically.