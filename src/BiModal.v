From SQEMA Require Import Utils ML Reify ListUtils.

Require Import Lists.List.
Require Import Nat.

Import ListNotations.
Section BiModal.
  (** Unary, binary modalities and nominals *)
  Context {opu opb nom : Set}.

  (** Fixed atoms to natural numbers as this covers all use-cases and it does no harm to have infinitely many *)
  Definition atom := nat.

  (** Modal formula in NNF with unary and binary modalities *)
  Inductive bmf : Set :=
  | b_verum : bmf
  | b_falsum : bmf
  | b_prop : atom -> bmf
  | b_nom : nom -> bmf
  | b_negprop : atom -> bmf
  | b_negnom : nom -> bmf
  | b_conj : bmf -> bmf -> bmf
  | b_disj : bmf -> bmf -> bmf
  | b_diau : opu -> bmf -> bmf
  | b_boxu : opu -> bmf -> bmf
  | b_diab : opb -> bmf -> bmf -> bmf
  | b_boxb : opb -> bmf -> bmf -> bmf.

  (** checks if formula is pure *)
  Fixpoint bmf_pure f : bool := match f with
                             | b_verum => true
                             | b_falsum => true
                             | b_prop x => false
                             | b_nom x => true
                             | b_negprop x => false
                             | b_negnom x => true
                             | b_conj x x0 => bmf_pure x && bmf_pure x0
                             | b_disj x x0 => bmf_pure x && bmf_pure x0
                             | b_diau x x0 => bmf_pure x0
                             | b_boxu x x0 => bmf_pure x0
                             | b_diab x x0 x1 => bmf_pure x0 && bmf_pure x1
                             | b_boxb x x0 x1 => bmf_pure x0 && bmf_pure x1
                             end.

  (** checks if formula is pure in the given atom *)
  Definition bmf_pure_in p : bmf -> bool := fix pui f := match f with
                                                     | b_verum => true
                                                     | b_falsum => true
                                                     | b_prop x => negb (p =? x)
                                                     | b_nom x => true
                                                     | b_negprop x => negb (p =? x)
                                                     | b_negnom x => true
                                                     | b_conj x x0 => andb (pui x) (pui x0)
                                                     | b_disj x x0 => andb (pui x) (pui x0)
                                                     | b_diau x x0 => pui x0
                                                     | b_boxu x x0 => pui x0
                                                     | b_diab x x0 x1 => andb (pui x0) (pui x1)
                                                     | b_boxb x x0 x1 => andb (pui x0) (pui x1)
                                                     end.
  Inductive PureIn p : bmf -> Prop :=
  | p_verum : PureIn p b_verum
  | p_falsum : PureIn p b_falsum
  | p_prop : forall x, ~ x = p -> PureIn p (b_prop x)
  | p_nom : forall n, PureIn p (b_nom n)
  | p_negprop : forall x, ~ x = p -> PureIn p (b_negprop x)
  | p_negnom : forall n, PureIn p (b_negnom n)
  | p_conj : forall a b, PureIn p a -> PureIn p b -> PureIn p (b_conj a b)
  | p_disj : forall a b, PureIn p a -> PureIn p b -> PureIn p (b_disj a b)
  | p_diau {o} : forall a, PureIn p a -> PureIn p (b_diau o a)
  | p_boxu {o} : forall a, PureIn p a -> PureIn p (b_boxu o a)
  | p_diab {o} : forall a b, PureIn p a -> PureIn p b -> PureIn p (b_diab o a b)
  | p_boxb {o} : forall a b, PureIn p a -> PureIn p b -> PureIn p (b_boxb o a b)
  .

  Hint Constructors PureIn.
  Lemma PureIn_reflect : forall p f, reflect (PureIn p f) (bmf_pure_in p f).
    induction f; cbn [bmf_pure_in]; try destruct eqb eqn:He; rewrite ?PeanoNat.Nat.eqb_eq, ?PeanoNat.Nat.eqb_neq in *; subst.
    1-8: constructor; auto; introversion; congruence.
    all: destruct bmf_pure_in; try destruct bmf_pure_in.
    all: try inversion IHf1; try inversion IHf2; try inversion IHf; constructor; auto.
    all: introversion; congruence.
  Qed.
  
  (** atoms in a formula *)
  Fixpoint bmf_atoms f : list atom := match f with
                                      | b_verum => []
                                      | b_falsum => []
                                      | b_prop x => [x]
                                      | b_nom x => []
                                      | b_negprop x => [x]
                                      | b_negnom x => []
                                      | b_conj x x0 => bmf_atoms x ++ bmf_atoms x0
                                      | b_disj x x0 => bmf_atoms x ++ bmf_atoms x0
                                      | b_diau x x0 => bmf_atoms x0
                                      | b_boxu x x0 => bmf_atoms x0
                                      | b_diab x x0 x1 => bmf_atoms x0 ++ bmf_atoms x1
                                      | b_boxb x x0 x1 => bmf_atoms x0 ++ bmf_atoms x1
                                      end.
  (** nominals in a formula *)
  Fixpoint bmf_nominals f : list nom := match f with
                                        | b_verum => []
                                        | b_falsum => []
                                        | b_prop x => []
                                        | b_nom x => [x]
                                        | b_negprop x => []
                                        | b_negnom x => [x]
                                        | b_conj x x0 => bmf_nominals x ++ bmf_nominals x0
                                        | b_disj x x0 => bmf_nominals x ++ bmf_nominals x0
                                        | b_diau x x0 => bmf_nominals x0
                                        | b_boxu x x0 => bmf_nominals x0
                                        | b_diab x x0 x1 => bmf_nominals x0 ++ bmf_nominals x1
                                        | b_boxb x x0 x1 => bmf_nominals x0 ++ bmf_nominals x1
                                        end. 
  (** checking if the givem formula is an atom (and producing that atom as a result) *)
  Definition bmf_is_atom' f := match f with
                               | b_verum => None
                               | b_falsum => None
                               | b_prop x => Some x
                               | b_nom x => None
                               | b_negprop x => None
                               | b_negnom x => None
                               | b_conj x x0 => None
                               | b_disj x x0 => None
                               | b_diau x x0 => None
                               | b_boxu x x0 => None
                               | b_diab x x0 x1 => None
                               | b_boxb x x0 x1 => None
                               end. 

  Inductive bboxarg : Set :=
  | ba_disj : bboxarg -> bboxarg -> bboxarg
  | ba_prop : atom -> bboxarg
  | ba_negprop : atom -> bboxarg
  | ba_nom : nom -> bboxarg
  | ba_negnom : nom -> bboxarg
  | ba_verum : bboxarg
  | ba_falsum : bboxarg
  | ba_diau : opu -> bdiaarg -> bboxarg
  | ba_boxu : opu -> bboxarg -> bboxarg
  | ba_diab : opb -> bdiaarg -> bdiaarg -> bboxarg
  | ba_boxb : opb -> bboxarg -> bboxarg -> bboxarg
  with bdiaarg : Set :=
       | da_conj : bdiaarg -> bdiaarg -> bdiaarg
       | da_prop : atom -> bdiaarg
       | da_negprop : atom -> bdiaarg
       | da_nom : nom -> bdiaarg
       | da_negnom : nom -> bdiaarg
       | da_verum : bdiaarg
       | da_falsum : bdiaarg
       | da_diau : opu -> bdiaarg -> bdiaarg
       | da_boxu : opu -> bboxarg -> bdiaarg
       | da_diab : opb -> bdiaarg -> bdiaarg -> bdiaarg
       | da_boxb : opb -> bboxarg -> bboxarg -> bdiaarg.
  
  (** Arbitrary length conjunction *)
  Definition b_bigconj := fold_right b_conj b_verum.
  (** Arbitrary length disjunction *)
  Definition b_bigdisj := fold_right b_disj b_falsum.

  Lemma bigconj_cons : forall (f : bmf) (fs : list bmf), b_bigconj (f :: fs) = b_conj f (b_bigconj fs). intros. auto. Qed.

  (** Negation preserving NNF *)
  Fixpoint b_neg f := match f with
                      | b_verum => b_falsum
                      | b_falsum => b_verum
                      | b_prop x => b_negprop x
                      | b_nom x => b_negnom x
                      | b_negprop x => b_prop x
                      | b_negnom x => b_nom x
                      | b_conj x x0 => b_disj (b_neg x) (b_neg x0)
                      | b_disj x x0 => b_conj (b_neg x) (b_neg x0)
                      | b_diau x x0 => b_boxu x (b_neg x0) 
                      | b_boxu x x0 => b_diau x (b_neg x0)
                      | b_diab x x0 x1 => b_boxb x (b_neg x0) (b_neg x1)
                      | b_boxb x x0 x1 => b_diab x (b_neg x0) (b_neg x1)
                      end.

  Lemma neg_inj : forall (f g : bmf), b_neg f = b_neg g <-> f = g.
    induction f, g; split; introversion; auto.
    - apply -> IHf1 in H1. apply -> IHf2 in H2. subst; reflexivity.
    - apply -> IHf1 in H1. apply -> IHf2 in H2. subst; reflexivity.
    - apply -> IHf in H2. subst; reflexivity.
    - apply -> IHf in H2. subst; reflexivity.
    - apply -> IHf1 in H2. apply -> IHf2 in H3. subst; reflexivity.
    - apply -> IHf1 in H2. apply -> IHf2 in H3. subst; reflexivity.
  Qed.

  Lemma dne : forall (f : bmf), b_neg (b_neg f) = f. 
    induction f; cbn [b_neg]; try rewrite IHf1, IHf2; try rewrite IHf; eauto. 
  Qed.

  Lemma neg_shift : forall (f g : bmf), b_neg f = g <-> f = b_neg g.
    induction f, g; split; introversion; cbn [b_neg]; auto; rewrite ?dne; auto.
  Qed.

  Lemma neg_bigconj : forall (f : list bmf), (b_neg (b_bigconj f)) = (b_bigdisj (map b_neg f)).
    induction f; eauto with datatypes.
    rewrite map_cons.
    replace (b_bigconj (a :: f)) with (b_conj a (b_bigconj f)) by eauto.
    cbn [b_neg].
    replace (b_bigdisj (b_neg a :: map b_neg f)) with (b_disj (b_neg a) (b_bigdisj (map b_neg f))) by eauto.
    rewrite IHf.
    reflexivity.
  Qed.
  Lemma neg_bigdisj : forall (f : list bmf), (b_neg (b_bigdisj f)) = (b_bigconj (map b_neg f)).
    induction f; eauto with datatypes.
    rewrite map_cons.
    replace (b_bigdisj (a :: f)) with (b_disj a (b_bigdisj f)) by eauto.
    cbn [b_neg].
    replace (b_bigconj (b_neg a :: map b_neg f)) with (b_conj (b_neg a) (b_bigconj (map b_neg f))) by eauto.
    rewrite IHf.
    reflexivity.
  Qed.

  Global Program Instance bmf_BPL : BPL bmf :=
    {| verum := b_verum;
       falsum := b_falsum;
       prop := b_prop;
       nomi := b_nom;
       conj := b_conj;
       disj := b_disj;
    |}.
  Global Instance bmf_PL : PL bmf :=
    {| neg := b_neg
    |}.
  Global Program Instance bmf_UML : UML bmf :=
    {| diau := b_diau;
       boxu := b_boxu
    |}.
  Global Program Instance bmf_BML : BML bmf :=
    {| diab := b_diab;
       boxb := b_boxb
    |}.
  Global Instance bmf_Reify {O} : Reify (BML O * PL O) bmf O :=
    {| reify inst := match inst with (G1, G2) => fix c f := match f with
                                                           | b_verum => verum
                                                           | b_falsum => falsum
                                                           | b_prop x => prop x
                                                           | b_nom x => nomi x
                                                           | b_negprop x => neg (prop x)
                                                           | b_negnom x => neg (nomi x)
                                                           | b_conj x x0 => conj (c x) (c x0)
                                                           | b_disj x x0 => disj (c x) (c x0)
                                                           | b_diau x x0 => diau x (c x0)
                                                           | b_boxu x x0 => boxu x (c x0)
                                                           | b_diab x x0 x1 => diab x (c x0) (c x1)
                                                           | b_boxb x x0 x1 => boxb x (c x0) (c x1)
                                                           end
                     end
    |}.

  Import ListNotations.
 
  (** Normalize bmf into bboxarg (splitting on conjunctions) *) 
  Fixpoint bmf2bboxarg (f : bmf) : list bboxarg :=
    match f with
    | b_prop x => [ba_prop x]
    | b_negprop x => [ba_negprop x]
    | b_nom x => [ba_nom x]
    | b_negnom x => [ba_negnom x]
    | b_falsum => [ba_falsum]
    | b_verum => [ba_verum]
    | b_disj x x0 => (map ba_disj (bmf2bboxarg x)) <:> bmf2bboxarg x0
    | b_conj x x0 => (bmf2bboxarg x) ++ (bmf2bboxarg x0)
    | b_diau o x => [fold_right ba_disj ba_falsum (map (ba_diau o) (bmf2bdiaarg x))]
    | b_boxu o x => map (ba_boxu o) (bmf2bboxarg x)
    | b_diab o x x0 => [fold_right ba_disj ba_falsum ((map (ba_diab o) (bmf2bdiaarg x)) <:> bmf2bdiaarg x0)]
    | b_boxb o x x0 => (map (ba_boxb o) (bmf2bboxarg x)) <:> bmf2bboxarg x0
    end
  (** Normalize bmf into bdiaarg (splitting on disjunctions) *)
  with bmf2bdiaarg (f : bmf) : list bdiaarg :=
         match f with
         | b_prop x => [da_prop x]
         | b_negprop x => [da_negprop x]
         | b_nom x => [da_nom x]
         | b_negnom x => [da_negnom x]
         | b_falsum => [da_falsum]
         | b_verum => [da_verum]
         | b_disj x x0 => bmf2bdiaarg x ++ bmf2bdiaarg x0
         | b_conj x x0 => (map da_conj (bmf2bdiaarg x)) <:> bmf2bdiaarg x0
         | b_diau o x => map (da_diau o) (bmf2bdiaarg x)
         | b_boxu o x => [fold_right da_conj da_verum (map (da_boxu o) (bmf2bboxarg x))]
         | b_diab o x x0 => (map (da_diab o) (bmf2bdiaarg x)) <:> bmf2bdiaarg x0
         | b_boxb o x x0 => [fold_right da_conj da_verum ((map (da_boxb o) (bmf2bboxarg x)) <:> bmf2bboxarg x0)]
         end.

  (** Distribute boxes and disjunction over conjunnction *)
  Fixpoint split_conj (f : bmf) : list bmf :=
    match f with
    | b_prop x => [b_prop x]
    | b_negprop x => [b_negprop x]
    | b_nom x => [b_nom x]
    | b_negnom x => [b_negnom x]
    | b_falsum => [b_falsum]
    | b_verum => [b_verum]
    | b_disj x x0 => (map b_disj (split_conj x)) <:> split_conj x0
    | b_conj x x0 => (split_conj x) ++ (split_conj x0)
    | b_diau o x => [b_diau o x]
    | b_boxu o x => map (b_boxu o) (split_conj x)
    | b_diab o x x0 => [b_diab o x x0]
    | b_boxb o x x0 => (map (b_boxb o) (split_conj x)) <:> split_conj x0
    end.
  
  (** Normalize bmf into conj normal form (splitting on conjunctions) *) 
  Fixpoint bmf2conj (f : bmf) : list bmf :=
    match f with
    | b_prop x => [b_prop x]
    | b_negprop x => [b_negprop x]
    | b_nom x => [b_nom x]
    | b_negnom x => [b_negnom x]
    | b_falsum => [b_falsum]
    | b_verum => [b_verum]
    | b_disj x x0 => (map b_disj (bmf2conj x)) <:> bmf2conj x0
    | b_conj x x0 => (bmf2conj x) ++ (bmf2conj x0)
    | b_diau o x => [b_bigdisj (map (b_diau o) (bmf2disj x))]
    | b_boxu o x => map (b_boxu o) (bmf2conj x)
    | b_diab o x x0 => [b_bigdisj ((map (b_diab o) (bmf2disj x)) <:> bmf2disj x0)]
    | b_boxb o x x0 => (map (b_boxb o) (bmf2conj x)) <:> bmf2conj x0
    end
  (** Normalize bmf into disj normal form (splitting on disjunctions) *)
  with bmf2disj (f : bmf) : list bmf :=
         match f with
         | b_prop x => [b_prop x]
         | b_negprop x => [b_negprop x]
         | b_nom x => [b_nom x]
         | b_negnom x => [b_negnom x]
         | b_falsum => [b_falsum]
         | b_verum => [b_verum]
         | b_disj x x0 => bmf2disj x ++ bmf2disj x0
         | b_conj x x0 => (map b_conj (bmf2disj x)) <:> bmf2disj x0
         | b_diau o x => map (b_diau o) (bmf2disj x)
         | b_boxu o x => [b_bigconj (map (b_boxu o) (bmf2conj x))]
         | b_diab o x x0 => (map (b_diab o) (bmf2disj x)) <:> bmf2disj x0
         | b_boxb o x x0 => [b_bigconj ((map (b_boxb o) (bmf2conj x)) <:> bmf2conj x0)]
         end.

  (** Embed bboxarg into bmf *)
  Fixpoint bboxarg2bmf f :=
    match f with
    | ba_prop x => b_prop x
    | ba_negprop x => b_neg (b_prop x)
    | ba_nom x => b_nom x
    | ba_negnom x => b_neg (b_nom x)
    | ba_falsum => b_falsum
    | ba_verum => b_verum
    | ba_disj x x0 => b_disj (bboxarg2bmf x) (bboxarg2bmf x0)
    | ba_diau o x => b_diau o (bdiaarg2bmf x)
    | ba_boxu o x => b_boxu o (bboxarg2bmf x)
    | ba_diab o x x0 => b_diab o (bdiaarg2bmf x) (bdiaarg2bmf x0) 
    | ba_boxb o x x0 => b_boxb o (bboxarg2bmf x) (bboxarg2bmf x0)
    end
  (** Embed bdiaarg into bmf *)
  with bdiaarg2bmf f :=
         match f with
         | da_prop x => b_prop x
         | da_negprop x => b_neg (b_prop x)
         | da_nom x => b_nom x
         | da_negnom x => b_neg (b_nom x)
         | da_falsum => b_falsum
         | da_verum => b_verum
         | da_conj x x0 => b_conj (bdiaarg2bmf x) (bdiaarg2bmf x0)
         | da_diau o x => b_diau o (bdiaarg2bmf x)
         | da_boxu o x => b_boxu o (bboxarg2bmf x)
         | da_diab o x x0 => b_diab o (bdiaarg2bmf x) (bdiaarg2bmf x0)
         | da_boxb o x x0 => b_boxb o (bboxarg2bmf x) (bboxarg2bmf x0)
         end.

  Global Instance bdiaarg_Reify {O} : Reify _ bdiaarg O := contramap bdiaarg2bmf bmf_Reify.
  Global Instance bboxarg_Reify {O} : Reify _ bboxarg O := contramap bboxarg2bmf bmf_Reify.
End BiModal.

Section Instances.
  (** map over nominals *)
  Fixpoint bmf_nmap {n m u b : Set} (nf : n -> m) (f : @bmf u b n) : @bmf u b m :=
    match f with
    | b_verum => b_verum
    | b_falsum => b_falsum
    | b_prop x => b_prop x
    | b_nom x => b_nom (nf x)
    | b_negprop x => b_negprop x
    | b_negnom x => b_negnom (nf x)
    | b_conj x x0 => b_conj (bmf_nmap nf x) (bmf_nmap nf x0)
    | b_disj x x0 => b_disj (bmf_nmap nf x) (bmf_nmap nf x0)
    | b_diau x x0 => b_diau x (bmf_nmap nf x0) 
    | b_boxu x x0 => b_boxu x (bmf_nmap nf x0)
    | b_diab x x0 x1 => b_diab x (bmf_nmap nf x0) (bmf_nmap nf x1)
    | b_boxb x x0 x1 => b_boxb x (bmf_nmap nf x0) (bmf_nmap nf x1)
    end.
  
  (** change nominal type in bboxarg *)
  Fixpoint bboxarg_nmap {n m u b : Set} (f : n -> m) (ba : @bboxarg u b n) : @bboxarg u b m :=
    match ba with
    | ba_nom x => ba_nom (f x)
    | ba_negnom x => ba_negnom (f x)
    | ba_disj x x0 => ba_disj (bboxarg_nmap f x) (bboxarg_nmap f x0)
    | ba_diau o x => ba_diau o (bdiaarg_nmap f x)
    | ba_boxu o x => ba_boxu o (bboxarg_nmap f x)
    | ba_diab o x x0 => ba_diab o (bdiaarg_nmap f x) (bdiaarg_nmap f x0) 
    | ba_boxb o x x0 => ba_boxb o (bboxarg_nmap f x) (bboxarg_nmap f x0)
    | ba_prop x => ba_prop x
    | ba_negprop x => ba_negprop x
    | ba_verum => ba_verum
    | ba_falsum => ba_falsum
    end
  (** change nominal type in bdiaarg *)
  with bdiaarg_nmap {n m u b : Set} (f : n -> m) da : @bdiaarg u b m :=
         match da with
         | da_nom x => da_nom (f x)
         | da_negnom x => da_negnom (f x)
         | da_conj x x0 => da_conj (bdiaarg_nmap f x) (bdiaarg_nmap f x0)
         | da_diau o x => da_diau o (bdiaarg_nmap f x)
         | da_boxu o x => da_boxu o (bboxarg_nmap f x)
         | da_diab o x x0 => da_diab o (bdiaarg_nmap f x) (bdiaarg_nmap f x0)
         | da_boxb o x x0 => da_boxb o (bboxarg_nmap f x) (bboxarg_nmap f x0)
         | da_verum => da_verum
         | da_falsum => da_falsum
         | da_prop x => da_prop x
         | da_negprop x => da_negprop x
         end.
End Instances.

(** pretty-printing notations *)
Infix "/\" := b_conj (only printing).
Infix "\/" := b_disj (only printing).
Notation "[ o ] ( a )" := (b_boxu o a) (at level 20, only printing).
Notation "< o > ( a )" := (b_diau o a) (at level 20, only printing).
Notation "[ o ] ( a b )" := (b_boxb o a b) (at level 20, only printing).
Notation "< o > ( a b )" := (b_diab o a b) (at level 20, only printing).
