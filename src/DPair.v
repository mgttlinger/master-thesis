(** Dependent pair / sigma for sets *)
Record DPair {A : Set} {B : A -> Set} : Set := MkDPair { fst : A; snd : B fst }.

Notation "( x ** y )" := (@DPair x y) : type_scope.
Notation "( x ** y )" := (MkDPair _ _ x y).

Definition dpair_fold {A : Set} {B : A -> Set} {c} (f : forall a, B a -> c) (dp : (A ** B)) : c := f (fst dp) (snd dp).
Definition dpair_unfold {A : Set} {B : A -> Set} {c} (f : (A ** B) -> c) : forall a, B a -> c := fun a Ba => f (a ** Ba). 