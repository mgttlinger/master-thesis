From SQEMA Require Import Utils BiModal.

Require Import Nat.

Section Simplify.
  Class Simplify t := { simplify : t -> t }.

  Context {opu opb nom : Set}.
  Definition bmf_simplify :=
    fix s (f : @ bmf opu opb nom) :=
      match f with
      | b_verum => b_verum
      | b_falsum => b_falsum
      | b_prop x => b_prop x
      | b_nom x => b_nom x
      | b_negprop x => b_negprop x
      | b_negnom x => b_negnom x
      | b_conj a b =>
        match s a with
        | b_verum => s b
        | b_falsum => b_falsum
        | b_prop x as a' => match s b with
                           | b_negprop y as b' => if x =? y then b_falsum
                                                 else b_conj a' b'
                           | b_prop y as b' => if x =? y then a' else b_conj a' b'
                           | b' => b_conj a' b'
                           end
        | b_negprop x as a' => match s b with
                              | b_prop y as b' => if x =? y then b_falsum
                                                 else b_conj a' b'
                              | b_negprop y as b' => if x =? y then a' else b_conj a' b'
                              | b' => b_conj a' b'
                              end
        | a' => match s b with
               | b_verum => a'
               | b_falsum => b_falsum
               | b' => b_conj a' b'
               end
        end 
      | b_disj a b =>
        match s a with
        | b_falsum => s b
        | b_verum => b_verum
        | b_prop x as a' => match s b with
                           | b_negprop y as b' => if x =? y then b_verum
                                                 else b_disj a' b'
                           | b_prop y as b' => if x =? y then a' else b_disj a' b'
                           | b' => b_disj a' b'
                           end
        | b_negprop x as a' => match s b with
                              | b_prop y as b' => if x =? y then b_verum
                                                 else b_disj a' b'
                              | b_negprop y as b' => if x =? y then a' else b_disj a' b'
                              | b' => b_disj a' b'
                              end
        | a' => match s b with
               | b_falsum => a'
               | b_verum => b_verum
               | b' => b_disj a' b'
               end
        end
      | b_diau op x => b_diau op (s x)
      | b_boxu op x => b_boxu op (s x)
      | b_diab op x0 x1 => b_diab op (s x0) (s x1)
      | b_boxb op x0 x1 => b_boxb op (s x0) (s x1)
      end.
  Global Instance bmf_Simplify : Simplify (@bmf opu opb nom) :=
    {| simplify := bmf_simplify |}. 
  
End Simplify.