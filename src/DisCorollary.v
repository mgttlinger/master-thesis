From SQEMA Require Import Utils Model BiModal SQEMA Satisfaction CT Inverse.

Require Import Classical.
(*** Proves that corollary 2.4.7 in W. Conradies PhD thesis is wrong *)
Section Corollary_incorrect.
  (** frame for the counterexample *)
  Program Definition frame : @bframe bool unit Empty_set := {| relu' op x y := x = true /\ y = false;
                                                            relb' _ _ _ _ := False;
                                                          |}.

  (** Proof that corollary 2.4.7 in the PhD thesis of Willem Conradie is wrong *)
  Theorem corollary_2_4_7_incorrect : let f := (b_diau (u_direct tt) b_verum : @bmf (FreeUInverse unit) (FreeBInverse Empty_set) Empty_set) in ~ ((forall V, bmf_satisfaction frame V true f) <-> forall V, V.(valN) (inr 0) = true -> forall (v : bool), bmf_satisfaction frame V v (copure (SQEMA f))).
    cbn.
    rewrite iff_to_and.
    apply or_not_and.
    left.
    intro H.
    assert (Ha: @valuation Empty_set bool -> exists v0 : bool, (true = true /\ v0 = false) /\ True) by (intro; exists false; auto).
    pose proof (H Ha {|valN _ := true; valP _ _ := False|} ltac:(cbn; auto) false) as H'.
    cbn in H'.
    destruct H' as [[[HF _] | F] _]; auto.
    congruence. 
  Qed.
End Corollary_incorrect.