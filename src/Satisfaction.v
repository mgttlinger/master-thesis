From SQEMA Require Import VectorUtils Container Algebra ML BiModal Equation System.
From SQEMA Require Export Model Reify.

Import VectorNotations.
Section Satisfaction.
  Context {op opb nom world : Set} {ops : nat -> Set} `{PInverse ops}.
  Definition atom := nat.
  
  Context {frb : @bframe world op opb} {pfr : @pframe world ops}.

  Variables (val : @valuation nom world).

  (* Definition Satisfaction_BPL : BPL (world -> Prop) := *)
  (*   {| verum := fun _ => True; *)
  (*      falsum := fun _ => False; *)
  (*      prop p := fun w => val.(valP) p w; *)
  (*      nomi n := fun w => w = val.(valN) n; *)
  (*      conj a b := fun w => (a w) /\ (b w); *)
  (*      disj a b := fun w => (a w) \/ (b w); *)
  (*   |}. *)

  (* Definition Satisfaction_PL : PL (world -> Prop) := *)
  (*   {| B := Satisfaction_BPL; *)
  (*      neg f := fun w => ~ f w; *)
  (*   |}. *)

  (* Definition Satisfaction_UML {fr : @frame world op} : UML (world -> Prop) := *)
  (*   {| diau mo f := fun w => exists v, (fr.(rel) mo w v = true) /\ f v; *)
  (*      boxu mo f := fun w => forall v, (fr.(rel) mo w v = true) -> f v *)
  (*   |}. *)

  (* Definition Satisfaction_BML : BML (world -> Prop) := *)
  (*   {| BU := Satisfaction_UML (fr := bframe_frame frb); *)
  (*      diab mo f g := fun w => exists u v, (frb.(relb) mo w u v = true) /\ f u /\ g v; *)
  (*      boxb mo f g := fun w => forall u v, (frb.(relb) mo w u v = true) -> f u /\ g v *)
  (*   |}. *)

 
  (* Definition Satisfaction_PML : PML (world -> Prop) := *)
  (*   {| pdia n mo f := fun w => exists (v : Vector.t world n), (pfr.(prel) n mo (w :: v) = true) /\ fold_right (fun a b => a /\ b) (VectorUtils.zipWith (fun a b => a b) f v) True; *)
  (*      pbox n mo f := fun w => forall (v : Vector.t world n), (pfr.(prel) n mo (w :: v) = true) -> fold_right (fun a b => a /\ b) (VectorUtils.zipWith (fun a b => a b) f v) True; *)
  (*   |}. *)

End Satisfaction.
Section Sat.
  Context {op opb world nom : Set} {ops : nat -> Set} `{PInverse ops}.
  
  Class Satisfaction m f := { satisfaction : f -> @valuation nom world -> world -> m -> Prop;
                              extension : f -> @valuation nom world -> m -> (world -> Prop) := fun fr val form w => satisfaction fr val w form 
                            }.
  Definition bmf_satisfaction fr val : world -> (@bmf (FreeUInverse op) (FreeBInverse opb) nom) -> Prop :=
    fix s w f := match f with
                 | b_verum => True
                 | b_falsum => False
                 | b_prop x => val.(valP) x w
                 | b_nom x => val.(valN) x = w
                 | b_negprop x => ~ val.(valP) x w
                 | b_negnom x => ~ val.(valN) x = w
                 | b_conj x x0 => s w x /\ s w x0
                 | b_disj x x0 => s w x \/ s w x0
                 | b_diau x x0 => exists v, fr.(relu) x w v /\ s v x0
                 | b_boxu x x0 => forall v, fr.(relu) x w v -> s v x0
                 | b_diab x x0 x1 => exists v u, fr.(relb) x w v u /\ (s v x0 /\ s u x1)
                 | b_boxb x x0 x1 => forall v u, fr.(relb) x w v u -> s v x0 \/ s u x1
                 end.
                   
  Global Instance bmf_Satisfaction : Satisfaction (@bmf (FreeUInverse op) (FreeBInverse opb) nom) (@bframe world op opb) :=
    {| satisfaction := bmf_satisfaction |}.
End Sat.

Module SatNotations.
  Notation "( F , V , w ) |= f" := (satisfaction F V w f) (right associativity, at level 11). 
  Notation "( F , w ) |= f" := (forall V, (F, V, w) |= f) (right associativity, at level 12).
  Notation "|= f" := (forall F w, (F, w) |= f) (right associativity, at level 13).
  Notation "[[ f ]] F V" := (extension F V f) (at level 14).
End SatNotations.

Section BMF.
  Context {nom opu opb world : Set}.
  Parameters (Fr : @bframe world opu opb) (w : world).

  Notation "'bmf_' n" := (@bmf (FreeUInverse opu) (FreeBInverse opb) n) (at level 20). 
  Notation "'bmf'" := (bmf_ nom).
  Notation "'bmf_plus'" := (bmf_ (nom + nat)).
  Notation "'eqn'" := (@eqn (FreeUInverse opu) (FreeBInverse opb) (nom + nat)).
  Notation "'system'" := (@system (FreeUInverse opu) (FreeBInverse opb) (nom + nat)).

  (** various relations and facts about them to state correctness *)
  Definition bmf_lframe (f g : bmf) := forall F w, (forall V, bmf_satisfaction (world := world) F V w f) <-> (forall V, bmf_satisfaction F V w g).
  Lemma bmf_lframe_refl : forall e, bmf_lframe e e. unfold bmf_lframe; tauto. Qed.
  Lemma bmf_lframe_trans : forall e f g, bmf_lframe e f -> bmf_lframe f g -> bmf_lframe e g.
    unfold bmf_lframe; intros; mp.
    split; intro; tauto.
  Qed.

  Definition bmf_plus_lframe (f g : bmf_plus) := forall F w, (forall V, V.(valN) (inr 0) = w -> bmf_satisfaction (world := world) F V w f) <-> (forall V, V.(valN) (inr 0) = w -> bmf_satisfaction F V w g).
  Lemma bmf_plus_lframe_refl : forall e, bmf_plus_lframe e e. unfold bmf_plus_lframe; tauto. Qed.
  Lemma bmf_plus_lframe_trans : forall e f g, bmf_plus_lframe e f -> bmf_plus_lframe f g -> bmf_plus_lframe e g.
    unfold bmf_plus_lframe; intros; mp.
    split; intro; tauto.
  Qed.

  Definition bmf_plus_lframe_sat (f g : bmf_plus) := forall F w, (exists V, V.(valN) (inr 0) = w /\ forall v, bmf_satisfaction (world := world) F V v f) <-> (exists V, V.(valN) (inr 0) = w /\ forall v, bmf_satisfaction F V v g).
  Lemma bmf_plus_lframe_sat_refl : forall e, bmf_plus_lframe_sat e e. unfold bmf_plus_lframe_sat; tauto. Qed.
  Lemma bmf_plus_lframe_sat_trans : forall e f g, bmf_plus_lframe_sat e f -> bmf_plus_lframe_sat f g -> bmf_plus_lframe_sat e g.
    unfold bmf_plus_lframe_sat; intros; mp.
    split; intro; tauto.
  Qed.

  
  Definition eqn_lframe (f g : eqn) := forall F w, (forall V, V.(valN) (inr 0) = w -> bmf_satisfaction (world := world) F V w (eqn_toformula f)) <-> (forall V, V.(valN) (inr 0) = w -> bmf_satisfaction F V w (eqn_toformula g)).
  Lemma eqn_lframe_refl : forall e, eqn_lframe e e. unfold eqn_lframe; tauto. Qed.
  Lemma eqn_lframe_trans : forall e f g, eqn_lframe e f -> eqn_lframe f g -> eqn_lframe e g.
    unfold eqn_lframe; intros; mp.
    split; intro Ha; tauto.
  Qed.

  Definition eqn_lframe_sat (f g : eqn) := forall F w, (exists V, V.(valN) (inr 0) = w /\ forall v, bmf_satisfaction (world := world) F V v (eqn_toformula f)) <-> (exists V, V.(valN) (inr 0) = w /\ forall v, bmf_satisfaction F V v (eqn_toformula g)).
  Lemma eqn_lframe_sat_refl : forall e, eqn_lframe_sat e e. unfold eqn_lframe_sat; tauto. Qed.
  Lemma eqn_lframe_sat_trans : forall e f g, eqn_lframe_sat e f -> eqn_lframe_sat f g -> eqn_lframe_sat e g.
    unfold eqn_lframe_sat; intros; mp.
    split; intro Ha; tauto.
  Qed.
  
  Definition sys_lframe (f g : system) := forall F w, (forall V, V.(valN) (inr 0) = w -> bmf_satisfaction (world := world) F V w (sys_toformula f)) <-> (forall V, V.(valN) (inr 0) = w -> bmf_satisfaction F V w (sys_toformula g)).
  Lemma sys_lframe_refl : forall e, sys_lframe e e. unfold sys_lframe; tauto. Qed.
  Lemma sys_lframe_trans : forall a b c, sys_lframe a b -> sys_lframe b c -> sys_lframe a c.
    unfold sys_lframe; intros; mp.
    split; intro; tauto.
  Qed.

  Definition sys_lframe_sat (f g : system) := forall F w, (exists V, V.(valN) (inr 0) = w /\ forall v, bmf_satisfaction (world := world) F V v (sys_toformula f)) <-> (exists V, V.(valN) (inr 0) = w /\ forall v, bmf_satisfaction F V v (sys_toformula g)).
  Lemma sys_lframe_sat_refl : forall e, sys_lframe_sat e e. unfold sys_lframe_sat; tauto. Qed.
  Lemma sys_lframe_sat_trans : forall e f g, sys_lframe_sat e f -> sys_lframe_sat f g -> sys_lframe_sat e g.
    unfold sys_lframe_sat; intros; mp.
    split; intro Ha; tauto.
  Qed.
  
  Definition bmf_bmf_plus_lframe (f : bmf) (g : bmf_plus) := forall F w, (forall V, bmf_satisfaction (world := world) F V w f) <-> (forall V, V.(valN) (inr 0) = w -> bmf_satisfaction F V w g).
  Lemma bmf_bmf_plus_lframe_trans_l : forall e f g, bmf_lframe e f -> bmf_bmf_plus_lframe f g -> bmf_bmf_plus_lframe e g.
    unfold bmf_lframe, bmf_bmf_plus_lframe; intros; mp.
    split; intro; tauto.
  Qed.
  Lemma bmf_bmf_plus_lframe_trans_r : forall e f g, bmf_bmf_plus_lframe e f -> bmf_plus_lframe f g -> bmf_bmf_plus_lframe e g.
    unfold bmf_lframe, bmf_bmf_plus_lframe, bmf_plus_lframe; intros; mp.
    split; intro; tauto.
  Qed.
  
  Definition eqn_sys_lframe (f : eqn) (g : system) := forall F w, (forall V, V.(valN) (inr 0) = w -> bmf_satisfaction (world := world) F V w (eqn_toformula f)) <-> (forall V, V.(valN) (inr 0) = w -> bmf_satisfaction F V w (sys_toformula g)).
  Lemma eqn_sys_lframe_trans_l : forall e f g, eqn_lframe e f -> eqn_sys_lframe f g -> eqn_sys_lframe e g.
    unfold eqn_lframe, eqn_sys_lframe; intros; mp.
    split; intro; tauto.
  Qed.
  Lemma eqn_sys_lframe_trans_r : forall e f g, eqn_sys_lframe e f -> sys_lframe f g -> eqn_sys_lframe e g.
    unfold eqn_lframe, eqn_sys_lframe, sys_lframe; intros; mp.
    split; intro; tauto.
  Qed.

  Definition eqn_sys_lframe_sat (f : eqn) (g : system) := forall F w, (exists V, V.(valN) (inr 0) = w /\ forall v, bmf_satisfaction (world := world) F V v (eqn_toformula f)) <-> (exists V, V.(valN) (inr 0) = w /\ forall v, bmf_satisfaction F V v (sys_toformula g)).
  Lemma eqn_sys_lframe_trans_sat_l : forall e f g, eqn_lframe_sat e f -> eqn_sys_lframe_sat f g -> eqn_sys_lframe_sat e g.
    unfold eqn_lframe_sat, eqn_sys_lframe_sat; intros; mp.
    split; intro; tauto.
  Qed.
  Lemma eqn_sys_lframe_trans_sat_r : forall e f g, eqn_sys_lframe_sat e f -> sys_lframe_sat f g -> eqn_sys_lframe_sat e g.
    unfold eqn_lframe_sat, eqn_sys_lframe_sat, sys_lframe_sat; intros; mp.
    split; intro; tauto.
  Qed.

  
  Definition sys_bmf_plus_lframe (f : system) (g : bmf_plus) := forall F w, (forall V, V.(valN) (inr 0) = w -> satisfaction (world := world) F V w (sys_toformula f)) <-> (forall V, V.(valN) (inr 0) = w -> satisfaction F V w g).

  Definition bmf_plus_eqn_lframe (f : bmf_plus) (g : eqn) := forall F (w : world), (forall V, V.(valN) (inr 0) = w -> bmf_satisfaction F V w f) <-> (forall V, V.(valN) (inr 0) = w -> bmf_satisfaction F V w (eqn_toformula g)).  
End BMF.

Import SatNotations.
Definition LFrame_corr {M N Fr w n1 n2} `{@Satisfaction w n1 M Fr} `{@Satisfaction w n2 N Fr} (a : M) (b : N) := forall F w, (F, w) |= a <-> (F, w) |= b.
