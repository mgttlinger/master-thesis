From SQEMA Require Import Algebra Container Enumerable CT Inverse.
Require Import Ensembles.
Require Import Vectors.Vector.

Import VectorNotations.

Section general_frame.
  Context {noms world : Set}.
  Definition atoms := nat.
  Context {opu opb : Set} {ops : nat -> Set}.

  (* Record general_frame `{HAO (F worlds) ops o} := { rel : worlds -> worlds -> bool }. *)
  
  (* Definition complement_closed a := forall (e : Ensemble worlds), In _ a e -> In _ a (Complement _ e). *)
  (* Definition union_closed a := forall (e f : Ensemble worlds), In _ a e /\ In _ a f -> In _ a (Union _ e f). *)
  (* Definition intersection_closed a := forall (e f : Ensemble worlds), In _ a e /\ In _ a f -> In _ a (Intersection _ e f). *)
  (* Definition box_closed a r := forall e, In _ a e -> exists f, In _ a f /\forall (w : worlds), In _ f w -> r w = e.   *)
  
  (* Record general_frame := { rel : worlds -> Ensemble worlds; *)
  (*                           admissible_sets : Ensemble (Ensemble worlds); *)
  (*                           admissible_comp_closed : complement_closed admissible_sets; *)
  (*                           admissible_union_closed : union_closed admissible_sets; *)
  (*                           admissible_inter_closed : intersection_closed admissible_sets; *)
  (*                           admissible_box_closed : box_closed admissible_sets rel *)
  (*                         }. *)
  
  (* Definition discrete gf := forall (w : worlds), In _ (admissible_sets gf) (Singleton _ w).  *)
  Record frame := { rel : opu -> world -> world -> Prop;
                    (* H :> HAO (F world) (fun n => match n with 1 => op | otherwise => Empty_set) (fun n => match n with 1 => ) *)
                  }.

  (** realation for given freely extended relation *)
  Fixpoint inverse_relation fbi_o (r : (opb -> world -> world -> world -> Prop)) : (world -> world -> world -> Prop) :=
    match fbi_o with
    | b_direct x => r x
    | b_inv_fst x => fun w v u => inverse_relation x r v w u
    | b_inv_snd x => fun w v u => inverse_relation x r u v w
    end.

  (** kripke frame with unary and binary relations which get extended with inverse relations *)
  Record bframe := { relu' : opu -> world -> world -> Prop;
                     relb' : opb -> world -> world -> world -> Prop;
                     relu : FreeUInverse opu -> world -> world -> Prop :=
                       fun o w v => match o with
                                 | u_direct o' => relu' o' w v
                                 | u_inverse o' => relu' o' v w
                                 end;
                     relb : FreeBInverse opb -> world -> world -> world -> Prop :=
                       fun o => inverse_relation o relb'
                   }.
  
  (* Definition bframe_frame bfr : frame := {| rel := bfr.(relu); *)
  (*                                           rel_inv := bfr.(relu_inv) *)
  (*                                        |}. *)
  (** Frame with polyadic relations *)
  Record pframe := { prel : forall n, ops n -> Vector.t world (S n) -> bool;
                   }.
  (** valuations (both nominals and atoms) *)
  Record valuation := { valP : atoms -> world -> Prop;
                        valN : noms -> world
                      }.
  
  (* Record model `{HAO (F worlds) ops o} := { (* frame : general_frame; *) *)
  (*                                           valP : props -> F worlds; *)
  (*                                           valN : noms -> { e : F worlds | exists f, inP f e /\ (forall g, inP g e -> f = g) } *)
  (*                                         }. *)
  
End general_frame.