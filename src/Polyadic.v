From SQEMA Require Import Utils Reify VectorUtils ListUtils DPair ML.
Require Import Lists.List.
From Coq.Vectors Require Fin Vector.
Require Import Program.Equality.
Require Import Omega.

Import ListNotations.

Section polyadic.
  Context {pro : Set} `{EqDec pro eq}.
  Context {nom : Set} `{EqDec nom eq}. (* leave empty for nominal free languages *)
  Context {op : nat -> Set} `{op_dec : forall n, EqDec (op n) eq}. (* modalities *)

  (** "small" polayadic modal formulas in nnf *)
  Inductive smf : Set :=
  | m_prop : pro -> smf
  | m_negprop : pro -> smf
  | m_nom : nom -> smf
  | m_negnom : nom -> smf
  | m_falsum : smf
  | m_verum : smf
  | m_disj : smf -> smf -> smf
  | m_conj : smf -> smf -> smf
  | m_dia {n} : op n -> Vector.t smf n -> smf
  | m_box {n} : op n -> Vector.t smf n -> smf.

  Hint Constructors smf.
  
  Inductive boxarg : Set :=
  | bc_disj : boxarg -> boxarg -> boxarg
  | bc_prop : pro -> boxarg
  | bc_negprop : pro -> boxarg
  | bc_nom : nom -> boxarg
  | bc_negnom : nom -> boxarg
  | bc_verum : boxarg
  | bc_falsum : boxarg
  | bc_dia {n} : op n -> Vector.t diaarg n -> boxarg
  | bc_box {n} : op n -> Vector.t boxarg n -> boxarg
  with diaarg : Set :=
       | dc_conj : diaarg -> diaarg -> diaarg
       | dc_prop : pro -> diaarg
       | dc_negprop : pro -> diaarg
       | dc_nom : nom -> diaarg
       | dc_negnom : nom -> diaarg
       | dc_verum : diaarg
       | dc_falsum : diaarg
       | dc_dia {n} : op n -> Vector.t diaarg n -> diaarg
       | dc_box {n} : op n -> Vector.t boxarg n -> diaarg.

  Inductive smf_mcnf : Set :=
  | mc_conj : smf_mcnf -> smf_mcnf -> smf_mcnf
  | mc_single : boxarg -> smf_mcnf.

  Inductive smf_mdnf : Set :=
  | md_disj : smf_mdnf -> smf_mdnf -> smf_mdnf
  | md_single : diaarg -> smf_mdnf.

  Coercion mc_single : boxarg >-> smf_mcnf.
  Coercion md_single : diaarg >-> smf_mdnf.
  
  Fixpoint conj_length f :=
    match f with
    | mc_conj x x0 => S (conj_length x + conj_length x0)
    | mc_single _ => 0
    end.
  Fixpoint disj_length f :=
    match f with
    | md_disj x x0 => S (disj_length x + disj_length x0)
    | md_single _ => 0
    end.

  Hint Unfold conj_length disj_length.

  Local Obligation Tactic := unfold complement, equiv; Tactics.program_simpl; autounfold; try (simpl; omega).
  
  Program Fixpoint mc_disj a b {measure (conj_length a + conj_length b)} :=
    match (a, b) with
    | (mc_single a', mc_single b') => bc_disj a' b' : smf_mcnf
    | (a, mc_conj x x0) => mc_conj (mc_disj a x) (mc_disj a x0)
    | (mc_conj x x0, b) => mc_conj (mc_disj x b) (mc_disj x0 b)
    end.

  Program Fixpoint md_conj a b {measure (disj_length a + disj_length b)} :=
    match (a, b) with
    | (md_single a', md_single b') => dc_conj a' b' : smf_mdnf
    | (a, md_disj x x0) => md_disj (md_conj a x) (md_conj a x0)
    | (md_disj x x0, b) => md_disj (md_conj x b) (md_conj x0 b)
    end.

  Class Clauses m c := { clauses : m -> list c;
                         unclauses : list c -> m;
                         single_join : list c -> m
                       }.
  
  Global Program Instance cnf_clauses : Clauses smf_mcnf boxarg :=
    {| clauses := fix c f := match f with
                             | mc_conj x x0 => c x ++ c x0
                             | mc_single x => [x]
                             end;
       unclauses := fold_right (fun c accu => mc_conj (mc_single c) accu) bc_verum;
       single_join := fun l => mc_single (fold_right bc_disj bc_falsum l)
    |}.
  Global Program Instance dnf_clauses : Clauses smf_mdnf diaarg :=
    {| clauses := fix c f := match f with
                             | md_disj x x0 => c x ++ c x0
                             | md_single x => [x]
                             end;
       unclauses := fold_right (fun c accu => md_disj (md_single c) accu) dc_falsum;
       single_join := fun l => md_single (fold_right dc_conj dc_verum l)
    |}.

  (** smf supports basic syntax *)
  Global Program Instance smf_BPL : BPL smf :=
    {| verum := m_verum;
       falsum := m_falsum;
       prop := m_prop;
       nomi := m_nom;
       conj := m_conj;
       disj := m_disj;
    |}.

  (** smf supports negation *)
  Global Program Instance smf_PL : PL smf :=
    {| neg := fix n f := match f with
                         | m_prop x => m_negprop x
                         | m_negprop x => m_prop x
                         | m_nom x => m_negnom x
                         | m_negnom x => m_nom x
                         | m_falsum => m_verum
                         | m_verum => m_falsum
                         | m_disj a b => m_conj (n a) (n b)
                         | m_conj a b => m_disj (n a) (n b)
                         | m_dia o args => m_box o (Vector.map n args)
                         | m_box o args => m_dia o (Vector.map n args)
                         end
    |}.
  
  (** smf supports polaydic modalities *)
  Global Program Instance smf_PML : PML smf :=
    {| pdia := @m_dia;
       pbox := @m_box
    |}.

  (** Conversion from polyadic modal formulas ito abstract syntax with polyadic modalities and inverses *)
  Global Program Instance smf_Reify {O} : Reify (@PML op O * @PL pro nom O) smf O :=
    {| reify inst := match inst with (Hi1, Hi2) => fix re f :=
                                       match f with
                                       | m_prop x => prop x
                                       | m_negprop x => neg (prop x)
                                       | m_nom x => nomi x
                                       | m_negnom x => neg (nomi x)
                                       | m_falsum => falsum
                                       | m_verum => verum
                                       | m_disj x x0 => disj (re x) (re x0)
                                       | m_conj x x0 => conj (re x) (re x0)
                                       | m_dia x x0 => pdia _ x (Vector.map re x0) 
                                       | m_box x x0 => pbox _ x (Vector.map re x0)
                                       end
                     end
    |}.

  (** Usable induction priciple for formulas *)  
  Section smf_ind'.
    Variable P : smf -> Prop.
    
    Hypothesis _prop : forall {p}, P (m_prop p).
    Hypothesis _nprop : forall {p}, P (m_negprop p).
    Hypothesis _nom : forall {n}, P (m_nom n).
    Hypothesis _nnom : forall {n}, P (m_negnom n).
    Hypothesis _falsum : P m_falsum.
    Hypothesis _verum : P m_verum.
    Hypothesis _disj : forall {a b}, P a -> P b -> P (m_disj a b).
    Hypothesis _conj : forall {a b}, P a -> P b -> P (m_conj a b).
    Hypothesis _dia : forall {n} (o : op n) {args}, Vector.Forall P args -> P (m_dia o args).
    Hypothesis _box : forall {n} (o : op n) {args}, Vector.Forall P args -> P (m_box o args).
    
    Fixpoint smf_ind' f {struct f} : P f :=
      match f with
      | m_prop x => _prop x
      | m_negprop x => _nprop x
      | m_nom x => _nom x
      | m_negnom x => _nnom x
      | m_falsum => _falsum
      | m_verum => _verum
      | m_disj x x0 => _disj x x0 (smf_ind' x) (smf_ind' x0)
      | m_conj x x0 => _conj x x0 (smf_ind' x) (smf_ind' x0)
      | m_dia x x0 => _dia _ x x0 ((fix v_fix {n} (v : Vector.t smf n) := match v with
                                                 | Vector.nil _ as a => Vector.Forall_nil P
                                                 | Vector.cons _ h n t => Vector.Forall_cons P h t (smf_ind' h) (v_fix t) 
                                                                         end) _ x0)
      | m_box x x0 => _box _ x x0 ((fix v_fix {n} (v : Vector.t smf n) := match v with
                                                 | Vector.nil _ as a => Vector.Forall_nil P
                                                 | Vector.cons _ h n t => Vector.Forall_cons P h t (smf_ind' h) (v_fix t) 
                                                 end) _ x0)
      end.
  End smf_ind'.

  Section smf_rec'.
    Variable P : smf -> Set.

        Hypothesis _prop : forall {p}, P (m_prop p).
    Hypothesis _nprop : forall {p}, P (m_negprop p).
    Hypothesis _nom : forall {n}, P (m_nom n).
    Hypothesis _nnom : forall {n}, P (m_negnom n).
    Hypothesis _falsum : P m_falsum.
    Hypothesis _verum : P m_verum.
    Hypothesis _disj : forall {a b}, P a -> P b -> P (m_disj a b).
    Hypothesis _conj : forall {a b}, P a -> P b -> P (m_conj a b).

    Inductive ForallS : forall n, Vector.t smf n -> Set := Fnil : ForallS _ (Vector.nil _) | Fcons n f t : P f -> ForallS n t -> ForallS _ (Vector.cons _ f _ t).
    
    Hypothesis _dia : forall {n} (o : op n) {args}, ForallS _ args -> P (m_dia o args).
    Hypothesis _box : forall {n} (o : op n) {args}, ForallS _ args -> P (m_box o args).
    
    Fixpoint smf_rec' f {struct f} : P f :=
      match f with
      | m_prop x => _prop x
      | m_negprop x => _nprop x
      | m_nom x => _nom x
      | m_negnom x => _nnom x
      | m_falsum => _falsum
      | m_verum => _verum
      | m_disj x x0 => _disj x x0 (smf_rec' x) (smf_rec' x0)
      | m_conj x x0 => _conj x x0 (smf_rec' x) (smf_rec' x0)
      | m_dia x x0 => _dia _ x x0 ((fix v_fix {n} (v : Vector.t smf n) := match v with
                                                 | Vector.nil _ as a => Fnil
                                                 | Vector.cons _ h n t => Fcons _ h t (smf_rec' h) (v_fix t) 
                                                                         end) _ x0)
      | m_box x x0 => _box _ x x0 ((fix v_fix {n} (v : Vector.t smf n) := match v with
                                                 | Vector.nil _ as a => Fnil
                                                 | Vector.cons _ h n t => Fcons _ h t (smf_rec' h) (v_fix t) 
                                                 end) _ x0)
      end.
  End smf_rec'.

  
  (* The trick is to not have fun s t here but fun s forall t *)
  Lemma ForallS_EqDec_fun {n} {v} (p : ForallS (fun s => forall t, {s = t} + {s = t -> False}) n v) : forall u, {v = u} + {v = u -> False}.
  dependent induction v; dependent induction u; try (solve [uneq]).
  - left; auto.
  - inversion p. clear_existT. destruct (H4 h0); [subst| uneq]. destruct (IHv H5 u); [left; f_equal; auto | uneq].
  Qed.

  
  Program Instance fineqdec n : EqDec (Fin.t n) eq.
  Next Obligation. revert y. induction x; dependent induction y; try (solve [uneq]). left;auto. destruct (IHx y); [left; subst; auto | uneq]. Defined.
  
  Program Instance listfineqdec n : EqDec (list (Fin.t n)) eq.
  Next Obligation. assert ({x = y} + {x <> y}) as Heq by (decide equality; deceq). destruct Heq; auto. Defined.

  
  Program Instance smf_EqDec : EqDec smf eq.
  Next Obligation. revert y. induction x using smf_rec'; induction y; try (solve [uneq]); try (solve [dec p p0; [left; auto | uneq]]); try (solve [dec n n0; [left; auto | uneq ]]); try (solve [left; auto]); try (solve [destruct (IHx y); [left; rewrite e; auto | uneq ]]); try (solve [destruct (IHx1 y1); [ destruct (IHx2 y2); [left; rewrite e; rewrite e0; auto | uneq ] | uneq ]]); try (solve [dec n n0; [idtac | uneq]; dec o o0; [idtac | uneq]; destruct ((ForallS_EqDec_fun H1) t); [left; f_equal; auto | uneq]]). Defined.

  (** Normalize smf into boxarg (splitting on conjunctions) *)
  Fixpoint smf2boxarg (f : smf) : list boxarg :=
    match f with
    | m_prop x => [bc_prop x]
    | m_negprop x => [bc_negprop x]
    | m_nom x => [bc_nom x]
    | m_negnom x => [bc_negnom x]
    | m_falsum => [bc_falsum]
    | m_verum => [bc_verum]
    | m_disj x x0 => ap (fmap bc_disj (smf2boxarg x)) (smf2boxarg x0)
    | m_conj x x0 => (smf2boxarg x) ++ (smf2boxarg x0)
    | m_dia x x0 => [fold_right bc_disj bc_falsum (fmap (bc_dia x) (traverse smf2diaarg x0))]
    | m_box x x0 => fmap (bc_box x) (traverse smf2boxarg x0)
    end
  (** Normalize smf into diaarg (splitting on disjunctions) *)
  with smf2diaarg (f : smf) : list diaarg :=
         match f with
         | m_prop x => [dc_prop x]
         | m_negprop x => [dc_negprop x]
         | m_nom x => [dc_nom x]
         | m_negnom x => [dc_negnom x]
         | m_falsum => [dc_falsum]
         | m_verum => [dc_verum]
         | m_disj x x0 => smf2diaarg x ++ smf2diaarg x0
         | m_conj x x0 => ap (fmap dc_conj (smf2diaarg x)) (smf2diaarg x0)
         | m_dia x x0 => fmap (dc_dia x) (traverse smf2diaarg x0)
         | m_box x x0 => [fold_right dc_conj dc_verum (fmap (dc_box x) (traverse smf2boxarg x0))]
         end.

  (** Normalize smf into modal conjunctive normal form *)
  Fixpoint smf2mcnf f : smf_mcnf :=
    match f with
    | m_prop x => bc_prop x
    | m_negprop x => bc_negprop x
    | m_nom x => bc_nom x
    | m_negnom x => bc_negnom x
    | m_falsum => bc_falsum
    | m_verum => bc_verum
    | m_disj x x0 => mc_disj (smf2mcnf x) (smf2mcnf x0)
    | m_conj x x0 => mc_conj (smf2mcnf x) (smf2mcnf x0)
    | m_dia x x0 => single_join (fmap (fun args => bc_dia x args) (traverse smf2diaarg x0))
    | m_box x x0 => unclauses (fmap (fun args => bc_box x args) (traverse smf2boxarg x0))
    end.

  (** Normalize smf into modal disjunctive normal form *)
  Fixpoint smf2mdnf f : smf_mdnf :=
    match f with
    | m_prop x => dc_prop x
    | m_negprop x => dc_negprop x
    | m_nom x => dc_nom x
    | m_negnom x => dc_negnom x
    | m_falsum => dc_falsum
    | m_verum => dc_verum
    | m_disj x x0 => md_disj (smf2mdnf x) (smf2mdnf x0)
    | m_conj x x0 => md_conj (smf2mdnf x) (smf2mdnf x0)
    | m_dia x x0 => unclauses (fmap (fun args => dc_dia x args) (traverse smf2diaarg x0))
    | m_box x x0 => single_join (fmap (fun args => dc_box x args) (traverse smf2boxarg x0))
    end.

  (** Embed boxarg into smf *)
  Fixpoint boxarg2smf f :=
    match f  with
    | bc_prop x => prop x
    | bc_negprop x => neg (prop x)
    | bc_nom x => nomi x
    | bc_negnom x => neg (nomi x)
    | bc_falsum => falsum
    | bc_verum => verum
    | bc_disj x x0 => (boxarg2smf x) \\// (boxarg2smf x0)
    | bc_dia x x0 => pdia _ x (fmap diaarg2smf x0)
    | bc_box x x0 => pbox _ x (fmap boxarg2smf x0)
    end
  (** Embed diaarg into smf *)
  with diaarg2smf f :=
         match f with
         | dc_prop x => prop x
         | dc_negprop x => neg (prop x)
         | dc_nom x => nomi x
         | dc_negnom x => neg (nomi x)
         | dc_falsum => falsum
         | dc_verum => verum
         | dc_conj x x0 => (diaarg2smf x) //\\ (diaarg2smf x0)
         | dc_dia x x0 => pdia _ x (fmap diaarg2smf x0)
         | dc_box x x0 => pbox _ x (fmap boxarg2smf x0)
         end.

  (** Embed mdnf into smf *)
  Fixpoint mdnf2smf f : smf :=
    match f with
    | md_disj x x0 => mdnf2smf x \\// mdnf2smf x0
    | md_single x => diaarg2smf x
    end.

  (* Derive a few instances *)
  Global Instance mdnf_BPL : BPL smf_mdnf := imap smf2mdnf mdnf2smf smf_BPL.
  Global Instance mdnf_PL : PL smf_mdnf := imap smf2mdnf mdnf2smf smf_PL.
  Global Instance mdnf_PML : PML smf_mdnf := imap smf2mdnf mdnf2smf smf_PML.
  Global Instance mdnf_Reify {O} : Reify _ smf_mdnf O := contramap mdnf2smf smf_Reify.
  Global Instance diaarg_Reify {O} : Reify _ diaarg O := contramap diaarg2smf smf_Reify.
  Global Instance boxarg_Reify {O} : Reify _ boxarg O := contramap boxarg2smf smf_Reify.
  (* Lemma fold_app_preserves_containment {n} {t} {u} : forall (v : Vector.t t n) p f, (exists e, Vector.In e v /\ In p (f e)) <-> In p (Vector.fold_right (fun e accu => f e ++ accu) v (@nil u)). *)
  (* Proof with (auto with vectors). *)
  (*   intros v. induction v; intros p f; simpl; split. *)
  (*   - intros [e [Hf _]]. inversion Hf. *)
  (*   - intro. contradiction.  *)
  (*   - intros [e [Hc Hp]]. rewrite in_app_iff. VectorUtils.predicate_cons... right. apply IHv. exists e... *)
  (*   - rewrite in_app_iff. intros [Hl | Hr]. exists h... destruct ((proj2 (IHv p f)) Hr) as [x [Xl Xr]]. exists x... *)
  (* Qed. *)
  
  (* Lemma substitute_unchanged_iff_not_p : forall (B : smf) (p : pro), ~In p (atoms B) -> forall A, B = B [ A \ p ]. *)
  (* Proof with VectorUtils.vector. *)
  (*   intros B p. induction B using smf_ind'; simpl; try rewrite ListUtils.neg_In_iff in *; intuition (auto with vectors); try (solve [deceq; VectorUtils.vector; congruence]); try (f_equal; VectorUtils.vector). *)
  (*   - rewrite VectorUtils.map_fusion. apply VectorUtils.map_fixpoint. destruct (@VectorUtils.forall_function _ _ args (fun B => ~In p (atoms B) -> forall A, B = B [A \ p])). intros. apply H4... intro. apply H2. apply fold_app_preserves_containment... exists (atoms f)... *)
  (*   - rewrite VectorUtils.map_fusion. apply VectorUtils.map_fixpoint. destruct (@VectorUtils.forall_function _ _ args (fun B => ~In p (atoms B) -> forall A, B = B [A \ p])). intros. apply H4... intro. apply H2. apply fold_app_preserves_containment... exists (atoms f)... *)
  (* Qed.     *)
        

  (* Lemma in_map : forall n t u (a : u) (f : t -> list u) (v : Vector.t t n), (exists l, In a (f l) /\ Vector.In l v) <-> In a (Vector.fold_right (@app u) (Vector.map f v) (@nil u)). *)
  (* Proof with VectorUtils.vector. *)
  (*   intros n t u a f v. induction v; simpl... *)
  (*   - split; try tauto. intros [l [_ Hf]]...  *)
  (*   - split; rewrite in_app_iff in *. *)
  (*     + intros [l [Ha Hl]]. VectorUtils.predicate_cons... right. apply IHv. exists l... *)
  (*     + destruct IHv. intros [Ha | Ha]. exists h... destruct (H2 Ha) as [l [Hl Hv]]. exists l...  *)
  (* Qed. *)
  
  (* Ltac refold := remember (fix re (f : smf) : list pro :=
        match f with
        | m_prop x => [x]
        | m_negprop x => [x]
        | m_nom _ => []
        | m_negnom _ => []
        | m_falsum => []
        | m_verum => []
        | m_disj x x0 => re x ++ re x0
        | m_conj x x0 => re x ++ re x0
        | @m_dia n _ x0 => Vector.fold_right (fun a b : list pro => a ++ b) (Vector.map re x0) []
        | @m_box n _ x0 => Vector.fold_right (fun a b : list pro => a ++ b) (Vector.map re x0) []
        end) as atms; remember (fix re (f : smf) : pro -> smf -> smf := *)
  (*                               match f with *)
  (*                               | m_prop x => fun (p : pro) (g : smf) => if equiv_dec p x then g else m_prop x *)
  (*                               | m_nom x => fun (_ : pro) (_ : smf) => m_nom x *)
  (*                               | m_falsum => fun (_ : pro) (_ : smf) => m_falsum *)
  (*                               | m_neg x => fun (p : pro) (g : smf) => m_neg (re x p g) *)
  (*                               | m_disj x x0 => fun (p : pro) (g : smf) => m_disj (re x p g) (re x0 p g) *)
  (*                               | @m_op n x x0 => *)
  (*                                 fun (p : pro) (g : smf) => *)
  (*                                   m_op x (Vector.map (fun e : pro -> smf -> smf => e p g) (Vector.map re x0)) *)
  (*                               end) as sbst. *)
      
  (* Theorem substitute_atom_containment : forall B p, In p (atoms B) -> forall A, In p (atoms A) <-> In p (atoms (B [A \ p])). *)
  (* Proof with VectorUtils.vector. *)
  (*   intros B p. split. *)
  (*   - induction B using smf_ind'; simpl in *; intuition auto with vectors... *)
  (*     + deceq... congruence. *)
  (*     + deceq... congruence. *)
  (*     + rewrite in_app_iff in *. destruct H1; [left | right]...  *)
  (*     + refold. rewrite <-Heqsbst in *. apply in_map. apply in_map in H1. destruct H1 as [e [He1 He2]]. eapply VectorUtils.forall_function in H2. rewrite VectorUtils.map_fusion. exists (sbst e p A). split. apply H2... apply VectorUtils.in_map_preservation with (f := fun el => sbst el p A)... assumption.  *)
  (*   - apply (smf_ind' (fun B => In p (atoms (B [A \ p])) -> In p (atoms A))); simpl... (* if I ise "normal" induction here the IH becomes unusable... Why? *) *)
  (*     + intro.  deceq... intro H2. inversion H2. congruence. inversion H3. *)
  (*     + intros. contradiction. *)
  (*     + intros. contradiction. *)
  (*     + intros a b H2 H3. rewrite in_app_iff in *. intros [H4 | H5]... *)
  (*     + simpl in H0. refold. rewrite <-Heqsbst. intros n opn args Hfa Hin. apply in_map in Hin. destruct Hin as [e [He1 He2]]. rewrite VectorUtils.map_fusion in He2. apply VectorUtils.in_exists in He2. destruct He2 as [e' [He'1 He'2]]. pose proof (proj2 (@VectorUtils.forall_function _ _ args (fun l => In p (atms (sbst l p A)) -> In p (atms A)))) as Hpfa. pose proof (Hpfa Hfa) as Hpfa. clear Hfa. simpl in Hpfa. eapply Hpfa.  apply He'1. rewrite He'2. assumption. *)
  (* Qed. *)
        
End polyadic.