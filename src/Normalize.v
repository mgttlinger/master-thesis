From SQEMA Require Import Utils ML Polarity Polyadic VectorUtils.

Section Normalize.
  Context {pro nom opu opb : Set} {ops : nat -> Set} `{EqDec pro eq} `{EqDec nom eq} `{forall n, EqDec (ops n) eq}.
  
  (** Shift all occurences of variable p to the left *)
  Class Normalize m := { normalize : forall (p : pro), m -> m }.

  Fixpoint m `{Polarity pro smf} p (f : @smf pro nom ops)  := match f with
                    | m_prop x => if p ==b x then 1 else 0
                    | m_negprop x => if p ==b x then 1 else 0
                    | m_nom x => 0
                    | m_negnom x => 0
                    | m_falsum => 0
                    | m_verum => 0
                    | m_disj x x0 => m p x + m p x0 * if negb (pure_in p x0) then 2 else 1
                    | m_conj x x0 => m p x + m p x0 * if negb (pure_in p x0) then 2 else 1
                    | m_dia x x0 => 1
                    | m_box x x0 => 1
                                                              end.
 (* Program Fixpoint norm_smf `{EqDec (@smf pro nom ops)} `{Polarity pro (@smf pro nom ops)} p f {measure (m p f)} := *)
 (*    let odisj := fix od a b := *)
 (*                   if a ==b b then a else (* idem *) *)
 (*                     if andb (pure_in p a) (pure_in p b) then m_disj a b else *)
 (*                       if pure_in p a then od b a else *)
 (*                         if a ==b neg b then m_verum else *)
 (*                           match (a, b) with *)
 (*                           | (m_verum, _) => m_verum (* absorb *) *)
 (*                           | (_, m_verum) => m_verum (* absorb *) *)
 (*                           | (m_falsum, o) => o (* neutral *) *)
 (*                           | (o, m_falsum) => o (* neutral *) *)
 (*                           | (m_disj a b, c) => norm_smf p (m_disj (od a c) b) *)
 (*                           | (m_conj a b, c) => norm_smf p (m_conj (od a c) (od a b)) *)
 (*                           | (a, b) => m_disj a b *)
 (*                           end in *)
 (*    let oconj := fix oc a b {struct a} := *)
 (*                   if a ==b b then a else (* idem *) *)
 (*                     if andb (pure_in p a) (pure_in p b) then m_conj a b else *)
 (*                       if pure_in p a then norm_smf p (m_conj b a) else *)
 (*                         if a ==b neg b then m_falsum else *)
 (*                           match (a, b) with *)
 (*                           | (m_falsum, _) => m_falsum (* absorb *) *)
 (*                           | (_, m_falsum) => m_falsum (* absorb *) *)
 (*                           | (m_verum, o) => o (* neutral *) *)
 (*                           | (o, m_verum) => o (* neutral *) *)
 (*                           | (a, b) => m_conj a b *)
 (*                           end in *)
 (*    let odia := fix odi {nv} (op : ops nv) (args : Vector.t smf nv) := *)
 (*                  if VectorUtils.existsb (fun f => f ==b m_falsum) args then m_falsum else m_dia op args *)
 (*    in *)
 (*    let obox := fix obo {nv} (op : ops nv) (args : Vector.t smf nv) := m_box op args *)
 (*    in *)
 (*    match f with *)
 (*    | m_prop x => m_prop x *)
 (*    | m_negprop x => m_negprop x *)
 (*    | m_nom x => m_nom x *)
 (*    | m_negnom x => m_negnom x *)
 (*    | m_falsum => m_falsum *)
 (*    | m_verum => m_verum *)
 (*    | m_dia o args => odia o (Vector.map (fun f => norm_smf p f) args) *)
 (*    | m_box o args => obox o (Vector.map (fun f => norm_smf p f) args) *)
 (*    | m_disj a b as i => let a' := norm_smf p a in *)
 (*                        let b' := norm_smf p b in *)
 (*                        odisj (a') (b') *)
 (*    | m_conj a b as i => let a' := norm_smf p a in *)
 (*                        let b' := norm_smf p b in *)
 (*                        oconj (a') (b') *)
 (*    end. *)
  
  (* Instance Normalize_sfm : Normalize (@smf pro nom ops) := *)
  (*   {| normalize := norm_smf          *)
  (*   |}. *)
  
End Normalize.
