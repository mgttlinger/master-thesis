From SQEMA Require Import Container.
Require Import Lists.List.

Class Enumerable t f `{Container f} := { enumerate : f t;
                                         enumerate_ok : forall e, inP e enumerate
                                       }.