From SQEMA Require Import Utils SQEMA BiModal Model ML CT Satisfaction Atoms SQEMAFacts Equation ListUtils.
Require Import Classical.
Require Import FunInd.
Require Import Nat.
Require Import Omega.
Require Import Sorting.Permutation.

Require Import List.
Import ListNotations.
Import ListUtils.

(*** Correctness of our corrected version of SQEMA for unary and binary modalities *)
Section SQEMACorrect.
  (** Sets for unary and binary modalities as well as nominals *)
  Notation opu := (SQEMAFacts.opu).
  Notation opb := (SQEMAFacts.opb).
  Notation nom := (SQEMAFacts.nom).
  (** Set of worlds which has to have decidable equality *)
  Context {ws : Set} `{Weq: EqDec ws eq}.
  
  Context `{Neq: EqDec (nom + nat) eq}.

  Lemma relu_inv : forall (F : @bframe ws opu opb) (o : FreeUInverse opu) (w v : ws), F.(relu) (inv o) v w <-> F.(relu) o w v.
    intros.
    destruct o; cbn; tauto.
  Qed.
  
  Lemma relb_inv_fst : forall (F : @bframe ws opu opb) o (w v u : ws), F.(relb) (inv_fst o) v w u <-> F.(relb) o w v u.
    intros.
    destruct o; cbn; tauto.
  Qed.
  
  Lemma relb_inv_snd : forall (F : @bframe ws opu opb) o (w v u : ws), F.(relb) (inv_snd o) u v w <-> F.(relb) o w v u.
    intros.
    destruct o; cbn; tauto.
  Qed.

  Notation "'bmf_' n" := (@bmf (FreeUInverse opu) (FreeBInverse opb) n) (at level 20). 
  Notation "'bmf'" := (bmf_ nom).
  Notation "'bmf_plus'" := (bmf_ (nom + nat)).
  Notation "'eqn'" := (@eqn (FreeUInverse opu) (FreeBInverse opb) (nom + nat)).
  Notation "'system'" := (@system (FreeUInverse opu) (FreeBInverse opb) (nom + nat)).
  Notation nom_i := (inr 0 : nom + nat).

  Tactic Notation "fmap_unfold" := cbv [fmap ListUtils.list_Functor fmap__ Equation.b Equation.a].
  Tactic Notation "ap_unfold" := cbv [_f _f__ _p__ fmap ListUtils.list_Functor fmap__ Equation.b Equation.a ap zip zip__ ListUtils.list_Applicative].
  
  Hint Resolve bmf_plus_lframe_refl bmf_plus_lframe_trans.
  Hint Resolve eqn_lframe_refl eqn_lframe_trans.
  Hint Resolve sys_lframe_refl sys_lframe_trans.
  Hint Resolve bmf_bmf_plus_lframe_trans_l bmf_bmf_plus_lframe_trans_r eqn_sys_lframe_trans_r eqn_sys_lframe_trans_l.
  Hint Resolve bmf_plus_lframe_sat_refl eqn_lframe_sat_refl sys_lframe_sat_refl.
  Hint Constructors TrivialIn SPosIn SNegIn PureIn Alpha.
  
  Lemma conj_impl : forall (A B C D : Prop), (A <-> C) -> (B <-> D) -> (A /\ B <-> C /\ D). intuition. Qed.
  Lemma disj_impl : forall (A B C D : Prop), (A <-> C) -> (B <-> D) -> (A \/ B <-> C \/ D). intuition. Qed. 

  Lemma bigconj_Forall : forall (fs : list bmf_plus) F (w : ws) V, (bmf_satisfaction F V w (b_bigconj fs)) <-> (Forall (bmf_satisfaction F V w) fs).
    intros fs F w V.
    induction fs; cbn.
    - intuition. 
    - rewrite IHfs. intuition; inversion H1; trivial. 
  Qed.

  Lemma bigdisj_Exists : forall (fs : list bmf_plus) F (w : ws) V, (bmf_satisfaction F V w (b_bigdisj fs)) <-> (Exists (bmf_satisfaction F V w) fs).
    intros fs F w V.
    induction fs; cbn.
    - intuition. inversion H. 
    - rewrite IHfs. intuition. inversion H1; intuition. 
  Qed.

  
  Lemma sys_unordered : forall (s s' : system), Permutation (eqns s) (eqns s') -> forall F (w : ws) V, V.(valN) nom_i = w /\ bmf_satisfaction F V w (sys_toformula s) <-> V.(valN) nom_i = w /\ bmf_satisfaction F V w (sys_toformula s').
    intros [s] [s'] Hp F w V.
    cbv [sys_toformula].
    cbn [eqns] in *.
    rewrite !bigconj_Forall.
    apply conj_impl; try tauto.
    apply ListUtils.Permutation_Forall.
    induction Hp; auto.
    - rewrite !map_cons.
      constructor; trivial.
    - rewrite !map_cons.
      constructor; trivial.
    - eapply Permutation_trans.
      apply IHHp1.
      apply IHHp2.
  Qed.
    
  Lemma sys_toformula_correct : forall (s : system), sys_bmf_plus_lframe (world := ws) s (sys_toformula s).
  Proof.
    intro.
    unfold sys_bmf_plus_lframe.
    tauto.
  Qed.
  Hint Resolve sys_toformula_correct.

  Lemma neg_i_here_falsum : forall P (F : @bframe ws opu opb) w, (forall V, V.(valN) nom_i = w -> bmf_satisfaction F V w (b_negnom nom_i : bmf_plus) \/ P) <-> (forall V, V.(valN (noms := nom + nat)) nom_i = w -> P).
    intros P F w.
    split; intros H V Hi; mp; eauto.
    intuition. 
  Qed.

  Hint Resolve neg_i_here_falsum.

  Lemma to_system_correct : forall (e : eqn), eqn_sys_lframe_sat (world := ws) e (to_system ([e])).
    unfold eqn_sys_lframe_sat.
    cbv [to_system sys_toformula eqns map b_bigconj fold_right].
    cbn [bmf_satisfaction]; intros.
    split; intros [V [Hv Ho]]; eauto.
    exists V; intuition. destruct (Ho v); auto.
  Qed.

  Hint Resolve to_system_correct.

  Functional Scheme fold_right_ind := Induction for fold_right Sort Prop.
  Functional Scheme map_ind := Induction for map Sort Prop.
  
  Lemma sys_sys_fold_preservation (f : system -> system -> system) : (forall s t u, sys_lframe_sat (world := ws) s t -> sys_lframe_sat (world := ws) s u -> sys_lframe_sat (world := ws) s (f t u)) -> forall s t u, sys_lframe_sat (world := ws) s t -> Forall (sys_lframe_sat (world := ws) s) u -> sys_lframe_sat (world := ws) s (fold_right f t u).
    intros Hf s t u Hst Hfa.
    functional induction (fold_right f t u); auto.
    inversion Hfa.
    apply Hf; auto.
  Qed.

  Lemma atom_sys_fold_preservation (f : nat -> system -> system) : (forall s t u, sys_lframe_sat (world := ws) s u -> sys_lframe_sat (world := ws) s (f t u)) -> forall s t u, sys_lframe_sat (world := ws) s t -> sys_lframe_sat (world := ws) s (fold_right f t u).
    intros Hf s t u Hst.
    functional induction (fold_right f t u); auto.
  Qed.

  Definition update_val (V : valuation (noms := nom + nat)) (p : nat) (nv : ws -> Prop) : valuation := {|valN := V.(valN); valP q := if q =? p then nv else V.(valP) q|}.

  Lemma update_irrelevant_pure : forall (f : bmf_plus) p, PureIn p f -> forall F V E w, bmf_satisfaction F V w f <-> bmf_satisfaction F (update_val V p E) w f.
    intros f p H.
    induction H; intros F V E w; cbn -[eqb]; try apply Nat.eqb_neq in H; rewrite ?H, <- ?IHPureIn1, <- ?IHPureIn2, <- ?IHPureIn; try tauto.
    split; intros [v [Hv Ho]]; exists v; split; trivial; [rewrite <- IHPureIn | rewrite -> IHPureIn]; solve [eauto].
    split; intros H' Hv; [rewrite <- IHPureIn | rewrite -> IHPureIn]; solve [eauto].
    split; intros [v [u [Hv [H1 H2]]]]; exists v; exists u; split; trivial; [rewrite <- IHPureIn1, <- IHPureIn2 | rewrite -> IHPureIn1, -> IHPureIn2]; solve [eauto].
    split; intros H' v u Hv; [rewrite <- IHPureIn1, <- IHPureIn2 | rewrite -> IHPureIn1, -> IHPureIn2]; solve [eauto].
  Qed.

  Definition update_valN {N : Set} `{EqDec N eq} (V : valuation (noms := N)) (n : N) (v : ws) : valuation := {|valN m := if n ==b m then v else V.(valN) m; valP := V.(valP)|}.

  Lemma update_irrelevant_fresh {N : Set} `{E: EqDec N eq} : forall (f : bmf_ N) n, FreshIn n f -> forall F V v w, bmf_satisfaction F V w f <-> bmf_satisfaction F (update_valN V n v) w f.
    intros f n H.
    induction H; intros F V v w; cbn -[equiv_dec]; deceq; try rewrite equivb_neg_refl in H; rewrite <- ?IHFreshIn1, <- ?IHFreshIn2, <- ?IHFreshIn; try tauto; try congruence.
    split; intros [v' [Hv' Ho']]; exists v'; split; trivial; [rewrite <- IHFreshIn | rewrite -> IHFreshIn]; solve [eauto].
    split; intros H' Hv; [rewrite <- IHFreshIn | rewrite -> IHFreshIn]; solve [eauto].
    split; intros [v' [u' [Hv [H1 H2]]]]; exists v'; exists u'; split; trivial; [rewrite <- IHFreshIn1, <- IHFreshIn2 | rewrite -> IHFreshIn1, -> IHFreshIn2]; solve [eauto].
    split; intros H' v' u' Hv; [rewrite <- IHFreshIn1, <- IHFreshIn2 | rewrite -> IHFreshIn1, -> IHFreshIn2]; solve [eauto].
  Qed.

  Lemma update_irrelevant_fresh' {N : Set} `{E: EqDec N eq} : forall (f : bmf_ N), forall F V V' (w : ws), V.(valP) = V'.(valP) -> (forall n, ~ V.(valN) n = V'.(valN) n -> FreshIn n f) -> bmf_satisfaction F V w f <-> bmf_satisfaction F V' w f.
    intros f F V V' w Hp Hf.
    revert w. induction f; intro w; cbn; rewrite ?Hp; try tauto.
    1,2: destruct (V.(valN) n == V'.(valN) n); try inversion e; try tauto; apply Hf in c; inversion c; congruence.
    1,2,5,6: assert (forall n, ~ V.(valN) n = V'.(valN) n -> FreshIn n f1 /\ FreshIn n f2) as H' by (intros n Hn; pose proof (Hf n Hn) as H'; inversion H'; auto); apply forall_dist' in H'; destruct H'.
    1,2: rewrite IHf1, IHf2; auto; tauto.
    split; intros [v [u [Rvu Ho]]]; exists v; exists u; split; trivial; try (rewrite IHf1, IHf2; tauto); rewrite <- IHf1, <- IHf2; tauto.
    split; intros H' v u Rvu; try (rewrite IHf1, IHf2; auto); rewrite <- IHf1, <- IHf2; solve [auto].
    all: assert (forall n, ~ V.(valN) n = V'.(valN) n -> FreshIn n f) as H' by (intros n Hn; pose proof (Hf n Hn) as H'; inversion H'; auto).
   split; intros [v [Rv Ho]]; exists v; split; trivial; try (rewrite IHf; tauto); rewrite <- IHf; tauto.
   split; intros H'' v Rv; try (rewrite IHf; auto); rewrite <- IHf; solve [auto].
  Qed.
  
  Definition valuation_widening (V : @valuation nom ws) w : @valuation (nom + nat) ws := {| valP := V.(valP);
                                                                                          valN n := match n with
                                                                                                    | inl x => V.(valN) x
                                                                                                    | inr x => w
                                                                                                    end
                                                                                       |}.
  Definition valuation_narrowing (V : @valuation (nom + nat) ws) : @valuation nom ws := {| valP := V.(valP);
                                                                                         valN n := V.(valN) (inl n)
                                                                                      |}.
  Lemma valuation_id : forall V w, valuation_narrowing (valuation_widening V w) = V.
    intros V w.
    cbv [valuation_narrowing valuation_narrowing valN valP].
    destruct V.
    reflexivity.
  Qed.

  Lemma narrowing_undoes_nmap_widening : forall (f : bmf) F V w, bmf_satisfaction F V w (bmf_nmap inl f) <-> bmf_satisfaction F (valuation_narrowing V) w f.
    intros f F V w.
    revert w.
    induction f; intro w; try solve [cbv; simpl; tauto]; cbn [bmf_nmap bmf_satisfaction]; rewrite ?IHf1, ?IHf2, ?IHf; try tauto.
    split; intros [v [Rv H]]; exists v; split; auto; [rewrite <- IHf | rewrite IHf]; auto.
    split; intros H v Rv; auto; [rewrite <- IHf | rewrite IHf]; auto.
    split; intros [v [u [Rv [H1 H2]]]]; exists v, u; split; auto; [rewrite <- IHf1, <- IHf2 | rewrite IHf1, IHf2]; auto.
    split; intros H v u Rv; (destruct (H v u Rv); [left | right]; auto); apply IHf1 || apply IHf2; auto.
  Qed.  
  
  Lemma nom_widening_correct : forall (d : bmf), bmf_bmf_plus_lframe (world := ws) (d) (bmf_nmap inl d).
    intros d.
    assert (HF: forall f n, FreshIn (inr n: nom + nat) (bmf_nmap inl f)) by (induction f; intro n'; cbv; constructor; try congruence; auto).
    intros F w.
    split; intros H V.
    - intros Hv.
      pose proof (H (valuation_narrowing V)).
      rewrite narrowing_undoes_nmap_widening; auto.
    - pose proof (H (valuation_widening V w) ltac:(cbv; auto)) as H'.
      rewrite narrowing_undoes_nmap_widening, valuation_id in H'; auto.
  Qed.  

  Lemma prefix_i_correct : forall (d : bmf_plus) F (w : ws), (exists V, V.(valN) nom_i = w /\ bmf_satisfaction F V w d) <-> (exists V, V.(valN) nom_i = w /\ forall v, bmf_satisfaction F V v (eqn_toformula (prefix_i d))).
    unfold prefix_i.
    cbv [eqn_toformula Equation.a Equation.b].
    intros d F w.
    split; intros [V [Hv Ho]]; cbn [bmf_satisfaction]; exists V.
    all: intuition.
    - rewrite Hv.
      destruct (w == v); intuition.
      rewrite <- e.
      right; trivial. 
    - destruct (Ho w); intuition.
  Qed.

  (** uses classical tauto! *)
 Lemma bmf_sat_neg : forall (g : bmf_plus) F (w : ws) V, (bmf_satisfaction F V w g) <-> (~ bmf_satisfaction F V w (b_neg g)).
   intros g F; induction g; cbn [bmf_satisfaction b_neg]; try tauto; try solve [split; auto; tauto].
   all: intros V w'.
   all: rewrite ?IHg, ?IHg1, ?IHg2.
   all: try tauto.
   - split.
     + intros [v [Hv Hg]] Hf. rewrite IHg in Hg. contradiction (Hf v Hv).
     + intro H. apply not_all_ex_not in H. destruct H as [n Hn]. exists n. split; [apply not_imply_elim in Hn | apply not_imply_elim2 in Hn]; trivial. apply IHg; trivial. 
   - split.
     + intro H. apply all_not_not_ex. intro n. apply or_not_and. rewrite <- IHg. pose proof (H n) as Hn. apply imply_to_or in Hn. trivial. 
     + intros Hf v Hv. rewrite IHg. intro H. apply Hf. exists v; intuition.  
   - split.
     + intros [v [u [Hvu [Hv Hu]]]].
       rewrite IHg1 in Hv.
       rewrite IHg2 in Hu.
       intro Hf.
       apply Hv.
       destruct (Hf v u); tauto.
     + intros Hf. apply not_all_ex_not in Hf. destruct Hf as [v Hf]. apply not_all_ex_not in Hf. destruct Hf as [u Huv].
       exists v. exists u.
       split; [apply not_imply_elim in Huv| apply not_imply_elim2 in Huv]; trivial.
       rewrite IHg1, IHg2.
       tauto.
   - split.
     + intros H [v [u [Hvu H']]].
       pose proof (H v u Hvu) as H''.
       rewrite IHg1, IHg2 in H''.
       tauto. 
     + intros Hf v u Hvu.
       apply not_ex_all_not with (n := v) in Hf.
       apply not_ex_all_not with (n := u) in Hf.
       apply not_and_or in Hf.
       destruct Hf; try congruence.
       apply not_and_or in H.
       rewrite IHg1, IHg2.
       trivial.
 Qed.
 

  
  (** needs dne? *)
  Lemma bmf_plus_neg_satisfaction : forall (f : bmf_plus) F w, (~ exists V, V.(valN) nom_i = w /\ bmf_satisfaction F V w f) <-> (forall V, V.(valN) nom_i = w -> bmf_satisfaction (world := ws) F V w (b_neg f)).
    intros f F w.
    split.
    + intros Hf V. rewrite bmf_sat_neg, dne. apply not_ex_all_not with (n := V) in Hf. apply not_and_or in Hf. intro Hv. destruct Hf; auto. 
    + intros H [V Hv]. destruct Hv as [Hv Ho]. rewrite bmf_sat_neg in Ho. intuition. 
  Qed.


  (** needs dne? *)
  Lemma bmf_plus_neg_satisfaction' : forall (f : bmf_plus) F w, (forall V, V.(valN) nom_i = w -> exists v, bmf_satisfaction (world := ws) F V v (b_neg f)) <-> (~ exists V, V.(valN) nom_i = w /\ forall v, bmf_satisfaction F V v f).
    intros f F w.
    split.
    - intros Hf [V [Hv Ho]]. destruct (Hf V Hv) as [v Hvf]. rewrite bmf_sat_neg, dne in Hvf. contradiction (Ho v).
    - intros Hf V Hv. apply not_ex_all_not with (n := V) in Hf. apply not_and_or in Hf. destruct Hf; try congruence.
      apply not_all_ex_not in H. destruct H as [v Hfv]. exists v. apply bmf_sat_neg; rewrite dne; auto.
  Qed.
  

  Lemma dne_bmf_correct (pf : eqn -> bmf_plus) : forall (f : bmf_plus), (forall (g : eqn), bmf_plus_lframe_sat (world := ws) (eqn_toformula g) (pf g)) -> forall F (w : ws), (forall V, V.(valN) nom_i = w -> bmf_satisfaction F V w f) <-> (forall V, V.(valN) nom_i = w -> exists v, bmf_satisfaction F V v (b_neg (pf (prefix_i (b_neg f))))).
    intros f Hpf F w.
    rewrite bmf_plus_neg_satisfaction'.
    rewrite <- (Hpf (prefix_i (b_neg f)) F).
    rewrite <- prefix_i_correct.
    rewrite bmf_plus_neg_satisfaction.
    rewrite dne.
    tauto. 
  Qed.
  
  Lemma bigconj_dist : forall (l : list bmf_plus) P F (w : ws), (forall V, (P V : Prop) -> bmf_satisfaction F V w (b_bigconj l)) <-> Forall (fun f => forall V, P V -> bmf_satisfaction F V w f) l.
    induction l; intros P F w.
    - intuition. cbn. auto.
    - cbn. split.
      + intro H. constructor. intros V Hv. destruct (H V Hv); trivial.
        rewrite forall_dist' in H.
        destruct H as [_ H].
        rewrite IHl in H; trivial.
      + introversion.
        rewrite <- IHl in H3.
        rewrite forall_dist'.
        auto.
  Qed. 

  Lemma bigconj_split_conj:
      forall (f : bmf_plus) (F : bframe) (w : ws) (V : valuation),
        bmf_satisfaction F V w f <-> bmf_satisfaction F V w (b_bigconj (split_conj f)).
    induction f; intros F w V; cbn; try tauto.
    rewrite bigconj_Forall, <- Forall_app, <- !bigconj_Forall, <- IHf1, <- IHf2. tauto.
    rewrite IHf1, IHf2, !bigconj_Forall, !Forall_forall.
    split.
    intros H x H'.
    rewrite in_map_iff in H'. destruct H' as [[f bf2] [Ef' If']]. subst.
    rewrite in_prod_iff in If'. destruct If' as [If Ibf2].
    rewrite in_map_iff in If. destruct If as [bf1 [Ef Ibf1]]. subst.
    cbn.
    destruct H as [H | H]; [left | right]; intuition.
    intros H.
    classical_left.
    apply not_all_ex_not in H0.
    destruct H0 as [f2e Hf2e].
    apply imply_to_and in Hf2e.
    destruct Hf2e as [If2e NSf2e].
    intros f1e If1e.
    pose proof (H (b_disj f1e f2e)) as H'.
    rewrite in_map_iff in H'.
    assert (exists x : (bmf_plus -> bmf_plus) * bmf_plus, (let (f, e) := x in f e) = (b_disj f1e f2e) /\ In x (list_prod (map b_disj (split_conj f1)) (split_conj f2))) as Ha by (exists (b_disj f1e, f2e); split; auto; rewrite in_prod_iff, in_map_iff; eauto).
    apply H' in Ha.
    cbn in Ha.
    destruct Ha; auto; congruence.
    rewrite bigconj_Forall, <- Forall_map with (P := fun g => bmf_satisfaction F V w (b_boxu o g)); try tauto; cbn [bmf_satisfaction]; rewrite Forall_pullout; split; intros H v; [rewrite <- bigconj_Forall, <- IHf | pose proof (H v) as H'; rewrite <- bigconj_Forall in H'; rewrite IHf ]; solve [auto].
    rewrite bigconj_Forall.
    rewrite Forall_forall.
    split.
    intros H x Ix; rewrite in_map_iff in Ix; destruct Ix as [[el1 el2] [Pel Plp]]; rewrite in_prod_iff in Plp; destruct Plp as [Hd He]; rewrite in_map_iff in Hd; destruct Hd as [x' [A B]]; rewrite <- Pel, <- A; cbn [bmf_satisfaction].
    subst.
    intros v u H0.
    apply H in H0.
    rewrite IHf1, IHf2, !bigconj_Forall, !Forall_forall in H0.
    intuition.
    intros H v u Rvu.
    rewrite IHf1, IHf2, !bigconj_Forall, !Forall_forall.
    classical_left.
    apply not_all_ex_not in H0.
    destruct H0 as [f2e Hf2e].
    apply imply_to_and in Hf2e.
    destruct Hf2e as [If2e NSf2e].
    intros f1e If1e.
    pose proof (H (b_boxb o f1e f2e)) as H'.
    rewrite in_map_iff in H'.
    assert (exists x : (bmf_plus -> bmf_plus) * bmf_plus, (let (f, e) := x in f e) = (b_boxb o f1e f2e) /\ In x (list_prod (map (b_boxb o) (split_conj f1)) (split_conj f2))) as Ha by (exists (b_boxb o f1e, f2e); split; auto; rewrite in_prod_iff, in_map_iff; eauto).
    apply H' in Ha.
    cbn in Ha.
    destruct (Ha v u); auto; congruence.    
  Qed.
  
  Lemma split_conj_correct : forall (f : bmf_plus) F (w : ws), (forall V, V.(valN) nom_i = w -> bmf_satisfaction F V w f) <-> (forall V, V.(valN) nom_i = w -> Forall (fun f' => bmf_satisfaction F V w f') (split_conj f)).
    intros f F w.
    split; intros H V Hv; pose proof (H V Hv) as H'; clear Hv.
    rewrite <- bigconj_Forall, <- bigconj_split_conj; auto.
    rewrite <- bigconj_Forall, <- bigconj_split_conj in H'; auto.
  Qed.
 
  
  Lemma post_prefix_pre_correct' (rp : eqn -> system) : (forall g, eqn_sys_lframe_sat (world := ws) g (rp g)) -> forall (f : bmf) F (w : ws), (forall V, bmf_satisfaction F V w f) <-> forall V, V.(valN) nom_i = w -> Forall (fun f' => exists v : ws, bmf_satisfaction F V v f') (postprocess' (map rp (map prefix_i (preprocess f)))).
  Proof.
    intros H f.
    unfold postprocess', preprocess.
    rewrite !map_map.
    rewrite <- map_map with (g := fun x => b_neg (sys_toformula (rp (prefix_i x)))).
    rewrite !map_map.
    intros F w.
    rewrite -> (nom_widening_correct _ F w).
    rewrite split_conj_correct.
    rewrite <- !Forall_pullout.
    rewrite <- Forall_map with (P := fun f' => forall V, V.(valN) nom_i = w -> exists v : ws, bmf_satisfaction F V v (b_neg (sys_toformula (rp (prefix_i (b_neg f')))))); try tauto.
    induction (split_conj (bmf_nmap inl f)); try solve [split; auto].
    rewrite <- !Forall_cons, <- IHl.
    apply conj_impl; try tauto.
    rewrite <- (dne_bmf_correct (fun x => sys_toformula (rp x))); auto; tauto.
  Qed.
  
  
  Lemma elim_triv_pure_id : forall (f : bmf_plus) p, PureIn p f -> elim_triv p f = f.
    intros f p H.
    unfold elim_triv, bmf_elim_triv.
    induction H; auto.
    1,2: destruct (x =? p) eqn:Hxp; rewrite ?PeanoNat.Nat.eqb_eq in Hxp; auto; congruence.
    all: rewrite ?IHPureIn1, ?IHPureIn2, ?IHPureIn; solve [auto].
  Qed.

  Lemma elim_triv_pos : forall (f : bmf_plus) p, SPosIn p f -> forall F V v, bmf_satisfaction F V v (elim_triv p f) <-> bmf_satisfaction F (update_val V p (fun _ => True)) v f.
    intros f p H F V.
    induction H; intro v; cbn; try tauto.
    1,2: deceq; cbn; try tauto; apply Nat.eqb_eq in D; try congruence.
    1,2: rewrite ?IHSPosIn1, ?IHSPosIn2; tauto.
    split; intros [v' [Rv' Ho]]; exists v'; (split; [auto | apply IHSPosIn; auto]).
    split; intros H' v' Rv'; apply IHSPosIn; apply H'; auto.
    split; intros [v' [u' [Rv' [H1 H2]]]]; exists v', u'; (split; [auto | split; [apply IHSPosIn1 | apply IHSPosIn2]; auto]).
    split; intros H' v' u' Rv'; (destruct (H' v' u' Rv'); [left | right]; [apply IHSPosIn1 | apply IHSPosIn2]; auto).
  Qed.
  Lemma pos_mon : forall (f : bmf_plus) p, SPosIn p f -> forall F V v, bmf_satisfaction F V v f -> bmf_satisfaction F (update_val V p (fun _ => True)) v f.
    intros f p H F V.    
    induction H; intros v; cbn; try solve [deceq; try tauto; apply Nat.eqb_eq in D; congruence]; auto.
    intros [H1 H2]; split; auto.
    intros [H1 | H2]; [left | right]; auto.
    intros [v' [Rv' Ho]]; exists v'; auto.
    intros [v' [u' [Rv' [H1 H2]]]]; exists v', u'; auto.
    intros H' v' u' Rvu'; destruct (H' v' u' Rvu'); [left | right]; auto.
  Qed.
  
  Lemma elim_triv_neg : forall (f : bmf_plus) p, SNegIn p f -> forall F V v, bmf_satisfaction F V v (elim_triv p f) <-> bmf_satisfaction F (update_val V p (fun _ => False)) v f.
    intros f p H F V.
    induction H; intro v; cbn; try tauto.
    1,2: deceq; cbn; try tauto; apply Nat.eqb_eq in D; try congruence.
    1,2: rewrite ?IHSNegIn1, ?IHSNegIn2; tauto.
    split; intros [v' [Rv' Ho]]; exists v'; (split; [auto | apply IHSNegIn; auto]).
    split; intros H' v' Rv'; apply IHSNegIn; apply H'; auto.
    split; intros [v' [u' [Rv' [H1 H2]]]]; exists v', u'; (split; [auto | split; [apply IHSNegIn1 | apply IHSNegIn2]; auto]).
    split; intros H' v' u' Rv'; (destruct (H' v' u' Rv'); [left | right]; [apply IHSNegIn1 | apply IHSNegIn2]; auto).
  Qed.
  Lemma neg_mon : forall (f : bmf_plus) p, SNegIn p f -> forall F V v, bmf_satisfaction F V v f -> bmf_satisfaction F (update_val V p (fun _ => False)) v f.
    intros f p H F V.    
    induction H; intros v; cbn; try solve [deceq; try tauto; apply Nat.eqb_eq in D; congruence]; auto.
    intros [H1 H2]; split; auto.
    intros [H1 | H2]; [left | right]; auto.
    intros [v' [Rv' Ho]]; exists v'; auto.
    intros [v' [u' [Rv' [H1 H2]]]]; exists v', u'; auto.
    intros H' v' u' Rvu'; destruct (H' v' u' Rvu'); [left | right]; auto.
  Qed.
  
  Lemma elim_triv_correct : forall (e : eqn) p, ETrivialIn p e -> eqn_lframe_sat (world := ws) e (elim_triv p e).
    intros [av bv] p Ht.
    unfold elim_triv, e_elim_triv, ETrivialIn in *.
    cbn [a b] in *.
    intros F w.
    cbv [eqn_toformula a b].
    inversion Ht.
    - inversion H.
      rewrite !elim_triv_pure_id; auto.
      tauto.
    - inversion H. cbn [bmf_satisfaction]. split; intros [V [Hv Ho]]; [exists V | exists (update_val V p (fun _ => True))]; (split; [simpl; auto | intro v; destruct (Ho v); [left | right]]); apply elim_triv_pos; auto; apply pos_mon; auto.
    - inversion H. cbn [bmf_satisfaction]. split; intros [V [Hv Ho]]; [exists V | exists (update_val V p (fun _ => False))]; (split; [simpl; auto | intro v; destruct (Ho v); [left | right]]); apply elim_triv_neg; auto; apply neg_mon; auto.
  Qed.
  
  Lemma elim_triv_preserves_purity : forall (f : bmf_plus) p q, PureIn q f -> PureIn q (elim_triv p f).
    intros f p q H.
    induction H; cbn [elim_triv bmf_elim_triv]; auto; deceq; auto.
  Qed.

  Lemma elim_triv_preserves_pos : forall (f : bmf_plus) p q, SPosIn q f -> SPosIn q (elim_triv p f).
    intros f p q H.
    induction H; cbn [elim_triv bmf_elim_triv]; auto; deceq; auto.
  Qed.

  Lemma elim_triv_preserves_neg : forall (f : bmf_plus) p q, SNegIn q f -> SNegIn q (elim_triv p f).
    intros f p q H.
    induction H; cbn [elim_triv bmf_elim_triv]; auto; deceq; auto.
  Qed.
  
  Lemma elim_triv_preserves_trivial : forall (e : eqn) p q, ETrivialIn q e -> ETrivialIn q (elim_triv p e).
    intros [av bv] p q.
    unfold ETrivialIn.
    cbn [a b elim_triv e_elim_triv].
    intros Ht.
    inversion Ht;
      [constructor 1 | constructor 2 | constructor 3];
      inversion H;
      constructor;
      auto using elim_triv_preserves_purity, elim_triv_preserves_pos, elim_triv_preserves_neg.
  Qed.    
    
  Lemma eliminate_triv_correct : forall (e : eqn), eqn_lframe_sat (world := ws) e (eliminate_trivial e).
    unfold eliminate_trivial.
    intro e.
    remember (filter _ _) as ts. 
    assert (Forall (fun el => trivial_in e el = true) ts).
    subst ts.
    induction (atoms e); cbn [filter]; auto.
    destruct trivial_in eqn:Ht; auto.
    clear Heqts.
    revert H.
    induction ts; intros H; eauto.
    cbn [fold_right].
    inversion H; subst.
    eapply eqn_lframe_sat_trans. apply IHts; auto.
    apply elim_triv_correct.
    clear IHts H.
    induction ts; cbn [fold_right].
    - apply <- reflect_iff. apply H2. apply ETrivialIn_trivial_in_reflect.
    - apply elim_triv_preserves_trivial.
      inversion H3; auto.
  Qed.
  


  Lemma copure_fold_bind {T U} : forall (f : T -> U -> failure U) (s : U) (l : list T), copure (fold_right (fun a b => bind (f a) b) (succ: s) l) = fold_right (fun a b => copure (f a b)) s l.
    intros f s l.
    induction l; auto with datatypes.
    cbn [fold_right].
    rewrite <- IHl; clear IHl.
    remember ((fold_right (fun (a0 : T) (b : failure U) => bind (f a0) b) (succ: s) l)) as w.
    cbv [bind join join__ failure_Monad fmap failure_Functor fmap__].
    destruct w; simpl; auto.
  Qed.

 

  Lemma PureIn_polarity : forall p (f : bmf_plus), PureIn p f -> bmf_polarity_of p f = p_none.
    intros p f H.
    induction H; cbn [bmf_polarity_of]; auto.
    1,2: destruct eqb eqn:Hb; rewrite ?PeanoNat.Nat.eqb_eq, ?PeanoNat.Nat.eqb_neq in *; congruence.
    all: rewrite ?IHPureIn1, ?IHPureIn2, ?IHPure; auto.
  Qed.
    
  Lemma alpha_beta_exclusive : forall (f : eqn) p, is_alpha p f = true -> is_beta p f = false.
    intros f p.
    rewrite <- (reflect_iff _ _ (Alpha_reflect _ _)).
    intro. unfold is_beta.
    inversion H;
      cbv [polarity_of e_polarity a b];
      cbn [bmf_polarity_of mult polarity_monoid];
      rewrite PeanoNat.Nat.eqb_refl;
      cbn [polarity_add];
      inversion H0;
      cbn [bmf_polarity_of];
      auto.
    1,2: destruct (x =? p) eqn:Hb; solve [auto].
    all: rewrite ?PureIn_polarity; auto.
    1,2: destruct (x =? p) eqn:Hb; solve [auto].
  Qed.

  Lemma beta_neg_subst_update_val':
    forall (p : BiModal.atom) (oa f : bmf_plus) (F : bframe) (V : valuation) (v : ws),
      PureIn p oa ->
      SNegIn p f ->
      bmf_satisfaction F V v (f [oa \ ~ p]) ->
      bmf_satisfaction F (update_val V p (fun w' : ws => bmf_satisfaction F V w' (b_neg oa))) v f.
    intros p oa f F V w Hp Hn.
    revert w.
    induction Hn; intro w; cbn -[eqb]; try apply Nat.eqb_neq in H; rewrite ?H; auto; deceq; rewrite <- ?bmf_sat_neg; auto.
    intros [H1 H2]; split; [apply IHHn1 | apply IHHn2]; solve [auto].
    intros [H1 | H2]; [left | right]; [apply IHHn1 | apply IHHn2]; solve [auto].
    intros [v [Hv' Ho]]; exists v; split; trivial; apply IHHn; solve [auto].
    intros [v [u [Hv' [H1 H2]]]]; exists v; exists u; split; trivial; split; [apply IHHn1 | apply IHHn2]; solve [auto].
    intros H v u Hv'; destruct (H v u Hv'); [left | right]; [apply IHHn1 | apply IHHn2]; solve [auto].
  Qed.
    
  Lemma beta_neg_subst_update_val:
    forall (p : BiModal.atom) (oa : bmf_plus) (eb : eqn) (F : bframe) (V : valuation) (v : ws),
      Beta p eb ->
      PureIn p oa ->
      bmf_satisfaction F V v (eqn_toformula (eb [oa \ ~ p])) ->
      bmf_satisfaction F (update_val V p (fun w' : ws => bmf_satisfaction F V w' (b_neg oa))) v (eqn_toformula eb).
    intros p oa eb F V v Hb Hp Hf.
    unfold Beta in Hb.
    destruct eb as [av bv].
    cbn [neg_subst e_neg_subst a b] in Hf.
    cbv [eqn_toformula a b] in *.
    cbn [bmf_satisfaction] in *.
    inversion Hb.
    destruct Hf as [Hf | Hf]; [left | right]; apply beta_neg_subst_update_val'; auto.
  Qed.
  
  Lemma ack':
    forall (p : BiModal.atom) (oa f : bmf_plus) (F : bframe) (V : valuation) (w : ws),
      SNegIn p f ->
      bmf_satisfaction F V w f ->
      (forall v, valP V p v \/ bmf_satisfaction F V v oa) ->
      bmf_satisfaction F V w (f [oa \ ~ p]).
    intros p oa f F V w Hn.
    revert w.
    induction Hn; intros w Hf Himp; cbn -[eqb] in *; auto.
    deceq; auto; apply Nat.eqb_eq in D; subst; destruct (Himp w); congruence.
    split; [apply IHHn1 | apply IHHn2]; solve [intuition].
    destruct Hf as [H1 | H2]; [left | right]; [apply IHHn1 | apply IHHn2]; solve [intuition].
    destruct Hf as [v [Hv Ho]]; exists v; split; trivial; apply IHHn; solve [intuition].
    destruct Hf as [v [u [Hv [H1 H2]]]]; exists v; exists u; split; trivial; split; [apply IHHn1 | apply IHHn2]; solve [intuition].
    intros v u Hv; destruct (Hf v u Hv) as [H1 | H2]; [left | right]; [apply IHHn1 | apply IHHn2]; solve [intuition].
  Qed.
  
  Lemma ack : forall p (oa : bmf_plus) eb, PureIn p oa -> Beta p eb -> sys_lframe_sat (world := ws) (to_system ([{|a := b_prop p; b:= oa|}; eb])) (to_system ([neg_subst oa p eb])).
    intros p oa eb Hp Hb F w.
    cbv [to_system sys_toformula eqns map].
    unfold eqn_toformula at 1.
    cbv [a b].
    cbv [b_bigconj fold_right].
    cbn [bmf_satisfaction].
    split; intros [V [Hv Ho]].
    - exists V. split; trivial.
      rewrite forall_dist.
      split; trivial.
      intro v.
      rewrite !forall_dist in Ho.
      destruct Ho as [Himp [Heb _]].
      destruct eb as [av bv].
      cbn [neg_subst e_neg_subst a b].
      unfold Beta in Hb.
      cbv [eqn_toformula a b] in *.
      inversion Hb.
      cbn [bmf_satisfaction] in *.
      destruct (Heb v) as [H' | H']; [left | right]; apply ack'; auto.
    - rewrite forall_dist in Ho. destruct Ho as [Ho _].
      pose (V' := update_val V p (fun w' => bmf_satisfaction F V w' (b_neg oa))).
      exists V'.
      split; trivial.
      unfold V' at 1; cbn [valP update_val]; rewrite Nat.eqb_refl.
      intro v.
      split.
      + unfold V'. rewrite <- update_irrelevant_pure; auto. rewrite bmf_sat_neg, dne. intuition. apply imply_to_or. tauto.
      + split; trivial.
        unfold V'.
        apply beta_neg_subst_update_val; auto.
  Qed.
  
  
  Lemma join_aplphas : forall p (als : list bmf_plus) os, sys_lframe_sat (world := ws) (to_system (map (fun f => {|a:=b_prop p;b:=f|}) als ++ os)) (to_system ({|a:=b_prop p;b:=b_bigconj als|} :: os)).
    intros p als os F w.
    split; intros [V [Hv Ho]]; exists V; split; trivial; intro v; pose proof (Ho v) as Ho'; clear Ho; revert Ho'.
    all: cbv [to_system sys_toformula eqns].
    all: rewrite map_cons, map_app, map_map.
    all: cbv [eqn_toformula]; cbn [a b b_bigconj fold_right bmf_satisfaction].
    all: rewrite !bigconj_Forall, <- ListUtils.Forall_app.
    all: rewrite <- Forall_map with (P := fun x => bmf_satisfaction F V v (b_disj (b_prop p) x)); try tauto.
    all: cbn [bmf_satisfaction].
    all: rewrite ListUtils.Forall_unrelated_disj.
    all: tauto.
  Qed.

  Fixpoint join_eqns (l : list eqn) : eqn := match l with
                                             | e :: t => let '(Build_eqn aca acb) := join_eqns t in
                                                        {|a:=b_conj (eqn_toformula e) aca; b:= acb|}
                                             | nil => {|b:=b_falsum;a:=b_verum|}
                                             end.

  Lemma join_eqns_always_a : forall (l : list eqn), (join_eqns l).(b) = b_falsum.
    induction l; cbn; auto.
    destruct (join_eqns l) as [av bv].
    cbn in *.
    trivial.
  Qed.

  Lemma eqn_join_to_system : forall (l : list eqn) F V (w : ws), bmf_satisfaction F V w (eqn_toformula (join_eqns l)) <-> bmf_satisfaction F V w (sys_toformula (to_system l)).
    intros l F V w.
    induction l.
    - cbv. tauto.
    - cbv [to_system sys_toformula eqns]; cbn [join_eqns map].
      rewrite bigconj_cons.
      change (b_bigconj (map eqn_toformula ?h)) with (sys_toformula (to_system h)).
      destruct (join_eqns l) as [aca acb] eqn:H.
      unfold eqn_toformula at 1.
      replace (b {|a:=b_conj (eqn_toformula a) aca; b:=acb|}) with (b (join_eqns (a :: l))).
      rewrite join_eqns_always_a.
      cbn [Equation.a b].
      cbn [bmf_satisfaction].
      rewrite <- IHl, <- H.
      unfold eqn_toformula at 3.
      rewrite join_eqns_always_a.
      rewrite H.
      cbn [Equation.a].
      cbn [bmf_satisfaction].
      tauto.
      cbn. rewrite H.
      auto.
  Qed.
  
  Lemma join_betas : forall (os : eqn) bs, sys_lframe_sat (world := ws) (to_system (os :: bs)) (to_system ([os; join_eqns bs])).
    intros os bs.
    cbv [to_system].
    intros F w.
    cbv [sys_toformula eqns].
    cbn [map].
    split; intros [V [Hv Ho]]; exists V; split; trivial; intro v; pose proof (Ho v) as H'; clear Ho; revert H'.
    all: rewrite !bigconj_cons.
    all: cbn [bmf_satisfaction].
    all: rewrite eqn_join_to_system.
    all: change (b_bigconj (map eqn_toformula ?h)) with (sys_toformula (to_system h)).
    all: cbv [b_bigconj fold_right].
    all: cbn [bmf_satisfaction].
    all: tauto.
  Qed.
  
    
  Lemma join_betas_beta : forall p l, Forall (Beta p) l -> Beta p (join_eqns l).
    intros p l.
    induction l; cbn [join_eqns].
    - intro. repeat constructor.
    - intro H.
      inversion H.
      destruct a as [av bv].
      inversion H2.
      subst.
      apply IHl in H3.
      clear IHl H.
      destruct (join_eqns l).
      inversion H3.
      cbv [eqn_toformula Equation.a Equation.b].
      constructor; cbn [Equation.a Equation.b]; auto.
  Qed.

  Lemma collect_filter_alpha:
    forall (p : pro) (s : list eqn) (F : bframe) (V : valuation) (v : ws),
      Forall (bmf_satisfaction F V v) (map eqn_toformula (filter (is_alpha p) s)) <->
      Forall (bmf_satisfaction F V v)
             (map (fun x : bmf_plus => eqn_toformula {| a := b_prop p; b := x |}) (collect (is_alpha' p) (filter (is_alpha p) s))).
    intros p s F V v.
    remember (filter (is_alpha p) s) as fas.
    pose proof (filter_Forall (is_alpha p) s) as H.
    eapply Forall_impl with (Q := Alpha p) in H.
    rewrite <- Heqfas in H.
    clear Heqfas.
    induction H; cbn [map]; try solve [intuition].
    rewrite <- Forall_cons.
    rewrite IHForall; clear IHForall.
    cbn [collect fold_right].
    rewrite f_match_option.
    destruct H;
      unfold is_alpha' at 2;
      cbn [a b bmf_pure_in];
      rewrite Nat.eqb_refl;
      cbn [negb andb];
      cbv [is_atom'' is_atom' IsAtom_bmf bmf_is_atom'];
      apply (reflect_iff _ _ (PureIn_reflect _ _)) in H;
      rewrite H, Nat.eqb_refl;
      cbn [negb andb map];
      rewrite <- Forall_cons.
    tauto.
    split; intros [H1 H2]; split; trivial; revert H1; cbn [eqn_toformula a b bmf_satisfaction]; tauto.
    intros a Ha. eapply reflect_iff. apply Alpha_reflect; trivial. assumption.
  Qed.


  Lemma split_alpha_bata_alpha_beta : forall p (s : system) (afs : list bmf_plus) (bs : list eqn), split_alpha_beta p s = Some (afs, bs) -> Forall (PureIn p) afs /\ Forall (Beta p) bs /\ sys_lframe_sat (world := ws) s (to_system (map (fun f => {|a:=b_prop p;b:=f|}) afs ++ bs)).
    intros p s afs bs.
    unfold split_alpha_beta.
    unfold split_system.
    destruct s as [s].
    cbn [eqns].
    deceq; introversion.
    split; try split.
    - unfold is_alpha', collect.
      clear D H H1 H2.
      induction (filter (is_alpha p) s); cbn [fold_right]; auto.
      rewrite f_match_option.
      destruct a as [av bv]; cbn [Equation.a b]; auto.
      deceq; auto.
      all: constructor; auto.
      all: eapply reflect_iff; [apply PureIn_reflect | idtac].
      1: rewrite andb_true_iff in D.
      2: rewrite andb_true_iff in D0.
      all: intuition. 
    - apply Forall_impl with (P := fun x => is_beta p x = true).
      intro e. apply reflect_iff. exact (Beta_reflect e p).
      apply filter_Forall.
    - eapply sys_lframe_sat_trans with (f := {| eqns := filter (is_alpha p) s ++ filter (is_beta p) s|}).
      {
        clear H H1 H2.
        assert (Forall (fun x => is_alpha p x = true \/ is_beta p x = true) s).
        rewrite andb_true_iff in D.
        destruct D as [D _].
        unfold is_empty in D.
        match_hyp' H.
        rewrite filter_empty in H.
        eapply Forall_impl; try apply H.
        intro e'. cbn.
        intro H'.
        destruct (is_alpha p e'), (is_beta p e'); cbn [andb negb] in *; try tauto; try inversion H'.
        inversion D.
        clear D.
        intros F w.
        split; intros [V [Hv Ho]]; exists V; split; trivial; intro v; pose proof (Ho v) as H'; clear Ho; revert H'; cbv [sys_toformula eqns]; rewrite ?bigconj_Forall;
          induction s; auto;
          rewrite !map_cons;
          inversion H; subst.
        all: destruct (is_alpha p a) eqn:Hia; cbn [filter]; rewrite !Hia.
        all: try pose proof (alpha_beta_exclusive _ _ Hia) as Hnb; rewrite ?Hnb.
        all: destruct H2 as [Hi | Hi]; inversion Hi; rewrite ?Hi.
        all: cbn [app map].
        all: try solve [rewrite <- !Forall_cons; intuition].
        all: rewrite map_app, map_cons, <- Forall_app, <- !Forall_cons in *.
        all: intuition.
      }
      intros F w.
      split; intros [V [Hv Ho]]; exists V; split; trivial; intro v; pose proof (Ho v) as H'; clear Ho; revert H'; cbv [sys_toformula eqns to_system].
      all: rewrite !map_app, map_map.
      all: rewrite !bigconj_Forall, <- !Forall_app.
      all: intro H'; split; try solve [intuition]; revert H'.
      all: intuition.
      all: apply collect_filter_alpha; auto.
  Qed.

  Lemma ack_correct : forall (a : nat) (s : system), sys_lframe_sat (world := ws) s (copure (ackermann_rule a s)).
    intros p [s].
    unfold ackermann_rule.
    rewrite f_match_option.
    destruct split_alpha_beta eqn:Hsab; auto.
    destruct p0.
    destruct (split_alpha_bata_alpha_beta p _ _ _ Hsab) as [Hal [Hbe Hs]].
    cbn [copure failure_CoPure copure__].
    eapply sys_lframe_sat_trans. exact Hs.
    clear Hsab Hs s.
    eapply sys_lframe_sat_trans. apply join_aplphas.
    assert (PureIn p (b_bigconj l)) by (induction l; cbn [b_bigconj fold_right]; auto; constructor; inversion Hal; auto).
    clear Hal.
    eapply sys_lframe_sat_trans. apply join_betas.
    assert (Beta p (join_eqns l0)) by auto using join_betas_beta.
    eapply sys_lframe_sat_trans. apply ack; auto.
    cbv [to_system].
    intros F w.
    split; intros [V [Hv Ho]]; exists V; split; trivial; intro v; pose proof (Ho v) as H'; clear Ho; revert H'; cbv [sys_toformula eqns to_system].
    all: cbn [map].
    all: clear Hbe H0 H Hv; remember (b_bigconj l) as l'; clear Heql'.
    all: induction l0.
    1,3: cbv; tauto.
    1: unfold neg_subst at 1, e_neg_subst.
    2: unfold neg_subst at 2, e_neg_subst.
    all: rewrite join_eqns_always_a, !map_cons, !bigconj_cons.
    all: cbn [join_eqns].
    all: destruct (join_eqns l0) as [av bv] eqn:H.
    all: cbn [Equation.a].
    1: unfold eqn_toformula at 1.
    2: unfold eqn_toformula at 3.
    all: change (b_falsum [l' \ ~ p]) with (b_falsum : bmf_plus).
    all: cbn [Equation.a b bmf_satisfaction b_bigconj fold_right].
    1: unfold neg_subst at 1, bmf_NegSubst.
    2: unfold neg_subst at 3, bmf_NegSubst.
    all: cbn [bmf_satisfaction].
    - intros [[[A1 A2] | B] _]; try inversion B.
      split; trivial.
      apply IHl0.
      rewrite <- H.
      unfold neg_subst.
      cbn [e_neg_subst].
      rewrite join_eqns_always_a, H.
      cbv [Equation.a eqn_toformula b_bigconj fold_right b].
      change (b_falsum [l' \ ~ p]) with (b_falsum : bmf_plus).
      cbn [bmf_satisfaction]; auto.
    - intros [A1 A2].
      split; trivial.
      left.
      unfold neg_subst.
      cbn [bmf_satisfaction].
      split; trivial.
      pose proof (IHl0 A2) as H'.
      revert H'.
      rewrite <- H.
      unfold neg_subst.
      cbn [e_neg_subst].
      rewrite join_eqns_always_a, H.
      change (b_falsum [l' \ ~ p]) with (b_falsum : bmf_plus).
      cbn [Equation.a].
      cbv [Equation.a eqn_toformula b_bigconj fold_right b].
      cbn [bmf_satisfaction]; auto.
      tauto.
  Qed.

  Lemma sys_split3_lframe : forall p q (es : list eqn), (forall e, p e = true -> q e = false) -> forall F (w : ws) V, V.(valN) nom_i = w /\ bmf_satisfaction F V w (sys_toformula {|eqns := es|}) <-> V.(valN) nom_i = w /\ bmf_satisfaction F V w (sys_toformula {|eqns := filter (fun e => negb (p e) && negb (q e)) es ++ filter p es ++ filter q es |}).
    intros p q es Hpnq F w V.
    apply sys_unordered.
    cbn [eqns].
    induction es; auto.
    cbn [filter].
    destruct (p a) eqn:Hp.
    - pose proof (Hpnq _ Hp).
      rewrite H.
      cbn [andb negb].
      eapply Permutation_trans.
      apply perm_skip. apply IHes.
      apply Permutation_middle.
    - destruct (q a) eqn:Hq; cbn [andb negb].
      eapply Permutation_trans; try apply perm_skip; try apply IHes.
      rewrite app_assoc.
      rewrite app_assoc.
      apply Permutation_middle.
      apply perm_skip; trivial.
  Qed.


  Lemma copure_snd_succ_lemma {T U}: forall v (c : T) (r : U), (v = (c, succ: r)) -> (copure (snd v) = r).
    intros.
    rewrite H.
    reflexivity.
  Qed.

  Lemma copure_snd_fail_lemma {T U}: forall v (c : T) (r : U), (v = (c, fail: r)) -> (copure (snd v) = r).
    intros.
    rewrite H.
    reflexivity.
  Qed.
 
  Ltac unlet := lazymatch goal with
                | |- context[let (_, _) := ?y in _] => destruct y eqn:?D
                end.

  Definition stream_fresh' (n : nom + nat) (ed : eqn) l := (EFreshIn n ed /\ Forall (EFreshIn n) l) /\ n <> nom_i.
  Definition stream_fresh (fn : Stream nat) (ed : eqn) l := forall n, stream_fresh' (inr (fn n)) ed l.
  Definition stream_unique (fn : Stream nat) := (forall n n', ~ n = n' -> ~ fn n = fn n').
  
  Definition same_eqn_sat (f g : eqn) := forall F (w : ws) V, V.(valN) nom_i = w -> (forall v, bmf_satisfaction F V v (eqn_toformula f)) <-> (forall v, bmf_satisfaction F V v (eqn_toformula g)).

  Lemma same_eqn_sat_eqn_sat : forall f g, same_eqn_sat f g -> eqn_lframe_sat (world := ws) f g.
    intros f g.
    unfold same_eqn_sat, eqn_lframe_sat.
    intros.
    split.
    - intros H'.
      destruct H' as [V [Hv H']].
      rewrite H in H'; auto.
      exists V.
      repeat (split; trivial); congruence.
    - intros [V' [Hv' H']]. rewrite <- H in H'; auto.
      exists V'.
      split; trivial.
  Qed.

  Definition same_sys_sat (f g : system) := forall F (w : ws) V, V.(valN) nom_i = w -> (forall v, bmf_satisfaction F V v (sys_toformula f)) <-> (forall v, bmf_satisfaction F V v (sys_toformula g)).
  
  
  Definition same_eqn_sys_sat (f : eqn) (g : system) := forall F (w : ws) V, V.(valN) nom_i = w -> (forall v, bmf_satisfaction F V v (eqn_toformula f)) <-> (forall v, bmf_satisfaction F V v (sys_toformula g)).
  
  Definition similar_eqn_sys_sat (f : eqn) (g : system) := forall F (w : ws) V, V.(valN) nom_i = w -> (forall v, bmf_satisfaction F V v (eqn_toformula f)) <-> (exists V', V'.(valN) nom_i = w /\ V.(valP) = V'.(valP) /\ (forall n, ~ V.(valN) n = V'.(valN) n -> EFreshIn n f) /\ forall v, bmf_satisfaction F V' v (sys_toformula g)).


  Definition same_eqn_freshness (f g : eqn) := forall n, EFreshIn n f <-> EFreshIn n g.
  Definition same_sys_freshness (f g : system) := forall n, SFreshIn n f <-> SFreshIn n g.
  Definition same_eqn_sys_freshness (f : eqn) (g : system) := forall n, EFreshIn n f <-> SFreshIn n g.
  Definition sys_freshness (f g : system) := forall n, SFreshIn n g -> SFreshIn n f.
  Definition eqn_sys_freshness (f : eqn) (g : system) := forall n, SFreshIn n g -> EFreshIn n f.
  
  
  Lemma similar_eqn_sys_sat_eqn_sys_sat : forall f g, similar_eqn_sys_sat f g -> eqn_sys_lframe_sat (world := ws) f g.
    intros f g.
    unfold similar_eqn_sys_sat, eqn_sys_lframe_sat.
    intros.
    mp. clear H H0.
    split; intros [V [Hv Ho]]; pose proof (H1 V Hv) as H; clear H1.
    - apply H in Ho.
      destruct Ho as [V' [Hv' [Hp [Hf Hg]]]].
      exists V'. split; auto.
    - exists V. split; trivial. apply H.
      exists V. repeat (split; trivial); try congruence.
  Qed.

  Lemma same_eqn_sys_sat_similar_eqn_sys_sat : forall f g, same_eqn_sys_sat f g -> similar_eqn_sys_sat f g.
    intros f g.
    unfold same_eqn_sys_sat, similar_eqn_sys_sat.
    intros.
    split.
    - intros H'.
      rewrite H in H'; auto.
      exists V.
      repeat (split; trivial); congruence.
    - intros [V' [Hv [Hp [Hn Ho]]]]. rewrite <- H in Ho; auto.
      intro v. rewrite update_irrelevant_fresh'; try apply Ho; auto.  
      intros n' Hn'.
      cbv [eqn_toformula].
      unfold EFreshIn in Hn.
      destruct (Hn n'); auto.
      constructor; intuition.
  Qed.

  Lemma similar_eqn_sys_sat_trans_l' : forall f g h, same_eqn_freshness f g -> same_eqn_sat f g -> similar_eqn_sys_sat g h -> similar_eqn_sys_sat f h.
    intros f g h HFr Hf Hs.
    intros F w V Hv.
    split.
    - intro H.
      rewrite (Hf F w V) in H; auto.
      rewrite (Hs F w V) in H; auto.
      destruct H as [V' [Hv' [Hp' [Hn' Ho]]]].
      exists V'.
      do 2 (split; trivial); try congruence.
      split; trivial.
      intros n HnV'.
      destruct (Hn' n HnV').
      split; trivial;
      apply HFr; auto.
    - intros [V' [Hv' [Hp [Hn' Ho]]]].
      rewrite (Hf F w V); auto.
      rewrite (Hs F w V); auto.
      exists V'.
      do 2 (split; trivial); try congruence.
      split; trivial.
      intros n HnV'.
      destruct (Hn' n HnV').
      split; trivial;
      apply HFr; auto.
  Qed.
  
  Definition similar_sys_sat (f g : system) := forall F (w : ws) V, V.(valN) nom_i = w -> (forall v, bmf_satisfaction F V v (sys_toformula f)) <-> (exists V', V'.(valN) nom_i = w /\ V.(valP) = V'.(valP) /\ (forall n, ~ V.(valN) n = V'.(valN) n -> SFreshIn n f) /\ forall v, bmf_satisfaction F V' v (sys_toformula g)).
  
  Lemma similar_eqn_sys_sat_trans_r' : forall f g h, eqn_sys_freshness f g -> sys_freshness g h -> similar_eqn_sys_sat f g -> similar_sys_sat g h -> similar_eqn_sys_sat f h.
    intros f g h Hx1 Hx2 Hf Hs.
    intros F w V Hv.
    split.
    - intro H.
      rewrite (Hf F w V) in H; auto.
      destruct H as [V' [Hv' [Hp' [Hn' H]]]].
      rewrite (Hs F w V') in H; auto.
      destruct H as [V'' [Hv'' [Hp'' [Hn'' H]]]].
      exists V''.
      do 2 (split; trivial); try congruence.
      split; trivial.
      intros n HnV'.
      destruct (V.(valN) n == V'.(valN) n).
      + rewrite e in HnV'.
        apply Hx1.
        apply (Hn'' n HnV').
      + apply (Hn' n c).
    - intros [V' [Hv' [Hp [Hn' Ho]]]].
      rewrite (Hf F w V); auto.
      exists V'.
      do 2 (split; trivial); try congruence.
      split; auto.
      rewrite (Hs F w V'); auto.
      exists V'.
      repeat (split; trivial); try congruence.
  Qed.

  Lemma Forall_nil_verum {T} (P : T -> Prop) : True <-> Forall P []. intuition. Qed.
  Lemma FreshIn_conj {N : Set} : forall f g (n : N), FreshIn n f /\ FreshIn n g <-> FreshIn n (b_conj f g).
    intros. intuition.
    constructor; auto.
    all: inversion H; auto.
  Qed.
  Lemma FreshIn_disj {N : Set} : forall f g (n : N), FreshIn n f /\ FreshIn n g <-> FreshIn n (b_disj f g).
    intros. intuition.
    constructor; auto.
    all: inversion H; auto.
  Qed.
  Lemma FreshIn_boxu {N : Set} : forall f o (n : N), FreshIn n f <-> FreshIn n (b_boxu o f).
    intros. intuition.
    constructor; auto.
    all: inversion H; auto.
  Qed.
  Lemma FreshIn_diau {N : Set} : forall f o (n : N), FreshIn n f <-> FreshIn n (b_diau o f).
    intros. intuition.
    constructor; auto.
    all: inversion H; auto.
  Qed.
  Lemma FreshIn_diab {N : Set} : forall o f g (n : N), FreshIn n f /\ FreshIn n g <-> FreshIn n (b_diab o f g).
    intros. intuition.
    constructor; auto.
    all: inversion H; auto.
  Qed.
  Lemma FreshIn_boxb {N : Set} : forall o f g (n : N), FreshIn n f /\ FreshIn n g <-> FreshIn n (b_boxb o f g).
    intros. intuition.
    constructor; auto.
    all: inversion H; auto.
  Qed.
  Lemma FreshIn_bigconj : forall l n, Forall (EFreshIn n) l <-> FreshIn n (b_bigconj (map eqn_toformula l)).
    intros.
    induction l.
    - cbn [map b_bigconj fold_right].
      intuition; constructor.
    - rewrite <- Forall_cons.
      cbn [map b_bigconj fold_right].
      unfold eqn_toformula at 1.
      rewrite IHl.
      unfold EFreshIn.
      intuition.
      constructor; auto.
      constructor; auto.
      1,2: inversion H1;
        inversion H4;
        auto.
      inversion H1; auto.
  Qed.

  Lemma advance_freshness :
    forall s e fn l, stream_fresh fn e l /\ stream_unique fn -> 
                stream_fresh (advance s fn) e l /\ stream_unique (advance s fn).
    intros s e fn l [H1 H2].
    split; induction s; auto;
    cbv [advance] in *;
    cbn [plus];
    intro n;
    rewrite !plus_n_Sm.
    exact (IHs (S n)).
    intro n'.
    rewrite !plus_n_Sm.
    auto.
  Qed.
  
  Ltac freshness_tauto := cbv [SFreshIn EFreshIn to_system eqns boxui diaui boxbi diabi boxu diau diab boxb eqn_toformula];
                          repeat (rewrite <- ?FreshIn_bigconj, <- ?Forall_cons, <- ?Forall_app, <- ?Forall_nil_verum, <- ?FreshIn_conj, <- ?FreshIn_disj, <- ?FreshIn_boxb, <- ?FreshIn_boxu, <- ?FreshIn_diab, <- ?FreshIn_diau;
                          cbn [a b]);
                          intros; auto; try tauto.
  
  Lemma same_sys_similar_sys : forall f g, same_sys_sat f g -> similar_sys_sat f g.
    intros f g.
    unfold same_sys_sat, similar_sys_sat.
    intros H F w V Hv.
    split.
    - intro H'.
      exists V.
      repeat (split; trivial).
      intros n Hn; congruence.
      rewrite <- (H F w V); auto.
    - intros [V' [Hv' [Hp' [Hn' Ho]]]].
      rewrite <- (H F w V' Hv') in Ho.
      intro v.
      eapply update_irrelevant_fresh'; eauto.
      intros n Hn.
      pose proof (Hn' n Hn) as Hh.
      revert Hh.
      destruct f as [f].
      cbv [sys_toformula eqns SFreshIn].
      clear H Ho Hn'.
      induction f; cbn [map b_bigconj fold_right].
      freshness_tauto; constructor.
      freshness_tauto.
  Qed.
  
  Ltac bmf_tauto := let Hr := fresh "A" in
                    let vr := fresh "v" in
                    cbv [to_system eqn_toformula sys_toformula a b eqns map b_bigconj fold_right boxui boxu bmf_UML];
                    cbn [bmf_satisfaction];
                    split; intros Hr vr; pose proof (Hr vr); try tauto.

  Lemma same_eqn_unordered : forall f g, same_eqn_sat {|a:=f;b:=g|} {|b:=f;a:=g|}. intros f g F w V. bmf_tauto. Qed.
  
  Lemma eqn_unordered_freshness : forall f g, same_eqn_freshness {|a:=f;b:=g|} {|b:=f;a:=g|}. intros f g n. freshness_tauto. Qed.


  
  Lemma similar_sys_sat_trans : forall f g h, similar_sys_sat f g -> similar_sys_sat g h -> sys_freshness f g -> sys_freshness g h -> similar_sys_sat f h.
    intros f g h Sfg Sgh Ffg Fgh F w V Hv.
    split.
    - intro H.
      rewrite (Sfg F w V) in H; auto.
      destruct H as [V' [Hv' [Hp' [Hn' H]]]].
      rewrite (Sgh F w V') in H; auto.
      destruct H as [V'' [Hv'' [Hp'' [Hn'' H]]]].
      exists V''. split; auto.
      rewrite Hp', Hp''.
      split; auto.
      split; auto.
      intros n HVn.
      destruct (V.(valN) n == V'.(valN) n).
      + rewrite e in HVn.
        apply Ffg.
        apply Hn''.
        auto.
      + apply Hn'.
        auto.
    - intros [V' [Hv' [Hp' [Hn' H]]]].
      rewrite (Sfg F w V); auto.
      exists V'.
      do 3 (split; trivial).
      rewrite (Sgh F w V'); auto.
      exists V'.
      do 3 (split; trivial).
      intuition.
  Qed.

  Lemma similar_sys_sat_trans_l' : forall f g h, same_sys_sat f g -> similar_sys_sat g h -> same_sys_freshness f g -> similar_sys_sat f h.
    intros f g h Sfg Sgh Ffg F w V Hv.
    split.
    - intro H.
      rewrite (Sfg F w V) in H; auto.
      rewrite (Sgh F w V) in H; auto.
      destruct H as [V'' [Hv'' [Hp'' [Hn'' H]]]].
      exists V''. repeat split; auto.
      intros.
      apply Ffg; auto.
    - intros [V' [Hv' [Hp' [Hn' H]]]].
      rewrite (Sfg F w V); auto.
      rewrite (Sgh F w V); auto.
      exists V'.
      do 3 (split; trivial).
      intros.
      apply Ffg; auto.
  Qed.
  Lemma similar_sys_sat_trans_r' : forall f g h, same_sys_sat g h -> similar_sys_sat f g -> similar_sys_sat f h.
    intros f g h Sfg Sgh F w V Hv.
    split.
    - intro H.
      rewrite (Sgh F w V) in H; auto.
      destruct H as [V' [Hv' [Hp' [Hn' H]]]].
      rewrite (Sfg F w V') in H; auto.
      exists V'. repeat split; auto.
    - intros [V' [Hv' [Hp' [Hn' H]]]].
      rewrite (Sgh F w V); auto.
      exists V'. repeat split; auto.
      rewrite (Sfg F w V'); auto.
  Qed.

  Lemma same_es_app_l : forall e s s', same_eqn_sys_sat e (to_system s) -> same_sys_sat (to_system (e :: s')) (to_system (s ++ s')).
    intros e s s' Ses F w V Hv.
    unfold to_system, sys_toformula.
    cbn [eqns].
    rewrite map_cons, bigconj_cons, map_app.
    cbn [bmf_satisfaction].
    rewrite forall_dist, (Ses F w V); auto.
    split.
    - intros [H1 H2] v.
      rewrite bigconj_Forall, <- Forall_app, <- !bigconj_Forall.
      intuition.
    - intros H.
      rewrite <- forall_dist.
      intro v.
      pose proof (H v).
      rewrite bigconj_Forall, <- Forall_app, <- !bigconj_Forall in H0.
      intuition.
  Qed.

  Lemma same_e_cons : forall e e' s, same_eqn_sat e e' -> same_sys_sat (to_system (e :: s)) (to_system (e' :: s)).
    intros e e' s See' F w V Hv.
    unfold to_system, sys_toformula.
    cbn [eqns].
    rewrite !map_cons, !bigconj_cons.
    cbn [bmf_satisfaction].
    rewrite !forall_dist, (See' F w V); tauto.
  Qed.
  
  Lemma bigconj_app' : forall (fs fs' : list bmf_plus) (F : bframe) V,
      (forall (v : ws), bmf_satisfaction F V v (b_bigconj (fs ++ fs'))) <->
      (forall (v : ws), bmf_satisfaction F V v (b_bigconj fs) /\ bmf_satisfaction F V v (b_bigconj fs')).
    intros fs fs' F V.
    split.
    - intros H v.
      pose proof (H v) as H'.
      rewrite bigconj_Forall, <- Forall_app, <- !bigconj_Forall in H'.
      auto.
    - intros H v. 
      rewrite bigconj_Forall, <- Forall_app, <- !bigconj_Forall.
      auto.
  Qed.

  (** and rule *)
  Lemma and_split_correct_l : forall (f g h : bmf_plus), same_eqn_sys_sat {|a:=f; b:=b_conj g h|} (to_system ([{|b:=f;a:=h|}; {|b:=f;a:=g|}])). intros f g h F w V Hv. bmf_tauto. Qed.
  Lemma and_split_freshness_l : forall (f g h : bmf_plus), same_eqn_sys_freshness {|a:=f; b:=b_conj g h|} (to_system ([{|b:=f;a:=h|}; {|b:=f;a:=g|}])). intros f g h n. freshness_tauto. Qed.
  Lemma and_split_correct_r : forall (f g h : bmf_plus), same_eqn_sys_sat {|b:=f; a:=b_conj g h|} (to_system ([{|b:=f;a:=h|}; {|b:=f;a:=g|}])). intros f g h F w V Hv. bmf_tauto. Qed.
  Lemma and_split_freshness_r : forall (f g h : bmf_plus), same_eqn_sys_freshness {|b:=f; a:=b_conj g h|} (to_system ([{|b:=f;a:=h|}; {|b:=f;a:=g|}])). intros f g h n. freshness_tauto. Qed.

  (** or rule *)
  Lemma or_switch_correct_l_1 : forall (f g h : bmf_plus), same_eqn_sat {|a:=f; b:=b_disj g h|} {|a:=g;b:=b_disj f h|}.
    intros f g h F w V Hv. bmf_tauto. Qed.
  Lemma or_switch_freshness_l_1 : forall (f g h : bmf_plus), same_eqn_freshness {|a:=f; b:=b_disj g h|} {|a:=g;b:=b_disj f h|}.
    intros f g h n. freshness_tauto. Qed.
  Lemma or_switch_correct_l_2 : forall (f g h : bmf_plus), same_eqn_sat {|a:=f; b:=b_disj g h|} {|a:=h;b:=b_disj g f|}.
    intros f g h F w V Hv. bmf_tauto. Qed.
  Lemma or_switch_freshness_l_2 : forall (f g h : bmf_plus), same_eqn_freshness {|a:=f; b:=b_disj g h|} {|a:=h;b:=b_disj g f|}.
    intros f g h n. freshness_tauto. Qed.
  Lemma or_switch_correct_r_1 : forall (f g h : bmf_plus), same_eqn_sat {|b:=f; a:=b_disj g h|} {|a:=g;b:=b_disj f h|}.
    intros f g h F w V Hv. bmf_tauto. Qed.
  Lemma or_switch_freshness_r_1 : forall (f g h : bmf_plus), same_eqn_freshness {|b:=f; a:=b_disj g h|} {|a:=g;b:=b_disj f h|}.
    intros f g h n. freshness_tauto. Qed.
  Lemma or_switch_correct_r_2 : forall (f g h : bmf_plus), same_eqn_sat {|b:=f; a:=b_disj g h|} {|a:=h;b:=b_disj g f|}.
    intros f g h F w V Hv. bmf_tauto. Qed.
  Lemma or_switch_freshness_r_2 : forall (f g h : bmf_plus), same_eqn_freshness {|b:=f; a:=b_disj g h|} {|a:=h;b:=b_disj g f|}.
    intros f g h n. freshness_tauto. Qed.

  (** unary box rule *)
  Lemma boxu_correct_l : forall (f g : bmf_plus) o, same_eqn_sat {|a:=f; b:=b_boxu o g|} {|a:=g;b:=boxui o f|}.
    intros f g o F w V Hv.
    cbv [to_system eqn_toformula sys_toformula a b eqns map b_bigconj fold_right boxui boxu bmf_UML].
    cbn [bmf_satisfaction].
    split; intros H v. 
    all: classical_right.
    all: intros v' Hv'.
    1: rewrite relu_inv in Hv'.
    2: rewrite <- relu_inv in Hv'.
    all: destruct (H v'); try congruence.
    all: exfalso; apply H0; apply H1; auto.
  Qed.
  Lemma boxu_freshness_l : forall (f g : bmf_plus) o, same_eqn_freshness {|a:=f; b:=b_boxu o g|} {|a:=g;b:=boxui o f|}.
    intros f g o n. freshness_tauto. Qed.
  Lemma boxu_correct_r : forall (f g : bmf_plus) o, same_eqn_sat {|b:=f; a:=b_boxu o g|} {|a:=g;b:=boxui o f|}.
    intros f g o F w V Hv.    
    cbv [to_system eqn_toformula sys_toformula a b eqns map b_bigconj fold_right boxui boxu bmf_UML].
    cbn [bmf_satisfaction].
    split; intros H v. 
    - classical_right.
      intros v' Hv'.
      rewrite relu_inv in Hv'.
      destruct (H v'); try congruence.
      exfalso; apply H0; apply H1; auto.
    - classical_left.
      intros v' Hv'.
      rewrite <- relu_inv in Hv'.
      destruct (H v'); try congruence.
      exfalso; apply H0; apply H1; auto. 
  Qed.
  Lemma boxu_freshness_r : forall (f g : bmf_plus) o, same_eqn_freshness {|b:=f; a:=b_boxu o g|} {|a:=g;b:=boxui o f|}.
    intros f g o n. freshness_tauto. Qed.

  (** binary box rule *)
  Lemma boxb_switch_correct_l_1 : forall (f g h : bmf_plus) o, same_eqn_sat {|a:=f; b:=b_boxb o g h|} {|a:=g;b:=boxbi o true f h|}.
    intros f g h o F w V Hv.
    cbv [to_system eqn_toformula sys_toformula a b eqns map b_bigconj fold_right boxbi boxb bmf_BML].
    cbn [bmf_satisfaction].
    split; intros H v. 
    - classical_right.
      intros v' u' Hv'.
      rewrite relb_inv_fst in Hv'.
      destruct (H v').
      left; auto.
      destruct (H1 v u'); auto.
      congruence.
    - classical_right.
      intros v' u' Hv'.
      rewrite <- relb_inv_fst in Hv'.
      destruct (H v').
      left; auto.
      destruct (H1 v u'); auto.
      congruence.
  Qed.
  Lemma boxb_switch_freshness_l_1 : forall (f g h : bmf_plus) o, same_eqn_freshness {|a:=f; b:=b_boxb o g h|} {|a:=g;b:=boxbi o true f h|}.
    intros f g h o n. freshness_tauto. Qed.
  
  Lemma boxb_switch_correct_l_2 : forall (f g h : bmf_plus) o, same_eqn_sat {|a:=f; b:=b_boxb o g h|} {|a:=h;b:=boxbi o false g f|}.
    intros f g h o F w V Hv.
    cbv [to_system eqn_toformula sys_toformula a b eqns map b_bigconj fold_right boxbi boxb bmf_BML].
    cbn [bmf_satisfaction].
    split; intros H v. 
    - classical_right.
      intros v' u' Hv'.
      rewrite relb_inv_snd in Hv'.
      destruct (H u').
      right; auto.
      destruct (H1 v' v); auto.
      congruence.
    - classical_right.
      intros v' u' Hv'.
      rewrite <- relb_inv_snd in Hv'.
      destruct (H u').
      right; auto.
      destruct (H1 v' v); auto.
      congruence.
  Qed.
  Lemma boxb_switch_freshness_l_2 : forall (f g h : bmf_plus) o, same_eqn_freshness {|a:=f; b:=b_boxb o g h|} {|a:=h;b:=boxbi o false g f|}.
    intros f g h o n. freshness_tauto. Qed.
  
  Lemma boxb_switch_correct_r_1 : forall (f g h : bmf_plus) o, same_eqn_sat {|b:=f; a:=b_boxb o g h|} {|a:=g;b:=boxbi o true f h|}.
    intros f g h o F w V Hv.
    cbv [to_system eqn_toformula sys_toformula a b eqns map b_bigconj fold_right boxbi boxb bmf_BML].
    cbn [bmf_satisfaction].
    split; intros H v. 
    - classical_right.
      intros v' u' Hv'.
      rewrite relb_inv_fst in Hv'.
      destruct (H v').
      destruct (H1 v u'); auto.
      congruence.
      left; auto.
    - classical_left.
      intros v' u' Hv'.
      rewrite <- relb_inv_fst in Hv'.
      destruct (H v').
      left; auto.
      destruct (H1 v u'); auto.
      congruence.
  Qed.
  Lemma boxb_switch_freshness_r_1 : forall (f g h : bmf_plus) o, same_eqn_freshness {|b:=f; a:=b_boxb o g h|} {|a:=g;b:=boxbi o true f h|}.
    intros f g h o n. freshness_tauto. Qed.
  Lemma boxb_switch_correct_r_2 : forall (f g h : bmf_plus) o, same_eqn_sat {|b:=f; a:=b_boxb o g h|} {|a:=h;b:=boxbi o false g f|}.
    intros f g h o F w V Hv.
    cbv [to_system eqn_toformula sys_toformula a b eqns map b_bigconj fold_right boxbi boxb bmf_BML].
    cbn [bmf_satisfaction].
    split; intros H v. 
    - classical_right.
      intros v' u' Hv'.
      rewrite relb_inv_snd in Hv'.
      destruct (H u').
      destruct (H1 v' v); auto.
      congruence.
      right; auto.
    - classical_left.
      intros v' u' Hv'.
      rewrite <- relb_inv_snd in Hv'.
      destruct (H u').
      right; auto.
      destruct (H1 v' v); auto.
      congruence.
  Qed.
  Lemma boxb_switch_freshness_r_2 : forall (f g h : bmf_plus) o, same_eqn_freshness {|b:=f; a:=b_boxb o g h|} {|a:=h;b:=boxbi o false g f|}.
    intros f g h o n. freshness_tauto. Qed.
  
  Lemma at_lemma : forall P w, (forall (v : ws), (~ w = v \/ P v)) <-> P w.
    intros P w.
    split; intro H.
    - destruct (H w); try congruence.
    - intro v. destruct (w == v).
      + inversion e. right; subst; auto.
      + left. congruence.
  Qed.  

  (** unary diamond rule *)
  Lemma diau_correct_l : forall n o (f : bmf_plus) l k, stream_fresh' k {|a:=b_negnom n;b:=b_diau o f|} l -> similar_sys_sat (to_system ({|a:=b_negnom n; b:=b_diau o f|} :: l)) (to_system ([{|a:=b_negnom n; b:=b_diau o (b_nom k)|}; {|a:=b_negnom k;b:=f|}] ++ l)).
    intros n o f l k [[Hf Hl] Hk] F w V Hv.
    cbv [eqn_toformula boxbi boxb bmf_BML to_system sys_toformula eqns].
    rewrite map_app, !map_cons, !bigconj_cons.
    cbn [bmf_satisfaction a b b_bigconj fold_right map app].
    assert (~k = n) by (destruct Hf as [Hf _]; cbn in Hf; inversion Hf; auto).
    rewrite !forall_dist, !at_lemma.
    split. 
    - intros [H' Hsl].
      destruct H' as [v' [Rv' Hv']].
      exists (update_valN V k v').
      cbn [update_valN valN].
      rewrite equivb_refl.
      rewrite equivb_neg_refl in H; rewrite H.
      rewrite equivb_neg_refl in Hk; rewrite Hk.
      split; trivial.
      rewrite !forall_dist, !at_lemma.
      split; auto.      
      split.
      intros n' Hn'.
      destruct (k ==b n') eqn:Hkn'; try congruence.
      rewrite <- equivb_refl' in Hkn'.
      subst.
      revert Hl Hf.
      freshness_tauto.
      split; eauto.
      split.
      apply update_irrelevant_fresh; auto.
      revert Hf; freshness_tauto.
      intro v.
      apply update_irrelevant_fresh; auto.
      revert Hl; freshness_tauto.
    - intros [V' [Hv' [Hp [Hn Ho]]]].
      unfold SFreshIn in Hn.
      cbn [eqns] in Hn.
      rewrite !forall_dist in Ho.
      destruct Ho as [H1 [H2 H3]].
      rewrite at_lemma in H1.
      rewrite at_lemma in H2.
      destruct H1 as [v [Rv Vv]].
      split.
      exists v; split; auto.
      destruct (valN V n == valN V' n); [rewrite e; auto | idtac].
      pose proof (Hn n c) as H';
        inversion H'; inversion H4; cbn [a] in H6; inversion H6; congruence.
      rewrite <- Vv.
      assert (H': forall n', ~ V'.(valN) n' = V.(valN) n' -> FreshIn n' f) by (intros n' Hn'; rewrite neq_sym in Hn'; pose proof (Hn n' Hn') as H'; revert H'; freshness_tauto).
      rewrite update_irrelevant_fresh'; eauto.
      intro v'.
      rewrite update_irrelevant_fresh'; eauto.
      intros n' Hn'. pose proof (Hn n' Hn') as H'. revert H'. freshness_tauto.
  Qed.
  Lemma diau_freshness_l : forall n o (f : bmf_plus) l k, sys_freshness (to_system ({|a:=b_negnom n; b:=b_diau o f|} :: l)) (to_system ([{|a:=b_negnom n; b:=b_diau o (b_nom k)|}; {|a:=b_negnom k;b:=f|}] ++ l)).
    intros n o f l k n'. freshness_tauto. Qed.

  Lemma diau_correct_r : forall n o f l k, stream_fresh' k {|b:=b_negnom n;a:=b_diau o f|} l -> similar_sys_sat (to_system ({|b:=b_negnom n; a:=b_diau o f|} :: l)) (to_system ([{|a:=b_negnom n; b:=b_diau o (b_nom k)|}; {|a:=b_negnom k;b:=f|}] ++ l)). 
    intros n o f l k [[Hf Hl] Hk].
    eapply similar_sys_sat_trans_l'.
    apply same_e_cons.
    apply same_eqn_unordered.
    apply diau_correct_l; trivial.
    revert Hf.
    unfold stream_fresh'.
    freshness_tauto.
    intro n'; freshness_tauto.
  Qed.
  
  Lemma diau_freshness_r : forall n o f l k, sys_freshness (to_system ({|b:=b_negnom n; a:=b_diau o f|} :: l)) (to_system ([{|a:=b_negnom n; b:=b_diau o (b_nom k)|}; {|a:=b_negnom k;b:=f|}] ++ l)).
    intros n o f l k n'. freshness_tauto. Qed.

  Lemma valn_update_irrelevant {N : Set} `{EqDec N eq} : forall (k l : N) V v, ~l = k -> V.(valN) k = (update_valN V l v).(valN) k.
    intros k l V v Hkl.
    unfold update_valN.
    cbn [valN].
    rewrite equivb_neg_refl in Hkl. rewrite Hkl.
    reflexivity.
  Qed.  

  (** binary diamond rule *)
  Lemma diab_correct_l : forall n o (f g : bmf_plus) r k l, stream_fresh' k {|a:=b_negnom n;b:=b_diab o f g|} r -> stream_fresh' l {|a:=b_negnom n;b:=b_diab o f g|} r -> ~ l = k -> similar_sys_sat (to_system ({|a:=b_negnom n; b:=b_diab o f g|} :: r)) (to_system ([{|a:=b_negnom n; b:=b_diab o (b_nom k) (b_nom l)|}; {|a:=b_negnom k;b:=f|}; {|a:=b_negnom l;b:=g|}] ++ r)).
    intros n o f g r k l [[Hkf Hkr] Hk] [[Hlf Hlr] Hl] Hkl F w V Hv.
    cbv [eqn_toformula boxbi boxb bmf_BML to_system sys_toformula eqns].
    cbn [bmf_satisfaction a b b_bigconj fold_right map].
    rewrite !forall_dist, !at_lemma.
    assert (~k = n) as Hkn by (destruct Hkf as [Hf _]; cbn in Hf; inversion Hf; auto).
    assert (~l = n) as Hln by (destruct Hlf as [Hf _]; cbn in Hf; inversion Hf; auto).
    split. 
    - intros [[v' [u' [Rvu' Hv']]] Hr].
      exists (update_valN (update_valN V k v') l u').
      rewrite !map_app, !map_cons.
      cbn [a b update_valN valN].
      cbv [SFreshIn eqns].
      rewrite !equivb_neg_refl in Hk; rewrite Hk.
      rewrite !equivb_neg_refl in Hl; rewrite Hl.
      split; trivial.
      do 2 (split; auto).      
      intros n' Hn'.
      destruct (l ==b n') eqn:Hln'.
      rewrite <- equivb_refl' in Hln'. rewrite <- Hln'; auto.
      destruct (k ==b n') eqn:Hkn'.
      rewrite <- equivb_refl' in Hkn'. rewrite <- Hkn'; auto.
      congruence.
      rewrite bigconj_app', !bigconj_cons.
      cbn [bmf_satisfaction].
      rewrite !forall_dist, !at_lemma.
      repeat split; eauto.
      exists v', u'.
      cbv [update_valN].
      cbn [valN].
      rewrite equivb_neg_refl in Hkn. rewrite !Hkn.
      rewrite equivb_neg_refl in Hln. rewrite !Hln.
      rewrite equivb_neg_refl in Hkl. rewrite !Hkl.
      rewrite !equivb_refl; auto.
      1,2: rewrite <- !update_irrelevant_fresh; try (revert Hkf Hlf; freshness_tauto); cbn [valN]. 
      rewrite <- valn_update_irrelevant; cbn [valN update_valN]; auto; rewrite equivb_refl; intuition.
      cbn [valN update_valN]; auto; rewrite equivb_refl; intuition.
      intro v.
      rewrite <- !update_irrelevant_fresh; auto.
      all: revert Hkr Hlr; freshness_tauto.
    - rewrite map_app, !map_cons.
      cbn [a b].
      intros [V' [Hv' [Hp [Hn Ho]]]].
      rewrite bigconj_app', !bigconj_cons in Ho.
      cbn [bmf_satisfaction] in Ho.
      rewrite !forall_dist, !at_lemma in Ho.
      destruct Ho as [[H1 [H2 [H3 _]]] H4].
      destruct H1 as [v' [u' [Ruv' [Hkv Hlu]]]].
      rewrite Hkv in H2.
      rewrite Hlu in H3.
      split.
      exists v', u'.
      split.
      destruct (V.(valN) n == V'.(valN) n).
      rewrite e; trivial.
      apply Hn in c.
      inversion c.
      inversion H1.
      cbn [a] in H6.
      inversion H6.
      congruence.
      rewrite <- update_irrelevant_fresh'  with (V0 := V) in H2; auto.
      rewrite <- update_irrelevant_fresh'  with (V0 := V) in H3; auto.
      1,2: intros n' Hn'; apply Hn in Hn'; revert Hn'; freshness_tauto.
      intro v; rewrite <- update_irrelevant_fresh'; eauto.
      intros n' Hn'; rewrite neq_sym in Hn'; apply Hn in Hn'; revert Hn'; freshness_tauto.
  Qed.
  Lemma diab_freshness_l : forall n o (f g : bmf_plus) r k l, sys_freshness (to_system ({|a:=b_negnom n; b:=b_diab o f g|} :: r)) (to_system ([{|a:=b_negnom n; b:=b_diab o (b_nom k) (b_nom l)|}; {|a:=b_negnom k;b:=f|}; {|a:=b_negnom l;b:=g|}] ++ r)).
    intros n o f g r k l n'. freshness_tauto. Qed.
  
  Lemma diab_correct_r :  forall n o (f g : bmf_plus) r k l, stream_fresh' k {|b:=b_negnom n;a:=b_diab o f g|} r -> stream_fresh' l {|b:=b_negnom n;a:=b_diab o f g|} r -> ~ l = k -> similar_sys_sat (to_system ({|b:=b_negnom n; a:=b_diab o f g|} :: r)) (to_system ([{|a:=b_negnom n; b:=b_diab o (b_nom k) (b_nom l)|}; {|a:=b_negnom k;b:=f|}; {|a:=b_negnom l;b:=g|}] ++ r)). 
     intros n o f g r k l [[Hfk Hkr] Hk] [[Hfl Hlr] Hl] Hkl.
     eapply similar_sys_sat_trans_l'.
     apply same_e_cons. apply same_eqn_unordered.
     apply diab_correct_l; trivial.
     1,2: revert Hfk Hfl; unfold stream_fresh'; freshness_tauto.
     intro n'. revert Hfk Hfl. freshness_tauto.          
  Qed.

  Lemma diab_freshness_r : forall n o (f g : bmf_plus) r k l, sys_freshness (to_system ({|b:=b_negnom n; a:=b_diab o f g|} :: r)) (to_system ([{|a:=b_negnom n; b:=b_diab o (b_nom k) (b_nom l)|}; {|a:=b_negnom k;b:=f|}; {|a:=b_negnom l;b:=g|}] ++ r)). 
    intros n o f g r k l n'. freshness_tauto. Qed.
    
  Hint Resolve and_split_correct_l and_split_freshness_l and_split_correct_r and_split_freshness_r or_switch_correct_l_1 or_switch_freshness_l_1 or_switch_correct_l_2 or_switch_freshness_l_2 or_switch_correct_r_1 or_switch_freshness_r_1 or_switch_correct_r_2 or_switch_freshness_r_2 boxu_correct_l boxu_freshness_l boxu_correct_r boxu_freshness_r boxb_switch_correct_l_1 boxb_switch_freshness_l_1 boxb_switch_correct_l_2 boxb_switch_freshness_l_2 boxb_switch_correct_r_1 boxb_switch_freshness_r_1 boxb_switch_correct_r_2 boxb_switch_freshness_r_2 diau_freshness_r diau_freshness_l diab_freshness_l diab_freshness_r.



  Lemma failure_same_sat : forall (e : eqn) l, same_sys_sat (to_system (e :: l)) (to_system ([e] ++ l)).
    intros e l F w V Hv. cbn. bmf_tauto. Qed.

  Lemma Permutation_same_sat : forall (l1 l2 : list eqn), Permutation l1 l2 -> same_sys_sat (to_system l1) (to_system l2).
    intros l1 l2 P.
    induction P; intros F w V Hv; cbv [to_system sys_toformula eqns]; rewrite ?map_cons, ?bigconj_cons; cbn [bmf_satisfaction].
    - tauto.
    - change (b_bigconj (map eqn_toformula ?v)) with (sys_toformula (to_system v)).
      rewrite !forall_dist.
      rewrite <- (IHP F w V); auto.
      tauto.
    - change (b_bigconj (map eqn_toformula ?v)) with (sys_toformula (to_system v)).
      rewrite !forall_dist.
      tauto.
    - rewrite (IHP1 F w V), (IHP2 F w V); auto.
      tauto.
  Qed.

  Lemma Permutation_sys_freshness : forall (l1 l2 : list eqn), Permutation l1 l2 -> sys_freshness (to_system l1) (to_system l2).
    intros l1 l2 P n.
    induction P; freshness_tauto.
  Qed.
  
  Lemma Permutation_trans_l : forall (l1 l2 l3 : list eqn), Permutation l1 l2 -> similar_sys_sat (to_system l2) (to_system l3) -> similar_sys_sat (to_system l1) (to_system l3).
    intros l1 l2 l3 P S.
    eapply similar_sys_sat_trans_l'; try apply S.
    apply Permutation_same_sat; auto.
    clear S.
    intro n. induction P; freshness_tauto.
  Qed.

  Lemma Permutation_trans_r : forall (l1 l2 l3 : list eqn), Permutation l2 l3 -> similar_sys_sat (to_system l1) (to_system l2) -> similar_sys_sat (to_system l1) (to_system l3).
    intros l1 l2 l3 P S.
    eapply similar_sys_sat_trans_r'; try apply S.
    apply Permutation_same_sat; auto.
  Qed.


  Lemma same_sys_freshness_app_r : forall f g h, same_sys_freshness (to_system f) (to_system g) -> same_sys_freshness (to_system (f ++ h)) (to_system (g ++ h)).
    intros f g h H n.
    pose proof (H n) as H'.
    revert H'.
    freshness_tauto.
  Qed.
  Lemma same_sys_freshness_app_l : forall f g h, same_sys_freshness (to_system f) (to_system g) -> same_sys_freshness (to_system (h ++ f)) (to_system (h ++ g)).
    intros f g h H n.
    pose proof (H n) as H'.
    revert H'.
    freshness_tauto.
  Qed.

  Lemma singleton_freshness : forall e f, same_eqn_sys_freshness e f -> same_sys_freshness (to_system ([e])) f.
    intros e f H n.
    pose proof (H n) as H'.
    revert H'.
    freshness_tauto.
  Qed.
  
  Lemma singleton_freshness_cons : forall e f l, same_eqn_sys_freshness e (to_system f) -> same_sys_freshness (to_system (e :: l)) (to_system (f ++ l)).
    intros e f l H n.
    pose proof (H n) as H'.
    revert H'.
    freshness_tauto.
  Qed.

  Lemma freshness_cons : forall e e' l, same_eqn_freshness e e' -> same_sys_freshness (to_system (e :: l)) (to_system (e' :: l)).
    intros e f l H n.
    pose proof (H n) as H'.
    revert H'.
    freshness_tauto.
  Qed.
    
  Lemma sys_freshness_refl : forall l, sys_freshness l l.
    intros l n; tauto.
  Qed.

  Lemma sys_freshness_trans : forall l m n, sys_freshness l m -> sys_freshness m n -> sys_freshness l n.
    intros l m n. unfold sys_freshness. auto.
  Qed.


  Hint Resolve failure_same_sat same_eqn_sat_eqn_sat same_eqn_sys_sat_similar_eqn_sys_sat similar_eqn_sys_sat_eqn_sys_sat same_sys_similar_sys same_sys_freshness_app_l same_sys_freshness_app_r singleton_freshness singleton_freshness_cons sys_freshness_refl freshness_cons.

  Lemma hidden_copure {T U} : forall f (g : T -> U), (match f with
                                                | succ: x => g x
                                                | fail: x => g x
                                                end) = g (copure f).
    destruct f;
    auto.
  Qed.

  Tactic Notation "freshness" "with" hyp(H) := let H' := fresh "H'" in
                                               let n := fresh "n" in
                                               intro n; unfold stream_fresh, stream_fresh', stream_unique in *; pose proof (H n) as H'; revert H'; freshness_tauto.

  Tactic Notation "freshness" "with" hyp(H) constr(n) := let H' := fresh "H'" in
                                                         unfold stream_fresh, stream_fresh', stream_unique in *; pose proof (H n) as H'; revert H'; freshness_tauto.



  Ltac auto_purity :=
    repeat match goal with
           | p: pro |- _ => extend (@p_verum opu opb (nom + nat) p)
           | p: pro |- _ => extend (@p_falsum opu opb (nom + nat) p)
           | p: pro |- context[b_nom ?n] => extend (@p_nom opu opb (nom + nat) p n)
           | p: pro |- context[b_negnom ?n] => extend (@p_negnom opu opb (nom + nat) p n)
           | H: true = false |- _ => congruence
           | H: false = true |- _ => congruence
           | H: ?h = true |- _ => rewrite !H
           | H: ?h = false |- _ => rewrite !H
           | H: true = ?h |- _ => rewrite <- !H
           | H: false = ?h |- _ => rewrite <- !H
           | H: ?a && ?b = true |- context[?a] => rewrite andb_true_iff in H; destruct H
           | H: ?a && ?b = true |- context[?b] => rewrite andb_true_iff in H; destruct H
           | H: ?a && ?b = true, H': ?a = false |- _ => rewrite andb_true_iff in H; destruct H; congruence
           | H: ?a && ?b = true, H': ?b = false |- _ => rewrite andb_true_iff in H; destruct H; congruence 
           | H: bmf_pure_in ?p ?f = false, H': PureIn ?p ?f |- _ => apply (reflect_iff _ _ (PureIn_reflect _ _)) in H'; congruence 
           | H: PureIn ?p ?f |- context[bmf_pure_in ?p ?f] => apply (reflect_iff _ _ (PureIn_reflect _ _)) in H
           | H: PureIn ?p (b_conj ?f ?g) |- _ => notHyp (PureIn p f) || notHyp (PureIn p g); inversion H
           | H: PureIn ?p (b_disj ?f ?g) |- _ => notHyp (PureIn p f) || notHyp (PureIn p g); inversion H
           | H: PureIn ?p (b_diab _ ?f ?g) |- _ => notHyp (PureIn p f) || notHyp (PureIn p g); inversion H
           | H: PureIn ?p (b_boxb _ ?f ?g) |- _ => notHyp (PureIn p f) || notHyp (PureIn p g); inversion H
           | H: PureIn ?p (b_diau _ ?f) |- _ => notHyp (PureIn p f); inversion H
           | H: PureIn ?p (b_boxu _ ?f) |- _ => notHyp (PureIn p f); inversion H
           | H: bmf_pure_in ?p ?f = true |- _ => notHyp (PureIn p f); apply (reflect_iff _ _ (PureIn_reflect _ _)) in H
           | H: PureIn ?p (b_prop ?q) |- _ => notHyp (~ q = p); inversion H
           | H: PureIn ?p (b_negprop ?q) |- _ => notHyp (~ q = p); inversion H
           | H: ?p = ?q |- context[?p =? ?q] => rewrite H, Nat.eqb_refl
           | H: ?q = ?p |- context[?p =? ?q] => rewrite H, Nat.eqb_refl
           | |- context[?p =? ?p] => rewrite Nat.eqb_refl
           | H: ~ ?p = ?q |- context[?p =? ?q] => apply Nat.eqb_neq in H; rewrite H
           | H: ~ ?p = ?q |- context[?q =? ?p] => apply Nat.eqb_neq in H; rewrite Nat.eqb_sym in H; rewrite H
           | H: bmf_pure_in ?p ?f = false |- _ => cbn in H
           end.

  Lemma nom_freshness : forall fn : Stream nat, stream_unique fn -> forall n n', n <> n' -> FreshIn (inr (fn n) : nom + nat) (b_nom (inr (fn n'))).
    intros fn H0 n n' Hnn'.
    constructor; introversion; apply (H0 n n' Hnn'); auto.
  Qed.
  Lemma negnom_freshness : forall fn : Stream nat, stream_unique fn -> forall n n', n <> n' -> FreshIn (inr (fn n) : nom + nat) (b_negnom (inr (fn n'))).
    intros fn H0 n n' Hnn'.
    constructor; introversion; apply (H0 n n' Hnn'); auto.
  Qed.

  Lemma same_sys_freshness_sys_freshness : forall l1 l2, same_sys_freshness l1 l2 -> sys_freshness l1 l2.
    unfold same_sys_freshness, sys_freshness.
    intros l1 l2 H.
    freshness with H.
  Qed.
  
  Tactic Notation "auto" "complexity" := simpl; auto_purity; simpl; try omega; try solve [deceq; simpl; omega].

  Hint Resolve nom_freshness negnom_freshness same_sys_freshness_sys_freshness.
  Hint Unfold stream_fresh stream_fresh' stream_unique.
  
  Program Fixpoint rule_transform_fresh (p : pro) (fn : Stream nat) (ed : eqn) {measure (complexity p ed)} : forall l, stream_fresh fn ed l /\ stream_unique fn -> let fn' := fst (rule_transform p fn ed) in (forall n, (Forall (EFreshIn (inr (fn' n))) (copure (snd (rule_transform p fn ed)) ++ l)) /\ ~ inr (fn' n) = nom_i) /\ stream_unique fn' := _.
  Next Obligation.
    rewrite rule_transform_unfold.
    rewrite !f_if.
    deceq.
    cbn; split; auto; freshness with H.
    destruct ed as [av bv]; cbn [a b].
    destruct av; cbn; destruct bv; cbn.
    all: lazymatch goal with
         | |- context[rule_transform] => idtac
         | |- _ => solve[split; auto; freshness with H]
         end.
    all: rewrite ?f_if; cbn [fst snd].
    all: deceq.
    all: lazymatch goal with
         | |- context[rule_transform] => idtac
         | |- _ => solve [split; auto; freshness with H]
         end.
    all: rewrite ?hidden_copure.
    all: lazymatch goal with
         | |- _ /\ stream_unique (fst (rule_transform ?p ?fn ?e)) => unfold stream_unique; apply rule_transform_fresh; [auto complexity | split; auto; freshness with H ]
         | |- ?ow => idtac
         end.
    all: unfold stream_unique; rewrite <- forall_dist.
    all: intro n.
    all: repeat unlet.
    all: repeat rewrite ?match_match_failure, ?f_if, ?f_match_failure, ?hidden_copure.
    all: cbn [fst snd].
    all: repeat (change (copure (succ: ?h)) with (h); change (copure (fail: ?h)) with (h)).
    all: revert n.
    all: rewrite forall_dist.
    all: change (forall f1 n', f1 <> n' -> ?fn f1 <> ?fn n') with (stream_unique fn).
    all: lazymatch goal with
         | H: stream_fresh ?fn ?ed ?l,
           D1: rule_transform ?p ?fn ?e = (?s, ?f), D2: rule_transform ?p ?s ?e0 = (?s0, ?f0)
           |- _ => (** /\ *)
            pose proof (rule_transform_fresh p fn e) as H_;
              rewrite D1 in H_; cbn [fst snd Equation.a Equation.b] in H_;
              assert (H''' : stream_fresh fn e (e0 :: l)) by freshness with H;
              assert (T: complexity p e < complexity p ed) by (auto complexity);  (* if that is inlined it gets significantly slower*)
              destruct (H_ T (e0 :: l) (Logic.conj H''' H0)) as [H' H0']; clear H_ T;
            pose proof (rule_transform_fresh p s e0) as H_;
              rewrite D2 in H_; cbn [fst snd Equation.a Equation.b] in H_;
              assert (H'''' : stream_fresh s e0 (copure f ++ l)) by freshness with H';
              assert (T: complexity p e0 < complexity p ed) by (auto complexity);
              destruct (H_ T (copure f ++ l) (Logic.conj H'''' H0')) as [H'' H0'']; clear H_ T;
                split; auto; rewrite <- app_assoc; auto
         | H: stream_fresh ?fn ?ed ?l,
           D1: rule_transform ?p ?fn' ?e = (?s, ?f)
           |- (forall n, Forall (EFreshIn (inr (?s0 n))) ((?o :: copure ?f) ++ ?l) /\ inr (?s0 n) <> nom_i) /\ stream_unique ?s0 => (** diamond *)
            pose proof (rule_transform_fresh p fn' e) as H_;
              rewrite D1 in H_; cbn [fst snd Equation.a Equation.b] in H_;
              assert (H''' : stream_fresh fn' e (o :: l)); cbv [advance plus Equation.a Equation.b]; [ intro n'; split; freshness with H n'; repeat split; freshness_tauto; auto; freshness with H (S n'); freshness with H (S (S n')) | idtac];
              assert (T: complexity p e < complexity p ed) by (auto complexity);
              assert (H_': stream_unique fn') by (cbv [advance]; cbn [plus]; auto);
              destruct (H_ T (o :: l) (Logic.conj H''' H_')) as [H' H0']; clear H_ T H_'; split; [ rewrite !forall_dist; repeat split; freshness with H' | auto ]
         | H: stream_fresh ?fn ?ed ?l,
           D1: rule_transform ?p ?fn' ?e = (?s0, ?f),
           D2: (let (_, c) := rule_transform ?p ?s0 ?e0 in (_, _)) = (_, ?f0)
           |- (forall n, Forall (EFreshIn (inr (?s1 n))) ((?o :: copure ?f0) ++ ?l) /\ inr (?s1 n) <> nom_i) /\ stream_unique ?s1 => (** bin diamond *)
            destruct (rule_transform p s0) as [s' c] eqn:D3;
               inversion D2;
               repeat (rewrite ?hidden_copure, ?match_match_failure, ?f_match_failure;
               repeat change (copure (fail: ?x)) with (x); repeat change (copure (succ: ?x)) with (x));
               subst; clear D2;
            pose proof (rule_transform_fresh p fn' e) as H_;
              rewrite D1 in H_; cbn [fst snd Equation.a Equation.b] in H_;
              assert (H''' : stream_fresh fn' e (e0 :: o :: l)); cbv [advance plus Equation.a Equation.b]; [ intro n'; split; freshness with H n'; repeat split; freshness_tauto; auto; freshness with H (S n'); freshness with H (S (S n')) | idtac];
              assert (T: complexity p e < complexity p ed) by (auto complexity);
              assert (H_': stream_unique fn') by (cbv [advance]; cbn [plus]; auto);
              destruct (H_ T (e0 :: o :: l) (Logic.conj H''' H_')) as [H' H0']; clear H_ T H_';
            pose proof (rule_transform_fresh p s0 e0) as H_;
              rewrite D3 in H_; cbn [fst snd Equation.a Equation.b] in H_;
              assert (H''''' : stream_fresh s0 e0 (copure f ++ o :: l)); cbv [advance plus Equation.a Equation.b]; [ intro n'; split; freshness with H' n'; repeat split; freshness_tauto; auto; freshness with H' (S n'); freshness with H' (S (S n')) | idtac];
              assert (T: complexity p e0 < complexity p ed) by (auto complexity);
              assert (H_': stream_unique s0) by (cbv [advance]; cbn [plus]; auto);
              destruct (H_ T (copure f ++ o :: l) (Logic.conj H''''' H_')) as [H'' H0'']; clear H_ T H_';
              split; [ freshness with H'' | auto ]
         end.
  Qed.
  

  Program Fixpoint rule_transform_sys_fresh (p : pro) (fn : Stream nat) (ed : eqn) {measure (complexity p ed)} : forall l, stream_fresh fn ed l /\ stream_unique fn -> sys_freshness (to_system (ed :: l)) (to_system (copure (snd (rule_transform p fn ed)) ++ l)) := _.
  Next Obligation.
    rewrite rule_transform_unfold.
    rewrite !f_if.
    cbn [snd].
    deceq; auto.
    all: destruct ed as [av bv].
    all: cbn [a b] in *.
    all: destruct av; destruct bv; cbn [snd].
    all: auto.
    all: rewrite ?f_if.
    all: cbn [snd].
    all: lazymatch goal with
         | |- sys_freshness (to_system (?e :: ?l)) (to_system (copure (snd (rule_transform ?p ?fn ?e')) ++ ?l)) =>
           eapply sys_freshness_trans; [idtac | apply (rule_transform_sys_fresh p fn e')]; [assert (same_sys_freshness (to_system (e :: l)) (to_system (e' :: l))) as H' by (apply freshness_cons; auto); freshness with H' | auto complexity | split; auto; freshness with H]
         | |- ?ow => idtac
         end.
    all: deceq.
    all: repeat change (copure (fail: ?x)) with (x); repeat change (copure (succ: ?x)) with (x).
    all: cbn [app].
    all: auto.
    all: lazymatch goal with
         | |- sys_freshness (to_system (?e :: ?l)) (to_system (copure (snd (rule_transform ?p ?fn ?e')) ++ ?l)) =>
           eapply sys_freshness_trans; [idtac | apply (rule_transform_sys_fresh p fn e')]; [assert (same_sys_freshness (to_system (e :: l)) (to_system (e' :: l))) as H' by (apply freshness_cons; auto); freshness with H' | auto complexity; shelve | split; auto; freshness with H]
         | |- ?ow => idtac
         end.
    all: cbv [fcapp fmap fmap__ env_Functor failure_Functor bind failure_Monad join join__ m_f envt_Functor].
    all: repeat unlet.
    all: repeat rewrite ?f_if, ?hidden_copure, ?match_match_failure, ?f_match_failure in *.
    all: cbn [snd].
    all: repeat change (copure (fail: ?x)) with (x); repeat change (copure (succ: ?x)) with (x).
    all: lazymatch goal with
         |  D1: rule_transform ?p ?fn ?e = (?s, ?f), D2: rule_transform ?p ?s ?e0 = (_, ?f0) |- sys_freshness (to_system (?ed :: ?l)) (to_system ((copure ?f0 ++ copure ?f) ++ ?l)) => (** /\ - rule *)
            pose proof (rule_transform_sys_fresh p fn e) as H_;
              rewrite D1 in H_; cbn [fst snd Equation.a Equation.b] in H_;
              assert (H''' : stream_fresh fn e (e0 :: l)) by freshness with H;
              match type of H_ with
              | ?TT -> ?Ht -> _ => assert (T : TT) by (auto complexity; shelve)
              end;
              pose proof (H_ T (e0 :: l) (Logic.conj H''' H0)) as H'; clear H_ T;
            destruct (rule_transform_fresh p fn e (e0 :: l) ltac:(split; auto; freshness with H)) as [H_' H_''];
              rewrite !D1 in *; cbn [fst snd] in *;   
            pose proof (rule_transform_sys_fresh p s e0 ltac:(auto complexity) (copure f ++ l) ltac:(split; auto; freshness with H_')) as H_;
              rewrite !D2 in *; cbn [fst snd] in *;
            rewrite <- app_assoc;
            eapply sys_freshness_trans; [ eapply sys_freshness_trans; [idtac | eapply Permutation_sys_freshness; apply Permutation_sym; apply Permutation_middle] | apply H_ ];
            eapply sys_freshness_trans; [idtac | apply H'];
            apply same_sys_freshness_sys_freshness; change (?a :: ?b :: l) with ([a; b] ++ l);
            apply singleton_freshness_cons; intro n; freshness_tauto 
         | |- _ => idtac
         end.
    all: lazymatch goal with
         | D0: rule_transform ?p (advance 1 ?fn) ?e = (_, ?f) |- sys_freshness _ (to_system ((?e' :: copure ?f) ++ ?l)) => (** diamond rule *)
            pose proof (rule_transform_sys_fresh p (advance 1 fn) e ltac:(auto complexity) (e' :: l) ltac:( cbv [advance plus a b]; split; auto;
               repeat split; cbn [a b]; auto;
               [ freshness with H (S n)
               | revert n; freshness with H; repeat split; auto; freshness with H (S n)
               | freshness with H (S n)
               ])
              ) as H_;
              rewrite D0 in H_; cbn [fst snd Equation.a Equation.b] in H_;
            cbn [app];
            eapply sys_freshness_trans; [ idtac | eapply sys_freshness_trans; [apply H_ | apply Permutation_sys_freshness; apply Permutation_sym; apply Permutation_middle ]];
            change (?a :: ?b :: l) with ([a; b] ++ l);
            eapply sys_freshness_trans; [apply diau_freshness_l || apply diau_freshness_r | intro n; freshness with H 0]  
         | |- _ => idtac                                                    
         end.
    all: lazymatch goal with
         | D0: rule_transform ?p (advance 2 ?fn) ?e = (?s0, ?f),
           D1: (let (_, c) := rule_transform ?p ?s0 ?e0 in (_, _)) = (_, ?f0) |- sys_freshness _ (to_system ((?e' :: copure ?f0) ++ ?l)) => (** binary diamond rule *)
           destruct (rule_transform p s0) as [s' c] eqn:D4;
             inversion D1;
             repeat (rewrite ?hidden_copure, ?match_match_failure, ?f_match_failure;
             repeat change (copure (fail: ?x)) with (x); repeat change (copure (succ: ?x)) with (x));
             subst; clear D1;
           eapply sys_freshness_trans; [apply diab_freshness_l || apply diab_freshness_r | idtac];
             cbn [app];
             eapply sys_freshness_trans; [apply Permutation_sys_freshness; eapply perm_trans; [apply perm_swap | apply perm_skip; apply perm_swap] | idtac];
               pose proof (rule_transform_sys_fresh p (advance 2 fn) e ltac:(auto complexity) (e0 :: e' :: l) ltac:(cbv [advance plus Equation.a Equation.b]; split; auto; intro n'; split; freshness with H n'; repeat split; freshness_tauto; auto; freshness with H (S n'); freshness with H (S (S n')))) as H_;
               rewrite !D0 in H_; cbn [snd] in H_;
                 eapply sys_freshness_trans; [apply H_ | idtac];
                   eapply sys_freshness_trans; [apply Permutation_sys_freshness; apply Permutation_sym; apply Permutation_middle | idtac];
                     clear H_;
                     pose proof (rule_transform_sys_fresh p s0 e0 ltac:(auto complexity) (copure f ++ e' :: l)) as H_;
                     rewrite !D4 in H_; cbn [snd] in H_;
                       eapply sys_freshness_trans; [apply H_ | rewrite app_assoc; apply Permutation_sys_freshness; apply Permutation_sym; apply Permutation_middle]; clear H_;
                         pose proof (rule_transform_fresh p (advance 2 fn) e (e0 :: e' :: l) ltac:(cbv [advance plus Equation.a Equation.b]; split; auto; intro n'; split; freshness with H n'; repeat split; freshness_tauto; auto; freshness with H (S n'); freshness with H (S (S n')))) as H_;
                         rewrite D0 in H_; cbn [snd fst] in H_;
                           split; auto;
                             destruct H_ as [H_ H_']; freshness with H_
         end.
    Unshelve.
    all: inversion D0; auto_purity.
    all: inversion D1; auto_purity.
  Qed.


  Lemma similar_sys_sat_refl : forall f, similar_sys_sat f f.
    intros f.
    apply same_sys_similar_sys.
    red. intros; tauto.
  Qed.

  Hint Resolve similar_sys_sat_refl.
    
  Program Fixpoint rule_transform_lemma (p : pro) (fn : Stream nat) (ed : eqn) {measure (complexity p ed)} : forall l, stream_fresh fn ed l /\ stream_unique fn -> similar_sys_sat (to_system (ed :: l)) (to_system (copure (snd (rule_transform p fn ed)) ++ l)) := _.
  Next Obligation.
    rewrite rule_transform_unfold.
    rewrite !f_if.
    cbn [snd].
    destruct (is_alpha p ed || is_beta p ed) eqn:Hab.
    - cbn [copure copure__ failure_CoPure]. auto.
    - rewrite orb_false_iff in Hab.
      destruct Hab as [Ha Hb].
      destruct ed as [av bv].
      cbn [Equation.a Equation.b] in *.
      destruct av, bv.
      all: rewrite ?f_if.
      all: cbn [snd app].
      all: repeat change (copure (fail: ?x)) with (x); repeat change (copure (succ: ?x)) with (x).
      all: cbn [app].
      all: auto.
      all: cbv [fcapp fmap fmap__ env_Functor failure_Functor bind failure_Monad join join__ m_f envt_Functor].
      all: deceq.
      all: auto.
      all: repeat rewrite ?f_if, ?hidden_copure, ?match_match_failure, ?f_match_failure in *.
      all: repeat unlet.
      all: repeat rewrite ?f_if, ?hidden_copure, ?match_match_failure, ?f_match_failure in *.
      all: repeat change (snd (_,?b)) with (b).
      all: repeat change (copure (fail: ?x)) with (x); repeat change (copure (succ: ?x)) with (x).
      all: lazymatch goal with
           | |- similar_sys_sat (to_system (?ed :: ?l)) (to_system (copure (snd (rule_transform ?p ?fn ?e)) ++ ?l)) =>
             (** [ ]  and \/ rule *)
             apply similar_sys_sat_trans_l' with (g := to_system (e :: l)); try apply same_e_cons; auto;
             apply rule_transform_lemma; [ auto complexity; inversion D; auto_purity | split; auto; freshness with H ]
           | _ => idtac
           end.
      all: lazymatch goal with
           |  H1: rule_transform ?p ?fn ?e = (?s, ?f), H2: rule_transform ?p ?s ?e0 = (_, ?f0) |- similar_sys_sat _ (to_system ((copure ?f0 ++ copure ?f) ++ ?l)) => (** /\ - rule *)
              apply similar_sys_sat_trans_l' with (g := to_system ([e0; e] ++ l)); try apply same_es_app_l; auto;
              pose proof (rule_transform_fresh p fn e (e0 :: l)) as H_;
              rewrite H1 in H_; cbn [fst snd] in H_;
              assert (H''' : stream_fresh fn e (e0 :: l)) by freshness with H;
              destruct (H_ (Logic.conj H''' H0)) as [H' H0']; clear H_;
              pose proof (rule_transform_fresh p s e0 (copure f ++ l)) as H_;
              rewrite H2 in H_; cbn [fst snd] in H_;
              assert (H'''' : stream_fresh s e0 (copure f ++ l)) by freshness with H';
              destruct (H_ (Logic.conj H'''' H0')) as [H'' H0'']; clear H_;
              cbn [app];
              eapply similar_sys_sat_trans_l' with (g := to_system (e :: e0 :: l)); [apply Permutation_same_sat; constructor | idtac | intro n; freshness_tauto ];
              apply similar_sys_sat_trans with (g := to_system (copure (snd (rule_transform p fn e)) ++ e0 :: l)); [ apply rule_transform_lemma; [ auto complexity | split; auto; freshness with H ] | idtac | apply rule_transform_sys_fresh; solve [auto] | rewrite H1; cbn [snd]; apply sys_freshness_trans with (m := to_system (e0 :: copure f ++ l));
              [ apply Permutation_sys_freshness; apply Permutation_sym; apply Permutation_middle |
                rewrite <- app_assoc; assert (E: f0 = snd (rule_transform p s e0)) by (rewrite H2; cbn; reflexivity); rewrite E; apply rule_transform_sys_fresh; auto] ]; rewrite H1; cbn [snd]; 
              apply similar_sys_sat_trans_l' with (g := to_system (e0 :: copure f ++ l)); 
              [ apply Permutation_same_sat; apply Permutation_sym; apply Permutation_middle | idtac | intro n; freshness_tauto ];
              apply similar_sys_sat_trans with (g := to_system (copure (snd (rule_transform p s e0)) ++ copure f ++ l));
              [apply rule_transform_lemma; [ auto complexity | split; auto; solve [freshness with H'] ] | rewrite H2; cbn [snd]; rewrite app_assoc; apply same_sys_similar_sys; intros F w V Hv; tauto | apply rule_transform_sys_fresh; solve [auto] | rewrite <- app_assoc; rewrite H2; cbn [snd]; solve [auto] ]
           | H1: rule_transform ?p ?fn ?e0 = (?f, _ ?l0), H2: rule_transform ?p ?f ?e = (_, _ ?l) |- similar_sys_sat _ (to_system (?l ++ ?l0)) =>
             assert (l0 = copure (snd (rule_transform p fn e0))) as ?H by (symmetry; try exact (copure_snd_fail_lemma _ _ _ H1); try exact (copure_snd_succ_lemma _ _ _ H1));
               assert (l = copure (snd (rule_transform p f e))) as ?H' by (symmetry; try exact (copure_snd_fail_lemma _ _ _ H2); try exact (copure_snd_succ_lemma _ _ _ H2));
               eapply similar_eqn_sys_sat_trans_r' with (g := to_system ([e; e0])); auto
           | _ => idtac
           end.
      all: lazymatch goal with
           | D0: rule_transform ?p (advance 1 ?fn) ?e = (_, ?f) |- similar_sys_sat _ (to_system (({|a:=b_negnom ?s;b:=b_diau ?o (b_nom (inr (?fn 0))) |} :: copure ?f) ++ ?l)) => (** diamond rule *)
             apply similar_sys_sat_trans with (g := (to_system ([{| a := b_negnom s; b := b_diau o (b_nom (inr (fn 0))) |};e] ++ l))); auto;
             [ apply diau_correct_l || apply diau_correct_r; auto; freshness with H 0
             | idtac
             | cbn [app]; apply sys_freshness_trans with (m := to_system (e :: {| a := b_negnom s; b := b_diau o (b_nom (inr (fn 0))) |} :: l)); [ apply Permutation_sys_freshness; constructor | idtac ];
               apply sys_freshness_trans with (m := to_system (copure f ++ {| a := b_negnom s; b := b_diau o (b_nom (inr (fn 0))) |} :: l)); [ idtac | apply Permutation_sys_freshness; apply Permutation_sym; apply Permutation_middle ];
               replace (copure f) with (copure (snd (rule_transform p (advance 1 fn) e))) by (rewrite D0; cbn [snd]; reflexivity);
               apply rule_transform_sys_fresh;
               cbv [advance plus a b]; split; auto;
               repeat split; cbn [a b]; auto;
               [ freshness with H (S n)
               | idtac
               | freshness with H (S n)
               ];
               revert n; freshness with H; repeat split; auto; freshness with H (S n)
             ];
             apply similar_sys_sat_trans_l' with (g := (to_system (e :: {| a := b_negnom s; b := b_diau o (b_nom (inr (fn 0))) |} :: l))); [ cbn [app]; apply Permutation_same_sat; solve [constructor] | idtac | intro n'; freshness_tauto ];
             apply similar_sys_sat_trans_r' with (g := (to_system (copure f ++ {| a := b_negnom s; b := b_diau o (b_nom (inr (fn 0))) |} :: l))); cbn [app]; [ apply Permutation_same_sat; apply Permutation_sym; solve [apply Permutation_middle] | idtac ];
             replace (copure f) with (copure (snd (rule_transform p (advance 1 fn) e))) by (rewrite D0; cbn [snd]; reflexivity);
             apply rule_transform_lemma;
             [ auto complexity
             | cbv [advance plus a b]; split; auto
             ];
             repeat split; cbn [a b]; auto;
             [ freshness with H (S n)
             | idtac
             | freshness with H (S n)
             ];
             revert n; freshness with H; repeat split; auto; freshness with H (S n)
           | _ => idtac
           end.
      all:  destruct (rule_transform p s0) as [s' c] eqn:D2;
               inversion D1;
               repeat (rewrite ?hidden_copure, ?match_match_failure, ?f_match_failure;
                       repeat change (copure (fail: ?x)) with (x); repeat change (copure (succ: ?x)) with (x));
               subst; clear D1.
      all: lazymatch goal with
           | D0: rule_transform ?p (advance 2 ?fn) ?e = (?s0, ?f),
             D2: rule_transform ?p ?s0 ?e0 = (?s1, ?c) |- similar_sys_sat _ (to_system (({|a:=b_negnom ?s;b:=diab ?o (b_nom (inr (?fn 0))) (b_nom (inr (?fn 1))) |} :: copure ?c ++ copure ?f0) ++ ?l)) => (** binary diamond rule *)
                 apply similar_sys_sat_trans with (g := (to_system ([{| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0))) (b_nom (inr (fn 1))) |};e ; e0] ++ l))); auto;
        [ apply diab_correct_l || apply diab_correct_r; auto; introversion; apply (H0 1 0); auto
        | idtac
        | apply sys_freshness_trans with (m := to_system (e :: e0 :: {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0))) (b_nom (inr (fn 1)))|} :: l)); [ apply Permutation_sys_freshness; eapply Permutation_trans; [ constructor 3 | constructor 2; constructor 3 ] | idtac ];
      apply sys_freshness_trans with (m := to_system (copure f ++ copure c ++ {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0))) (b_nom (inr (fn 1)))|} :: l)); [ idtac | apply Permutation_sys_freshness; cbn [app]; apply Permutation_sym; eapply Permutation_trans; [ apply Permutation_middle | rewrite app_assoc; apply Permutation_app_tail; apply Permutation_app_comm ] ];
      apply sys_freshness_trans with (m := to_system (copure f ++ e0 :: {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0))) (b_nom (inr (fn 1)))|} :: l));
      [ replace (copure f) with (copure (snd (rule_transform p (advance 2 fn) e))) by (rewrite D0; cbn [snd]; reflexivity); apply rule_transform_sys_fresh; cbv [advance plus a b]; split; auto;
        repeat split; cbn [a b]; auto; [ freshness with H (S (S n)) | revert n; freshness with H; repeat split; auto; freshness with H (S (S n)) | freshness with H (S (S n)) ]
        | apply sys_freshness_trans with (m := to_system (e0 :: copure f ++ {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0))) (b_nom (inr (fn 1))) |} :: l)); [ apply Permutation_sys_freshness; apply Permutation_sym; apply Permutation_middle | idtac ];
      apply sys_freshness_trans with (m := to_system (copure c ++ copure f ++ {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0))) (b_nom (inr (fn 1))) |} :: l)); [ idtac | apply Permutation_sys_freshness; rewrite !app_assoc; apply Permutation_app_tail; apply Permutation_app_comm ];
      replace (copure c) with (copure (snd (rule_transform p s0 e0))) by (rewrite D2; cbn [snd]; reflexivity);
      apply rule_transform_sys_fresh;
      pose proof (rule_transform_fresh p (advance 2 fn) e (e0 :: {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0) : nom + nat)) (b_nom (inr (fn 1))) |} :: l)) as H_;
      rewrite D0 in H_; cbn [snd fst] in H_;
      assert (stream_fresh (advance 2 fn) e (e0 :: {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0): nom + nat)) (b_nom (inr (fn 1) : nom + nat)) |} :: l) /\ stream_unique (advance 2 fn)) as H' by (
      cbv [advance]; cbn [plus]; split; auto;
        repeat split; cbn [a b]; auto; [ freshness with H (S (S n)) | idtac | freshness with H (S (S n)) ];
      revert n; freshness with H; repeat split; auto; freshness with H (S (S n)));
      destruct (H_ H') as [H1 H2]; split; auto; freshness with H1; auto ]
        ]; cbn [app];
      eapply Permutation_trans_l with (l2 := e :: e0 :: {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0))) (b_nom (inr (fn 1)))|} :: l); [ eapply Permutation_trans; [ constructor 3 | constructor 2; constructor 3 ] | idtac ];     
      eapply similar_sys_sat_trans;
        [ apply rule_transform_lemma with (p0 := p) (fn := advance 2 fn); [ auto complexity | cbv [advance plus a b]; split; auto;  repeat split; cbn [a b]; auto; [ freshness with H (S (S n)) | revert n; freshness with H; repeat split; auto; freshness with H (S (S n)) | freshness with H (S (S n)) ] ]
        | idtac
        | apply rule_transform_sys_fresh; cbv [advance plus a b]; split; auto;  repeat split; cbn [a b]; auto; [ freshness with H (S (S n)) | revert n; freshness with H; repeat split; auto; freshness with H (S (S n)) | freshness with H (S (S n)) ]
        | rewrite D0; cbn [snd];  eapply sys_freshness_trans; [ apply Permutation_sys_freshness; apply Permutation_sym; apply Permutation_middle
      | eapply sys_freshness_trans;
      [ apply rule_transform_sys_fresh with (fn := s0) (p := p);
      pose proof (rule_transform_fresh p (advance 2 fn) e (e0 :: {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0) : nom + nat)) (b_nom (inr (fn 1))) |} :: l)) as H_;
      rewrite D0 in H_; cbn [snd fst] in H_;
      assert (stream_fresh (advance 2 fn) e (e0 :: {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0): nom + nat)) (b_nom (inr (fn 1) : nom + nat)) |} :: l) /\ stream_unique (advance 2 fn)) as H' by (
      cbv [advance]; cbn [plus]; split; auto;
        repeat split; cbn [a b]; auto; [ freshness with H (S (S n)) | idtac | freshness with H (S (S n)) ];
      revert n; freshness with H; repeat split; auto; freshness with H (S (S n)));
      destruct (H_ H') as [H1 H2]; split; auto; freshness with H1; auto
      | rewrite D2; cbn [snd]; apply Permutation_sys_freshness; rewrite app_assoc; apply Permutation_sym; apply Permutation_middle ]]
        ];  rewrite D0; cbn [snd]; 
      eapply Permutation_trans_l; [ apply Permutation_sym; apply Permutation_middle | idtac ];
      eapply similar_sys_sat_trans;
      [ apply rule_transform_lemma with (fn := s0) (p0 := p);
     [ auto complexity
     | pose proof (rule_transform_fresh p (advance 2 fn) e (e0 :: {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0) : nom + nat)) (b_nom (inr (fn 1))) |} :: l)) as H_;
      rewrite D0 in H_; cbn [snd fst] in H_;
      assert (stream_fresh (advance 2 fn) e (e0 :: {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0): nom + nat)) (b_nom (inr (fn 1) : nom + nat)) |} :: l) /\ stream_unique (advance 2 fn)) as H' by (
      cbv [advance]; cbn [plus]; split; auto;
        repeat split; cbn [a b]; auto; [ freshness with H (S (S n)) | idtac | freshness with H (S (S n)) ];
      revert n; freshness with H; repeat split; auto; freshness with H (S (S n)));
      destruct (H_ H') as [H1 H2]; split; auto; freshness with H1; auto ]
      | apply same_sys_similar_sys; apply Permutation_same_sat; rewrite D2; cbn [snd]; rewrite app_assoc; apply Permutation_sym; apply Permutation_middle
      | idtac
      | apply Permutation_sys_freshness; rewrite D2; cbn [snd]; rewrite app_assoc; apply Permutation_sym; apply Permutation_middle 
      ];
      apply rule_transform_sys_fresh; pose proof (rule_transform_fresh p (advance 2 fn) e (e0 :: {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0) : nom + nat)) (b_nom (inr (fn 1))) |} :: l)) as H_;
      rewrite D0 in H_; cbn [snd fst] in H_;
      assert (stream_fresh (advance 2 fn) e (e0 :: {| a := b_negnom s; b := b_diab o (b_nom (inr (fn 0): nom + nat)) (b_nom (inr (fn 1) : nom + nat)) |} :: l) /\ stream_unique (advance 2 fn)) as H' by (
      cbv [advance]; cbn [plus]; split; auto;
        repeat split; cbn [a b]; auto; [ freshness with H (S (S n)) | idtac | freshness with H (S (S n))];
      revert n; freshness with H; repeat split; auto; freshness with H (S (S n)));
      destruct (H_ H') as [H1 H2]; split; auto; freshness with H1; auto
           | _ => idtac
           end.
  Qed.
  
  Lemma next_nom_never0 : forall (s : system), ~ next_nom s = 0. cbv [next_nom ListUtils.maxBy]. intros s. introversion. Qed.

  Lemma similar_sys_sat_sys_sat : forall s1 s2, similar_sys_sat s1 s2 -> sys_lframe_sat (world := ws) s1 s2.
    intros s1 s2.
    unfold similar_sys_sat, sys_lframe_sat.
    intros H F w.
    split; intros [V [Hv Ho]].
    - rewrite H in Ho; eauto.
      destruct Ho as [V' [Hv' [Hp' [Hn' Ho']]]].
      exists V'; split; auto.
    - exists V; split; auto.
      rewrite H; auto.
      exists V; repeat split; auto.
      intros; congruence.
  Qed.
  
  
  Lemma copure_snd_fcapp {A} : forall fcl (fl : failure (list A)), copure (snd (fcapp fcl fl)) = copure (snd (A := nat -> nat) fcl) ++ copure fl.
    intros fcl fl.
    unfold fcapp.
    fmap_unfold.
    unfold env_Functor, failure_Functor.
    destruct fcl as [fn v].
    cbn [snd].
    cbv [bind join failure_Monad join__].
    rewrite f_match_failure.
    change (copure (fail: ?h)) with (h).
    rewrite hidden_copure.
    fmap_unfold.
    unfold env_Functor, failure_Functor.
    simpl.
    rewrite !f_match_failure.
    change (copure (fail: ?h)) with (h).
    change (copure (succ: ?h)) with (h).
    rewrite hidden_copure, f_match_failure, hidden_copure.
    change (copure (succ: ?h)) with (h).
    reflexivity.
  Qed.  

  Lemma recurse'_app : forall (l l' l'' : list eqn) (p : pro) (fn : Stream nat),
      recurse' fn (rule_transform p) l (l' ++ l'') = recurse' fn (rule_transform p) l l' ++ l''.
    intros l l' l'' p.
    induction l; cbn; auto.
    intro fn.
    destruct (rule_transform p fn a) as [s' rl] eqn:D.
    rewrite hidden_copure, IHl, app_assoc.
    reflexivity.
  Qed.

  Lemma recurse'_sys_fresh : forall l lr fn p, (forall n, (Forall (EFreshIn (inr (fn n))) (l ++ lr)) /\ ~ inr (fn n) = nom_i) /\ (forall n n', ~ n = n' -> ~ fn n = fn n') -> sys_freshness (to_system (l ++ lr)) (to_system (recurse' fn (rule_transform p) l lr)).
    induction l.
    - cbn. auto.
    - cbn. intros lr fn p [H H0].
      destruct (rule_transform p fn a) as [s' rl] eqn:D.
      rewrite hidden_copure.
      eapply sys_freshness_trans; [apply (rule_transform_sys_fresh p fn a (l ++ lr) ltac:(split; auto; freshness with H)) | idtac].
      rewrite D.
      cbn [snd].
      eapply sys_freshness_trans; [apply Permutation_sys_freshness; apply Permutation_app_comm | idtac ].
      eapply sys_freshness_trans; [idtac | apply Permutation_sys_freshness; apply Permutation_app_comm ].
      rewrite <- app_assoc, <- recurse'_app.
      apply IHl.
      destruct (rule_transform_fresh p fn a (l ++ lr) ltac:(split; auto; freshness with H)) as [H' H0'].
      rewrite D in *.
      cbn [fst snd] in *.
      split; auto; freshness with H'.
  Qed.
  
  Lemma recurse'_correct : forall l lr fn p, (forall n, (Forall (EFreshIn (inr (fn n))) (l ++ lr)) /\ ~ inr (fn n) = nom_i) /\ (forall n n', ~ n = n' -> ~ fn n = fn n') -> sys_lframe_sat (world := ws) (to_system (l ++ lr)) (to_system (recurse' fn (rule_transform p) l lr)).
    intros l lr fn p [H H0].
    apply similar_sys_sat_sys_sat.
    revert H. revert H0. revert fn. revert lr.
    induction l.
    - simpl. intros. apply same_sys_similar_sys. intros F w V Hv. tauto.
    - intros. cbn [recurse'].
      destruct rule_transform as [s' rl] eqn:D.
      pose proof (rule_transform_fresh p fn a (l ++ lr) ltac:(split; auto; freshness with H)) as Hf.
      rewrite D in Hf. cbn [fst snd] in Hf. destruct Hf as [Hf Hf0].
      cbn [app].
      eapply similar_sys_sat_trans; [apply (rule_transform_lemma p fn a); split; auto; freshness with H | idtac | apply rule_transform_sys_fresh; split; auto; freshness with H | idtac]; rewrite D; cbn [snd].
      eapply Permutation_trans_l; [ apply Permutation_app_comm | eapply Permutation_trans_r; [apply Permutation_app_comm | idtac]].
      rewrite <- app_assoc, <- recurse'_app.
      apply IHl; auto.
      freshness with Hf.
      eapply sys_freshness_trans; [apply Permutation_sys_freshness; apply Permutation_app_comm | idtac ].
      eapply sys_freshness_trans; [idtac | apply Permutation_sys_freshness; apply Permutation_app_comm ].
      rewrite <- app_assoc, <- recurse'_app.
      apply recurse'_sys_fresh.
      split; auto; freshness with Hf.
  Qed.  
  
  Lemma Forall_ignore_filter {T} : forall p P (l : list T), Forall P l -> Forall P (filter p l).
    induction l; cbn; auto.
    introversion; subst.
    destruct p; auto.
  Qed.

  Lemma bmf_nominals_not_fresh : forall f : bmf_plus, Forall (fun n => ~ FreshIn n f) (bmf_nominals f).
    induction f; cbn; auto.
    1,2: constructor; auto; introversion; congruence.
    1,2,5,6: rewrite <- Forall_app; split; [ eapply Forall_impl; [idtac | apply IHf1] | eapply Forall_impl; [idtac | apply IHf2]]; intros; introversion; congruence.
    all: eapply Forall_impl; [idtac | apply IHf]; intros; introversion; congruence.
  Qed.  

  Lemma maxBy_correct {T} : forall (l : list T) (f : T -> nat) e, e = maxBy f l -> Forall (fun e' => e' <= e) (map f l).
    intros l f.
    induction l; intro e; cbn; auto.
    destruct (le_gt_dec (f a) (maxBy f l)); [rewrite max_r; auto | rewrite max_l; try omega].
    intro H; constructor; auto.
    rewrite H; auto.
    constructor; auto.
    rewrite H; auto.
    eapply Forall_impl; [idtac | apply IHl; eauto].
    simpl.
    intros.
    omega.
  Qed.

  Lemma eqn_nominals_bmf_nominals : forall e : eqn, nominals e = nominals (eqn_toformula e).
    intros e.
    cbn; reflexivity.
  Qed.

      
  Lemma maxBy_app {T} : forall (l1 l2 : list T) f, maxBy f (l1 ++ l2) = Nat.max (maxBy f l1) (maxBy f l2).
    intros l1 l2 f.
    induction l1; auto.
    cbn [app maxBy].
    rewrite IHl1, Nat.max_assoc.
    auto.
  Qed.

  Lemma FreshIn_spec : forall (f : bmf_plus) n, FreshIn n f <-> ~ In n (bmf_nominals f).
    induction f; intro; cbn.
    1,2,3,5: split; intro; auto; constructor.
    1,2: split; intro H; (inversion H; intro Hf; destruct Hf; try congruence) || constructor; auto.
    1,2,5,6: rewrite neg_In_iff.
    all: rewrite <- ?IHf, <- ?IHf1, <- ?IHf2, <- ?FreshIn_diab, <-?FreshIn_boxb, <- ?FreshIn_boxu, <- ?FreshIn_diau, <-?FreshIn_disj, <- ?FreshIn_conj.
    all: tauto.
  Qed.
  
  Lemma EFreshIn_spec : forall (e : eqn) n, EFreshIn n e <-> ~ In n (nominals e).
    intros [av bv] n.
    cbv [EFreshIn a b nominals e_nominals mult list_Monoid Nominals_bmf].
    rewrite neg_In_iff.
    apply conj_impl; apply FreshIn_spec.
  Qed.  
    
  Lemma next_nom_fresh : forall (s : list eqn) (n : nat), Forall (EFreshIn (inr (next_nom {| eqns := s |} + n))) s.
    intros s n.
    rewrite Forall_forall.
    intros e H.
    rewrite EFreshIn_spec.
    cbv [next_nom nominals s_nominal eqns e_nominals Nominals_bmf mult list_Monoid].
    remember (fun e : nom + nat => match e with inl _ => 0 | inr x => x end) as f.
    remember (maxBy f (flat_map (fun e0 : eqn => bmf_nominals (Equation.a e0) ++ bmf_nominals (Equation.b e0)) s)) as ms.
    pose proof (maxBy_correct (flat_map (fun e0 : eqn => bmf_nominals (Equation.a e0) ++ bmf_nominals (Equation.b e0)) s) f ms Heqms).
    rewrite Forall_forall in H0.
    pose proof (in_flat_map (fun e0 : eqn => bmf_nominals (Equation.a e0) ++ bmf_nominals (Equation.b e0)) s (inr (S ms + n))).
    intro Hf.
    assert (exists x : eqn, In x s /\ In (inr (S ms + n)) ((fun e0 : eqn => bmf_nominals (a e0) ++ bmf_nominals (b e0)) x)) by (exists e; auto).
    apply H1 in H2.
    apply (in_map f) in H2.
    rewrite Heqf, <- Heqf in H2.
    apply H0 in H2.
    omega.
  Qed.
  
  
  Lemma recurse_correct : forall (p : nat) (s : system), sys_lframe_sat (world := ws) s
                                                             (to_system
                                                                (recurse' (fun off : nat => next_nom s + off)
                                                                          (rule_transform p)
                                                                          (filter (fun e : eqn => negb (is_alpha p e) && negb (is_beta p e)) (eqns s)) (filter (is_alpha p) (eqns s) ++ filter (is_beta p) (eqns s)))).
    intros p [s].
    cbn [eqns].
    apply sys_lframe_sat_trans with (f := to_system ((filter (fun e : eqn => negb (is_alpha p e) && negb (is_beta p e)) s) ++ filter (is_alpha p) s ++ filter (is_beta p) s)).
    apply similar_sys_sat_sys_sat.
    apply same_sys_similar_sys.
    intros F w V Hv.
    induction s; cbv [sys_toformula eqns]; cbn [filter app to_system]; try tauto.
    rewrite !map_app, !bigconj_app'.
    destruct is_alpha eqn:Ha; cbn [negb andb map]; (apply alpha_beta_exclusive in Ha; rewrite Ha) || destruct is_beta; cbn [negb map]; rewrite !bigconj_cons; cbn [bmf_satisfaction]; rewrite !forall_dist, !bigconj_app', !forall_dist, ?bigconj_cons; cbn [bmf_satisfaction]; rewrite ?forall_dist, IHs; cbv [to_system sys_toformula eqns]; rewrite !map_app, !bigconj_app', !forall_dist, !bigconj_app', !forall_dist; tauto.
    apply recurse'_correct.
    split; [rewrite forall_dist | intros; omega].
    split; [idtac | intro f; introversion ].
    intro n. rewrite <- !Forall_app.
    repeat split; apply Forall_ignore_filter; apply next_nom_fresh.
  Qed.
    
  
  Lemma eliminate_inorder_correct : forall (s : system) (a : list nat), sys_lframe_sat (world := ws) s (copure (eliminate_inorder s a)).
    intros s a.
    unfold eliminate_inorder.
    rewrite copure_fold_bind.
    apply atom_sys_fold_preservation; eauto.
    clear s a.
    intros s a u Hsu.
    eapply sys_lframe_sat_trans; eauto.
    clear Hsu s.
    cbv [eliminate bmf_plus_eliminate].
    unfold split_system.
    eapply sys_lframe_sat_trans with (f := (to_system (recurse' _ _ _ _))); try apply ack_correct.
    apply recurse_correct.
  Qed.
  
  Lemma elim_all_correct : forall (e : eqn), Forall (sys_lframe_sat (world := ws) (to_system ([e]))) (elim_all e).
    intros e.
    cbv [elim_all].
    induction (ListUtils.permutations (atoms e)); cbn -[copure]; constructor; auto.
    apply eliminate_inorder_correct.
  Qed.
  
  Hint Resolve elim_all_correct.


  Lemma eliminate_all_correct : forall (e : eqn), eqn_sys_lframe_sat (world := ws) e (eliminate_all e).
    intro e. unfold eliminate_all.
    destruct Atoms.pure; eauto.
    eapply eqn_sys_lframe_trans_sat_r; eauto.
    apply sys_sys_fold_preservation; eauto.
    unfold better_system.
    intros s t u. intros.
    destruct (length (nodup equiv_dec (atoms t)) <? length (nodup equiv_dec (atoms u))); trivial.
    destruct (length (nodup equiv_dec (atoms u)) <? length (nodup equiv_dec (atoms t))); trivial.
    destruct (length (atoms t) <? length (atoms u)); trivial.    
  Qed.

 
  Lemma copure_traverse_checkpurity : forall l : list bmf_plus, copure ((traverse check_purity) l) = l.
    cbv [traverse fmap fmap__ list_Traversable list_Functor].
    simpl.
    induction l; auto.
    cbv [sequence sequence__] in *.
    cbn [map lSequence].
    ap_unfold.
    cbn [failure_Applicative failure_Functor].
    rewrite f_match_failure.
    cbv [copure copure__ failure_CoPure].
    rewrite hidden_copure.
    rewrite f_match_failure.
    cbv [copure copure__ failure_CoPure].
    rewrite !hidden_copure.
    rewrite IHl.
    rewrite f_match_failure.
    cbv [copure copure__ failure_CoPure].
    rewrite !hidden_copure, copure_check_purity.
    destruct (check_purity a) eqn:D; unfold check_purity in D; match_hyp; inversion D.
    auto.
    rewrite hidden_copure, f_match_failure.
    cbv [copure copure__ failure_CoPure].
    rewrite hidden_copure, IHl.
    auto.
  Qed.
      
  Theorem SQEMA'_correct : forall (f : bmf), forall F (w : ws), (forall V, bmf_satisfaction F V w f) <-> forall V, V.(valN) nom_i = w -> Forall (fun f' => exists v, bmf_satisfaction F V v f') (copure (SQEMA' f)).
    intros f F w.
    cbv [SQEMA'].
    rewrite copure_traverse_checkpurity.
    rewrite <- map_map.
    rewrite <- map_map with (f := prefix_i).
    rewrite map_map.
    apply post_prefix_pre_correct'.
    clear f; intro g.
    eapply eqn_sys_lframe_trans_sat_l; try eapply eliminate_triv_correct.
    apply eliminate_all_correct.
  Qed.
End SQEMACorrect.