\chap{Introduction}
\section{Motivation}
\label{sec:motivation}

This work is concerned with algorithms computing first-order correspondents of modal or second-order formulas.
To the best of our knowledge --- although most have been implemented --- none of the algorithms mentioned in this work have been verified using computer checked proofs.
The algorithm SQEMA proposed by \textcite{conradie2006algorithmicII} will be covered in detail and implemented in Coq thereby combining the two fields of computer aided proving and correspondence algorithms. 


\subsection{Computer Aided Proofs}
\label{subsec:cap}

Carrying out proofs the pen and paper way is convenient because trivial things can be left out to focus just on the important parts.
However, being human we make mistakes to wrongly misjudge things as trivial.
To avoid those kinds of mistakes and ensure correctness of proofs in the literature, we largely rely on peer review which again involves a human factor.

To mitigate this risk for errors staying unnoticed, we can use computers to check the correctness of proofs.
One such methodology is computer aided proving using a dependently typed language like Coq, Agda, Idris, or one of the many others, so that theorems can be stated as types.
Proofs are then instances of those types following the notions of \textit{propositions as types} and \textit{programs as proofs}, also known as the Curry-Howard-Correspondence \cite{curry1934functionality, howard1980formulae, wadler2015propositions}.

Having a proof checked by a computer comes at the cost of having to implement and prove every detail that would have otherwise been left out.
Proof assistants like Coq or Idris deal with this issue by having a meta language allowing automating construction of proof terms and therefore automate proving to some extent.
In Coq this is done using the Ltac language \cite{delahaye2000tactic} or plugins written in OCaml, while Idris has the so called elaborator reflection \cite{christiansen2016elaborator, christiansen2016practical}.

When formalizing algorithms using proof assistants, this also has the added benefit of producing a reference implementation of the algorithm to experiment with.
If proofs are implemented using automation, small changes to the algorithm might still be covered by the automation or only require small changes to the proofs, making it convenient to experiment with extending the algorithm.

\subsection{Correspondence Theory}
\label{subsec:corr_th}

Correspondence theory is concerned with identifying formulas in different logics that express the same properties and therefore, can be used interchangeably.
In modal logic it is well-known that formulas interpreted over frames can be easily translated into \ac{MSOL} (see \Cref{subsec:so_st}).
However, there are clearly properties on frames which can be expressed in \ac{FOL}, which is generally beneficial both for algorithmic and human readability reasons (see \Cref{subsec:correspondents}).

For those reasons, finding such correspondents has attracted continued interest throughout the years (see \Cref{subsec:algo_corr}).
There have been multiple algorithms developed, either specifically for this task, or more generally for eliminating second-order quantifiers from formulas which can be used for modal logic as well by translating to \ac{MSOL} first.


\section{Modal Logic}
\label{sec:ml}

We are dealing with hybrid polyadic modal logic in this work with syntax and semantics essentially following the setup of \textcite*{goranko2001sahlqvist, goranko2002sahlqvist} with minor deviations discussed in \Cref{subsub:red_syn}.


\subsection{Syntax}
\label{subsec:ml_syntax}

Let \(\tau = (O_0,\rho_0)\) be a \uline{modal similarity type} consisting of a set \(O_0\) of \uline{base modal terms} together with a \uline{base arity function} \(\rho_0 : O_0 \to \mathbb{N}\), \(\mathcal{A}\) be the set of \uline{atoms}, \(\mathcal{N}\) be the set of \uline{nominals} such that all those sets are disjoint.
We refer to both atoms and nominals as well as their negation as \uline{literals}.

Define now the sets of modal terms (MT), modal formulas (MF) and arity function \(\rho : \text{MT} \to \mathbb{N}\) mutually.
\begin{definition}[Modal Terms]
  $$\text{MT: } \alpha,\beta ::= \iota_0 \mid \iota_1 \mid \iota_2 \mid \phi \mid \alpha(\beta_1,\dots,\beta_{\rho(\alpha)}) \mid o \mid \alpha^{-i}$$
  where \(0 < i \leq \rho(\alpha)\), \(\phi \in\) MF not containing any literals, and \(o \in O_0\) 
\end{definition}
\begin{definition}[Arity Function \(\rho\)]
  We define \(\rho\) recursively:
  \begin{align*}
    \rho(\iota_0) = 0 \qquad  \rho(\iota_1) &= 1 \qquad \rho(\iota_2) = 2 \\
    \rho(\phi) &= 0 \\
    \rho(\alpha(\beta_1,\dots,\beta_n)) &= \sum_{i = 1}^n\rho(\beta_i) \\
    \rho(o) &= \rho_0(o) \\
    \rho(\alpha^{-i}) &= \rho(\alpha) \\
  \end{align*}
\end{definition}
\begin{definition}[Modal Formulas]
  $$\text{MF: } \phi,\psi ::= p \mid \nom j \mid \neg\phi \mid \phi \lor \psi \mid \Dia{\alpha}(\phi_1, \dots, \phi_{\rho(\alpha)})$$
  where \(\alpha \in \text{MT}\), \(p \in \mathcal{A}\) and \(\nom j \in \mathcal{N}\).
\end{definition}

From that syntax we define additional common syntactic constructs:
\begin{align*}
\phi \impl \psi &:= \neg\phi \lor \psi \\
\phi \land \psi &:= \neg(\neg\phi \lor \neg\psi) \\
\phi \eqv \psi &:= (\phi \impl \psi) \land (\psi \impl \phi) \\
\MBox{\alpha}{(\phi_1,\dots,\phi_n)} &:= \neg\Dia{\alpha}{(\neg\phi_1,\dots,\neg\phi_n)} \\
\IBox{-i}{\alpha}{(\phi_1,\dots,\phi_n)} &:= \MBox{\alpha^{-i}}{(\phi_1,\dots,\phi_n)}
\end{align*}
\subsection{Semantics}
\label{subsec:sem}

\begin{definition}[Kripke \(\tau\)-Frame]
A \uline{(Kripke) \(\tau\)-frame} \(\mathcal{F} = (W, \{R_o\}_{o \in O_0})\)
consists of a set of \uline{worlds} \(W\) and a \((\rho(o)+1)\)-ary
\uline{accessibility relation} \(R_o\) for each base modal term \(o \in O\).  
\end{definition}

As modal terms are interpreted into relations over the worlds of the frame, we define these relations for the other elements of MT as
\begin{align*}
R_{\iota_0} &:= W \\
R_{\iota_1} &:= \{(w,w) \mid w \in W\} \\
R_{\iota_2} &:= \{(w,w,w) \mid w \in W\} \\
R_\phi &:= \{w \mid (\mathcal{F},w) \Vdash \phi\} \\
R_{\alpha^{-i}} &:= \{(w_i, w_1, \dots, w_{i-1}, w, w_{i+1}, \dots, w_n) \mid (w, w_1, \dots, w_n) \in R_\alpha\} \\
R_{\alpha(\beta_1,\dots,\beta_n)} &:= \{ (w, w_{11}, \dots, w_{1b_1},\dots, w_{n1}, \dots, w_{nb_n}) \mid \exists {u_1} \dots u_n . R_\alpha(w, {u_1}, \dots, u_n) \land \\ 
&\qquad\bigwedge_{i = 1}^{n} R_{\beta_i}(u_i,w_{i1},\dots, w_{ib_i}) \}
\end{align*}
where \(\alpha\) has arity \(n\) and \(b_1,\dots,b_n\) are the arities of \(\beta_1,\dots,\beta_n\).

\begin{definition}[Kripke \(\tau\)-Model based on a \(\tau\)-Frame \(\mathcal{F}\)]
A \uline{Kripke \(\tau\)-model based on a \(\tau\)-frame \(\mathcal{F}\)} is a triple \(\mathcal{M} = (\mathcal{F}, V, N)\) 
where \(V : \mathcal{A} \to 2^W\) is the \uline{valuation}  and \(N : \mathcal{N} \to W\) the \uline{nominal valuation}.  
\end{definition}

\begin{definition}[Pointed \(\tau\)-Frame]
A \uline{pointed \(\tau\)-frame} \((\mathcal{F},w)\) is a pair of a frame \(\mathcal{F}\) with a world \(w \in W\).
Similarly, a \uline{pointed \(\tau\)-model} is a pair of a model with a world.  
\end{definition}

\begin{definition}[Pointed Model Validity]
  Validity of a modal formula in a pointed model is defined recursively:
  \begin{align*}
    (\mathcal{M},w) &\Vdash p \text{ iff } w \in V(p) \\
    (\mathcal{M},w) &\Vdash \nom j \text{ iff } w = N(\nom j) \\
    (\mathcal{M},w) &\Vdash \neg\phi \text{ iff } (\mathcal{M},w) \not\Vdash \phi \\
    (\mathcal{M},w) &\Vdash \phi \lor \psi \text{ iff } (\mathcal{M},w) \Vdash \phi \text{ or } (\mathcal{M},w) \Vdash \psi \\
    (\mathcal{M},w) &\Vdash \Dia{\alpha}{(\phi_1,\dots,\phi_{\rho(\alpha)})} \text{ iff there exist } w_1\dots w_{\rho(\alpha)} \text{ such that } \\ 
                    &\qquad R_\alpha(w, w_1,\dots,w_{\rho(\alpha)}) \text{ and } (\mathcal{M},w_i) \Vdash \phi_i \\ 
                    &\qquad\text{for each } 1 \leq i \leq \rho(\alpha)
  \end{align*}
\end{definition}
Note that we stick to the notions of \textcite{conradie2006algorithmic, conradie2006algorithmicII} here and talk about pointed model validity instead of satisfaction, which is used in the standard reference by \textcite{blackburn2002modal}. %Chagrov, Zakharyaschev use validity or being true. Hughes and Creswell also don'T talk about satisfaction
There, validity of a modal formula is what we will call model or frame validity.

Now that we have defined semantics of modal terms as well as modal formulas, we can give an intuition for the derived modal terms.
If we recall the resulting relation for \(\alpha(\beta_1,\dots,\beta_n)\)
\begin{align*}
  R_{\alpha(\beta_1,\dots,\beta_n)} &:= \{ (w, w_{11}, \dots, w_{1b_1},\dots, w_{n1}, \dots, w_{nb_n}) \mid \exists u_1 \dots u_n . R_\alpha(wu_1, \dots, u_n) \land \\ 
                              &\qquad\bigwedge_{i = 1}^{n} R_{\beta_i}(u_i,w_{i1},\dots, w_{ib_i}) \}
\end{align*}
and validity of a modality over a modal term, we get that such an applied modal term is equivalent to taking one step with the $R_\alpha$ relation and then from each resulting world a step with the respective $R_{\beta_i}$ relation.
Hence, these two formulas are semantically equivalent.
$$\Dia{\alpha(\beta_1,\dots,\beta_n)}{(\phi_{11},\dots,\phi_{1b_1},\dots)} \equiv \Dia{\alpha}{(\Dia{\beta_1}{(\phi_{11},\dots,\phi_{1b_1})},\dots)}$$
We also defined modal formulas to be modal terms if they contain no literals.
This resulted in the relation
\(R_\phi := \{w \mid (\mathcal{F},w) \Vdash \phi\}\)
which leads to the following semantic equivalence
\(\Dia{\phi}{} \equiv \phi \equiv \MBox{\phi}{}\).
Note that we use validity on pointed frames here given that valuation and nominal valuation of a model are only necessary when the formula contains literals.
The $\iota$ modal terms had the relations
\[R_{\iota_0} := W \qquad R_{\iota_1} := \{(w,w) \mid w \in W\} \qquad R_{\iota_2} := \{(w,w,w) \mid w \in W\}\]
resulting in the following definitions and semantic equivalences.
\[\Dia{\iota_0}{} =: \top \qquad \MBox{\iota_0}{} =: \bot\]
\[\MBox{\iota_1}{\phi} \equiv \phi \equiv \Dia{\iota_1}{\phi} \qquad \Dia{\iota_2}{(\phi, \psi)} \equiv \phi \land \psi \qquad \MBox{\iota_2}{(\phi, \psi)} \equiv \phi \lor \psi\]

Apart from \(\iota_0\) and the inverse modalities we see from those equivalences that having the other modal terms does not increase the expressivity of our language.
Having them in the language however simplifies a few definitions later on in \Cref{subsec:inductive}.

\subsection{Frame Validity}
\label{subsec:frame_sem}

The notion of pointed model validity naturally lifts to the level of (pointed) frames.

\begin{definition}[Pointed Frame Validity] A formula \(\phi\) is \uline{valid in a pointed frame \((\mathcal{F},w)\)}, written \((\mathcal{F},w) \Vdash \phi\) iff for all models \(\mathcal{M}\) based on \(\mathcal{F}\), \((\mathcal{M},w) \Vdash \phi\).  
\end{definition}
\begin{definition}[Frame Validity] A formula \(\phi\) is \uline{valid in a frame \(\mathcal{F}\)}, written \(\mathcal{F} \Vdash \phi\) iff for all worlds \(w\), \((\mathcal{F},w) \Vdash \phi\). This may also be called \uline{global frame validity} given that the formula is valid everywhere in \(\mathcal{F}\).
\end{definition}

Given that a model \(\mathcal{M}\) based on a frame \(\mathcal{F}\) adds a valuation \(V\) and a nominal valuation \(N\), pointed frame validity of a formula can also be stated equivalently as:
\[(\mathcal{F},w) \Vdash \phi \text{ iff for all valuations } V \text{ and nominal valuations } N,\; ((\mathcal{F},V,N),w) \Vdash \phi\]

If interpreted over frames, the formulas express properties of the frame itself allowing to classify frames by the modal formulas valid therein.
This gives rise to the concept of \uline{frame definability} where classes of frames are characterized by the set of formulas valid exactly in each member of that class.

We will also need the notions of local frame validity and global frame satisfiability.
\begin{definition}[Local Frame Validity]
  A formula \(\phi\) is \uline{locally valid in a frame \(\mathcal{F}\)} iff there exists a world \(w\) such that \((\mathcal{F},w) \Vdash \phi\).  
\end{definition}
\begin{definition}[Global Frame Satisfiability]
  A formula \(\phi\) is \uline{globally satisfiable in a frame \(\mathcal{F}\)} iff there exist a valuation \(V\) and nominal valuation \(N\) such that \((\mathcal{F},V,N) \Vdash \phi\).  
\end{definition}

\section{Correspondence}
\label{sec:correspondence}

Depending on the structure which a modal formula is interpreted on, there are different logics to embed the respective view on modal logic in.

\subsection{First-Order/Model Standard Translation}
\label{subsec:fo_st}
Modal logic interpreted on models or pointed models as defined in \Cref{subsec:sem} can be embedded into \ac{FOL}.
Let \(P_a\) be a unary predicate symbol for each \(a \in \mathcal{A}\), \(R_\alpha\) a \((\rho(\alpha) + 1)\)-ary relation for any \(\alpha \in \text{MT}\), and \(n_{\nom{k}}\) a function symbol of arity \(0\) for each \(\nom k \in \mathcal{N}\) defined to have the same meaning as given by the valuations in the given model.
Hence, we are treating the Kripke model as a first-order model.


\begin{definition}[First-Order Standard Translation]
  We define a two argument function \(ST\) recursively that takes a modal formula and an individual variable producing a \ac{FOL} formula expressing the same property in the model \(\mathcal{M}\) if the symbol \(x\) is substituted by \(w\).
  Written formally \((\mathcal{M}, w) \Vdash \phi \text{ iff } \mathcal{M} \models ST(\phi, x)[w/x]\).
  \begin{align*}
    ST(a,x) := P_a(x) &\qquad ST(\nom j,x) := x = n_{\nom j} \\
    ST(\neg\phi,x) &:= \neg ST(\phi,x) \\
    ST(\phi \lor \psi,x) &:= ST(\phi, x) \lor ST(\psi, x) \\
    ST(\Dia{\alpha}{(\phi_1,\dots,\phi_{\rho(\alpha)})},x) &:= \exists z_1\dots z_{\rho(\alpha)} . R_\alpha(x,z_1,\dots,z_{\rho(\alpha)}) \land \\ 
                      &\qquad\bigwedge_{i=1}^{\rho(\alpha)}ST(\phi_i,z_i)
  \end{align*}  
\end{definition}

\subsection{Second-Order/Frame Standard Translation}
\label{subsec:so_st}
If we want a similar translation for validity on frames defined in \Cref{subsec:frame_sem}, we do not have valuations for our literals to define the corresponding predicate and function symbols in our translation.
Given that frame validity talks about all valuations for literals, we will need to capture that notion in our translation by quantifying over unary predicate variables, corresponding to the atoms in the formula as well as individual variables corresponding to nominals occurring in the formula.
Given that we are quantifying over unary predicate variables we can no longer do this in \ac{FOL} but rather need \ac{MSOL}.

\begin{definition}[Second-Order Standard Translation]
  The \uline{second-order/frame\linebreak standard translation} of a formula \(\phi\) is obtained as \(\forall \bar{P} \forall \bar{n} . ST(\phi, x)\) where \(\bar{P}\) and \(\bar{n}\), are vectors of unary predicate symbols representing atoms, and individual variables resembling nominals in \(\phi\) respectively.
  \[(\mathcal{F}, w) \Vdash \phi \text{ iff } \mathcal{F} \models \forall \bar{P} \forall \bar{n} . ST(\phi, x)[w/x]\]
\end{definition}

\subsection{First-Order Correspondents}
\label{subsec:correspondents}

If we translate a formula \(\phi\) using the second-order translation and \(\phi\) does not contain any atoms, the resulting formula \(\forall \bar{n}.ST(\phi,x)\) does only quantify over individual variables, so evidently there exists a \ac{FOL} formula expressing the same condition on frames as \(\phi\). 
\begin{definition}[Elementary Formula]
  A formula is called \uline{elementary} if it has a corresponding first-order formula expressing the same frame property.\footnote{Note that being an elementary formula neither implies nor is implied by completeness with respect to some elementary class of frames.}
\end{definition}
Dealing with a \ac{FOL} formula instead of a \ac{MSOL} formula has many benefits:
\begin{itemize}
\item Computationally easier to reason about (see \Cref{tbl:complexities}).
  \begin{table}[h]
    \centering
    \begin{tabularx}{.65\textwidth}{r|ccc}
      & Model checking & VAL & SAT \\
      \hline
      \ac{FOL} & PSPACE-complete & half-decidable & undecidable \\
      \ac{MSOL} & PSPACE-complete & undecidable & undecidable
    \end{tabularx}
    \caption{Complexity comparison of \ac{FOL} and \ac{MSOL} \cite{stockmeyer1974complexity, vardi1982complexity, frick2004complexity}}
    \label{tbl:complexities}
  \end{table}
\item There are more algorithms capable of handling \ac{FOL} as input that there are for \ac{MSOL}.
\item It is easier to grasp as a human what a formula is saying when there are not any quantified predicates. 
\end{itemize}

We distinguish between local and global first-order correspondents.
\begin{definition}[Local First-Order Correspondent]
  A \ac{FOL} formula \(\psi\) with a single free variable \(x\) is a \uline{local first-order correspondent} of modal formula \(\phi\) iff for all frames \(\mathcal{F}\) and worlds \(w\),
  \[(\mathcal{F}, w) \Vdash \phi \text{ iff } \mathcal{F} \models \psi[w/x]\]
\end{definition}
\begin{definition}[Global First-Order Correspondent]
  A \ac{FOL} formula \(\psi\) is a \uline{global first-order correspondent} of modal formula \(\phi\) iff for all frames \(\mathcal{F}\),
  \[\mathcal{F} \Vdash \phi \text{ iff } \mathcal{F} \models \psi\]
\end{definition}
Note that we can easily convert a locally corresponding \ac{FOL} formula \(\psi\) with single free variable \(x\) to a globally corresponding one, by forming \(\forall x.\psi\).

Now the question arises, which modal formulas are elementary and if we can compute their correspondents in an automated fashion.
\Textcite{chagrova1991undecidable, chagrov2006truth} proved that first-order definability of intuitionistic logic is undecidable.
The modal case follows from this result, so it is in general undecidable whether a modal formula interpreted on frames has a first-order correspondent.

\subsection{Algorithmic Correspondence}
\label{subsec:algo_corr}

Despite the undecidability of the class of elementary formulas, there have been many attempts to approximate this class and algorithms computing correspondents for such subclasses. 

Early contributions to the field were made in the 70s by Sahlqvist \cite{sahlqvist1975completeness} and van Benthem \cite{van1984correspondence} by discovering a large subclass of elementary formulas, namely the Sahlqvist formulas, and the Sahlqvist-van Benthem algorithm for computing first-order correspondents for this class of modal formulas.
Later in the 90s the correspondence algorithms SCAN \cite{gabbay1992quantifier, gabbay1993hilbert, engel1996quantifier} was developed for computing first-order correspondents to second-order formulas.
The algorithm DLS \cite{szalas1993correspondence, doherty1997computing, conradie2006strength} built upon this using a result by \textcite{ackermann1935untersuchungen} for elimination.

Later Goranko et al. published a line of literature \cite{goranko2001sahlqvist, goranko2002sahlqvist, vakarelov2005generalizations, vakarelov2005modal, goranko2006elementary} resulting in the algorithm SQEMA and its various extensions \cite{conradie2006algorithmic, conradie2006algorithmicII, conradie2007algorithmic, conradie2009algorithmic, conradie2010algorithmic} which work on modal formulas directly without the need to first translate into another logic by using a modal version of the Ackermann lemma.
SQEMA also inspired the algorithm MSQEL based on the \(\text{MA}^{sw}_{red}\)-calculus developed in \textcite{schmidt2012ackermann}. 
All those algorithms work by syntactic transformations justified by semantic properties of modal logic on Kripke frames while there also was research about algebraic treatment of formulas instead \cite{kracht1991internal, kracht1993completeness, conradie2014unified, conradie2017algebraic}.
This was later used to build the algorithm ALBA which extends the idea of SQEMA to the setting of distributive modal logic and is based on an algebraic view on modal logic \cite{conradie2012algorithmic, ma2017unified}.

The algorithm SCAN works on \ac{SOL} formulas of the shape\linebreak \(\exists P_1\dots P_n. \phi\), where \(\phi\) is a \ac{FOL} formula and uses a resolution procedure after skolemization to eliminate those quantified predicates.
It has the undesirable property of not being guaranteed to terminate even for formulas which are known to have first-order correspondents.
DLS improves upon this by always terminating and also lifting the restriction to only deal with existential second-order quantifiers.
It introduced the Ackermann lemma as a means for eliminating predicate symbols from formulas but still had to rely on skolemization and unskolemization which is not always possible.
As far as SCAN and DLS are concerned, neither of the two is strictly more powerful than the other algorithm.
Thus, there are formulas on which SCAN succeeds but DLS does not and vice versa.

SQEMA properly extends both of them and removes the need for skolemization by working on modal formulas directly which do not have binders.
The elimination in SQEMA --- similar to DLS --- uses a modal variant of the Ackermann lemma to remove atoms from the formula.
Furthermore, it uses nominals to move from a local to a global perspective while still computing local frame correspondents.
This is not an issue as far as the standard translation is concerned, given that nominals only require quantification over individual variables.

\section{Notation}
\label{sec:notation}
Throughout this work we will stick to the following naming conventions: 

\begin{tabularx}{\linewidth}{r l l}
  Meaning & characters & example\\
  \hline
  Propositional variables & lowercase latin & $p, q, r$\\
  Nominals & bold lowercase latin & $\nom i, \nom j, \nom k$\\
  Relations & lowercase greek at beginning of alphabet & $\alpha, \beta$\\
  Formulas & lowercase greek later in the alphabet & $\phi, \chi, \psi$\\
  Sets of formulas & uppercase greek letters & $\Gamma, \Sigma$
\end{tabularx}